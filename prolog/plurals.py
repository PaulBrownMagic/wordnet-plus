import sys
from textblob import Word

print("""
/** <module> Noun Inflection
Singular and Plural Noun inflection,
based on WordNet3.1 nouns.

@author Paul Brown
*/


:- module(noun_inflection,
      [ inflection/2
      ]
).

%! inflection(?Singular, ?Plural)
%
% True iff Plural is the pluralized form
% of Singular.
%
% At least one of the args must be ground.
inflection(S, P) :-
    inf(S, P), !.
inflection(S, P) :-
    (ground(S) ; ground(P)),
    atom_concat(S, s, P), !.

%! inf(S, P)
% Helper for inflection
% Contains all exceptions
% to the pluralise by adding
% just an 's' to the end of
% the word 'rule'.
""")

def esc(word):
    return word.replace("'", r"\'")

for line in sys.stdin:
    singular = line.strip()
    plural = Word(line).pluralize().splitlines()[0]
    if (singular + "s" != plural):
        print(f"inf('{esc(singular)}', '{esc(plural)}').")
