
/** <module> Noun Inflection
Singular and Plural Noun inflection,
based on WordNet3.1 nouns.

@author Paul Brown
*/


:- module(noun_inflection,
      [ inflection/2
      ]
).

%! inflection(?Singular, ?Plural)
%
% True iff Plural is the pluralized form
% of Singular.
%
% At least one of the args must be ground.
inflection(S, P) :-
    inf(S, P), !.
inflection(S, P) :-
    (ground(S) ; ground(P)),
    atom_concat(S, s, P), !.

%! inf(S, P)
% Helper for inflection
% Contains all exceptions
% to the pluralise by adding
% just an 's' to the end of
% the word 'rule'.

inf('entity', 'entities').
inf('physical entity', 'physical entities').
inf('abstract entity', 'abstract entities').
inf('dwarf', 'dwarves').
inf('life', 'lives').
inf('causal agency', 'causal agencies').
inf('person', 'people').
inf('somebody', 'somebodies').
inf('plant life', 'plant lives').
inf('noesis', 'noeses').
inf('process', 'processes').
inf('physical process', 'physical processes').
inf('human activity', 'human activities').
inf('quantity', 'quantities').
inf('kindness', 'kindnesses').
inf('benignity', 'benignities').
inf('abdominoplasty', 'abdominoplasties').
inf('course of action', 'courses of action').
inf('brush', 'brushes').
inf('fetch', 'fetches').
inf('reciprocity', 'reciprocities').
inf('delivery', 'deliveries').
inf('obstetrical delivery', 'obstetrical deliveries').
inf('derring-do', 'derring-does').
inf('discovery', 'discoveries').
inf('egress', 'egresses').
inf('rally', 'rallies').
inf('recovery', 'recoveries').
inf('touch', 'touches').
inf('entry', 'entries').
inf('ingress', 'ingresses').
inf('clinch', 'clinches').
inf('dispatch', 'dispatches').
inf('despatch', 'despatches').
inf('climax', 'climaxes').
inf('sexual climax', 'sexual climaxes').
inf('success', 'successes').
inf('smash', 'smashes').
inf('pass', 'passes').
inf('loss', 'losses').
inf('breach', 'breaches').
inf('breach of contract', 'breaches of contract').
inf('anticipatory breach', 'anticipatory breaches').
inf('constructive breach', 'constructive breaches').
inf('breach of duty', 'breaches of duty').
inf('breach of the covenant of warranty', 'breaches of the covenant of warranty').
inf('breach of promise', 'breaches of promise').
inf('breach of trust', 'breaches of trust').
inf('breach of trust with fraudulent intent', 'breaches of trust with fraudulent intent').
inf('breach of warranty', 'breaches of warranty').
inf('material breach', 'material breaches').
inf('partial breach', 'partial breaches').
inf('smirch', 'smirches').
inf('parapraxis', 'parapraxes').
inf('botch', 'botches').
inf('bond-trading activity', 'bond-trading activities').
inf('distress', 'distresses').
inf('impress', 'impresses').
inf('occupancy', 'occupancies').
inf('preoccupancy', 'preoccupancies').
inf('grant-in-aid', 'grants-in-aid').
inf('catch', 'catches').
inf('pinch', 'pinches').
inf('taking into custody', 'takings into custody').
inf('bait and switch', 'bait and switches').
inf('private treaty', 'private treaties').
inf('delivery', 'deliveries').
inf('remission of sin', 'remissions of sin').
inf('jail delivery', 'jail deliveries').
inf('carrying into action', 'carryings into action').
inf('thaumaturgy', 'thaumaturgies').
inf('spiccato', 'spiccatoes').
inf('launch', 'launches').
inf('pass', 'passes').
inf('toss', 'tosses').
inf('pitch', 'pitches').
inf('pitch', 'pitches').
inf('delivery', 'deliveries').
inf('shy', 'shies').
inf('change-of-pace', 'changes-of-pace').
inf('change-of-pace ball', 'changes-of-pace ball').
inf('off-speed pitch', 'off-speed pitches').
inf('overhand pitch', 'overhand pitches').
inf('submarine pitch', 'submarine pitches').
inf('wild pitch', 'wild pitches').
inf('charity toss', 'charity tosses').
inf('push', 'pushes').
inf('press', 'presses').
inf('accommodation reflex', 'accommodation reflexes').
inf('Babinski reflex', 'Babinski reflexes').
inf('belch', 'belches').
inf('blush', 'blushes').
inf('flush', 'flushes').
inf('emesis', 'emeses').
inf('hematemesis', 'hematemeses').
inf('haematemesis', 'haematemeses').
inf('hyperemesis', 'hyperemeses').
inf('crash', 'crashes').
inf('smash', 'smashes').
inf('base on balls', 'bases on balls').
inf('pass', 'passes').
inf('fly', 'flies').
inf('pop fly', 'pop flies').
inf('pop-fly', 'pop-flies').
inf('sacrifice fly', 'sacrifice flies').
inf('safety', 'safeties').
inf('kayo', 'kayoes').
inf('lash', 'lashes').
inf('whiplash', 'whiplashes').
inf('punch', 'punches').
inf('box', 'boxes').
inf('counterpunch', 'counterpunches').
inf('parry', 'parries').
inf('knockout punch', 'knockout punches').
inf('KO punch', 'KO punches').
inf('Sunday punch', 'Sunday punches').
inf('rabbit punch', 'rabbit punches').
inf('sucker punch', 'sucker punches').
inf('kiss', 'kisses').
inf('kiss', 'kisses').
inf('buss', 'busses').
inf('smooch', 'smooches').
inf('soul kiss', 'soul kisses').
inf('deep kiss', 'deep kisses').
inf('French kiss', 'French kisses').
inf('catch', 'catches').
inf('snatch', 'snatches').
inf('fair catch', 'fair catches').
inf('shoestring catch', 'shoestring catches').
inf('mesh', 'meshes').
inf('autopsy', 'autopsies').
inf('necropsy', 'necropsies').
inf('check-out procedure', 'checks-out procedure').
inf('ophthalmoscopy', 'ophthalmoscopies').
inf('caress', 'caresses').
inf('convergency', 'convergencies').
inf('self-discovery', 'self-discoveries').
inf('rediscovery', 'rediscoveries').
inf('diagnosis', 'diagnoses').
inf('medical diagnosis', 'medical diagnoses').
inf('prenatal diagnosis', 'prenatal diagnoses').
inf('differential diagnosis', 'differential diagnoses').
inf('prognosis', 'prognoses').
inf('medical prognosis', 'medical prognoses').
inf('fix', 'fixes').
inf('cuckoldry', 'cuckoldries').
inf('delegacy', 'delegacies').
inf('laying on of hands', 'layings on of hands').
inf('agency', 'agencies').
inf('pac-man strategy', 'pac-man strategies').
inf('scorched-earth policy', 'scorched-earth policies').
inf('double-blind study', 'double-blind studies').
inf('make-do', 'make-does').
inf('crutch', 'crutches').
inf('tooth', 'teeth').
inf('primary', 'primaries').
inf('direct primary', 'direct primaries').
inf('closed primary', 'closed primaries').
inf('open primary', 'open primaries').
inf('breech delivery', 'breech deliveries').
inf('frank breech', 'frank breeches').
inf('frank breech delivery', 'frank breech deliveries').
inf('cesarean delivery', 'cesarean deliveries').
inf('caesarean delivery', 'caesarean deliveries').
inf('caesarian delivery', 'caesarian deliveries').
inf('abdominal delivery', 'abdominal deliveries').
inf('forceps delivery', 'forceps deliveries').
inf('midwifery', 'midwiferies').
inf('safety', 'safeties').
inf('point after touchdown', 'points after touchdown').
inf('tally', 'tallies').
inf('economy', 'economies').
inf('economy of scale', 'economies of scale').
inf('flux', 'fluxes').
inf('switch', 'switches').
inf('change of state', 'changes of state').
inf('backlash', 'backlashes').
inf('whitelash', 'whitelashes').
inf('white backlash', 'white backlashes').
inf('apostasy', 'apostasies').
inf('veto', 'vetoes').
inf('pocket veto', 'pocket vetoes').
inf('finish', 'finishes').
inf('immunity', 'immunities').
inf('granting immunity', 'granting immunities').
inf('fix', 'fixes').
inf('official immunity', 'official immunities').
inf('sovereign immunity', 'sovereign immunities').
inf('transactional immunity', 'transactional immunities').
inf('use immunity', 'use immunities').
inf('testimonial immunity', 'testimonial immunities').
inf('subversive activity', 'subversive activities').
inf('putting to death', 'puttings to death').
inf('drive-by killing', 'drives-by killing').
inf('dispatch', 'dispatches').
inf('despatch', 'despatches').
inf('butchery', 'butcheries').
inf('drive-by shooting', 'drives-by shooting').
inf('retch', 'retches').
inf('regress', 'regresses').
inf('jumping-off point', 'jumpings-off point').
inf('point of departure', 'points of departure').
inf('rise to power', 'rises to power').
inf('entry', 'entries').
inf('induction of labor', 'inductions of labor').
inf('hypnogenesis', 'hypnogeneses').
inf('groundbreaking ceremony', 'groundbreaking ceremonies').
inf('paternity', 'paternities').
inf('cookery', 'cookeries').
inf('progress', 'progresses').
inf('catharsis', 'catharses').
inf('katharsis', 'katharses').
inf('catharsis', 'catharses').
inf('katharsis', 'katharses').
inf('electrolysis', 'electrolyses').
inf('wash', 'washes').
inf('brush', 'brushes').
inf('redress', 'redresses').
inf('remedy', 'remedies').
inf('fix', 'fixes').
inf('quick fix', 'quick fixes').
inf('quicky', 'quickies').
inf('anastylosis', 'anastyloses').
inf('reassembly', 'reassemblies').
inf('change of color', 'changes of color').
inf('bleach', 'bleaches').
inf('splash', 'splashes').
inf('approach', 'approaches').
inf('access', 'accesses').
inf('landing approach', 'landing approaches').
inf('progress', 'progresses').
inf('push', 'pushes').
inf('life history', 'life histories').
inf('march', 'marches').
inf('foot', 'feet').
inf('hitch', 'hitches').
inf('single-foot', 'single-feet').
inf('lurch', 'lurches').
inf('lurch', 'lurches').
inf('march', 'marches').
inf('countermarch', 'countermarches').
inf('quick march', 'quick marches').
inf('routemarch', 'routemarches').
inf('dash', 'dashes').
inf('lap of honour', 'laps of honour').
inf('touristry', 'touristries').
inf('fly-by', 'fly-bies').
inf('pass', 'passes').
inf('solo', 'soloes').
inf('ground-controlled approach', 'ground-controlled approaches').
inf('mush', 'mushes').
inf('ministry', 'ministries').
inf('delivery', 'deliveries').
inf('service of process', 'services of process').
inf('carry', 'carries').
inf('fireman\'s carry', 'fireman\'s carries').
inf('fix', 'fixes').
inf('reentry', 'reentries').
inf('motility', 'motilities').
inf('body English', 'body Englishes').
inf('lurch', 'lurches').
inf('pitch', 'pitches').
inf('reach', 'reaches').
inf('stretch', 'stretches').
inf('toss', 'tosses').
inf('change of direction', 'changes of direction').
inf('outreach', 'outreaches').
inf('change of course', 'changes of course').
inf('change of magnitude', 'changes of magnitude').
inf('spasmolysis', 'spasmolyses').
inf('pinch', 'pinches').
inf('crush', 'crushes').
inf('crunch', 'crunches').
inf('abatement of a nuisance', 'abatements of a nuisance').
inf('stretch', 'stretches').
inf('roughness', 'roughnesses').
inf('change of integrity', 'changes of integrity').
inf('mix', 'mixes').
inf('conflux', 'confluxes').
inf('coalescency', 'coalescencies').
inf('notch', 'notches').
inf('slash', 'slashes').
inf('gash', 'gashes').
inf('autotomy', 'autotomies').
inf('metamorphosis', 'metamorphoses').
inf('metamorphosis', 'metamorphoses').
inf('change of shape', 'changes of shape').
inf('flex', 'flexes').
inf('crouch', 'crouches').
inf('hunch', 'hunches').
inf('activity', 'activities').
inf('domesticity', 'domesticities').
inf('praxis', 'praxes').
inf('mycophagy', 'mycophagies').
inf('anthropophagy', 'anthropophagies').
inf('slavery', 'slaveries').
inf('way of life', 'ways of life').
inf('line of least resistance', 'lines of least resistance').
inf('path of least resistance', 'paths of least resistance').
inf('clinch', 'clinches').
inf('bastinado', 'bastinadoes').
inf('strappado', 'strappadoes').
inf('strapado', 'strapadoes').
inf('cruelty', 'cruelties').
inf('atrocity', 'atrocities').
inf('inhumanity', 'inhumanities').
inf('brutality', 'brutalities').
inf('barbarity', 'barbarities').
inf('savagery', 'savageries').
inf('festivity', 'festivities').
inf('eurythmy', 'eurythmies').
inf('eurhythmy', 'eurhythmies').
inf('playfulness', 'playfulnesses').
inf('jocularity', 'jocularities').
inf('nightlife', 'nightlives').
inf('night life', 'night lives').
inf('fireman', 'firemen').
inf('hobby', 'hobbies').
inf('spare-time activity', 'spare-time activities').
inf('cup of tea', 'cups of tea').
inf('dish', 'dishes').
inf('confectionery', 'confectioneries').
inf('crucifix', 'crucifixes').
inf('long fly', 'long flies').
inf('jackknife', 'jackknives').
inf('archery', 'archeries').
inf('sumo', 'sumoes').
inf('tauromachy', 'tauromachies').
inf('catch', 'catches').
inf('virtual reality', 'virtual realities').
inf('pachinko', 'pachinkoes').
inf('ring-around-the-rosy', 'rings-around-the-rosy').
inf('ring-around-a-rosy', 'rings-around-a-rosy').
inf('ring-a-rosy', 'ring-a-rosies').
inf('water polo', 'water poloes').
inf('golf', 'golves').
inf('professional golf', 'professional golves').
inf('round of golf', 'rounds of golf').
inf('miniature golf', 'miniature golves').
inf('shinny', 'shinnies').
inf('rugby', 'rugbies').
inf('Chinaman', 'Chinamen').
inf('googly', 'googlies').
inf('polo', 'poloes').
inf('squash', 'squashes').
inf('break of serve', 'breaks of serve').
inf('cat and mouse', 'cat and mice').
inf('hopscotch', 'hopscotches').
inf('double Dutch', 'double Dutches').
inf('going to Jerusalem', 'goings to Jerusalem').
inf('casino', 'casinoes').
inf('cassino', 'cassinoes').
inf('faro', 'faroes').
inf('Go Fish', 'Go Fishes').
inf('Chicago', 'Chicagoes').
inf('rummy', 'rummies').
inf('gin rummy', 'gin rummies').
inf('knock rummy', 'knock rummies').
inf('basket rummy', 'basket rummies').
inf('cinch', 'cinches').
inf('pitch', 'pitches').
inf('auction pitch', 'auction pitches').
inf('royal casino', 'royal casinoes').
inf('spade casino', 'spade casinoes').
inf('chess', 'chesses').
inf('go', 'goes').
inf('lotto', 'lottoes').
inf('bingo', 'bingoes').
inf('beano', 'beanoes').
inf('keno', 'kenoes').
inf('ludo', 'ludoes').
inf('shove-halfpenny', 'shove-halfpennies').
inf('shove-ha\'penny', 'shove-ha\'pennies').
inf('sporting life', 'sporting lives').
inf('game of chance', 'games of chance').
inf('lottery', 'lotteries').
inf('conviviality', 'convivialities').
inf('high jinx', 'high jinges').
inf('revelry', 'revelries').
inf('orgy', 'orgies').
inf('debauch', 'debauches').
inf('debauchery', 'debaucheries').
inf('drunken revelry', 'drunken revelries').
inf('coquetry', 'coquetries').
inf('folly', 'follies').
inf('foolery', 'fooleries').
inf('tomfoolery', 'tomfooleries').
inf('craziness', 'crazinesses').
inf('lunacy', 'lunacies').
inf('mishegoss', 'mishegosses').
inf('buffoonery', 'buffooneries').
inf('japery', 'japeries').
inf('frivolity', 'frivolities').
inf('hotfoot', 'hotfeet').
inf('drollery', 'drolleries').
inf('waggery', 'waggeries').
inf('pleasantry', 'pleasantries').
inf('nightlife', 'nightlives').
inf('night life', 'night lives').
inf('intermezzo', 'intermezzoes').
inf('nautch', 'nautches').
inf('nauch', 'nauches').
inf('choreography', 'choreographies').
inf('pas de deux', 'pas de deuxes').
inf('pas de trois', 'pas de trois').
inf('bolero', 'boleroes').
inf('lindy', 'lindies').
inf('fandango', 'fandangoes').
inf('flamenco', 'flamencoes').
inf('shimmy', 'shimmies').
inf('tango', 'tangoes').
inf('mambo', 'mamboes').
inf('do-si-do', 'do-si-does').
inf('dance of death', 'dances of death').
inf('bel canto', 'bel cantoes').
inf('lullaby', 'lullabies').
inf('apery', 'aperies').
inf('mimicry', 'mimicries').
inf('parody', 'parodies').
inf('mockery', 'mockeries').
inf('panto', 'pantoes').
inf('business', 'businesses').
inf('stage business', 'stage businesses').
inf('sleight of hand', 'sleights of hand').
inf('liveliness', 'livelinesses').
inf('flurry', 'flurries').
inf('ado', 'adoes').
inf('fuss', 'fusses').
inf('hurry', 'hurries').
inf('rush', 'rushes').
inf('dash', 'dashes').
inf('scurry', 'scurries').
inf('rush', 'rushes').
inf('pass', 'passes').
inf('forward pass', 'forward passes').
inf('flare pass', 'flare passes').
inf('screen pass', 'screen passes').
inf('lateral pass', 'lateral passes').
inf('spot pass', 'spot passes').
inf('jugglery', 'juggleries').
inf('switch', 'switches').
inf('give-and-go', 'give-and-goes').
inf('smash', 'smashes').
inf('butterfly', 'butterflies').
inf('clock golf', 'clock golves').
inf('approach', 'approaches').
inf('pitch', 'pitches').
inf('thrash', 'thrashes').
inf('cinch', 'cinches').
inf('piece of cake', 'pieces of cake').
inf('facility', 'facilities').
inf('utility', 'utilities').
inf('housewifery', 'housewiferies').
inf('unfinished business', 'unfinished businesses').
inf('business', 'businesses').
inf('line of work', 'lines of work').
inf('specialty', 'specialties').
inf('speciality', 'specialities').
inf('walk of life', 'walks of life').
inf('facility', 'facilities').
inf('admiralty', 'admiralties').
inf('bishopry', 'bishopries').
inf('captaincy', 'captaincies').
inf('chaplaincy', 'chaplaincies').
inf('chieftaincy', 'chieftaincies').
inf('commandery', 'commanderies').
inf('curacy', 'curacies').
inf('deanery', 'deaneries').
inf('generalcy', 'generalcies').
inf('incumbency', 'incumbencies').
inf('lieutenancy', 'lieutenancies').
inf('magistracy', 'magistracies').
inf('mayoralty', 'mayoralties').
inf('prelacy', 'prelacies').
inf('presidency', 'presidencies').
inf('President of the United States', 'Presidents of the United States').
inf('regency', 'regencies').
inf('residency', 'residencies').
inf('Attorney General of the United States', 'Attorneys General of the United States').
inf('Secretary of Agriculture', 'Secretarys of Agriculture').
inf('Secretary of Commerce', 'Secretarys of Commerce').
inf('Secretary of Defense', 'Secretarys of Defense').
inf('Secretary of Education', 'Secretarys of Education').
inf('Secretary of Energy', 'Secretarys of Energy').
inf('Secretary of Health and Human Services', 'Secretarys of Health and Human Services').
inf('Secretary of Housing and Urban Development', 'Secretarys of Housing and Urban Development').
inf('Secretary of Labor', 'Secretarys of Labor').
inf('Secretary of State', 'Secretarys of State').
inf('Secretary of the Interior', 'Secretarys of the Interior').
inf('Secretary of the Treasury', 'Secretarys of the Treasury').
inf('Secretary of Transportation', 'Secretarys of Transportation').
inf('Secretary of Veterans Affairs', 'Secretarys of Veterans Affairs').
inf('Secretary of War', 'Secretarys of War').
inf('Secretary of the Navy', 'Secretarys of the Navy').
inf('Secretary of Commerce and Labor', 'Secretarys of Commerce and Labor').
inf('Secretary of Health Education and Welfare', 'Secretarys of Health Education and Welfare').
inf('seigniory', 'seigniories').
inf('seigneury', 'seigneuries').
inf('seismography', 'seismographies').
inf('vice-presidency', 'vice-presidencies').
inf('salt mine', 'salt ours').
inf('business life', 'business lives').
inf('professional life', 'professional lives').
inf('basketry', 'basketries').
inf('cabinetry', 'cabinetries').
inf('carpentry', 'carpentries').
inf('masonry', 'masonries').
inf('plumbery', 'plumberies').
inf('pottery', 'potteries').
inf('practice of law', 'practices of law').
inf('practice of medicine', 'practices of medicine').
inf('theology', 'theologies').
inf('committal to writing', 'committals to writing').
inf('cryptography', 'cryptographies').
inf('steganography', 'steganographies').
inf('stenography', 'stenographies').
inf('joinery', 'joineries').
inf('pyrotechny', 'pyrotechnies').
inf('accountancy', 'accountancies').
inf('single entry', 'single entries').
inf('double entry', 'double entries').
inf('first in first out', 'firsts in firsts out').
inf('last in first out', 'lasts in first out').
inf('butchery', 'butcheries').
inf('photography', 'photographies').
inf('drudgery', 'drudgeries').
inf('hand-to-hand struggle', 'hands-to-hands struggle').
inf('slavery', 'slaveries').
inf('difficulty', 'difficulties').
inf('bench press', 'bench presses').
inf('incline bench press', 'incline bench presses').
inf('press', 'presses').
inf('military press', 'military presses').
inf('snatch', 'snatches').
inf('stretch', 'stretches').
inf('tummy crunch', 'tummy crunches').
inf('consultancy', 'consultancies').
inf('cosmetology', 'cosmetologies').
inf('optometry', 'optometries').
inf('quackery', 'quackeries').
inf('analysis', 'analyses').
inf('tally', 'tallies').
inf('scrutiny', 'scrutinies').
inf('inquiry', 'inquiries').
inf('enquiry', 'enquiries').
inf('research', 'researches').
inf('search', 'searches').
inf('operations research', 'operations researches').
inf('biological research', 'biological researches').
inf('stem-cell research', 'stem-cell researches').
inf('embryonic stem-cell research', 'embryonic stem-cell researches').
inf('marketing research', 'marketing researches').
inf('market research', 'market researches').
inf('market analysis', 'market analyses').
inf('product research', 'product researches').
inf('consumer research', 'consumer researches').
inf('microscopy', 'microscopies').
inf('electron microscopy', 'electron microscopies').
inf('scientific research', 'scientific researches').
inf('endoscopy', 'endoscopies').
inf('celioscopy', 'celioscopies').
inf('colonoscopy', 'colonoscopies').
inf('culdoscopy', 'culdoscopies').
inf('gastroscopy', 'gastroscopies').
inf('hysteroscopy', 'hysteroscopies').
inf('proctoscopy', 'proctoscopies').
inf('sigmoidoscopy', 'sigmoidoscopies').
inf('flexible sigmoidoscopy', 'flexible sigmoidoscopies').
inf('gonioscopy', 'gonioscopies').
inf('keratoscopy', 'keratoscopies').
inf('rhinoscopy', 'rhinoscopies').
inf('search', 'searches').
inf('study', 'studies').
inf('time and motion study', 'time and motion studies').
inf('time-and-motion study', 'time-and-motion studies').
inf('time-motion study', 'time-motion studies').
inf('motion study', 'motion studies').
inf('time study', 'time studies').
inf('work study', 'work studies').
inf('fluorescence microscopy', 'fluorescence microscopies').
inf('anatomy', 'anatomies').
inf('urinalysis', 'urinalyses').
inf('uranalysis', 'uranalyses').
inf('scatology', 'scatologies').
inf('case study', 'case studies').
inf('chemical analysis', 'chemical analyses').
inf('qualitative analysis', 'qualitative analyses').
inf('polarography', 'polarographies').
inf('quantitative analysis', 'quantitative analyses').
inf('quantitative chemical analysis', 'quantitative chemical analyses').
inf('colorimetry', 'colorimetries').
inf('colorimetric analysis', 'colorimetric analyses').
inf('volumetric analysis', 'volumetric analyses').
inf('acidimetry', 'acidimetries').
inf('alkalimetry', 'alkalimetries').
inf('volumetric analysis', 'volumetric analyses').
inf('gravimetric analysis', 'gravimetric analyses').
inf('cost analysis', 'cost analyses').
inf('fundamental analysis', 'fundamental analyses').
inf('fundamentals analysis', 'fundamentals analyses').
inf('technical analysis', 'technical analyses').
inf('spectroscopy', 'spectroscopies').
inf('spectrometry', 'spectrometries').
inf('spectroscopic analysis', 'spectroscopic analyses').
inf('spectrum analysis', 'spectrum analyses').
inf('spectrographic analysis', 'spectrographic analyses').
inf('dialysis', 'dialyses').
inf('apheresis', 'aphereses').
inf('pheresis', 'phereses').
inf('plasmapheresis', 'plasmaphereses').
inf('plateletpheresis', 'plateletphereses').
inf('hemodialysis', 'hemodialyses').
inf('haemodialysis', 'haemodialyses').
inf('mass spectroscopy', 'mass spectroscopies').
inf('microwave spectroscopy', 'microwave spectroscopies').
inf('analogy', 'analogies').
inf('brush', 'brushes').
inf('livery', 'liveries').
inf('reflexology', 'reflexologies').
inf('therapy', 'therapies').
inf('modality', 'modalities').
inf('diathermy', 'diathermies').
inf('aromatherapy', 'aromatherapies').
inf('chemotherapy', 'chemotherapies').
inf('electrotherapy', 'electrotherapies').
inf('heliotherapy', 'heliotherapies').
inf('hormone replacement therapy', 'hormone replacement therapies').
inf('hormone-replacement therapy', 'hormone-replacement therapies').
inf('immunotherapy', 'immunotherapies').
inf('infrared therapy', 'infrared therapies').
inf('inflation therapy', 'inflation therapies').
inf('iontophoresis', 'iontophoreses').
inf('iontotherapy', 'iontotherapies').
inf('antipyresis', 'antipyreses').
inf('megavitamin therapy', 'megavitamin therapies').
inf('occupational therapy', 'occupational therapies').
inf('adenoidectomy', 'adenoidectomies').
inf('adrenalectomy', 'adrenalectomies').
inf('suprarenalectomy', 'suprarenalectomies').
inf('appendectomy', 'appendectomies').
inf('appendicectomy', 'appendicectomies').
inf('angioplasty', 'angioplasties').
inf('arthrodesis', 'arthrodeses').
inf('arthroplasty', 'arthroplasties').
inf('arthroscopy', 'arthroscopies').
inf('autoplasty', 'autoplasties').
inf('brain surgery', 'brain surgeries').
inf('psychosurgery', 'psychosurgeries').
inf('cautery', 'cauteries').
inf('chemosurgery', 'chemosurgeries').
inf('colostomy', 'colostomies').
inf('craniotomy', 'craniotomies').
inf('cryosurgery', 'cryosurgeries').
inf('cholecystectomy', 'cholecystectomies').
inf('clitoridectomy', 'clitoridectomies').
inf('laparoscopic cholecystectomy', 'laparoscopic cholecystectomies').
inf('lap choly', 'lap cholies').
inf('electrosurgery', 'electrosurgeries').
inf('enterostomy', 'enterostomies').
inf('enterotomy', 'enterotomies').
inf('surgery', 'surgeries').
inf('surgical process', 'surgical processes').
inf('wrong-site surgery', 'wrong-site surgeries').
inf('embolectomy', 'embolectomies').
inf('endarterectomy', 'endarterectomies').
inf('eye surgery', 'eye surgeries').
inf('cosmetic surgery', 'cosmetic surgeries').
inf('rhytidectomy', 'rhytidectomies').
inf('rhytidoplasty', 'rhytidoplasties').
inf('gastrectomy', 'gastrectomies').
inf('gastroenterostomy', 'gastroenterostomies').
inf('gastrostomy', 'gastrostomies').
inf('heart surgery', 'heart surgeries').
inf('closed-heart surgery', 'closed-heart surgeries').
inf('open-heart surgery', 'open-heart surgeries').
inf('coronary bypass', 'coronary bypasses').
inf('coronary bypass surgery', 'coronary bypass surgeries').
inf('port-access coronary bypass surgery', 'port-access coronary bypass surgeries').
inf('minimally invasive coronary bypass surgery', 'minimally invasive coronary bypass surgeries').
inf('hemorrhoidectomy', 'hemorrhoidectomies').
inf('haemorrhoidectomy', 'haemorrhoidectomies').
inf('hemostasis', 'hemostases').
inf('haemostasis', 'haemostases').
inf('hypophysectomy', 'hypophysectomies').
inf('hysterectomy', 'hysterectomies').
inf('hysterotomy', 'hysterotomies').
inf('radical hysterectomy', 'radical hysterectomies').
inf('panhysterectomy', 'panhysterectomies').
inf('total hysterectomy', 'total hysterectomies').
inf('gastromy', 'gastromies').
inf('cataract surgery', 'cataract surgeries').
inf('intracapsular surgery', 'intracapsular surgeries').
inf('extracapsular surgery', 'extracapsular surgeries').
inf('cyclodestructive surgery', 'cyclodestructive surgeries').
inf('filtration surgery', 'filtration surgeries').
inf('iridectomy', 'iridectomies').
inf('iridotomy', 'iridotomies').
inf('keratotomy', 'keratotomies').
inf('radial keratotomy', 'radial keratotomies').
inf('laser-assisted subepithelial keratomileusis', 'laser-assisted subepithelial keratomileuses').
inf('laser trabecular surgery', 'laser trabecular surgeries').
inf('laser-assisted in situ keratomileusis', 'laser-assisted in situ keratomileuses').
inf('vitrectomy', 'vitrectomies').
inf('perineotomy', 'perineotomies').
inf('episiotomy', 'episiotomies').
inf('ileostomy', 'ileostomies').
inf('intestinal bypass', 'intestinal bypasses').
inf('jejunostomy', 'jejunostomies').
inf('keratoplasty', 'keratoplasties').
inf('lipectomy', 'lipectomies').
inf('selective lipectomy', 'selective lipectomies').
inf('suction lipectomy', 'suction lipectomies').
inf('mastopexy', 'mastopexies').
inf('neuroplasty', 'neuroplasties').
inf('otoplasty', 'otoplasties').
inf('laminectomy', 'laminectomies').
inf('laparotomy', 'laparotomies').
inf('laparoscopy', 'laparoscopies').
inf('laryngectomy', 'laryngectomies').
inf('lithotomy', 'lithotomies').
inf('cholelithotomy', 'cholelithotomies').
inf('lobectomy', 'lobectomies').
inf('amygdalotomy', 'amygdalotomies').
inf('callosotomy', 'callosotomies').
inf('callosectomy', 'callosectomies').
inf('lobotomy', 'lobotomies').
inf('leukotomy', 'leukotomies').
inf('leucotomy', 'leucotomies').
inf('prefrontal lobotomy', 'prefrontal lobotomies').
inf('prefrontal leukotomy', 'prefrontal leukotomies').
inf('prefrontal leucotomy', 'prefrontal leucotomies').
inf('frontal lobotomy', 'frontal lobotomies').
inf('transorbital lobotomy', 'transorbital lobotomies').
inf('lumpectomy', 'lumpectomies').
inf('major surgery', 'major surgeries').
inf('microsurgery', 'microsurgeries').
inf('robotic telesurgery', 'robotic telesurgeries').
inf('minor surgery', 'minor surgeries').
inf('mastectomy', 'mastectomies').
inf('modified radical mastectomy', 'modified radical mastectomies').
inf('radical mastectomy', 'radical mastectomies').
inf('simple mastectomy', 'simple mastectomies').
inf('mastoidectomy', 'mastoidectomies').
inf('meniscectomy', 'meniscectomies').
inf('nephrectomy', 'nephrectomies').
inf('neurectomy', 'neurectomies').
inf('oophorectomy', 'oophorectomies').
inf('ovariectomy', 'ovariectomies').
inf('oophorosalpingectomy', 'oophorosalpingectomies').
inf('ophthalmectomy', 'ophthalmectomies').
inf('orchidectomy', 'orchidectomies').
inf('orchiectomy', 'orchiectomies').
inf('pancreatectomy', 'pancreatectomies').
inf('pneumonectomy', 'pneumonectomies').
inf('prostatectomy', 'prostatectomies').
inf('salpingectomy', 'salpingectomies').
inf('septectomy', 'septectomies').
inf('sigmoidectomy', 'sigmoidectomies').
inf('splenectomy', 'splenectomies').
inf('stapedectomy', 'stapedectomies').
inf('sympathectomy', 'sympathectomies').
inf('thrombectomy', 'thrombectomies').
inf('thyroidectomy', 'thyroidectomies').
inf('tonsillectomy', 'tonsillectomies').
inf('myotomy', 'myotomies').
inf('myringectomy', 'myringectomies').
inf('myringoplasty', 'myringoplasties').
inf('myringotomy', 'myringotomies').
inf('neurosurgery', 'neurosurgeries').
inf('rhinoplasty', 'rhinoplasties').
inf('orchiopexy', 'orchiopexies').
inf('orchotomy', 'orchotomies').
inf('osteotomy', 'osteotomies').
inf('ostomy', 'ostomies').
inf('palatopharyngoplasty', 'palatopharyngoplasties').
inf('uvulopalatopharyngoplasty', 'uvulopalatopharyngoplasties').
inf('phalloplasty', 'phalloplasties').
inf('phlebectomy', 'phlebectomies').
inf('plastic surgery', 'plastic surgeries').
inf('reconstructive surgery', 'reconstructive surgeries').
inf('anaplasty', 'anaplasties').
inf('polypectomy', 'polypectomies').
inf('proctoplasty', 'proctoplasties').
inf('rectoplasty', 'rectoplasties').
inf('rhinotomy', 'rhinotomies').
inf('rhizotomy', 'rhizotomies').
inf('sclerotomy', 'sclerotomies').
inf('transsexual surgery', 'transsexual surgeries').
inf('strabotomy', 'strabotomies').
inf('taxis', 'taxes').
inf('tracheostomy', 'tracheostomies').
inf('tracheotomy', 'tracheotomies').
inf('tympanoplasty', 'tympanoplasties').
inf('uranoplasty', 'uranoplasties').
inf('cryocautery', 'cryocauteries').
inf('electrocautery', 'electrocauteries').
inf('thermocautery', 'thermocauteries').
inf('nephrotomy', 'nephrotomies').
inf('thoracotomy', 'thoracotomies').
inf('valvotomy', 'valvotomies').
inf('valvulotomy', 'valvulotomies').
inf('phlebotomy', 'phlebotomies').
inf('laying on of hands', 'layings on of hands').
inf('physical therapy', 'physical therapies').
inf('physiotherapy', 'physiotherapies').
inf('phytotherapy', 'phytotherapies').
inf('herbal therapy', 'herbal therapies').
inf('psychotherapy', 'psychotherapies').
inf('behavior therapy', 'behavior therapies').
inf('aversion therapy', 'aversion therapies').
inf('exposure therapy', 'exposure therapies').
inf('implosion therapy', 'implosion therapies').
inf('reciprocal-inhibition therapy', 'reciprocal-inhibition therapies').
inf('token economy', 'token economies').
inf('client-centered therapy', 'client-centered therapies').
inf('group therapy', 'group therapies').
inf('group psychotherapy', 'group psychotherapies').
inf('family therapy', 'family therapies').
inf('hypnotherapy', 'hypnotherapies').
inf('play therapy', 'play therapies').
inf('psychoanalysis', 'psychoanalyses').
inf('analysis', 'analyses').
inf('depth psychology', 'depth psychologies').
inf('hypnoanalysis', 'hypnoanalyses').
inf('self-analysis', 'self-analyses').
inf('radiotherapy', 'radiotherapies').
inf('radiation therapy', 'radiation therapies').
inf('actinotherapy', 'actinotherapies').
inf('phototherapy', 'phototherapies').
inf('radium therapy', 'radium therapies').
inf('X-ray therapy', 'X-ray therapies').
inf('chrysotherapy', 'chrysotherapies').
inf('shock therapy', 'shock therapies').
inf('electroconvulsive therapy', 'electroconvulsive therapies').
inf('electroshock therapy', 'electroshock therapies').
inf('insulin shock therapy', 'insulin shock therapies').
inf('metrazol shock therapy', 'metrazol shock therapies').
inf('speech therapy', 'speech therapies').
inf('thermotherapy', 'thermotherapies').
inf('thrombolytic therapy', 'thrombolytic therapies').
inf('naturopathy', 'naturopathies').
inf('naprapathy', 'naprapathies').
inf('osteopathy', 'osteopathies').
inf('osteoclasis', 'osteoclases').
inf('stylostixis', 'stylostixes').
inf('G-Jo', 'G-Joes').
inf('autogenic therapy', 'autogenic therapies').
inf('allopathy', 'allopathies').
inf('homeopathy', 'homeopathies').
inf('homoeopathy', 'homoeopathies').
inf('hydropathy', 'hydropathies').
inf('hydrotherapy', 'hydrotherapies').
inf('vasectomy', 'vasectomies').
inf('vasotomy', 'vasotomies').
inf('vasovasostomy', 'vasovasostomies').
inf('vulvectomy', 'vulvectomies').
inf('splash', 'splashes').
inf('duty', 'duties').
inf('capacity', 'capacities').
inf('lieu', 'lieu').
inf('behalf', 'behalves').
inf('lineman', 'linemen').
inf('guard duty', 'guard duties').
inf('sentry duty', 'sentry duties').
inf('sentry go', 'sentry goes').
inf('fatigue duty', 'fatigue duties').
inf('sea-duty', 'sea-duties').
inf('shipboard duty', 'shipboard duties').
inf('shore duty', 'shore duties').
inf('trespass', 'trespasses').
inf('alienation of affection', 'alienations of affection').
inf('invasion of privacy', 'invasions of privacy').
inf('trespass', 'trespasses').
inf('continuing trespass', 'continuing trespasses').
inf('trespass on the case', 'trespasses on the case').
inf('delinquency', 'delinquencies').
inf('juvenile delinquency', 'juvenile delinquencies').
inf('mischievousness', 'mischievousnesses').
inf('deviltry', 'deviltries').
inf('devilry', 'devilries').
inf('rascality', 'rascalities').
inf('roguery', 'rogueries').
inf('roguishness', 'roguishnesses').
inf('monkey business', 'monkey businesses').
inf('familiarity', 'familiarities').
inf('impropriety', 'improprieties').
inf('liberty', 'liberties').
inf('abnormality', 'abnormalities').
inf('irregularity', 'irregularities').
inf('indecency', 'indecencies').
inf('impropriety', 'improprieties').
inf('immodesty', 'immodesties').
inf('obscenity', 'obscenities').
inf('peccadillo', 'peccadilloes').
inf('carelessness', 'carelessnesses').
inf('neglect of duty', 'neglects of duty').
inf('wastefulness', 'wastefulnesses').
inf('waste of effort', 'wastes of effort').
inf('waste of energy', 'wastes of energy').
inf('waste of material', 'wastes of material').
inf('waste of money', 'wastes of money').
inf('waste of time', 'wastes of time').
inf('prodigality', 'prodigalities').
inf('lavishness', 'lavishnesses').
inf('highlife', 'highlives').
inf('high life', 'high lives').
inf('legal injury', 'legal injuries').
inf('injury', 'injuries').
inf('injury', 'injuries').
inf('unfairness', 'unfairnesses').
inf('iniquity', 'iniquities').
inf('shabbiness', 'shabbinesses').
inf('immorality', 'immoralities').
inf('wickedness', 'wickednesses').
inf('iniquity', 'iniquities').
inf('villainy', 'villainies').
inf('deviltry', 'deviltries').
inf('devilry', 'devilries').
inf('enormity', 'enormities').
inf('sexual immorality', 'sexual immoralities').
inf('blasphemy', 'blasphemies').
inf('depravity', 'depravities').
inf('pornography', 'pornographies').
inf('porno', 'pornoes').
inf('child pornography', 'child pornographies').
inf('intemperateness', 'intemperatenesses').
inf('intemperateness', 'intemperatenesses').
inf('harlotry', 'harlotries').
inf('profligacy', 'profligacies').
inf('licentiousness', 'licentiousnesses').
inf('looseness', 'loosenesses').
inf('drunkenness', 'drunkennesses').
inf('dishonesty', 'dishonesties').
inf('knavery', 'knaveries').
inf('treachery', 'treacheries').
inf('perfidy', 'perfidies').
inf('double cross', 'double crosses').
inf('quackery', 'quackeries').
inf('piracy', 'piracies').
inf('fakery', 'fakeries').
inf('trickery', 'trickeries').
inf('chicanery', 'chicaneries').
inf('duplicity', 'duplicities').
inf('dupery', 'duperies').
inf('hoax', 'hoaxes').
inf('jugglery', 'juggleries').
inf('affectedness', 'affectednesses').
inf('four flush', 'four flushes').
inf('envy', 'envies').
inf('covetousness', 'covetousnesses').
inf('rapacity', 'rapacities').
inf('laziness', 'lazinesses').
inf('gluttony', 'gluttonies').
inf('act of terrorism', 'acts of terrorism').
inf('barratry', 'barratries').
inf('champerty', 'champerties').
inf('battery', 'batteries').
inf('assault and battery', 'assault and batteries').
inf('felony', 'felonies').
inf('forgery', 'forgeries').
inf('barratry', 'barratries').
inf('infringement of copyright', 'infringements of copyright').
inf('disturbance of the peace', 'disturbances of the peace').
inf('breach of the peace', 'breaches of the peace').
inf('public nudity', 'public nudities').
inf('perjury', 'perjuries').
inf('bearing false witness', 'bearing false witnesses').
inf('lying under oath', 'lyings under oath').
inf('thuggery', 'thuggeries').
inf('bigamy', 'bigamies').
inf('snatch', 'snatches').
inf('bribery', 'briberies').
inf('barratry', 'barratries').
inf('commercial bribery', 'commercial briberies').
inf('fraud in fact', 'frauds in fact').
inf('fraud in law', 'frauds in law').
inf('fraud in the factum', 'frauds in the factum').
inf('fraud in the inducement', 'frauds in the inducement').
inf('bunco', 'buncoes').
inf('bunko', 'bunkoes').
inf('larceny', 'larcenies').
inf('thievery', 'thieveries').
inf('robbery', 'robberies').
inf('armed robbery', 'armed robberies').
inf('lese majesty', 'lese majesties').
inf('biopiracy', 'biopiracies').
inf('dacoity', 'dacoities').
inf('dakoity', 'dakoities').
inf('highway robbery', 'highway robberies').
inf('piracy', 'piracies').
inf('grand larceny', 'grand larcenies').
inf('petit larceny', 'petit larcenies').
inf('petty larceny', 'petty larcenies').
inf('petty', 'petties').
inf('burglary', 'burglaries').
inf('try', 'tries').
inf('go', 'goes').
inf('pass', 'passes').
inf('immunohistochemistry', 'immunohistochemistries').
inf('phase I', 'phase we').
inf('Ministry of Transportation test', 'Ministrys of Transportation test').
inf('baby', 'babies').
inf('labor of love', 'labors of love').
inf('labour of love', 'labours of love').
inf('speleology', 'speleologies').
inf('spelaeology', 'spelaeologies').
inf('candidacy', 'candidacies').
inf('sally', 'sallies').
inf('defecation reflex', 'defecation reflexes').
inf('rectal reflex', 'rectal reflexes').
inf('clench', 'clenches').
inf('clutch', 'clutches').
inf('custody', 'custodies').
inf('conservancy', 'conservancies').
inf('prisoner of war censorship', 'prisoners of war censorship').
inf('security', 'securities').
inf('aikido', 'aikidoes').
inf('judo', 'judoes').
inf('tae kwon do', 'tae kwon does').
inf('taekwondo', 'taekwondoes').
inf('cardiography', 'cardiographies').
inf('electrocardiography', 'electrocardiographies').
inf('echocardiography', 'echocardiographies').
inf('echoencephalography', 'echoencephalographies').
inf('mouth-to-mouth resuscitation', 'mouths-to-mouths resuscitation').
inf('kiss of life', 'kisses of life').
inf('oscitancy', 'oscitancies').
inf('coprophagy', 'coprophagies').
inf('geophagy', 'geophagies').
inf('munch', 'munches').
inf('pinch', 'pinches').
inf('necrophagy', 'necrophagies').
inf('scatophagy', 'scatophagies').
inf('safe sex', 'safe sexes').
inf('sexual activity', 'sexual activities').
inf('sex', 'sexes').
inf('sex activity', 'sex activities').
inf('sexual congress', 'sexual congresses').
inf('congress', 'congresses').
inf('ass', 'asses').
inf('nooky', 'nookies').
inf('piece of ass', 'pieces of ass').
inf('piece of tail', 'pieces of tail').
inf('roll in the hay', 'rolls in the hay').
inf('hank panky', 'hank pankies').
inf('love life', 'love lives').
inf('criminal congress', 'criminal congresses').
inf('extramarital sex', 'extramarital sexes').
inf('adultery', 'adulteries').
inf('sodomy', 'sodomies').
inf('buggery', 'buggeries').
inf('anal sex', 'anal sexes').
inf('facts of life', 'factss of life').
inf('biogenesis', 'biogeneses').
inf('biogeny', 'biogenies').
inf('cross', 'crosses').
inf('dihybrid cross', 'dihybrid crosses').
inf('monohybrid cross', 'monohybrid crosses').
inf('reciprocal cross', 'reciprocal crosses').
inf('testcross', 'testcrosses').
inf('test-cross', 'test-crosses').
inf('oral sex', 'oral sexes').
inf('promiscuity', 'promiscuities').
inf('promiscuousness', 'promiscuousnesses').
inf('lechery', 'lecheries').
inf('homosexuality', 'homosexualities').
inf('queerness', 'queernesses').
inf('gayness', 'gaynesses').
inf('bisexuality', 'bisexualities').
inf('heterosexuality', 'heterosexualities').
inf('straightness', 'straightnesses').
inf('pederasty', 'pederasties').
inf('paederasty', 'paederasties').
inf('bestiality', 'bestialities').
inf('zooerasty', 'zooerasties').
inf('taxis', 'taxes').
inf('chemotaxis', 'chemotaxes').
inf('negative chemotaxis', 'negative chemotaxes').
inf('positive chemotaxis', 'positive chemotaxes').
inf('kinesis', 'kineses').
inf('reflex', 'reflexes').
inf('instinctive reflex', 'instinctive reflexes').
inf('innate reflex', 'innate reflexes').
inf('inborn reflex', 'inborn reflexes').
inf('unconditioned reflex', 'unconditioned reflexes').
inf('conditional reflex', 'conditional reflexes').
inf('conditioned reflex', 'conditioned reflexes').
inf('acquired reflex', 'acquired reflexes').
inf('knee-jerk reflex', 'knee-jerk reflexes').
inf('patellar reflex', 'patellar reflexes').
inf('startle reflex', 'startle reflexes').
inf('Moro reflex', 'Moro reflexes').
inf('flinch', 'flinches').
inf('light reflex', 'light reflexes').
inf('pupillary reflex', 'pupillary reflexes').
inf('miosis', 'mioses').
inf('myosis', 'myoses').
inf('mydriasis', 'mydriases').
inf('micturition reflex', 'micturition reflexes').
inf('pharyngeal reflex', 'pharyngeal reflexes').
inf('gag reflex', 'gag reflexes').
inf('pilomotor reflex', 'pilomotor reflexes').
inf('gooseflesh', 'goosefleshes').
inf('plantar reflex', 'plantar reflexes').
inf('rooting reflex', 'rooting reflexes').
inf('stretch reflex', 'stretch reflexes').
inf('myotactic reflex', 'myotactic reflexes').
inf('suckling reflex', 'suckling reflexes').
inf('mathematical process', 'mathematical processes').
inf('pleximetry', 'pleximetries').
inf('sensory activity', 'sensory activities').
inf('scrutiny', 'scrutinies').
inf('watch', 'watches').
inf('surveillance of disease', 'surveillances of disease').
inf('listening watch', 'listening watches').
inf('continuous receiver watch', 'continuous receiver watches').
inf('dekko', 'dekkoes').
inf('pedagogy', 'pedagogies').
inf('educational activity', 'educational activities').
inf('course of study', 'courses of study').
inf('course of instruction', 'courses of instruction').
inf('class', 'classes').
inf('art class', 'art classes').
inf('childbirth-preparation class', 'childbirth-preparation classes').
inf('life class', 'life classes').
inf('extracurricular activity', 'extracurricular activities').
inf('pedagogy', 'pedagogies').
inf('catechesis', 'catecheses').
inf('course of lectures', 'courses of lectures').
inf('directed study', 'directed studies').
inf('home study', 'home studies').
inf('shop class', 'shop classes').
inf('manual of arms', 'manuals of arms').
inf('puppetry', 'puppetries').
inf('pageantry', 'pageantries').
inf('tomography', 'tomographies').
inf('computerized tomography', 'computerized tomographies').
inf('computed tomography', 'computed tomographies').
inf('computerized axial tomography', 'computerized axial tomographies').
inf('computed axial tomography', 'computed axial tomographies').
inf('sonography', 'sonographies').
inf('ultrasonography', 'ultrasonographies').
inf('echography', 'echographies').
inf('A-scan ultrasonography', 'A-scan ultrasonographies').
inf('B-scan ultrasonography', 'B-scan ultrasonographies').
inf('positron emission tomography', 'positron emission tomographies').
inf('fluoroscopy', 'fluoroscopies').
inf('radioscopy', 'radioscopies').
inf('radiology', 'radiologies').
inf('photography', 'photographies').
inf('radiography', 'radiographies').
inf('roentgenography', 'roentgenographies').
inf('X-ray photography', 'X-ray photographies').
inf('xerography', 'xerographies').
inf('xeroradiography', 'xeroradiographies').
inf('angiography', 'angiographies').
inf('lymphangiography', 'lymphangiographies').
inf('lymphography', 'lymphographies').
inf('arteriography', 'arteriographies').
inf('arthrography', 'arthrographies').
inf('venography', 'venographies').
inf('cholangiography', 'cholangiographies').
inf('encephalography', 'encephalographies').
inf('myelography', 'myelographies').
inf('pyelography', 'pyelographies').
inf('intravenous pyelography', 'intravenous pyelographies').
inf('telephotography', 'telephotographies').
inf('telephotography', 'telephotographies').
inf('radiophotography', 'radiophotographies').
inf('cinematography', 'cinematographies').
inf('motion-picture photography', 'motion-picture photographies').
inf('creative activity', 'creative activities').
inf('creating from raw materials', 'creatings from raw materials').
inf('lip synch', 'lip synches').
inf('assembly', 'assemblies').
inf('disassembly', 'disassemblies').
inf('cottage industry', 'cottage industries').
inf('capacity', 'capacities').
inf('husbandry', 'husbandries').
inf('animal husbandry', 'animal husbandries').
inf('tree surgery', 'tree surgeries').
inf('industry', 'industries').
inf('cartography', 'cartographies').
inf('creating by mental acts', 'creatings by mental acts').
inf('adoxography', 'adoxographies').
inf('historiography', 'historiographies').
inf('lexicography', 'lexicographies').
inf('glyptography', 'glyptographies').
inf('gastronomy', 'gastronomies').
inf('fresco', 'frescoes').
inf('impasto', 'impastoes').
inf('perfumery', 'perfumeries').
inf('topiary', 'topiaries').
inf('pyrography', 'pyrographies').
inf('serigraphy', 'serigraphies').
inf('lithography', 'lithographies').
inf('neology', 'neologies').
inf('approach', 'approaches').
inf('plan of attack', 'plans of attack').
inf('creating by removal', 'creatings by removal').
inf('centesis', 'centeses').
inf('abdominocentesis', 'abdominocenteses').
inf('paracentesis', 'paracenteses').
inf('amniocentesis', 'amniocenteses').
inf('arthrocentesis', 'arthrocenteses').
inf('celiocentesis', 'celiocenteses').
inf('thoracocentesis', 'thoracocenteses').
inf('thoracentesis', 'thoracenteses').
inf('fetoscopy', 'fetoscopies').
inf('foetoscopy', 'foetoscopies').
inf('search', 'searches').
inf('strip search', 'strip searches').
inf('technology', 'technologies').
inf('automotive technology', 'automotive technologies').
inf('communications technology', 'communications technologies').
inf('digital communications technology', 'digital communications technologies').
inf('computer technology', 'computer technologies').
inf('high technology', 'high technologies').
inf('high tech', 'high teches').
inf('rail technology', 'rail technologies').
inf('war of nerves', 'wars of nerves').
inf('brush', 'brushes').
inf('clash', 'clashes').
inf('skirmish', 'skirmishes').
inf('insurgency', 'insurgencies').
inf('counterinsurgency', 'counterinsurgencies').
inf('mutiny', 'mutinies').
inf('hostility', 'hostilities').
inf('belligerency', 'belligerencies').
inf('banditry', 'banditries').
inf('robbery', 'robberies').
inf('sally', 'sallies').
inf('onrush', 'onrushes').
inf('intelligence activity', 'intelligence activities').
inf('recco', 'reccoes').
inf('reccy', 'reccies').
inf('reconnaissance by fire', 'reconnaissances by fire').
inf('reconnaissance in force', 'reconnaissances in force').
inf('battery', 'batteries').
inf('salvo', 'salvoes').
inf('actinometry', 'actinometries').
inf('algometry', 'algometries').
inf('anemography', 'anemographies').
inf('anemometry', 'anemometries').
inf('anthropometry', 'anthropometries').
inf('audiometry', 'audiometries').
inf('bathymetry', 'bathymetries').
inf('calorimetry', 'calorimetries').
inf('cephalometry', 'cephalometries').
inf('densitometry', 'densitometries').
inf('dosimetry', 'dosimetries').
inf('fetometry', 'fetometries').
inf('foetometry', 'foetometries').
inf('hydrometry', 'hydrometries').
inf('gravimetry', 'gravimetries').
inf('hypsometry', 'hypsometries').
inf('hypsography', 'hypsographies').
inf('micrometry', 'micrometries').
inf('pelvimetry', 'pelvimetries').
inf('photometry', 'photometries').
inf('cytophotometry', 'cytophotometries').
inf('divergency', 'divergencies').
inf('spirometry', 'spirometries').
inf('electromyography', 'electromyographies').
inf('mammography', 'mammographies').
inf('thermography', 'thermographies').
inf('mammothermography', 'mammothermographies').
inf('Rorschach', 'Rorschaches').
inf('inventory', 'inventories').
inf('taxonomy', 'taxonomies').
inf('typology', 'typologies').
inf('conchology', 'conchologies').
inf('numismatology', 'numismatologies').
inf('philately', 'philatelies').
inf('aerophilately', 'aerophilatelies').
inf('redundancy', 'redundancies').
inf('high fidelity', 'high fidelities').
inf('echo', 'echoes').
inf('mimicry', 'mimicries').
inf('process', 'processes').
inf('ceremony', 'ceremonies').
inf('tea ceremony', 'tea ceremonies').
inf('ceremony', 'ceremonies').
inf('religious ceremony', 'religious ceremonies').
inf('military ceremony', 'military ceremonies').
inf('apotheosis', 'apotheoses').
inf('watch', 'watches').
inf('orgy', 'orgies').
inf('popery', 'poperies').
inf('Briss', 'Brisses').
inf('church', 'churches').
inf('liturgy', 'liturgies').
inf('Christian liturgy', 'Christian liturgies').
inf('Office of the Dead', 'Offices of the Dead').
inf('placebo', 'placeboes').
inf('sacrament of the Eucharist', 'sacraments of the Eucharist').
inf('Eucharistic liturgy', 'Eucharistic liturgies').
inf('matrimony', 'matrimonies').
inf('marriage ceremony', 'marriage ceremonies').
inf('rite of passage', 'rites of passage').
inf('love match', 'love matches').
inf('anointing of the sick', 'anointings of the sick').
inf('Mass', 'Masses').
inf('High Mass', 'High Masses').
inf('Low Mass', 'Low Masses').
inf('Stations of the Cross', 'Stationss of the Cross').
inf('idolatry', 'idolatries').
inf('bardolatry', 'bardolatries').
inf('iconolatry', 'iconolatries').
inf('idolatry', 'idolatries').
inf('idiolatry', 'idiolatries').
inf('autolatry', 'autolatries').
inf('bibliolatry', 'bibliolatries').
inf('verbolatry', 'verbolatries').
inf('grammatolatry', 'grammatolatries').
inf('symbolatry', 'symbolatries').
inf('symbololatry', 'symbololatries').
inf('anthropolatry', 'anthropolatries').
inf('worship of man', 'worships of man').
inf('gyneolatry', 'gyneolatries').
inf('gynaeolatry', 'gynaeolatries').
inf('lordolatry', 'lordolatries').
inf('thaumatolatry', 'thaumatolatries').
inf('topolatry', 'topolatries').
inf('arborolatry', 'arborolatries').
inf('astrolatry', 'astrolatries').
inf('worship of heavenly bodies', 'worships of heavenly bodies').
inf('cosmolatry', 'cosmolatries').
inf('diabolatry', 'diabolatries').
inf('demonolatry', 'demonolatries').
inf('pyrolatry', 'pyrolatries').
inf('hagiolatry', 'hagiolatries').
inf('hierolatry', 'hierolatries').
inf('heliolatry', 'heliolatries').
inf('zoolatry', 'zoolatries').
inf('ichthyolatry', 'ichthyolatries').
inf('monolatry', 'monolatries').
inf('ophiolatry', 'ophiolatries').
inf('selenolatry', 'selenolatries').
inf('wall of silence', 'walls of silence').
inf('residency', 'residencies').
inf('occupancy', 'occupancies').
inf('tenancy', 'tenancies').
inf('inhabitancy', 'inhabitancies').
inf('supply', 'supplies').
inf('healthcare delivery', 'healthcare deliveries').
inf('health care delivery', 'health care deliveries').
inf('care delivery', 'care deliveries').
inf('inactivity', 'inactivities').
inf('recess', 'recesses').
inf('laziness', 'lazinesses').
inf('quiescency', 'quiescencies').
inf('dormancy', 'dormancies').
inf('idleness', 'idlenesses').
inf('shillyshally', 'shillyshallies').
inf('ascesis', 'asceses').
inf('chastity', 'chastities').
inf('celibacy', 'celibacies').
inf('sobriety', 'sobrieties').
inf('point duty', 'point duties').
inf('leniency', 'leniencies').
inf('clemency', 'clemencies').
inf('mercifulness', 'mercifulnesses').
inf('mercy', 'mercies').
inf('excess', 'excesses').
inf('orgy', 'orgies').
inf('prophylaxis', 'prophylaxes').
inf('social activity', 'social activities').
inf('generosity', 'generosities').
inf('unselfishness', 'unselfishnesses').
inf('accordance of rights', 'accordances of rights').
inf('pogy', 'pogies').
inf('philanthropy', 'philanthropies').
inf('charity', 'charities').
inf('usury', 'usuries').
inf('business', 'businesses').
inf('business activity', 'business activities').
inf('commercial activity', 'commercial activities').
inf('business', 'businesses').
inf('wash', 'washes').
inf('land-office business', 'land-office businesses').
inf('field of operation', 'fields of operation').
inf('line of business', 'lines of business').
inf('employee-owned business', 'employee-owned businesses').
inf('discount business', 'discount businesses').
inf('real-estate business', 'real-estate businesses').
inf('typography', 'typographies').
inf('meat-packing business', 'meat-packing businesses').
inf('agribusiness', 'agribusinesses').
inf('express', 'expresses').
inf('ferry', 'ferries').
inf('conveyance of title', 'conveyances of title').
inf('delivery', 'deliveries').
inf('livery', 'liveries').
inf('simony', 'simonies').
inf('barratry', 'barratries').
inf('telemetry', 'telemetries').
inf('thermometry', 'thermometries').
inf('thermogravimetry', 'thermogravimetries').
inf('tonometry', 'tonometries').
inf('viscometry', 'viscometries').
inf('viscosimetry', 'viscosimetries').
inf('going-out-of-business sale', 'goings-out-of-business sale').
inf('upholstery', 'upholsteries').
inf('gold rush', 'gold rushes').
inf('government activity', 'government activities').
inf('mastery', 'masteries').
inf('duty', 'duties').
inf('responsibility', 'responsibilities').
inf('burden of proof', 'burdens of proof').
inf('civic duty', 'civic duties').
inf('civic responsibility', 'civic responsibilities').
inf('jury duty', 'jury duties').
inf('filial duty', 'filial duties').
inf('incumbency', 'incumbencies').
inf('legal duty', 'legal duties').
inf('fiduciary duty', 'fiduciary duties').
inf('line of duty', 'lines of duty').
inf('polity', 'polities').
inf('not-for-profit', 'nots-for-profit').
inf('benefit of clergy', 'benefits of clergy').
inf('makeready', 'makereadies').
inf('putsch', 'putsches').
inf('leash', 'leashes').
inf('custody', 'custodies').
inf('restraint of trade', 'restraints of trade').
inf('levy', 'levies').
inf('penalty', 'penalties').
inf('death penalty', 'death penalties').
inf('kick in the butt', 'kicks in the butt').
inf('burning at the stake', 'burnings at the stake').
inf('rally', 'rallies').
inf('tug-of-war', 'tugs-of-war').
inf('rivalry', 'rivalries').
inf('battle of wits', 'battles of wits').
inf('set-to', 'set-toes').
inf('aggro', 'aggroes').
inf('passado', 'passadoes').
inf('parry', 'parries').
inf('free-for-all', 'frees-for-all').
inf('peace march', 'peace marches').
inf('protest march', 'protest marches').
inf('rebelliousness', 'rebelliousnesses').
inf('contumacy', 'contumacies').
inf('contempt of Congress', 'contempts of Congress').
inf('contempt of court', 'contempts of court').
inf('contumacy', 'contumacies').
inf('obstruction of justice', 'obstructions of justice').
inf('due process', 'due processes').
inf('action at law', 'actions at law').
inf('bankruptcy', 'bankruptcies').
inf('confession of judgment', 'confessions of judgment').
inf('confession of judgement', 'confessions of judgement').
inf('judgment by default', 'judgments by default').
inf('judgement by default', 'judgements by default').
inf('judgment of conviction', 'judgments of conviction').
inf('judgment in personam', 'judgments in personam').
inf('judgement in personam', 'judgements in personam').
inf('judgment in rem', 'judgments in rem').
inf('judgement in rem', 'judgements in rem').
inf('judgment of dismissal', 'judgments of dismissal').
inf('judgement of dismissal', 'judgements of dismissal').
inf('judgment on the merits', 'judgments on the merits').
inf('judgement on the merits', 'judgements on the merits').
inf('judgment on the pleadings', 'judgments on the pleadings').
inf('judgement on the pleadings', 'judgements on the pleadings').
inf('finding of fact', 'findings of fact').
inf('finding of law', 'findings of law').
inf('conclusion of law', 'conclusions of law').
inf('trial by ordeal', 'trials by ordeal').
inf('bill of review', 'bills of review').
inf('double jeopardy', 'double jeopardies').
inf('quo warranto', 'quo warrantoes').
inf('conformity', 'conformities').
inf('formality', 'formalities').
inf('nonconformity', 'nonconformities').
inf('concurrency', 'concurrencies').
inf('selflessness', 'selflessnesses').
inf('loyalty', 'loyalties').
inf('fetish', 'fetishes').
inf('fetich', 'fetiches').
inf('advocacy', 'advocacies').
inf('insistency', 'insistencies').
inf('calumny', 'calumnies').
inf('territoriality', 'territorialities').
inf('bitchery', 'bitcheries').
inf('discourtesy', 'discourtesies').
inf('offensive activity', 'offensive activities').
inf('easiness', 'easinesses').
inf('indelicacy', 'indelicacies').
inf('indignity', 'indignities').
inf('forgiveness', 'forgivenesses').
inf('mercy', 'mercies').
inf('politeness', 'politenesses').
inf('civility', 'civilities').
inf('courtesy', 'courtesies').
inf('gallantry', 'gallantries').
inf('thoughtfulness', 'thoughtfulnesses').
inf('assembly', 'assemblies').
inf('coming into court', 'comings into court').
inf('truancy', 'truancies').
inf('hooky', 'hookies').
inf('tit for tat', 'tits for tat').
inf('neutrality', 'neutralities').
inf('dissolution of marriage', 'dissolutions of marriage').
inf('whitewash', 'whitewashes').
inf('work to rule', 'works to rule').
inf('recusancy', 'recusancies').
inf('toss', 'tosses').
inf('ambush', 'ambushes').
inf('lying in wait', 'lyings in wait').
inf('stupidity', 'stupidities').
inf('folly', 'follies').
inf('foolishness', 'foolishnesses').
inf('imbecility', 'imbecilities').
inf('amnesty', 'amnesties').
inf('psalmody', 'psalmodies').
inf('hymnody', 'hymnodies').
inf('precedency', 'precedencies').
inf('solo', 'soloes').
inf('Alamo', 'Alamoes').
inf('battle of Atlanta', 'battles of Atlanta').
inf('battle of Austerlitz', 'battles of Austerlitz').
inf('Battle of Britain', 'Battles of Britain').
inf('Battle of Kerbala', 'Battles of Kerbala').
inf('Battle of the Ardennes Bulge', 'Battles of the Ardennes Bulge').
inf('Battle of the Bulge', 'Battles of the Bulge').
inf('Battle of the Marne', 'Battles of the Marne').
inf('battle of the Bismarck Sea', 'battles of the Bismarck Sea').
inf('Borodino', 'Borodinoes').
inf('battle of Boyne', 'battles of Boyne').
inf('battle of Brunanburh', 'battles of Brunanburh').
inf('Battle of Bull Run', 'Battles of Bull Run').
inf('battle of Bunker Hill', 'battles of Bunker Hill').
inf('Caporetto', 'Caporettoes').
inf('battle of Caporetto', 'battles of Caporetto').
inf('battle of Chattanooga', 'battles of Chattanooga').
inf('battle of Chickamauga', 'battles of Chickamauga').
inf('battle of the Coral Sea', 'battles of the Coral Sea').
inf('battle of Cowpens', 'battles of Cowpens').
inf('battle of Crecy', 'battles of Crecy').
inf('battle of Cunaxa', 'battles of Cunaxa').
inf('battle of Cynoscephalae', 'battles of Cynoscephalae').
inf('Battle of El Alamein', 'Battles of El Alamein').
inf('Battle of Flodden Field', 'Battles of Flodden Field').
inf('Battle of Fontenoy', 'Battles of Fontenoy').
inf('Battle of Fredericksburg', 'Battles of Fredericksburg').
inf('Battle of Gettysburg', 'Battles of Gettysburg').
inf('Battle of Granicus River', 'Battles of Granicus River').
inf('Battle of Guadalcanal', 'Battles of Guadalcanal').
inf('battle of Hastings', 'battles of Hastings').
inf('battle of Hohenlinden', 'battles of Hohenlinden').
inf('battle of Ipsus', 'battles of Ipsus').
inf('battle of Issus', 'battles of Issus').
inf('battle of Ivry', 'battles of Ivry').
inf('Iwo', 'Iwoes').
inf('invasion of Iwo', 'invasions of Iwo').
inf('Battle of Jena', 'Battles of Jena').
inf('battle of Jutland', 'battles of Jutland').
inf('Battle of Lake Trasimenus', 'Battles of Lake Trasimenus').
inf('battle of Langside', 'battles of Langside').
inf('Lepanto', 'Lepantoes').
inf('Battle of Lepanto', 'Battles of Lepanto').
inf('battle of Leuctra', 'battles of Leuctra').
inf('Battle of Little Bighorn', 'Battles of Little Bighorn').
inf('Battle of the Little Bighorn', 'Battles of the Little Bighorn').
inf('battle of Lule Burgas', 'battles of Lule Burgas').
inf('battle of Lutzen', 'battles of Lutzen').
inf('Battle of Magenta', 'Battles of Magenta').
inf('Battle of Maldon', 'Battles of Maldon').
inf('battle of Marathon', 'battles of Marathon').
inf('Marengo', 'Marengoes').
inf('battle of Marston Moor', 'battles of Marston Moor').
inf('Battle of Midway', 'Battles of Midway').
inf('battle of Minden', 'battles of Minden').
inf('Battle of Monmouth Court House', 'Battles of Monmouth Court House').
inf('Battle of Monmouth', 'Battles of Monmouth').
inf('Battle of Naseby', 'Battles of Naseby').
inf('Navarino', 'Navarinoes').
inf('battle of Navarino', 'battles of Navarino').
inf('Omdurman', 'Omdurmen').
inf('battle of Omdurman', 'battles of Omdurman').
inf('siege of Orleans', 'sieges of Orleans').
inf('battle of Panipat', 'battles of Panipat').
inf('Passero', 'Passeroes').
inf('Cape Passero', 'Cape Passeroes').
inf('battle of Pharsalus', 'battles of Pharsalus').
inf('battle of Philippi', 'battles of Philippi').
inf('battle of the Philippine Sea', 'battles of the Philippine Sea').
inf('battle of Plassey', 'battles of Plassey').
inf('battle of Plataea', 'battles of Plataea').
inf('battle of Poitiers', 'battles of Poitiers').
inf('Battle of Puebla', 'Battles of Puebla').
inf('Battle of Pydna', 'Battles of Pydna').
inf('Battle of Ravenna', 'Battles of Ravenna').
inf('Battle of Rocroi', 'Battles of Rocroi').
inf('Rossbach', 'Rossbaches').
inf('battle of Rossbach', 'battles of Rossbach').
inf('battle of St Mihiel', 'battles of St Mihiel').
inf('Salerno', 'Salernoes').
inf('Santiago', 'Santiagoes').
inf('battle of Saratoga', 'battles of Saratoga').
inf('Sempatch', 'Sempatches').
inf('battle of Sempatch', 'battles of Sempatch').
inf('battle of Shiloh', 'battles of Shiloh').
inf('battle of Pittsburgh Landing', 'battles of Pittsburgh Landing').
inf('battle of Soissons-Reims', 'battles of Soissons-Reims').
inf('battle of the Chemin-des-Dames', 'battles of the Chemin-des-Dames').
inf('battle of the Aisne', 'battles of the Aisne').
inf('Solferino', 'Solferinoes').
inf('battle of Solferino', 'battles of Solferino').
inf('Battle of the Somme', 'Battles of the Somme').
inf('Battle of the Somme', 'Battles of the Somme').
inf('Battle of the Spanish Armada', 'Battles of the Spanish Armada').
inf('battle of Spotsylvania Courthouse', 'battles of Spotsylvania Courthouse').
inf('siege of Syracuse', 'sieges of Syracuse').
inf('siege of Syracuse', 'sieges of Syracuse').
inf('battle of Tannenberg', 'battles of Tannenberg').
inf('battle of Tertry', 'battles of Tertry').
inf('battle of Teutoburger Wald', 'battles of Teutoburger Wald').
inf('battle of Tewkesbury', 'battles of Tewkesbury').
inf('battle of Thermopylae', 'battles of Thermopylae').
inf('battle of Trafalgar', 'battles of Trafalgar').
inf('Trasimeno', 'Trasimenoes').
inf('battle of Trasimeno', 'battles of Trasimeno').
inf('battle of Valmy', 'battles of Valmy').
inf('battle of Verdun', 'battles of Verdun').
inf('siege of Vicksburg', 'sieges of Vicksburg').
inf('battle of Wagram', 'battles of Wagram').
inf('Battle of Wake', 'Battles of Wake').
inf('Battle of Wake Island', 'Battles of Wake Island').
inf('Battle of Waterloo', 'Battles of Waterloo').
inf('siege of Yorktown', 'sieges of Yorktown').
inf('battle of Ypres', 'battles of Ypres').
inf('battle of Ypres', 'battles of Ypres').
inf('battle of Ypres', 'battles of Ypres').
inf('battle of Zama', 'battles of Zama').
inf('War between the States', 'Wars between the States').
inf('War of American Independence', 'Wars of American Independence').
inf('War of Greek Independence', 'Wars of Greek Independence').
inf('War of the Austrian Succession', 'Wars of the Austrian Succession').
inf('War of the Grand Alliance', 'Wars of the Grand Alliance').
inf('War of the League of Augsburg', 'Wars of the League of Augsburg').
inf('War of the Spanish Succession', 'Wars of the Spanish Succession').
inf('War of the Roses', 'Wars of the Roses').
inf('Wars of the Roses', 'Warss of the Roses').
inf('War of 1812', 'Wars of 1812').
inf('World War I', 'World War we').
inf('War to End War', 'Wars to End Wars').
inf('creepy-crawly', 'creepy-crawlies').
inf('beast of burden', 'beasts of burden').
inf('by-catch', 'by-catches').
inf('bycatch', 'bycatches').
inf('baby', 'babies').
inf('puppy', 'puppies').
inf('form genus', 'form genera').
inf('cross', 'crosses').
inf('herpes simplex', 'herpes simplexes').
inf('HSV-I', 'HSV-we').
inf('parvo', 'parvoes').
inf('Bacillus anthracis', 'Bacillus anthraces').
inf('bacteria family', 'bacteria families').
inf('bacteria genus', 'bacteria genera').
inf('Francisella tularensis', 'Francisella tularenses').
inf('Mycobacterium tuberculosis', 'Mycobacterium tuberculoses').
inf('entozoon', 'entozoa').
inf('ectozoon', 'ectozoa').
inf('epizoon', 'epizoa').
inf('protoctist family', 'protoctist families').
inf('protoctist genus', 'protoctist genera').
inf('protozoon', 'protozoa').
inf('seagrass', 'seagrasses').
inf('chlorophyll a', 'chlorophyll some').
inf('sargasso', 'sargassoes').
inf('sea-lettuce family', 'sea-lettuce families').
inf('Volvox', 'Volvoxes').
inf('genus Volvox', 'genus Volvoxes').
inf('sea moss', 'sea mosses').
inf('Irish moss', 'Irish mosses').
inf('Costia necatrix', 'Costia necatrices').
inf('Chilomastix', 'Chilomastixes').
inf('genus Chilomastix', 'genus Chilomastixes').
inf('Plasmodium vivax', 'Plasmodium vivaxes').
inf('genus Leucocytozoon', 'genus Leucocytozoa').
inf('leucocytozoon', 'leucocytozoa').
inf('soft-finned fish', 'soft-finned fish').
inf('fish family', 'fish families').
inf('fish genus', 'fish genera').
inf('cypriniform fish', 'cypriniform fish').
inf('loach', 'loaches').
inf('cyprinid fish', 'cyprinid fish').
inf('tench', 'tenches').
inf('roach', 'roaches').
inf('goldfish', 'goldfish').
inf('silverfish', 'silverfish').
inf('buffalo fish', 'buffalo fish').
inf('buffalofish', 'buffalofish').
inf('black buffalo', 'black buffaloes').
inf('hog molly', 'hog mollies').
inf('killifish', 'killifish').
inf('striped killifish', 'striped killifish').
inf('mayfish', 'mayfish').
inf('may fish', 'may fish').
inf('flagfish', 'flagfish').
inf('American flagfish', 'American flagfish').
inf('guppy', 'guppies').
inf('rainbow fish', 'rainbow fish').
inf('poeciliid fish', 'poeciliid fish').
inf('mosquitofish', 'mosquitofish').
inf('platy', 'platies').
inf('molly', 'mollies').
inf('squirrelfish', 'squirrelfish').
inf('reef squirrelfish', 'reef squirrelfish').
inf('deepwater squirrelfish', 'deepwater squirrelfish').
inf('soldierfish', 'soldierfish').
inf('soldier-fish', 'soldier-fish').
inf('flashlight fish', 'flashlight fish').
inf('flashlight fish', 'flashlight fish').
inf('dory', 'dories').
inf('boarfish', 'boarfish').
inf('boarfish', 'boarfish').
inf('cornetfish', 'cornetfish').
inf('pipefish', 'pipefish').
inf('needlefish', 'needlefish').
inf('dwarf pipefish', 'dwarf pipefish').
inf('deepwater pipefish', 'deepwater pipefish').
inf('snipefish', 'snipefish').
inf('bellows fish', 'bellows fish').
inf('shrimpfish', 'shrimpfish').
inf('shrimp-fish', 'shrimp-fish').
inf('trumpetfish', 'trumpetfish').
inf('embryo', 'embryoes').
inf('segmentation cavity', 'segmentation cavities').
inf('cleavage cavity', 'cleavage cavities').
inf('chordate family', 'chordate families').
inf('chordate genus', 'chordate genera').
inf('allantois', 'allantois').
inf('chorioallantois', 'chorioallantois').
inf('jawless fish', 'jawless fish').
inf('hagfish', 'hagfish').
inf('cartilaginous fish', 'cartilaginous fish').
inf('rabbitfish', 'rabbitfish').
inf('elasmobranch', 'elasmobranches').
inf('mako', 'makoes').
inf('shortfin mako', 'shortfin makoes').
inf('longfin mako', 'longfin makoes').
inf('Galeocerdo', 'Galeocerdoes').
inf('genus Galeocerdo', 'genus Galeocerdoes').
inf('dogfish', 'dogfish').
inf('smooth dogfish', 'smooth dogfish').
inf('American smooth dogfish', 'American smooth dogfish').
inf('spiny dogfish', 'spiny dogfish').
inf('Atlantic spiny dogfish', 'Atlantic spiny dogfish').
inf('Pacific spiny dogfish', 'Pacific spiny dogfish').
inf('Sphyrna tiburo', 'Sphyrna tiburoes').
inf('angelfish', 'angelfish').
inf('monkfish', 'monkfish').
inf('crampfish', 'crampfish').
inf('numbfish', 'numbfish').
inf('torpedo', 'torpedoes').
inf('sawfish', 'sawfish').
inf('smalltooth sawfish', 'smalltooth sawfish').
inf('guitarfish', 'guitarfish').
inf('devilfish', 'devilfish').
inf('bird family', 'bird families').
inf('bird genus', 'bird genera').
inf('bird of passage', 'birds of passage').
inf('genus Archaeopteryx', 'genus Archaeopteryxes').
inf('genus Archeopteryx', 'genus Archeopteryxes').
inf('archaeopteryx', 'archaeopteryxes').
inf('archeopteryx', 'archeopteryxes').
inf('ostrich', 'ostriches').
inf('cassowary', 'cassowaries').
inf('genus Apteryx', 'genus Apteryxes').
inf('apteryx', 'apteryxes').
inf('genus Anomalopteryx', 'genus Anomalopteryxes').
inf('anomalopteryx', 'anomalopteryxes').
inf('Alauda arvensis', 'Alauda arvenses').
inf('Anthus pratensis', 'Anthus pratenses').
inf('finch', 'finches').
inf('chaffinch', 'chaffinches').
inf('goldfinch', 'goldfinches').
inf('New World goldfinch', 'New World goldfinches').
inf('goldfinch', 'goldfinches').
inf('pine finch', 'pine finches').
inf('house finch', 'house finches').
inf('purple finch', 'purple finches').
inf('canary', 'canaries').
inf('common canary', 'common canaries').
inf('bullfinch', 'bullfinches').
inf('genus Junco', 'genus Juncoes').
inf('junco', 'juncoes').
inf('dark-eyed junco', 'dark-eyed juncoes').
inf('slate-colored junco', 'slate-colored juncoes').
inf('grass finch', 'grass finches').
inf('indigo finch', 'indigo finches').
inf('Plectrophenax', 'Plectrophenaxes').
inf('genus Plectrophenax', 'genus Plectrophenaxes').
inf('hawfinch', 'hawfinches').
inf('Pipilo', 'Pipiloes').
inf('genus Pipilo', 'genus Pipiloes').
inf('weaver finch', 'weaver finches').
inf('Java finch', 'Java finches').
inf('grassfinch', 'grassfinches').
inf('grass finch', 'grass finches').
inf('zebra finch', 'zebra finches').
inf('mamo', 'mamoes').
inf('petchary', 'petcharies').
inf('Tyrannus domenicensis domenicensis', 'Tyrannus domenicensis domenicenses').
inf('Contopus', 'Contopera').
inf('genus Contopus', 'genus Contopera').
inf('cock of the rock', 'cocks of the rock').
inf('cock of the rock', 'cocks of the rock').
inf('ant thrush', 'ant thrushes').
inf('Hylophylax', 'Hylophylaxes').
inf('genus Hylophylax', 'genus Hylophylaxes').
inf('thrush', 'thrushes').
inf('missel thrush', 'missel thrushes').
inf('mistle thrush', 'mistle thrushes').
inf('mistletoe thrush', 'mistletoe thrushes').
inf('song thrush', 'song thrushes').
inf('ring thrush', 'ring thrushes').
inf('hermit thrush', 'hermit thrushes').
inf('veery', 'veeries').
inf('Wilson\'s thrush', 'Wilson\'s thrushes').
inf('wood thrush', 'wood thrushes').
inf('Phylloscopus', 'Phylloscopera').
inf('genus Phylloscopus', 'genus Phylloscopera').
inf('Phylloscopus sibilatrix', 'Phylloscopus sibilatrices').
inf('water thrush', 'water thrushes').
inf('bird of paradise', 'birds of paradise').
inf('Dolichonyx', 'Dolichonyxes').
inf('genus Dolichonyx', 'genus Dolichonyxes').
inf('Corvus corax', 'Corvus coraxes').
inf('Perisoreus canadensis', 'Perisoreus canadenses').
inf('Cistothorus platensis', 'Cistothorus platenses').
inf('Dumetella carolinensis', 'Dumetella carolinenses').
inf('mocking thrush', 'mocking thrushes').
inf('brown thrush', 'brown thrushes').
inf('nuthatch', 'nuthatches').
inf('European nuthatch', 'European nuthatches').
inf('red-breasted nuthatch', 'red-breasted nuthatches').
inf('Sitta canadensis', 'Sitta canadenses').
inf('white-breasted nuthatch', 'white-breasted nuthatches').
inf('Sitta carolinensis', 'Sitta carolinenses').
inf('titmouse', 'titmice').
inf('tufted titmouse', 'tufted titmice').
inf('Parus carolinensis', 'Parus carolinenses').
inf('Hirundo', 'Hirundoes').
inf('genus Hirundo', 'genus Hirundoes').
inf('bird of prey', 'birds of prey').
inf('Buteo jamaicensis', 'Buteo jamaicenses').
inf('Buteo lagopus', 'Buteo lagopera').
inf('Falco', 'Falcoes').
inf('genus Falco', 'genus Falcoes').
inf('hobby', 'hobbies').
inf('bird of Jove', 'birds of Jove').
inf('harpy', 'harpies').
inf('Aquila rapax', 'Aquila rapaxes').
inf('bird of Minerva', 'birds of Minerva').
inf('bird of night', 'birds of night').
inf('Bubo', 'Buboes').
inf('genus Bubo', 'genus Buboes').
inf('Strix', 'Strices').
inf('genus Strix', 'genus Strices').
inf('Strix aluco', 'Strix alucoes').
inf('Sceloglaux', 'Sceloglauxes').
inf('genus Sceloglaux', 'genus Sceloglauxes').
inf('laughing jackass', 'laughing jackasses').
inf('Tyto', 'Tytoes').
inf('genus Tyto', 'genus Tytoes').
inf('amphibian family', 'amphibian families').
inf('amphibian genus', 'amphibian genera').
inf('mud puppy', 'mud puppies').
inf('mud puppy', 'mud puppies').
inf('Cryptobranchus alleganiensis', 'Cryptobranchus alleganienses').
inf('mud puppy', 'mud puppies').
inf('genus Bufo', 'genus Bufoes').
inf('bufo', 'bufoes').
inf('Bufo bufo', 'Bufo bufoes').
inf('Scaphiopus', 'Scaphiopera').
inf('genus Scaphiopus', 'genus Scaphiopera').
inf('spadefoot', 'spadefeet').
inf('western spadefoot', 'western spadefeet').
inf('southern spadefoot', 'southern spadefeet').
inf('plains spadefoot', 'plains spadefeet').
inf('Gastrophryne carolinensis', 'Gastrophryne carolinenses').
inf('Xenopus', 'Xenopera').
inf('genus Xenopus', 'genus Xenopera').
inf('reptile family', 'reptile families').
inf('reptile genus', 'reptile genera').
inf('redbelly', 'redbellies').
inf('Testudo', 'Testudoes').
inf('genus Testudo', 'genus Testudoes').
inf('Trionyx', 'Trionyxes').
inf('genus Trionyx', 'genus Trionyxes').
inf('gecko', 'geckoes').
inf('Ptychozoon', 'Ptychozoa').
inf('genus Ptychozoon', 'genus Ptychozoa').
inf('flying gecko', 'flying geckoes').
inf('fringed gecko', 'fringed geckoes').
inf('Coleonyx', 'Coleonyxes').
inf('genus Coleonyx', 'genus Coleonyxes').
inf('banded gecko', 'banded geckoes').
inf('Pygopus', 'Pygopera').
inf('genus Pygopus', 'genus Pygopera').
inf('blue-belly', 'blue-bellies').
inf('Anolis carolinensis', 'Anolis carolinenses').
inf('Cnemidophorus velox', 'Cnemidophorus veloxes').
inf('Draco', 'Dracoes').
inf('genus Draco', 'genus Dracoes').
inf('genus Moloch', 'genus Moloches').
inf('moloch', 'moloches').
inf('Lanthanotus borneensis', 'Lanthanotus borneenses').
inf('Varanus komodoensis', 'Varanus komodoenses').
inf('Alligator mississipiensis', 'Alligator mississipienses').
inf('Alligator sinensis', 'Alligator sinenses').
inf('genus Caiman', 'genus Caimen').
inf('caiman', 'caimen').
inf('cayman', 'caymen').
inf('spectacled caiman', 'spectacled caimen').
inf('genus Coelophysis', 'genus Coelophyses').
inf('coelophysis', 'coelophyses').
inf('Tyrannosaurus rex', 'Tyrannosaurus rexes').
inf('Natrix', 'Natrices').
inf('genus Natrix', 'genus Natrices').
inf('Natrix natrix', 'Natrix natrices').
inf('Bitis', 'Bitis').
inf('genus Bitis', 'genus Bitis').
inf('Agkistrodon contortrix', 'Agkistrodon contortrices').
inf('Crotalus atrox', 'Crotalus atroxes').
inf('carcass', 'carcasses').
inf('arthropod family', 'arthropod families').
inf('arthropod genus', 'arthropod genera').
inf('harvestman', 'harvestmen').
inf('poultry', 'poultries').
inf('Cornish', 'Cornishes').
inf('Rock Cornish', 'Rock Cornishes').
inf('biddy', 'biddies').
inf('biddy', 'biddies').
inf('broody', 'broodies').
inf('Meleagris gallopavo', 'Meleagris gallopavoes').
inf('Lyrurus tetrix', 'Lyrurus tetrices').
inf('Lagopus', 'Lagopera').
inf('genus Lagopus', 'genus Lagopera').
inf('horse of the wood', 'horses of the wood').
inf('Canachites canadensis', 'Canachites canadenses').
inf('Tympanuchus cupido', 'Tympanuchus cupidoes').
inf('Tympanuchus cupido cupido', 'Tympanuchus cupido cupidoes').
inf('Crax', 'Craxes').
inf('genus Crax', 'genus Craxes').
inf('genus Afropavo', 'genus Afropavoes').
inf('afropavo', 'afropavoes').
inf('Afropavo congensis', 'Afropavo congenses').
inf('Coturnix', 'Coturnixes').
inf('genus Coturnix', 'genus Coturnixes').
inf('Coturnix coturnix', 'Coturnix coturnixes').
inf('Pavo', 'Pavoes').
inf('genus Pavo', 'genus Pavoes').
inf('bird of Juno', 'birds of Juno').
inf('Lofortyx', 'Lofortyxes').
inf('genus Lofortyx', 'genus Lofortyxes').
inf('Perdix', 'Perdixes').
inf('genus Perdix', 'genus Perdixes').
inf('Perdix perdix', 'Perdix perdixes').
inf('Oreortyx', 'Oreortyxes').
inf('genus Oreortyx', 'genus Oreortyxes').
inf('dodo', 'dodoes').
inf('lory', 'lories').
inf('Conuropsis', 'Conuropses').
inf('genus Conuropsis', 'genus Conuropses').
inf('Conuropsis carolinensis', 'Conuropsis carolinenses').
inf('Geococcyx', 'Geococcyxes').
inf('genus Geococcyx', 'genus Geococcyxes').
inf('Centropus', 'Centropera').
inf('genus Centropus', 'genus Centropera').
inf('Centropus sinensis', 'Centropus sinenses').
inf('touraco', 'touracoes').
inf('turaco', 'turacoes').
inf('Alcedo', 'Alcedoes').
inf('genus Alcedo', 'genus Alcedoes').
inf('Dacelo', 'Daceloes').
inf('genus Dacelo', 'genus Daceloes').
inf('laughing jackass', 'laughing jackasses').
inf('tody', 'todies').
inf('Caprimulgus carolinensis', 'Caprimulgus carolinenses').
inf('guacharo', 'guacharoes').
inf('Steatornis caripensis', 'Steatornis caripenses').
inf('Jynx', 'Jynges').
inf('genus Jynx', 'genus Jynges').
inf('Pharomacrus mocino', 'Pharomacrus mocinoes').
inf('Oxyura jamaicensis', 'Oxyura jamaicenses').
inf('Aix', 'Aixes').
inf('genus Aix', 'genus Aixes').
inf('oldwife', 'oldwives').
inf('goose', 'geese').
inf('Chinese goose', 'Chinese geese').
inf('greylag goose', 'greylag geese').
inf('graylag goose', 'graylag geese').
inf('blue goose', 'blue geese').
inf('snow goose', 'snow geese').
inf('brant goose', 'brant geese').
inf('brent goose', 'brent geese').
inf('common brant goose', 'common brant geese').
inf('Canada goose', 'Canada geese').
inf('Canadian goose', 'Canadian geese').
inf('Branta canadensis', 'Branta canadenses').
inf('barnacle goose', 'barnacle geese').
inf('Branta leucopsis', 'Branta leucopses').
inf('mammal family', 'mammal families').
inf('mammal genus', 'mammal genera').
inf('bilby', 'bilbies').
inf('Macropus', 'Macropera').
inf('genus Macropus', 'genus Macropera').
inf('wallaby', 'wallabies').
inf('common wallaby', 'common wallabies').
inf('hare wallaby', 'hare wallabies').
inf('nail-tailed wallaby', 'nail-tailed wallabies').
inf('rock wallaby', 'rock wallabies').
inf('tree wallaby', 'tree wallabies').
inf('flying mouse', 'flying mice').
inf('Tasmanian wolf', 'Tasmanian wolves').
inf('pouched mouse', 'pouched mice').
inf('marsupial mouse', 'marsupial mice').
inf('cow', 'kine').
inf('calf', 'calves').
inf('calf', 'calves').
inf('shrewmouse', 'shrewmice').
inf('Sorex', 'Sorexes').
inf('genus Sorex', 'genus Sorexes').
inf('Potamogale velox', 'Potamogale veloxes').
inf('sponge genus', 'sponge genera').
inf('coelenterate family', 'coelenterate families').
inf('coelenterate genus', 'coelenterate genera').
inf('jellyfish', 'jellyfish').
inf('man-of-war', 'men-of-war').
inf('jellyfish', 'jellyfish').
inf('ctenophore family', 'ctenophore families').
inf('ctenophore genus', 'ctenophore genera').
inf('comb jelly', 'comb jellies').
inf('sea gooseberry', 'sea gooseberries').
inf('worm family', 'worm families').
inf('worm genus', 'worm genera').
inf('Fasciolopsis', 'Fasciolopses').
inf('genus Fasciolopsis', 'genus Fasciolopses').
inf('Turbatrix', 'Turbatrices').
inf('genus Turbatrix', 'genus Turbatrices').
inf('Dracunculus medinensis', 'Dracunculus medinenses').
inf('sea mouse', 'sea mice').
inf('leech', 'leeches').
inf('Hirudo', 'Hirudoes').
inf('genus Hirudo', 'genus Hirudoes').
inf('medicinal leech', 'medicinal leeches').
inf('horseleech', 'horseleeches').
inf('mollusk family', 'mollusk families').
inf('mollusk genus', 'mollusk genera').
inf('shellfish', 'shellfish').
inf('conch', 'conches').
inf('giant conch', 'giant conches').
inf('Helix', 'Helixes').
inf('genus Helix', 'genus Helixes').
inf('Helix hortensis', 'Helix hortenses').
inf('Limax', 'Limaxes').
inf('genus Limax', 'genus Limaxes').
inf('bleeding tooth', 'bleeding teeth').
inf('nudibranch', 'nudibranches').
inf('cowry', 'cowries').
inf('coat-of-mail shell', 'coats-of-mail shell').
inf('lamellibranch', 'lamellibranches').
inf('Ensis', 'Enses').
inf('genus Ensis', 'genus Enses').
inf('genus Teredo', 'genus Teredoes').
inf('teredo', 'teredoes').
inf('dibranch', 'dibranches').
inf('genus Octopus', 'genus Octopera').
inf('octopus', 'octopodes').
inf('devilfish', 'devilfish').
inf('Argonauta argo', 'Argonauta argoes').
inf('genus Loligo', 'genus Loligoes').
inf('loligo', 'loligoes').
inf('cuttlefish', 'cuttlefish').
inf('Maja squinado', 'Maja squinadoes').
inf('Homarus capensis', 'Homarus capenses').
inf('crawfish', 'crawfish').
inf('crayfish', 'crayfish').
inf('sea crawfish', 'sea crawfish').
inf('crayfish', 'crayfish').
inf('crawfish', 'crawfish').
inf('crawdaddy', 'crawdaddies').
inf('Old World crayfish', 'Old World crayfish').
inf('American crayfish', 'American crayfish').
inf('Mysis', 'Myses').
inf('genus Mysis', 'genus Myses').
inf('woodlouse', 'woodlice').
inf('sea louse', 'sea lice').
inf('whale louse', 'whale lice').
inf('fish louse', 'fish lice').
inf('Plicatoperipatus jamaicensis', 'Plicatoperipatus jamaicenses').
inf('Peripatopsis', 'Peripatopses').
inf('genus Peripatopsis', 'genus Peripatopses').
inf('Ephippiorhynchus senegalensis', 'Ephippiorhynchus senegalenses').
inf('Balaeniceps rex', 'Balaeniceps rexes').
inf('flamingo', 'flamingoes').
inf('Nycticorax', 'Nycticoraxes').
inf('genus Nycticorax', 'genus Nycticoraxes').
inf('Nycticorax nycticorax', 'Nycticorax nycticoraxes').
inf('Crex', 'Crexes').
inf('genus Crex', 'genus Crexes').
inf('Crex crex', 'Crex crexes').
inf('Gallinula chloropus', 'Gallinula chloropera').
inf('Turnix', 'Turnixes').
inf('genus Turnix', 'genus Turnixes').
inf('killdeer', 'killdeer').
inf('kildeer', 'kildeer').
inf('Actitis', 'Actitis').
inf('genus Actitis', 'genus Actitis').
inf('Philomachus pugnax', 'Philomachus pugnaxes').
inf('Scolopax', 'Scolopaxes').
inf('genus Scolopax', 'genus Scolopaxes').
inf('Gallinago', 'Gallinagoes').
inf('genus Gallinago', 'genus Gallinagoes').
inf('Gallinago gallinago', 'Gallinago gallinagoes').
inf('Himantopus', 'Himantopera').
inf('genus Himantopus', 'genus Himantopera').
inf('Himantopus himantopus', 'Himantopus himantopera').
inf('Haematopus', 'Haematopera').
inf('genus Haematopus', 'genus Haematopera').
inf('Phalaropus', 'Phalaropera').
inf('genus Phalaropus', 'genus Phalaropera').
inf('Steganopus', 'Steganopera').
inf('genus Steganopus', 'genus Steganopera').
inf('Sterna hirundo', 'Sterna hirundoes').
inf('man-of-war bird', 'men-of-war bird').
inf('solan goose', 'solan geese').
inf('solant goose', 'solant geese').
inf('booby', 'boobies').
inf('Phalacrocorax', 'Phalacrocoraxes').
inf('genus Phalacrocorax', 'genus Phalacrocoraxes').
inf('Phalacrocorax carbo', 'Phalacrocorax carboes').
inf('albatross', 'albatrosses').
inf('wandering albatross', 'wandering albatrosses').
inf('black-footed albatross', 'black-footed albatrosses').
inf('goony', 'goonies').
inf('devilfish', 'devilfish').
inf('sea wolf', 'sea wolves').
inf('common blackfish', 'common blackfish').
inf('blackfish', 'blackfish').
inf('sea cow', 'sea kine').
inf('Steller\'s sea cow', 'Steller\'s sea kine').
inf('Orycteropus', 'Orycteropera').
inf('genus Orycteropus', 'genus Orycteropera').
inf('bitch', 'bitches').
inf('brood bitch', 'brood bitches').
inf('pooch', 'pooches').
inf('doggy', 'doggies').
inf('Maltese', 'Maltese').
inf('Pekinese', 'Pekinese').
inf('Pekingese', 'Pekingese').
inf('Ibizan Podenco', 'Ibizan Podencoes').
inf('Sydney silky', 'Sydney silkies').
inf('Lhasa apso', 'Lhasa apsoes').
inf('malinois', 'malinois').
inf('Doberman', 'Dobermen').
inf('husky', 'huskies').
inf('Siberian husky', 'Siberian huskies').
inf('Mexican hairless', 'Mexican hairlesses').
inf('wolf', 'wolves').
inf('timber wolf', 'timber wolves').
inf('grey wolf', 'grey wolves').
inf('gray wolf', 'gray wolves').
inf('white wolf', 'white wolves').
inf('Arctic wolf', 'Arctic wolves').
inf('red wolf', 'red wolves').
inf('maned wolf', 'maned wolves').
inf('prairie wolf', 'prairie wolves').
inf('brush wolf', 'brush wolves').
inf('dingo', 'dingoes').
inf('Canis dingo', 'Canis dingoes').
inf('crab-eating fox', 'crab-eating foxes').
inf('strand wolf', 'strand wolves').
inf('aardwolf', 'aardwolves').
inf('fox', 'foxes').
inf('red fox', 'red foxes').
inf('black fox', 'black foxes').
inf('silver fox', 'silver foxes').
inf('red fox', 'red foxes').
inf('kit fox', 'kit foxes').
inf('prairie fox', 'prairie foxes').
inf('Vulpes velox', 'Vulpes veloxes').
inf('kit fox', 'kit foxes').
inf('Alopex', 'Alopexes').
inf('genus Alopex', 'genus Alopexes').
inf('Arctic fox', 'Arctic foxes').
inf('white fox', 'white foxes').
inf('Alopex lagopus', 'Alopex lagopera').
inf('blue fox', 'blue foxes').
inf('grey fox', 'grey foxes').
inf('gray fox', 'gray foxes').
inf('kitty', 'kitties').
inf('puss', 'pusses').
inf('pussy', 'pussies').
inf('tabby', 'tabbies').
inf('kitty', 'kitties').
inf('tabby', 'tabbies').
inf('Siamese', 'Siamese').
inf('blue point Siamese', 'blue point Siamese').
inf('Maltese', 'Maltese').
inf('Manx', 'Manges').
inf('Felis bengalensis', 'Felis bengalenses').
inf('genus Lynx', 'genus Lynges').
inf('lynx', 'lynges').
inf('common lynx', 'common lynges').
inf('Lynx lynx', 'Lynx lynges').
inf('Canada lynx', 'Canada lynges').
inf('Lynx canadensis', 'Lynx canadenses').
inf('bay lynx', 'bay lynges').
inf('spotted lynx', 'spotted lynges').
inf('desert lynx', 'desert lynges').
inf('leopardess', 'leopardesses').
inf('king of beasts', 'kings of beasts').
inf('lioness', 'lionesses').
inf('tigress', 'tigresses').
inf('Acinonyx', 'Acinonyxes').
inf('genus Acinonyx', 'genus Acinonyxes').
inf('sabertooth', 'saberteeth').
inf('grizzly', 'grizzlies').
inf('Viverricula malaccensis', 'Viverricula malaccenses').
inf('Cryptoprocta ferox', 'Cryptoprocta feroxes').
inf('Pteropus', 'Pteropera').
inf('genus Pteropus', 'genus Pteropera').
inf('flying fox', 'flying foxes').
inf('harpy', 'harpies').
inf('Cynopterus sphinx', 'Cynopterus sphinges').
inf('Tadarida brasiliensis', 'Tadarida brasilienses').
inf('quarry', 'quarries').
inf('animal foot', 'animal feet').
inf('foot', 'feet').
inf('fossorial foot', 'fossorial feet').
inf('cloven foot', 'cloven feet').
inf('bird\'s foot', 'bird\'s feet').
inf('webfoot', 'webfeet').
inf('zygodactyl foot', 'zygodactyl feet').
inf('heterodactyl foot', 'heterodactyl feet').
inf('webbed foot', 'webbed feet').
inf('lobate foot', 'lobate feet').
inf('tooth', 'teeth').
inf('belly', 'bellies').
inf('brush', 'brushes').
inf('horse\'s foot', 'horse\'s feet').
inf('gallfly', 'gallflies').
inf('scorpion fly', 'scorpion flies').
inf('hanging fly', 'hanging flies').
inf('firefly', 'fireflies').
inf('firefly', 'fireflies').
inf('deathwatch', 'deathwatches').
inf('Spanish fly', 'Spanish flies').
inf('louse', 'lice').
inf('sucking louse', 'sucking lice').
inf('common louse', 'common lice').
inf('head louse', 'head lice').
inf('Pediculus capitis', 'Pediculus capitis').
inf('body louse', 'body lice').
inf('crab louse', 'crab lice').
inf('pubic louse', 'pubic lice').
inf('bird louse', 'bird lice').
inf('biting louse', 'biting lice').
inf('louse', 'lice').
inf('chicken louse', 'chicken lice').
inf('shaft louse', 'shaft lice').
inf('Pulex', 'Pulexes').
inf('genus Pulex', 'genus Pulexes').
inf('gallfly', 'gallflies').
inf('Hessian fly', 'Hessian flies').
inf('fly', 'flies').
inf('housefly', 'houseflies').
inf('house fly', 'house flies').
inf('tsetse fly', 'tsetse flies').
inf('tzetze fly', 'tzetze flies').
inf('blowfly', 'blowflies').
inf('blow fly', 'blow flies').
inf('greenbottle fly', 'greenbottle flies').
inf('flesh fly', 'flesh flies').
inf('tachina fly', 'tachina flies').
inf('gadfly', 'gadflies').
inf('botfly', 'botflies').
inf('horse botfly', 'horse botflies').
inf('human botfly', 'human botflies').
inf('sheep botfly', 'sheep botflies').
inf('sheep gadfly', 'sheep gadflies').
inf('warble fly', 'warble flies').
inf('horsefly', 'horseflies').
inf('horse fly', 'horse flies').
inf('bee fly', 'bee flies').
inf('robber fly', 'robber flies').
inf('fruit fly', 'fruit flies').
inf('pomace fly', 'pomace flies').
inf('Ceratitis', 'Ceratitis').
inf('genus Ceratitis', 'genus Ceratitis').
inf('Mediterranean fruit fly', 'Mediterranean fruit flies').
inf('medfly', 'medflies').
inf('vinegar fly', 'vinegar flies').
inf('louse fly', 'louse flies').
inf('horsefly', 'horseflies').
inf('horn fly', 'horn flies').
inf('mosquito', 'mosquitoes').
inf('yellow-fever mosquito', 'yellow-fever mosquitoes').
inf('Asian tiger mosquito', 'Asian tiger mosquitoes').
inf('malarial mosquito', 'malarial mosquitoes').
inf('malaria mosquito', 'malaria mosquitoes').
inf('Culex', 'Culexes').
inf('genus Culex', 'genus Culexes').
inf('common mosquito', 'common mosquitoes').
inf('punky', 'punkies').
inf('sand fly', 'sand flies').
inf('sandfly', 'sandflies').
inf('crane fly', 'crane flies').
inf('blackfly', 'blackflies').
inf('black fly', 'black flies').
inf('Vespa crabro', 'Vespa crabroes').
inf('Sphecius speciosis', 'Sphecius specioses').
inf('gallfly', 'gallflies').
inf('chalcid fly', 'chalcid flies').
inf('chalcidfly', 'chalcidflies').
inf('Chalcis', 'Chalces').
inf('genus Chalcis', 'genus Chalces').
inf('chalcis fly', 'chalcis flies').
inf('ichneumon fly', 'ichneumon flies').
inf('sawfly', 'sawflies').
inf('Solenopsis', 'Solenopses').
inf('genus Solenopsis', 'genus Solenopses').
inf('Mastotermes darwiniensis', 'Mastotermes darwinienses').
inf('Anabrus simplex', 'Anabrus simplexes').
inf('walking leaf', 'walking leaves').
inf('cockroach', 'cockroaches').
inf('roach', 'roaches').
inf('oriental cockroach', 'oriental cockroaches').
inf('oriental roach', 'oriental roaches').
inf('Asiatic cockroach', 'Asiatic cockroaches').
inf('American cockroach', 'American cockroaches').
inf('Australian cockroach', 'Australian cockroaches').
inf('German cockroach', 'German cockroaches').
inf('giant cockroach', 'giant cockroaches').
inf('Mantis religioso', 'Mantis religiosoes').
inf('Cimex', 'Cimexes').
inf('genus Cimex', 'genus Cimexes').
inf('chinch', 'chinches').
inf('water boatman', 'water boatmen').
inf('whitefly', 'whiteflies').
inf('citrus whitefly', 'citrus whiteflies').
inf('greenhouse whitefly', 'greenhouse whiteflies').
inf('sweet-potato whitefly', 'sweet-potato whiteflies').
inf('plant louse', 'plant lice').
inf('louse', 'lice').
inf('blackfly', 'blackflies').
inf('greenfly', 'greenflies').
inf('ant cow', 'ant kine').
inf('woolly plant louse', 'woolly plant lice').
inf('grape louse', 'grape lice').
inf('jumping plant louse', 'jumping plant lice').
inf('harvest fly', 'harvest flies').
inf('Aphrophora saratogensis', 'Aphrophora saratogenses').
inf('lantern fly', 'lantern flies').
inf('lantern-fly', 'lantern-flies').
inf('bark-louse', 'bark-lice').
inf('bark louse', 'bark lice').
inf('booklouse', 'booklice').
inf('book louse', 'book lice').
inf('deathwatch', 'deathwatches').
inf('common booklouse', 'common booklice').
inf('mayfly', 'mayflies').
inf('dayfly', 'dayflies').
inf('shadfly', 'shadflies').
inf('stonefly', 'stoneflies').
inf('stone fly', 'stone flies').
inf('antlion fly', 'antlion flies').
inf('lacewing fly', 'lacewing flies').
inf('stink fly', 'stink flies').
inf('golden-eyed fly', 'golden-eyed flies').
inf('hemerobiid fly', 'hemerobiid flies').
inf('dobsonfly', 'dobsonflies').
inf('dobson fly', 'dobson flies').
inf('fish fly', 'fish flies').
inf('fish-fly', 'fish-flies').
inf('alderfly', 'alderflies').
inf('alder fly', 'alder flies').
inf('snakefly', 'snakeflies').
inf('spongefly', 'spongeflies').
inf('spongillafly', 'spongillaflies').
inf('dragonfly', 'dragonflies').
inf('damselfly', 'damselflies').
inf('caddis fly', 'caddis flies').
inf('caddis-fly', 'caddis-flies').
inf('caddice fly', 'caddice flies').
inf('caddice-fly', 'caddice-flies').
inf('silverfish', 'silverfish').
inf('onion louse', 'onion lice').
inf('butterfly', 'butterflies').
inf('nymphalid butterfly', 'nymphalid butterflies').
inf('brush-footed butterfly', 'brush-footed butterflies').
inf('four-footed butterfly', 'four-footed butterflies').
inf('mourning cloak butterfly', 'mourning cloak butterflies').
inf('Camberwell beauty', 'Camberwell beauties').
inf('tortoiseshell butterfly', 'tortoiseshell butterflies').
inf('painted beauty', 'painted beauties').
inf('Vanessa virginiensis', 'Vanessa virginienses').
inf('Limenitis', 'Limenitis').
inf('genus Limenitis', 'genus Limenitis').
inf('Limenitis astyanax', 'Limenitis astyanaxes').
inf('ringlet butterfly', 'ringlet butterflies').
inf('comma butterfly', 'comma butterflies').
inf('fritillary', 'fritillaries').
inf('emperor butterfly', 'emperor butterflies').
inf('peacock butterfly', 'peacock butterflies').
inf('danaid butterfly', 'danaid butterflies').
inf('monarch', 'monarches').
inf('monarch butterfly', 'monarch butterflies').
inf('milkweed butterfly', 'milkweed butterflies').
inf('pierid butterfly', 'pierid butterflies').
inf('cabbage butterfly', 'cabbage butterflies').
inf('southern cabbage butterfly', 'southern cabbage butterflies').
inf('sulphur butterfly', 'sulphur butterflies').
inf('sulfur butterfly', 'sulfur butterflies').
inf('lycaenid butterfly', 'lycaenid butterflies').
inf('hairstreak butterfly', 'hairstreak butterflies').
inf('genus Tortrix', 'genus Tortrices').
inf('tea tortrix', 'tea tortrices').
inf('tortrix', 'tortrices').
inf('orange tortrix', 'orange tortrices').
inf('tortrix', 'tortrices').
inf('Cerapteryx', 'Cerapteryxes').
inf('genus Cerapteryx', 'genus Cerapteryxes').
inf('Bombyx', 'Bombyxes').
inf('genus Bombyx', 'genus Bombyxes').
inf('Atticus atlas', 'Atticus atlantes').
inf('imago', 'imagoes').
inf('sea moss', 'sea mosses').
inf('echinoderm family', 'echinoderm families').
inf('echinoderm genus', 'echinoderm genera').
inf('starfish', 'starfish').
inf('basket fish', 'basket fish').
inf('sea lily', 'sea lilies').
inf('foot', 'feet').
inf('invertebrate foot', 'invertebrate feet').
inf('tube foot', 'tube feet').
inf('cony', 'conies').
inf('bunny', 'bunnies').
inf('cony', 'conies').
inf('mouse', 'mice').
inf('house mouse', 'house mice').
inf('Micromyx', 'Micromyxes').
inf('genus Micromyx', 'genus Micromyxes').
inf('harvest mouse', 'harvest mice').
inf('field mouse', 'field mice').
inf('fieldmouse', 'fieldmice').
inf('nude mouse', 'nude mice').
inf('European wood mouse', 'European wood mice').
inf('kangaroo mouse', 'kangaroo mice').
inf('New World mouse', 'New World mice').
inf('American harvest mouse', 'American harvest mice').
inf('harvest mouse', 'harvest mice').
inf('wood mouse', 'wood mice').
inf('white-footed mouse', 'white-footed mice').
inf('vesper mouse', 'vesper mice').
inf('Peromyscus leucopus', 'Peromyscus leucopera').
inf('deer mouse', 'deer mice').
inf('cactus mouse', 'cactus mice').
inf('cotton mouse', 'cotton mice').
inf('pygmy mouse', 'pygmy mice').
inf('grasshopper mouse', 'grasshopper mice').
inf('musquash', 'musquashes').
inf('field mouse', 'field mice').
inf('pine mouse', 'pine mice').
inf('meadow mouse', 'meadow mice').
inf('red-backed mouse', 'red-backed mice').
inf('Myopus', 'Myopera').
inf('genus Myopus', 'genus Myopera').
inf('Dicrostonyx', 'Dicrostonyxes').
inf('genus Dicrostonyx', 'genus Dicrostonyxes').
inf('pocket mouse', 'pocket mice').
inf('silky pocket mouse', 'silky pocket mice').
inf('plains pocket mouse', 'plains pocket mice').
inf('hispid pocket mouse', 'hispid pocket mice').
inf('Mexican pocket mouse', 'Mexican pocket mice').
inf('kangaroo mouse', 'kangaroo mice').
inf('jumping mouse', 'jumping mice').
inf('meadow jumping mouse', 'meadow jumping mice').
inf('dormouse', 'dormice').
inf('hazel mouse', 'hazel mice').
inf('Sciurus carolinensis', 'Sciurus carolinenses').
inf('Marmota monax', 'Marmota monaxes').
inf('Castor canadensis', 'Castor canadenses').
inf('cavy', 'cavies').
inf('wild cavy', 'wild cavies').
inf('Spalax', 'Spalaxes').
inf('genus Spalax', 'genus Spalaxes').
inf('hyrax', 'hyraxes').
inf('cony', 'conies').
inf('rock hyrax', 'rock hyraxes').
inf('Procavia capensis', 'Procavia capenses').
inf('filly', 'fillies').
inf('cow pony', 'cow ponies').
inf('pony', 'ponies').
inf('polo pony', 'polo ponies').
inf('bronco', 'broncoes').
inf('broncho', 'bronchoes').
inf('bucking bronco', 'bucking broncoes').
inf('Indian pony', 'Indian ponies').
inf('pony', 'ponies').
inf('Shetland pony', 'Shetland ponies').
inf('Welsh pony', 'Welsh ponies').
inf('Gallant Fox', 'Gallant Foxes').
inf('pony', 'ponies').
inf('palomino', 'palominoes').
inf('pinto', 'pintoes').
inf('ass', 'asses').
inf('domestic ass', 'domestic asses').
inf('burro', 'burroes').
inf('jackass', 'jackasses').
inf('jenny', 'jennies').
inf('jenny ass', 'jenny asses').
inf('hinny', 'hinnies').
inf('wild ass', 'wild asses').
inf('African wild ass', 'African wild asses').
inf('rhinoceros family', 'rhinoceros families').
inf('rhino', 'rhinoes').
inf('piggy', 'piggies').
inf('peccary', 'peccaries').
inf('collared peccary', 'collared peccaries').
inf('white-lipped peccary', 'white-lipped peccaries').
inf('Chiacoan peccary', 'Chiacoan peccaries').
inf('hippo', 'hippoes').
inf('first stomach', 'first stomaches').
inf('second stomach', 'second stomaches').
inf('third stomach', 'third stomaches').
inf('fourth stomach', 'fourth stomaches').
inf('ox', 'oxen').
inf('wild ox', 'wild oxen').
inf('ox', 'oxen').
inf('cow', 'kine').
inf('moo-cow', 'moo-kine').
inf('springing cow', 'springing kine').
inf('dogy', 'dogies').
inf('leppy', 'leppies').
inf('beef', 'beeves').
inf('Brahman', 'Brahmen').
inf('Welsh', 'Welshes').
inf('dairy cow', 'dairy kine').
inf('milch cow', 'milch kine').
inf('milk cow', 'milk kine').
inf('Brown Swiss', 'Brown Swisses').
inf('cattalo', 'cattaloes').
inf('beefalo', 'beefaloes').
inf('Old World buffalo', 'Old World buffaloes').
inf('buffalo', 'buffaloes').
inf('water buffalo', 'water buffaloes').
inf('water ox', 'water oxen').
inf('Asiatic buffalo', 'Asiatic buffaloes').
inf('Indian buffalo', 'Indian buffaloes').
inf('dwarf buffalo', 'dwarf buffaloes').
inf('Bubalus mindorensis', 'Bubalus mindorenses').
inf('Anoa mindorensis', 'Anoa mindorenses').
inf('Cape buffalo', 'Cape buffaloes').
inf('Asian wild ox', 'Asian wild oxen').
inf('American buffalo', 'American buffaloes').
inf('buffalo', 'buffaloes').
inf('musk ox', 'musk oxen').
inf('musk sheep', 'musk sheep').
inf('sheep', 'sheep').
inf('black sheep', 'black sheep').
inf('domestic sheep', 'domestic sheep').
inf('merino', 'merinoes').
inf('merino sheep', 'merino sheep').
inf('wild sheep', 'wild sheep').
inf('Marco Polo sheep', 'Marco Polo sheep').
inf('Marco Polo\'s sheep', 'Marco Polo\'s sheep').
inf('Dall sheep', 'Dall sheep').
inf('Dall\'s sheep', 'Dall\'s sheep').
inf('white sheep', 'white sheep').
inf('mountain sheep', 'mountain sheep').
inf('bighorn sheep', 'bighorn sheep').
inf('Rocky Mountain sheep', 'Rocky Mountain sheep').
inf('Ovis canadensis', 'Ovis canadenses').
inf('Barbary sheep', 'Barbary sheep').
inf('maned sheep', 'maned sheep').
inf('billy', 'billies').
inf('nanny', 'nannies').
inf('ibex', 'ibexes').
inf('Capra ibex', 'Capra ibexes').
inf('chamois', 'chamois').
inf('genus Addax', 'genus Addaxes').
inf('addax', 'addaxes').
inf('sassaby', 'sassabies').
inf('bongo', 'bongoes').
inf('Taurotragus oryx', 'Taurotragus oryxes').
inf('genus Oryx', 'genus Oryxes').
inf('oryx', 'oryxes').
inf('Pseudoryx', 'Pseudoryxes').
inf('genus Pseudoryx', 'genus Pseudoryxes').
inf('Pseudoryx nghetinhensis', 'Pseudoryx nghetinhenses').
inf('deer', 'deer').
inf('red deer', 'red deer').
inf('Cervus elaphus canadensis', 'Cervus elaphus canadenses').
inf('Japanese deer', 'Japanese deer').
inf('Virginia deer', 'Virginia deer').
inf('white-tailed deer', 'white-tailed deer').
inf('whitetail deer', 'whitetail deer').
inf('mule deer', 'mule deer').
inf('burro deer', 'burro deer').
inf('black-tailed deer', 'black-tailed deer').
inf('blacktail deer', 'blacktail deer').
inf('fallow deer', 'fallow deer').
inf('roe deer', 'roe deer').
inf('reindeer', 'reindeer').
inf('barking deer', 'barking deer').
inf('musk deer', 'musk deer').
inf('pere david\'s deer', 'pere david\'s deer').
inf('mouse deer', 'mouse deer').
inf('water deer', 'water deer').
inf('dromedary', 'dromedaries').
inf('guanaco', 'guanacoes').
inf('forefoot', 'forefeet').
inf('hindfoot', 'hindfeet').
inf('fitch', 'fitches').
inf('Ictonyx', 'Ictonyxes').
inf('genus Ictonyx', 'genus Ictonyxes').
inf('Lutra canadensis', 'Lutra canadenses').
inf('wood pussy', 'wood pussies').
inf('Mephitis', 'Mephitis').
inf('genus Mephitis', 'genus Mephitis').
inf('Mephitis mephitis', 'Mephitis mephitis').
inf('Mellivora capensis', 'Mellivora capenses').
inf('Arctonyx', 'Arctonyxes').
inf('genus Arctonyx', 'genus Arctonyxes').
inf('Gulo', 'Guloes').
inf('genus Gulo', 'genus Guloes').
inf('Gulo gulo', 'Gulo guloes').
inf('Easter bunny', 'Easter bunnies').
inf('church mouse', 'church mice').
inf('proboscis', 'probosces').
inf('armadillo', 'armadilloes').
inf('nine-banded armadillo', 'nine-banded armadilloes').
inf('Texas armadillo', 'Texas armadilloes').
inf('three-banded armadillo', 'three-banded armadilloes').
inf('peludo', 'peludoes').
inf('giant armadillo', 'giant armadilloes').
inf('pichiciago', 'pichiciagoes').
inf('pichiciego', 'pichiciegoes').
inf('fairy armadillo', 'fairy armadilloes').
inf('greater pichiciego', 'greater pichiciegoes').
inf('haunch', 'haunches').
inf('serictery', 'sericteries').
inf('primary', 'primaries').
inf('genus Homo', 'genus Homoes').
inf('homo', 'homoes').
inf('man', 'men').
inf('human', 'humen').
inf('humanity', 'humanities').
inf('man', 'men').
inf('Pithecanthropus', 'Pithecanthropera').
inf('genus Pithecanthropus', 'genus Pithecanthropera').
inf('Java man', 'Java men').
inf('Trinil man', 'Trinil men').
inf('Peking man', 'Peking men').
inf('Sinanthropus', 'Sinanthropera').
inf('genus Sinanthropus', 'genus Sinanthropera').
inf('Homo soloensis', 'Homo soloenses').
inf('Javanthropus', 'Javanthropera').
inf('genus Javanthropus', 'genus Javanthropera').
inf('Solo man', 'Solo men').
inf('Neandertal man', 'Neandertal men').
inf('Neanderthal man', 'Neanderthal men').
inf('Homo sapiens neanderthalensis', 'Homo sapiens neanderthalenses').
inf('Boskop man', 'Boskop men').
inf('modern man', 'modern men').
inf('Plesianthropus', 'Plesianthropera').
inf('genus Plesianthropus', 'genus Plesianthropera').
inf('Australopithecus afarensis', 'Australopithecus afarenses').
inf('Zinjanthropus', 'Zinjanthropera').
inf('genus Zinjanthropus', 'genus Zinjanthropera').
inf('Paranthropus', 'Paranthropera').
inf('genus Paranthropus', 'genus Paranthropera').
inf('Pongo', 'Pongoes').
inf('genus Pongo', 'genus Pongoes').
inf('bonobo', 'bonoboes').
inf('Mandrillus sphinx', 'Mandrillus sphinges').
inf('hanuman', 'hanumen').
inf('Callithrix', 'Callithrixes').
inf('genus Callithrix', 'genus Callithrixes').
inf('Lagothrix', 'Lagothrixes').
inf('genus Lagothrix', 'genus Lagothrixes').
inf('Daubentonia madagascariensis', 'Daubentonia madagascarienses').
inf('potto', 'pottoes').
inf('Perodicticus potto', 'Perodicticus pottoes').
inf('angwantibo', 'angwantiboes').
inf('golden potto', 'golden pottoes').
inf('Arctocebus calabarensis', 'Arctocebus calabarenses').
inf('genus Galago', 'genus Galagoes').
inf('galago', 'galagoes').
inf('bushbaby', 'bushbabies').
inf('bush baby', 'bush babies').
inf('colugo', 'colugoes').
inf('raccoon fox', 'raccoon foxes').
inf('potto', 'pottoes').
inf('gill arch', 'gill arches').
inf('branchial arch', 'branchial arches').
inf('syrinx', 'syringes').
inf('fish', 'fish').
inf('game fish', 'game fish').
inf('sport fish', 'sport fish').
inf('food fish', 'food fish').
inf('rough fish', 'rough fish').
inf('groundfish', 'groundfish').
inf('bottom fish', 'bottom fish').
inf('young fish', 'young fish').
inf('bony fish', 'bony fish').
inf('lobe-finned fish', 'lobe-finned fish').
inf('lungfish', 'lungfish').
inf('Australian lungfish', 'Australian lungfish').
inf('Queensland lungfish', 'Queensland lungfish').
inf('catfish', 'catfish').
inf('siluriform fish', 'siluriform fish').
inf('silurid fish', 'silurid fish').
inf('European catfish', 'European catfish').
inf('sheatfish', 'sheatfish').
inf('electric catfish', 'electric catfish').
inf('bullhead catfish', 'bullhead catfish').
inf('channel catfish', 'channel catfish').
inf('blue catfish', 'blue catfish').
inf('blue channel catfish', 'blue channel catfish').
inf('flathead catfish', 'flathead catfish').
inf('shovelnose catfish', 'shovelnose catfish').
inf('spoonbill catfish', 'spoonbill catfish').
inf('armored catfish', 'armored catfish').
inf('sea catfish', 'sea catfish').
inf('crucifix fish', 'crucifix fish').
inf('gadoid fish', 'gadoid fish').
inf('codfish', 'codfish').
inf('Urophycis', 'Urophyces').
inf('genus Urophycis', 'genus Urophyces').
inf('rattail fish', 'rattail fish').
inf('teleost fish', 'teleost fish').
inf('sandfish', 'sandfish').
inf('clupeid fish', 'clupeid fish').
inf('alewife', 'alewives').
inf('anchovy', 'anchovies').
inf('mediterranean anchovy', 'mediterranean anchovies').
inf('blackfish', 'blackfish').
inf('redfish', 'redfish').
inf('Salmo', 'Salmoes').
inf('genus Salmo', 'genus Salmoes').
inf('coho', 'cohoes').
inf('Oncorhynchus kisutch', 'Oncorhynchus kisutches').
inf('Salvelinus namaycush', 'Salvelinus namaycushes').
inf('whitefish', 'whitefish').
inf('lake whitefish', 'lake whitefish').
inf('cisco', 'ciscoes').
inf('round whitefish', 'round whitefish').
inf('Menominee whitefish', 'Menominee whitefish').
inf('Rocky Mountain whitefish', 'Rocky Mountain whitefish').
inf('Osmerus mordax', 'Osmerus mordaxes').
inf('ladyfish', 'ladyfish').
inf('bonefish', 'bonefish').
inf('lanternfish', 'lanternfish').
inf('lizardfish', 'lizardfish').
inf('snakefish', 'snakefish').
inf('snake-fish', 'snake-fish').
inf('lancetfish', 'lancetfish').
inf('lancet fish', 'lancet fish').
inf('wolffish', 'wolffish').
inf('handsaw fish', 'handsaw fish').
inf('moonfish', 'moonfish').
inf('ribbonfish', 'ribbonfish').
inf('dealfish', 'dealfish').
inf('oarfish', 'oarfish').
inf('king of the herring', 'kings of the herring').
inf('ribbonfish', 'ribbonfish').
inf('batfish', 'batfish').
inf('goosefish', 'goosefish').
inf('anglerfish', 'anglerfish').
inf('angler fish', 'angler fish').
inf('monkfish', 'monkfish').
inf('toadfish', 'toadfish').
inf('oyster fish', 'oyster fish').
inf('oyster-fish', 'oyster-fish').
inf('oysterfish', 'oysterfish').
inf('frogfish', 'frogfish').
inf('sargassum fish', 'sargassum fish').
inf('needlefish', 'needlefish').
inf('billfish', 'billfish').
inf('flying fish', 'flying fish').
inf('monoplane flying fish', 'monoplane flying fish').
inf('two-wing flying fish', 'two-wing flying fish').
inf('biplane flying fish', 'biplane flying fish').
inf('four-wing flying fish', 'four-wing flying fish').
inf('Scomberesox', 'Scomberesoxes').
inf('genus Scomberesox', 'genus Scomberesoxes').
inf('Scombresox', 'Scombresoxes').
inf('genus Scombresox', 'genus Scombresoxes').
inf('saury', 'sauries').
inf('billfish', 'billfish').
inf('spiny-finned fish', 'spiny-finned fish').
inf('percoid fish', 'percoid fish').
inf('perch', 'perches').
inf('climbing perch', 'climbing perches').
inf('perch', 'perches').
inf('yellow perch', 'yellow perches').
inf('European perch', 'European perches').
inf('pike-perch', 'pike-perches').
inf('pike perch', 'pike perches').
inf('dory', 'dories').
inf('blue pikeperch', 'blue pikeperches').
inf('sandfish', 'sandfish').
inf('pearlfish', 'pearlfish').
inf('pearl-fish', 'pearl-fish').
inf('robalo', 'robaloes').
inf('giant perch', 'giant perches').
inf('giant seaperch', 'giant seaperches').
inf('Asian seabass', 'Asian seabasses').
inf('white seabass', 'white seabasses').
inf('Esox', 'Esoxes').
inf('genus Esox', 'genus Esoxes').
inf('Esox masquinongy', 'Esox masquinongies').
inf('sunfish', 'sunfish').
inf('Pomoxis', 'Pomoxes').
inf('genus Pomoxis', 'genus Pomoxes').
inf('spotted sunfish', 'spotted sunfish').
inf('freshwater bass', 'freshwater basses').
inf('rock bass', 'rock basses').
inf('rock sunfish', 'rock sunfish').
inf('black bass', 'black basses').
inf('Kentucky black bass', 'Kentucky black basses').
inf('spotted black bass', 'spotted black basses').
inf('smallmouth bass', 'smallmouth basses').
inf('smallmouthed bass', 'smallmouthed basses').
inf('smallmouth black bass', 'smallmouth black basses').
inf('smallmouthed black bass', 'smallmouthed black basses').
inf('Micropterus dolomieu', 'Micropterus dolomieu').
inf('largemouth bass', 'largemouth basses').
inf('largemouthed bass', 'largemouthed basses').
inf('largemouth black bass', 'largemouth black basses').
inf('largemouthed black bass', 'largemouthed black basses').
inf('bass', 'basses').
inf('serranid fish', 'serranid fish').
inf('white perch', 'white perches').
inf('silver perch', 'silver perches').
inf('yellow bass', 'yellow basses').
inf('sea bass', 'sea basses').
inf('blackmouth bass', 'blackmouth basses').
inf('rock sea bass', 'rock sea basses').
inf('rock bass', 'rock basses').
inf('black sea bass', 'black sea basses').
inf('black bass', 'black basses').
inf('striped bass', 'striped basses').
inf('rockfish', 'rockfish').
inf('stone bass', 'stone basses').
inf('wreckfish', 'wreckfish').
inf('belted sandfish', 'belted sandfish').
inf('creole-fish', 'creole-fish').
inf('jewfish', 'jewfish').
inf('soapfish', 'soapfish').
inf('surfperch', 'surfperches').
inf('surffish', 'surffish').
inf('surf fish', 'surf fish').
inf('rainbow seaperch', 'rainbow seaperches').
inf('rainbow perch', 'rainbow perches').
inf('cardinalfish', 'cardinalfish').
inf('flame fish', 'flame fish').
inf('flamefish', 'flamefish').
inf('conchfish', 'conchfish').
inf('tilefish', 'tilefish').
inf('bluefish', 'bluefish').
inf('Pomatomus saltatrix', 'Pomatomus saltatrices').
inf('sergeant fish', 'sergeant fish').
inf('suckerfish', 'suckerfish').
inf('sucking fish', 'sucking fish').
inf('carangid fish', 'carangid fish').
inf('Caranx', 'Caranges').
inf('genus Caranx', 'genus Caranges').
inf('threadfish', 'threadfish').
inf('thread-fish', 'thread-fish').
inf('moonfish', 'moonfish').
inf('Atlantic moonfish', 'Atlantic moonfish').
inf('horsefish', 'horsefish').
inf('dollarfish', 'dollarfish').
inf('lookdown fish', 'lookdown fish').
inf('amberfish', 'amberfish').
inf('rudderfish', 'rudderfish').
inf('banded rudderfish', 'banded rudderfish').
inf('kingfish', 'kingfish').
inf('pompano', 'pompanoes').
inf('Florida pompano', 'Florida pompanoes').
inf('pilotfish', 'pilotfish').
inf('cigarfish', 'cigarfish').
inf('dolphinfish', 'dolphinfish').
inf('blanquillo', 'blanquilloes').
inf('tilefish', 'tilefish').
inf('characin fish', 'characin fish').
inf('cichlid fish', 'cichlid fish').
inf('muttonfish', 'muttonfish').
inf('porkfish', 'porkfish').
inf('pork-fish', 'pork-fish').
inf('Anisotremus surinamensis', 'Anisotremus surinamenses').
inf('pigfish', 'pigfish').
inf('hogfish', 'hogfish').
inf('sparid fish', 'sparid fish').
inf('porgy', 'porgies').
inf('red porgy', 'red porgies').
inf('pinfish', 'pinfish').
inf('squirrelfish', 'squirrelfish').
inf('sheepshead porgy', 'sheepshead porgies').
inf('northern porgy', 'northern porgies').
inf('southern porgy', 'southern porgies').
inf('sciaenid fish', 'sciaenid fish').
inf('drumfish', 'drumfish').
inf('jackknife-fish', 'jackknife-fish').
inf('silver perch', 'silver perches').
inf('channel bass', 'channel basses').
inf('redfish', 'redfish').
inf('jewfish', 'jewfish').
inf('surffish', 'surffish').
inf('surf fish', 'surf fish').
inf('kingfish', 'kingfish').
inf('chenfish', 'chenfish').
inf('kingfish', 'kingfish').
inf('queenfish', 'queenfish').
inf('weakfish', 'weakfish').
inf('spotted weakfish', 'spotted weakfish').
inf('goatfish', 'goatfish').
inf('red goatfish', 'red goatfish').
inf('yellow goatfish', 'yellow goatfish').
inf('Atherinopsis', 'Atherinopses').
inf('genus Atherinopsis', 'genus Atherinopses').
inf('Atherinopsis californiensis', 'Atherinopsis californienses').
inf('rudderfish', 'rudderfish').
inf('Kyphosus sectatrix', 'Kyphosus sectatrices').
inf('spadefish', 'spadefish').
inf('angelfish', 'angelfish').
inf('butterfly fish', 'butterfly fish').
inf('angelfish', 'angelfish').
inf('rock beauty', 'rock beauties').
inf('damselfish', 'damselfish').
inf('beaugregory', 'beaugregories').
inf('anemone fish', 'anemone fish').
inf('clown anemone fish', 'clown anemone fish').
inf('pigfish', 'pigfish').
inf('giant pigfish', 'giant pigfish').
inf('hogfish', 'hogfish').
inf('puddingwife', 'puddingwives').
inf('pudding-wife', 'pudding-wives').
inf('razor fish', 'razor fish').
inf('razor-fish', 'razor-fish').
inf('pearly razorfish', 'pearly razorfish').
inf('blackfish', 'blackfish').
inf('Tautoga onitis', 'Tautoga onitis').
inf('parrotfish', 'parrotfish').
inf('polly fish', 'polly fish').
inf('pollyfish', 'pollyfish').
inf('jawfish', 'jawfish').
inf('blennioid fish', 'blennioid fish').
inf('blenny', 'blennies').
inf('combtooth blenny', 'combtooth blennies').
inf('shanny', 'shannies').
inf('clinid fish', 'clinid fish').
inf('Chaenopsis', 'Chaenopses').
inf('genus Chaenopsis', 'genus Chaenopses').
inf('pikeblenny', 'pikeblennies').
inf('bluethroat pikeblenny', 'bluethroat pikeblennies').
inf('bracketed blenny', 'bracketed blennies').
inf('butterfish', 'butterfish').
inf('snakeblenny', 'snakeblennies').
inf('eelblenny', 'eelblennies').
inf('ghostfish', 'ghostfish').
inf('wolffish', 'wolffish').
inf('wolf fish', 'wolf fish').
inf('catfish', 'catfish').
inf('goby', 'gobies').
inf('sleeper goby', 'sleeper gobies').
inf('archerfish', 'archerfish').
inf('Toxotes jaculatrix', 'Toxotes jaculatrices').
inf('worm fish', 'worm fish').
inf('surgeonfish', 'surgeonfish').
inf('doctorfish', 'doctorfish').
inf('doctor-fish', 'doctor-fish').
inf('oilfish', 'oilfish').
inf('cutlassfish', 'cutlassfish').
inf('frost fish', 'frost fish').
inf('scombroid fish', 'scombroid fish').
inf('cero', 'ceroes').
inf('cero', 'ceroes').
inf('pintado', 'pintadoes').
inf('kingfish', 'kingfish').
inf('tunny', 'tunnies').
inf('long-fin tunny', 'long-fin tunnies').
inf('bonito', 'bonitoes').
inf('Atlantic bonito', 'Atlantic bonitoes').
inf('Chile bonito', 'Chile bonitoes').
inf('Chilean bonito', 'Chilean bonitoes').
inf('Pacific bonito', 'Pacific bonitoes').
inf('Sarda chiliensis', 'Sarda chilienses').
inf('bonito', 'bonitoes').
inf('oceanic bonito', 'oceanic bonitoes').
inf('swordfish', 'swordfish').
inf('sailfish', 'sailfish').
inf('Atlantic sailfish', 'Atlantic sailfish').
inf('billfish', 'billfish').
inf('spearfish', 'spearfish').
inf('butterfish', 'butterfish').
inf('stromateid fish', 'stromateid fish').
inf('dollarfish', 'dollarfish').
inf('California pompano', 'California pompanoes').
inf('harvestfish', 'harvestfish').
inf('driftfish', 'driftfish').
inf('driftfish', 'driftfish').
inf('barrelfish', 'barrelfish').
inf('black rudderfish', 'black rudderfish').
inf('Gobiesox', 'Gobiesoxes').
inf('genus Gobiesox', 'genus Gobiesoxes').
inf('clingfish', 'clingfish').
inf('skillet fish', 'skillet fish').
inf('skilletfish', 'skilletfish').
inf('Lobotes surinamensis', 'Lobotes surinamenses').
inf('silver jenny', 'silver jennies').
inf('Sillago', 'Sillagoes').
inf('genus Sillago', 'genus Sillagoes').
inf('ganoid fish', 'ganoid fish').
inf('dogfish', 'dogfish').
inf('paddlefish', 'paddlefish').
inf('Chinese paddlefish', 'Chinese paddlefish').
inf('Acipenser huso', 'Acipenser husoes').
inf('garfish', 'garfish').
inf('billfish', 'billfish').
inf('scorpaenoid fish', 'scorpaenoid fish').
inf('scorpaenid fish', 'scorpaenid fish').
inf('scorpionfish', 'scorpionfish').
inf('scorpion fish', 'scorpion fish').
inf('plumed scorpionfish', 'plumed scorpionfish').
inf('Pterois', 'Pterois').
inf('genus Pterois', 'genus Pterois').
inf('lionfish', 'lionfish').
inf('stonefish', 'stonefish').
inf('rockfish', 'rockfish').
inf('copper rockfish', 'copper rockfish').
inf('vermillion rockfish', 'vermillion rockfish').
inf('red rockfish', 'red rockfish').
inf('rosefish', 'rosefish').
inf('ocean perch', 'ocean perches').
inf('grubby', 'grubbies').
inf('lumpfish', 'lumpfish').
inf('snailfish', 'snailfish').
inf('alligatorfish', 'alligatorfish').
inf('convict fish', 'convict fish').
inf('convictfish', 'convictfish').
inf('butterflyfish', 'butterflyfish').
inf('plectognath fish', 'plectognath fish').
inf('triggerfish', 'triggerfish').
inf('queen triggerfish', 'queen triggerfish').
inf('oldwench', 'oldwenches').
inf('oldwife', 'oldwives').
inf('filefish', 'filefish').
inf('leatherfish', 'leatherfish').
inf('boxfish', 'boxfish').
inf('trunkfish', 'trunkfish').
inf('cowfish', 'cowfish').
inf('pufferfish', 'pufferfish').
inf('blowfish', 'blowfish').
inf('globefish', 'globefish').
inf('porcupinefish', 'porcupinefish').
inf('porcupine fish', 'porcupine fish').
inf('Diodon hystrix', 'Diodon hystrices').
inf('balloonfish', 'balloonfish').
inf('burrfish', 'burrfish').
inf('ocean sunfish', 'ocean sunfish').
inf('sunfish', 'sunfish').
inf('headfish', 'headfish').
inf('flatfish', 'flatfish').
inf('European flatfish', 'European flatfish').
inf('Hippoglossus stenolepsis', 'Hippoglossus stenolepses').
inf('Etropus', 'Etropera').
inf('genus Etropus', 'genus Etropera').
inf('tonguefish', 'tonguefish').
inf('tongue-fish', 'tongue-fish').
inf('thorax', 'thoraxes').
inf('prothorax', 'prothoraxes').
inf('A battery', 'A batteries').
inf('butchery', 'butcheries').
inf('abutment arch', 'abutment arches').
inf('academy', 'academies').
inf('access', 'accesses').
inf('approach', 'approaches').
inf('access', 'accesses').
inf('memory access', 'memory accesses').
inf('accessory', 'accessories').
inf('accessory', 'accessories').
inf('squeeze box', 'squeeze boxes').
inf('ace of clubs', 'aces of clubs').
inf('ace of diamonds', 'aces of diamonds').
inf('ace of hearts', 'aces of hearts').
inf('ace of spades', 'aces of spades').
inf('Lucy in the sky with diamonds', 'Lucys in the sky with diamonds').
inf('superman', 'supermen').
inf('active placebo', 'active placeboes').
inf('Zovirax', 'Zoviraxes').
inf('ecstasy', 'ecstasies').
inf('go', 'goes').
inf('adjustable wrench', 'adjustable wrenches').
inf('aerial torpedo', 'aerial torpedoes').
inf('Aertex', 'Aertexes').
inf('airbrush', 'airbrushes').
inf('airline business', 'airline businesses').
inf('air mattress', 'air mattresses').
inf('air-to-air missile', 'airs-to-airs missile').
inf('air-to-ground missile', 'airs-to-ground missile').
inf('air-to-surface missile', 'airs-to-surface missile').
inf('alcohol-in-glass thermometer', 'alcohols-in-glass thermometer').
inf('Fosamax', 'Fosamaxes').
inf('Allen wrench', 'Allen wrenches').
inf('alligator wrench', 'alligator wrenches').
inf('alms dish', 'alms dishes').
inf('Xanax', 'Xanaxes').
inf('alto relievo', 'alto relievoes').
inf('alto rilievo', 'alto rilievoes').
inf('ambulatory', 'ambulatories').
inf('ammo', 'ammoes').
inf('Polymox', 'Polymoxes').
inf('Trimox', 'Trimoxes').
inf('talisman', 'talismen').
inf('analog watch', 'analog watches').
inf('anamorphosis', 'anamorphoses').
inf('mechanical man', 'mechanical men').
inf('angiotensin I', 'angiotensin we').
inf('annex', 'annexes').
inf('antefix', 'antefixes').
inf('lobby', 'lobbies').
inf('anti-inflammatory', 'anti-inflammatories').
inf('antiquity', 'antiquities').
inf('apiary', 'apiaries').
inf('dress', 'dresses').
inf('apple of discord', 'apples of discord').
inf('gizmo', 'gizmoes').
inf('gismo', 'gismoes').
inf('approach trench', 'approach trenches').
inf('communication trench', 'communication trenches').
inf('apsis', 'apses').
inf('arch', 'arches').
inf('arch', 'arches').
inf('galosh', 'galoshes').
inf('golosh', 'goloshes').
inf('scene of action', 'scenes of action').
inf('theater in the round', 'theaters in the round').
inf('Ark of the Covenant', 'Arks of the Covenant').
inf('branch', 'branches').
inf('armory', 'armories').
inf('armoury', 'armouries').
inf('armory', 'armories').
inf('armoury', 'armouries').
inf('armory', 'armories').
inf('armoury', 'armouries').
inf('Artemision at Ephesus', 'Artemisions at Ephesus').
inf('artery', 'arteries').
inf('article of commerce', 'articles of commerce').
inf('artillery', 'artilleries').
inf('assembly', 'assemblies').
inf('assembly', 'assemblies').
inf('athletic facility', 'athletic facilities').
inf('atlas', 'atlantes').
inf('dress', 'dresses').
inf('auto accessory', 'auto accessories').
inf('autogiro', 'autogiroes').
inf('autogyro', 'autogyroes').
inf('automobile factory', 'automobile factories').
inf('auto factory', 'auto factories').
inf('car factory', 'car factories').
inf('aviary', 'aviaries').
inf('bird sanctuary', 'bird sanctuaries').
inf('volary', 'volaries').
inf('ax', 'axes').
inf('axis', 'axes').
inf('axis of rotation', 'axes of rotation').
inf('Zithromax', 'Zithromaxes').
inf('baby buggy', 'baby buggies').
inf('baby grand piano', 'baby grand pianoes').
inf('parlor grand piano', 'parlor grand pianoes').
inf('parlour grand piano', 'parlour grand pianoes').
inf('backbench', 'backbenches').
inf('back porch', 'back porches').
inf('backstitch', 'backstitches').
inf('frippery', 'fripperies').
inf('frivolity', 'frivolities').
inf('bakery', 'bakeries').
inf('balcony', 'balconies').
inf('balcony', 'balconies').
inf('balldress', 'balldresses').
inf('Fugo', 'Fugoes').
inf('ballot box', 'ballot boxes').
inf('Biro', 'Biroes').
inf('patch', 'patches').
inf('bandbox', 'bandboxes').
inf('bangalore torpedo', 'bangalore torpedoes').
inf('novelty', 'novelties').
inf('banjo', 'banjoes').
inf('baptistry', 'baptistries').
inf('baptistery', 'baptisteries').
inf('bargello', 'bargelloes').
inf('flame stitch', 'flame stitches').
inf('hurdy gurdy', 'hurdy gurdies').
inf('hurdy-gurdy', 'hurdy-gurdies').
inf('base of operations', 'bases of operations').
inf('basso relievo', 'basso relievoes').
inf('basso rilievo', 'basso rilievoes').
inf('bass', 'basses').
inf('double bass', 'double basses').
inf('contrabass', 'contrabasses').
inf('string bass', 'string basses').
inf('basting stitch', 'basting stitches').
inf('bastinado', 'bastinadoes').
inf('basuco', 'basucoes').
inf('batter\'s box', 'batter\'s boxes').
inf('battery', 'batteries').
inf('electric battery', 'electric batteries').
inf('battery', 'batteries').
inf('stamp battery', 'stamp batteries').
inf('battle-ax', 'battle-axes').
inf('battle dress', 'battle dresses').
inf('B battery', 'B batteries').
inf('string of beads', 'strings of beads').
inf('beany', 'beanies').
inf('busby', 'busbies').
inf('shako', 'shakoes').
inf('booby hatch', 'booby hatches').
inf('beer glass', 'beer glasses').
inf('belfry', 'belfries').
inf('bell arch', 'bell arches').
inf('bell foundry', 'bell foundries').
inf('bell glass', 'bell glasses').
inf('bell push', 'bell pushes').
inf('bypass', 'bypasses').
inf('bench', 'benches').
inf('bench', 'benches').
inf('bench press', 'bench presses').
inf('built in bed', 'builts in bed').
inf('bicycle-built-for-two', 'bicycle-built-for-twoes').
inf('bindery', 'binderies').
inf('biology laboratory', 'biology laboratories').
inf('birch', 'birches').
inf('bistro', 'bistroes').
inf('black box', 'black boxes').
inf('cosh', 'coshes').
inf('Blackwall hitch', 'Blackwall hitches').
inf('blackwash', 'blackwashes').
inf('blackwash', 'blackwashes').
inf('dummy', 'dummies').
inf('blanket stitch', 'blanket stitches').
inf('blouse', 'blice').
inf('blowtorch', 'blowtorches').
inf('torch', 'torches').
inf('blunderbuss', 'blunderbusses').
inf('body', 'bodies').
inf('suit of armor', 'suits of armor').
inf('suit of armour', 'suits of armour').
inf('coat of mail', 'coats of mail').
inf('personnel pouch', 'personnel pouches').
inf('human remains pouch', 'human remains pouches').
inf('bogy', 'bogies').
inf('bolero', 'boleroes').
inf('bolo', 'boloes').
inf('bolo knife', 'bolo knives').
inf('bolo', 'boloes').
inf('bongo', 'bongoes').
inf('bookbindery', 'bookbinderies').
inf('bookshelf', 'bookshelves').
inf('bottlebrush', 'bottlebrushes').
inf('merchantman', 'merchantmen').
inf('bouncing betty', 'bouncing betties').
inf('posy', 'posies').
inf('Bowie knife', 'Bowie knives').
inf('derby', 'derbies').
inf('box', 'boxes').
inf('box', 'boxes').
inf('box', 'boxes').
inf('box', 'boxes').
inf('box', 'boxes').
inf('box wrench', 'box wrenches').
inf('box end wrench', 'box end wrenches').
inf('brace wrench', 'brace wrenches').
inf('brass', 'brasses').
inf('brass', 'brasses').
inf('brass', 'brasses').
inf('bandeau', 'bandeaux').
inf('breadbox', 'breadboxes').
inf('bread knife', 'bread knives').
inf('jetty', 'jetties').
inf('breech', 'breeches').
inf('rear of barrel', 'rears of barrel').
inf('rear of tube', 'rears of tube').
inf('brewery', 'breweries').
inf('knickknackery', 'knickknackeries').
inf('wedding dress', 'wedding dresses').
inf('bristle brush', 'bristle brushes').
inf('broadax', 'broadaxes').
inf('broken arch', 'broken arches').
inf('brooch', 'brooches').
inf('broach', 'broaches').
inf('brush', 'brushes').
inf('brush', 'brushes').
inf('buggy', 'buggies').
inf('building complex', 'building complexes').
inf('complex', 'complexes').
inf('built-in bed', 'builts-in bed').
inf('bulldog wrench', 'bulldog wrenches').
inf('sheaf', 'sheaves').
inf('gunny', 'gunnies').
inf('coach', 'coaches').
inf('motorcoach', 'motorcoaches').
inf('jalopy', 'jalopies').
inf('butcher knife', 'butcher knives').
inf('butter dish', 'butter dishes').
inf('butter knife', 'butter knives').
inf('buttery', 'butteries').
inf('buttonhole stitch', 'buttonhole stitches').
inf('buttress', 'buttresses').
inf('cabin class', 'cabin classes').
inf('second class', 'second classes').
inf('economy class', 'economy classes').
inf('caddy', 'caddies').
inf('tea caddy', 'tea caddies').
inf('cafeteria facility', 'cafeteria facilities').
inf('calabash', 'calabashes').
inf('calash', 'calashes').
inf('calash', 'calashes').
inf('calico', 'calicoes').
inf('pipe of peace', 'pipes of peace').
inf('Calvary cross', 'Calvary crosses').
inf('cross of Calvary', 'crosses of Calvary').
inf('camber arch', 'camber arches').
inf('camo', 'camoes').
inf('belfry', 'belfries').
inf('cannery', 'canneries').
inf('canopy', 'canopies').
inf('canopy', 'canopies').
inf('canopy', 'canopies').
inf('canvass', 'canvasses').
inf('canvass', 'canvasses').
inf('canvass', 'canvasses').
inf('canvass', 'canvasses').
inf('capillary', 'capillaries').
inf('auto', 'autoes').
inf('caravansary', 'caravansaries').
inf('car battery', 'car batteries').
inf('automobile battery', 'automobile batteries').
inf('carbon copy', 'carbon copies').
inf('card index', 'card indexes').
inf('car-ferry', 'car-ferries').
inf('cargo', 'cargoes').
inf('cargo hatch', 'cargo hatches').
inf('carriage wrench', 'carriage wrenches').
inf('cartouch', 'cartouches').
inf('carving knife', 'carving knives').
inf('case knife', 'case knives').
inf('sheath knife', 'sheath knives').
inf('case knife', 'case knives').
inf('cashbox', 'cashboxes').
inf('money box', 'money boxes').
inf('casino', 'casinoes').
inf('gambling casino', 'gambling casinoes').
inf('cat box', 'cat boxes').
inf('catch', 'catches').
inf('catch', 'catches').
inf('duomo', 'duomoes').
inf('cautery', 'cauteries').
inf('cavetto', 'cavettoes').
inf('C battery', 'C batteries').
inf('compact disc read-only memory', 'compact disc read-only memories').
inf('Celebrex', 'Celebrexes').
inf('cello', 'celloes').
inf('violoncello', 'violoncelloes').
inf('Celtic cross', 'Celtic crosses').
inf('center punch', 'center punches').
inf('centrex', 'centrexes').
inf('Keflex', 'Keflexes').
inf('cereal box', 'cereal boxes').
inf('chafing dish', 'chafing dishes').
inf('chain stitch', 'chain stitches').
inf('chain stitch', 'chain stitches').
inf('chain wrench', 'chain wrenches').
inf('chair of state', 'chairs of state').
inf('potty', 'potties').
inf('sanctuary', 'sanctuaries').
inf('chancellery', 'chancelleries').
inf('chancery', 'chanceries').
inf('chandlery', 'chandleries').
inf('chandlery', 'chandleries').
inf('chantry', 'chantries').
inf('character-at-a-time printer', 'characters-at-a-time printer').
inf('chassis', 'chasses').
inf('chassis', 'chasses').
inf('chateau', 'chateaux').
inf('cheese press', 'cheese presses').
inf('shimmy', 'shimmies').
inf('teddy', 'teddies').
inf('chemistry laboratory', 'chemistry laboratories').
inf('chessman', 'chessmen').
inf('chest of drawers', 'chests of drawers').
inf('bureau', 'bureaux').
inf('cheval glass', 'cheval glasses').
inf('chiaroscuro', 'chiaroscuroes').
inf('chino', 'chinoes').
inf('chino', 'chinoes').
inf('choky', 'chokies').
inf('church', 'churches').
inf('ciderpress', 'ciderpresses').
inf('cigar box', 'cigar boxes').
inf('cigarillo', 'cigarilloes').
inf('cinch', 'cinches').
inf('Cipro', 'Ciproes').
inf('circuitry', 'circuitries').
inf('city university', 'city universities').
inf('civilian dress', 'civilian dresses').
inf('clinch', 'clinches').
inf('claro', 'claroes').
inf('clasp knife', 'clasp knives').
inf('jackknife', 'jackknives').
inf('claymore mine', 'claymore ours').
inf('clerestory', 'clerestories').
inf('clearstory', 'clearstories').
inf('clinch', 'clinches').
inf('clinch', 'clinches').
inf('clench', 'clenches').
inf('mercury-in-glass clinical thermometer', 'mercuries-in-glass clinical thermometer').
inf('Plavix', 'Plavixes').
inf('clothesbrush', 'clothesbrushes').
inf('clothespress', 'clothespresses').
inf('article of clothing', 'articles of clothing').
inf('haberdashery', 'haberdasheries').
inf('clove hitch', 'clove hitches').
inf('cloverleaf', 'cloverleaves').
inf('clutch', 'clutches').
inf('clutch', 'clutches').
inf('clutch', 'clutches').
inf('coach', 'coaches').
inf('four-in-hand', 'fours-in-hand').
inf('coal mine', 'coal ours').
inf('coatdress', 'coatdresses').
inf('coat of arms', 'coats of arms').
inf('blazonry', 'blazonries').
inf('coat of paint', 'coats of paint').
inf('coax', 'coaxes').
inf('cocktail dress', 'cocktail dresses').
inf('helix', 'helixes').
inf('coin box', 'coin boxes').
inf('nose candy', 'nose candies').
inf('colliery', 'collieries').
inf('color wash', 'color washes').
inf('colour wash', 'colour washes').
inf('Colossus of Rhodes', 'Colossuss of Rhodes').
inf('baby\'s dummy', 'baby\'s dummies').
inf('commissary', 'commissaries').
inf('commissary', 'commissaries').
inf('commodity', 'commodities').
inf('common ax', 'common axes').
inf('Dayton ax', 'Dayton axes').
inf('compass', 'compasses').
inf('compass', 'compasses').
inf('mariner\'s compass', 'mariner\'s compasses').
inf('compress', 'compresses').
inf('computer accessory', 'computer accessories').
inf('concert piano', 'concert pianoes').
inf('safety', 'safeties').
inf('condo', 'condoes').
inf('cone clutch', 'cone clutches').
inf('cone friction clutch', 'cone friction clutches').
inf('confectionery', 'confectioneries').
inf('confectionary', 'confectionaries').
inf('conservatory', 'conservatories').
inf('conservatory', 'conservatories').
inf('tangency', 'tangencies').
inf('contrafagotto', 'contrafagottoes').
inf('copper mine', 'copper ours').
inf('copy', 'copies').
inf('truss', 'trusses').
inf('corbel arch', 'corbel arches').
inf('cosmography', 'cosmographies').
inf('cosy', 'cosies').
inf('tea cosy', 'tea cosies').
inf('cozy', 'cozies').
inf('tea cozy', 'tea cozies').
inf('couch', 'couches').
inf('couch', 'couches').
inf('covered couch', 'covered couches').
inf('cover glass', 'cover glasses').
inf('tornado', 'tornadoes').
inf('creamery', 'creameries').
inf('crematory', 'crematories').
inf('crematory', 'crematories').
inf('crescent wrench', 'crescent wrenches').
inf('crochet stitch', 'crochet stitches').
inf('crockery', 'crockeries').
inf('Cross', 'Crosses').
inf('cross', 'crosses').
inf('crossbench', 'crossbenches').
inf('cross-stitch', 'cross-stitches').
inf('cross-stitch', 'cross-stitches').
inf('pry', 'pries').
inf('crown of thorns', 'crowns of thorns').
inf('crucifix', 'crucifixes').
inf('crutch', 'crutches').
inf('cryocautery', 'cryocauteries').
inf('watch glass', 'watch glasses').
inf('cubby', 'cubbies').
inf('snuggery', 'snuggeries').
inf('cuddy', 'cuddies').
inf('cuirass', 'cuirasses').
inf('curiosity', 'curiosities').
inf('oddity', 'oddities').
inf('peculiarity', 'peculiarities').
inf('rarity', 'rarities').
inf('drapery', 'draperies').
inf('gash', 'gashes').
inf('cut glass', 'cut glasses').
inf('cutlass', 'cutlasses').
inf('cutlery', 'cutleries').
inf('cutlery', 'cutleries').
inf('cyclopean masonry', 'cyclopean masonries').
inf('dado', 'dadoes').
inf('dado', 'dadoes').
inf('dairy', 'dairies').
inf('ambo', 'amboes').
inf('soapbox', 'soapboxes').
inf('day nursery', 'day nurseries').
inf('deanery', 'deaneries').
inf('defense laboratory', 'defense laboratories').
inf('delf', 'delves').
inf('dental floss', 'dental flosses').
inf('floss', 'flosses').
inf('depilatory', 'depilatories').
inf('depository', 'depositories').
inf('depositary', 'depositaries').
inf('repository', 'repositories').
inf('house of detention', 'houses of detention').
inf('detox', 'detoxes').
inf('two', 'twoes').
inf('dialog box', 'dialog boxes').
inf('nappy', 'nappies').
inf('pessary', 'pessaries').
inf('diary', 'diaries').
inf('dice box', 'dice boxes').
inf('dicky', 'dickies').
inf('dicky', 'dickies').
inf('digital-to-analog converter', 'digitals-to-analog converter').
inf('digital watch', 'digital watches').
inf('analog-to-digital converter', 'analogs-to-digital converter').
inf('dildo', 'dildoes').
inf('dimity', 'dimities').
inf('dinghy', 'dinghies').
inf('dory', 'dories').
inf('dinky', 'dinkies').
inf('dinner dress', 'dinner dresses').
inf('tux', 'tuxes').
inf('tuxedo', 'tuxedoes').
inf('diplomatic pouch', 'diplomatic pouches').
inf('DIP switch', 'DIP switches').
inf('dual inline package switch', 'dual inline package switches').
inf('diptych', 'diptyches').
inf('disco', 'discoes').
inf('dish', 'dishes').
inf('dish', 'dishes').
inf('disk access', 'disk accesses').
inf('disk clutch', 'disk clutches').
inf('floppy', 'floppies').
inf('dispatch box', 'dispatch boxes').
inf('dispensary', 'dispensaries').
inf('distillery', 'distilleries').
inf('ditch', 'ditches').
inf('diving dress', 'diving dresses').
inf('docking facility', 'docking facilities').
inf('dogtooth', 'dogteeth').
inf('dog wrench', 'dog wrenches').
inf('gizmo', 'gizmoes').
inf('gismo', 'gismoes').
inf('thingummy', 'thingummies').
inf('whatsis', 'whatses').
inf('doily', 'doilies').
inf('doyly', 'doylies').
inf('dolly', 'dollies').
inf('dolly', 'dollies').
inf('dolly', 'dollies').
inf('dolman', 'dolmen').
inf('dolman', 'dolmen').
inf('cromlech', 'cromleches').
inf('domino', 'dominoes').
inf('domino', 'dominoes').
inf('domino', 'dominoes').
inf('room access', 'room accesses').
inf('dormitory', 'dormitories').
inf('dormitory', 'dormitories').
inf('double-bitted ax', 'double-bitted axes').
inf('Western ax', 'Western axes').
inf('double clinch', 'double clinches').
inf('double stitch', 'double stitches').
inf('columbary', 'columbaries').
inf('drainage ditch', 'drainage ditches').
inf('drapery', 'draperies').
inf('drawknife', 'drawknives').
inf('dress', 'dresses').
inf('vanity', 'vanities').
inf('full dress', 'full dresses').
inf('gallery', 'galleries').
inf('drill press', 'drill presses').
inf('drop arch', 'drop arches').
inf('drop press', 'drop presses').
inf('drop-leaf', 'drop-leaves').
inf('droshky', 'droshkies').
inf('drosky', 'droskies').
inf('highly active antiretroviral therapy', 'highly active antiretroviral therapies').
inf('drug of abuse', 'drugs of abuse').
inf('pharmacy', 'pharmacies').
inf('dry battery', 'dry batteries').
inf('dry fly', 'dry flies').
inf('dry masonry', 'dry masonries').
inf('dummy', 'dummies').
inf('tipper lorry', 'tipper lorries').
inf('dune buggy', 'dune buggies').
inf('beach buggy', 'beach buggies').
inf('duplex', 'duplexes').
inf('duplex', 'duplexes').
inf('dynamo', 'dynamoes').
inf('ecce homo', 'ecce homoes').
inf('effigy', 'effigies').
inf('electric toothbrush', 'electric toothbrushes').
inf('embassy', 'embassies').
inf('embroidery', 'embroideries').
inf('empty', 'empties').
inf('enginery', 'engineries').
inf('entry', 'entries').
inf('epilating wax', 'epilating waxes').
inf('erasable programmable read-only memory', 'erasable programmable read-only memories').
inf('escape hatch', 'escape hatches').
inf('estradiol patch', 'estradiol patches').
inf('express', 'expresses').
inf('eyewash', 'eyewashes').
inf('eyepatch', 'eyepatches').
inf('patch', 'patches').
inf('facility', 'facilities').
inf('fax', 'faxes').
inf('factory', 'factories').
inf('manufactory', 'manufactories').
inf('fagot stitch', 'fagot stitches').
inf('faggot stitch', 'faggot stitches').
inf('fancy dress', 'fancy dresses').
inf('fan tracery', 'fan traceries').
inf('felly', 'fellies').
inf('ferry', 'ferries').
inf('fess', 'fesses').
inf('festoonery', 'festooneries').
inf('tarboosh', 'tarbooshes').
inf('field artillery', 'field artilleries').
inf('field glass', 'field glasses').
inf('glass', 'glasses').
inf('spyglass', 'spyglasses').
inf('fig leaf', 'fig leaves').
inf('figure of eight', 'figures of eight').
inf('finery', 'fineries').
inf('firebox', 'fireboxes').
inf('fireman\'s ax', 'fireman\'s axes').
inf('fire trench', 'fire trenches').
inf('first class', 'first classes').
inf('fishery', 'fisheries').
inf('piscary', 'piscaries').
inf('fish knife', 'fish knives').
inf('flambeau', 'flambeaux').
inf('flash', 'flashes').
inf('photoflash', 'photoflashes').
inf('flash', 'flashes').
inf('torch', 'torches').
inf('flashlight battery', 'flashlight batteries').
inf('flash memory', 'flash memories').
inf('flat arch', 'flat arches').
inf('straight arch', 'straight arches').
inf('flatbed press', 'flatbed presses').
inf('cylinder press', 'cylinder presses').
inf('flat bench', 'flat benches').
inf('flat wash', 'flat washes').
inf('flight of stairs', 'flights of stairs').
inf('flight of steps', 'flights of steps').
inf('floating mine', 'floating ours').
inf('marine mine', 'marine ours').
inf('story', 'stories').
inf('floss', 'flosses').
inf('bed of flowers', 'beds of flowers').
inf('lavatory', 'lavatories').
inf('flute glass', 'flute glasses').
inf('fly', 'flies').
inf('fly', 'flies').
inf('fly gallery', 'fly galleries').
inf('flying buttress', 'flying buttresses').
inf('transparency', 'transparencies').
inf('gimcrackery', 'gimcrackeries').
inf('trumpery', 'trumperies').
inf('foot', 'feet').
inf('ottoman', 'ottomen').
inf('smithy', 'smithies').
inf('evening dress', 'evening dresses').
inf('fortress', 'fortresses').
inf('assembly', 'assemblies').
inf('foot', 'feet').
inf('foundry', 'foundries').
inf('four-in-hand', 'fours-in-hand').
inf('French polish', 'French polishes').
inf('fresco', 'frescoes').
inf('friary', 'friaries').
inf('friction clutch', 'friction clutches').
inf('front bench', 'front benches').
inf('front porch', 'front porches').
inf('funeral church', 'funeral churches').
inf('refractory', 'refractories').
inf('piece of furniture', 'pieces of furniture').
inf('article of furniture', 'articles of furniture').
inf('Lasix', 'Lasixes').
inf('gadgetry', 'gadgetries').
inf('gallery', 'galleries').
inf('gallery', 'galleries').
inf('gallery', 'galleries').
inf('gallery', 'galleries').
inf('art gallery', 'art galleries').
inf('picture gallery', 'picture galleries').
inf('brolly', 'brollies').
inf('gantry', 'gantries').
inf('gauntry', 'gauntries').
inf('garlic press', 'garlic presses').
inf('garnish', 'garnishes').
inf('garter stitch', 'garter stitches').
inf('gazebo', 'gazeboes').
inf('gearbox', 'gearboxes').
inf('gear box', 'gear boxes').
inf('gelly', 'gellies').
inf('gharry', 'gharries').
inf('boom box', 'boom boxes').
inf('sash', 'sashes').
inf('glass', 'glasses').
inf('drinking glass', 'drinking glasses').
inf('glass', 'glasses').
inf('lazaretto', 'lazarettoes').
inf('glossy', 'glossies').
inf('glyptography', 'glyptographies').
inf('golden calf', 'golden calves').
inf('gold leaf', 'gold leaves').
inf('gold mine', 'gold ours').
inf('gold mine', 'gold ours').
inf('Gothic arch', 'Gothic arches').
inf('calabash', 'calabashes').
inf('graffito', 'graffiti').
inf('granary', 'granaries').
inf('grand piano', 'grand pianoes').
inf('granny', 'grannies').
inf('jambeau', 'jambeaux').
inf('Greek cross', 'Greek crosses').
inf('greengrocery', 'greengroceries').
inf('greengrocery', 'greengroceries').
inf('nursery', 'nurseries').
inf('grocery', 'groceries').
inf('grocery', 'groceries').
inf('safety', 'safeties').
inf('gunnery', 'gunneries').
inf('gyrocompass', 'gyrocompasses').
inf('gyro', 'gyroes').
inf('haberdashery', 'haberdasheries').
inf('hackney coach', 'hackney coaches').
inf('hairbrush', 'hairbrushes').
inf('half cross stitch', 'half cross stitches').
inf('half hitch', 'half hitches').
inf('Hall of Fame', 'Halls of Fame').
inf('hall of residence', 'halls of residence').
inf('hand ax', 'hand axes').
inf('emergency', 'emergencies').
inf('hand glass', 'hand glasses').
inf('magnifying glass', 'magnifying glasses').
inf('hand glass', 'hand glasses').
inf('hanky', 'hankies').
inf('harness', 'harnesses').
inf('harness', 'harnesses').
inf('cembalo', 'cembaloes').
inf('hashish', 'hashishes').
inf('hasheesh', 'hasheeshes').
inf('haschisch', 'haschisches').
inf('hash', 'hashes').
inf('chapeau', 'chapeaux').
inf('hatbox', 'hatboxes').
inf('hatch', 'hatches').
inf('oasis', 'oases').
inf('headdress', 'headdresses').
inf('headscarf', 'headscarves').
inf('hemming-stitch', 'hemming-stitches').
inf('hemstitch', 'hemstitches').
inf('hemstitch', 'hemstitches').
inf('heraldry', 'heraldries').
inf('highball glass', 'highball glasses').
inf('hitch', 'hitches').
inf('hitch', 'hitches').
inf('hitch', 'hitches').
inf('hobby', 'hobbies').
inf('holy of holies', 'holies of holies').
inf('homestretch', 'homestretches').
inf('homing torpedo', 'homing torpedoes').
inf('hood latch', 'hood latches').
inf('hubbly-bubbly', 'hubbly-bubblies').
inf('hook wrench', 'hook wrenches').
inf('horsebox', 'horseboxes').
inf('hosiery', 'hosieries').
inf('infirmary', 'infirmaries').
inf('hostelry', 'hostelries').
inf('hotbox', 'hotboxes').
inf('hotel-casino', 'hotel-casinoes').
inf('hotel-casino', 'hotel-casinoes').
inf('hourglass', 'hourglasses').
inf('house of cards', 'houses of cards').
inf('house of correction', 'houses of correction').
inf('hutch', 'hutches').
inf('shanty', 'shanties').
inf('hunting watch', 'hunting watches').
inf('hunting knife', 'hunting knives').
inf('hutch', 'hutches').
inf('hydraulic press', 'hydraulic presses').
inf('Esidrix', 'Esidrixes').
inf('Atarax', 'Ataraxes').
inf('hypo', 'hypoes').
inf('ice ax', 'ice axes').
inf('iconography', 'iconographies').
inf('ignition switch', 'ignition switches').
inf('forgery', 'forgeries').
inf('incendiary', 'incendiaries').
inf('Indiaman', 'Indiamen').
inf('block of metal', 'blocks of metal').
inf('inhalation general anesthetic', 'inhalations general anesthetic').
inf('inhalation general anaesthetic', 'inhalations general anaesthetic').
inf('inside clinch', 'inside clinches').
inf('brainchild', 'brainchildren').
inf('instrumentality', 'instrumentalities').
inf('instrument of execution', 'instruments of execution').
inf('instrument of punishment', 'instruments of punishment').
inf('instrument of torture', 'instruments of torture').
inf('iron foundry', 'iron foundries').
inf('ironmongery', 'ironmongeries').
inf('irrigation ditch', 'irrigation ditches').
inf('Sporanox', 'Sporanoxes').
inf('jack-in-the-box', 'jacks-in-the-box').
inf('poky', 'pokies').
inf('Jaws of Life', 'Jawss of Life').
inf('Jerusalem cross', 'Jerusalem crosses').
inf('jeweler\'s glass', 'jeweler\'s glasses').
inf('jewelled headdress', 'jewelled headdresses').
inf('jeweled headdress', 'jeweled headdresses').
inf('jewelry', 'jewelries').
inf('jewellery', 'jewelleries').
inf('jimdandy', 'jimdandies').
inf('jimmy', 'jimmies').
inf('jemmy', 'jemmies').
inf('joinery', 'joineries').
inf('jolly', 'jollies').
inf('joss', 'josses').
inf('journal box', 'journal boxes').
inf('fetish', 'fetishes').
inf('fetich', 'fetiches').
inf('jukebox', 'jukeboxes').
inf('pinny', 'pinnies').
inf('jury box', 'jury boxes').
inf('Kakemono', 'Kakemonoes').
inf('Kantrex', 'Kantrexes').
inf('ketch', 'ketches').
inf('kimono', 'kimonoes').
inf('vegetable patch', 'vegetable patches').
inf('kitchen match', 'kitchen matches').
inf('kitsch', 'kitsches').
inf('novelty', 'novelties').
inf('knife', 'knives').
inf('knife', 'knives').
inf('knit stitch', 'knit stitches').
inf('plain stitch', 'plain stitches').
inf('knitting stitch', 'knitting stitches').
inf('boss', 'bosses').
inf('knobkerry', 'knobkerries').
inf('koto', 'kotoes').
inf('kylix', 'kylixes').
inf('cylix', 'cylixes').
inf('laboratory', 'laboratories').
inf('research laboratory', 'research laboratories').
inf('science laboratory', 'science laboratories').
inf('lab bench', 'lab benches').
inf('laboratory bench', 'laboratory benches').
inf('Labyrinth of Minos', 'Labyrinths of Minos').
inf('lally', 'lallies').
inf('lamasery', 'lamaseries').
inf('lancet arch', 'lancet arches').
inf('land mine', 'land ours').
inf('ground-emplaced mine', 'ground-emplaced ours').
inf('lash', 'lashes').
inf('lasso', 'lassoes').
inf('Lastex', 'Lastexes').
inf('latch', 'latches').
inf('latch', 'latches').
inf('door latch', 'door latches').
inf('latex', 'latexes').
inf('Latin cross', 'Latin crosses').
inf('tincture of opium', 'tinctures of opium').
inf('launch', 'launches').
inf('laundry', 'laundries').
inf('laundry', 'laundries').
inf('wash', 'washes').
inf('court of law', 'courts of law').
inf('court of justice', 'courts of justice').
inf('lazaretto', 'lazarettoes').
inf('lazy daisy stitch', 'lazy daisy stitches').
inf('lead-acid battery', 'lead-acid batteries').
inf('leaf', 'leaves').
inf('lean-to', 'lean-toes').
inf('lean-to tent', 'leans-to tent').
inf('leash', 'leashes').
inf('Lego', 'Legoes').
inf('lending library', 'lending libraries').
inf('circulating library', 'circulating libraries').
inf('paper knife', 'paper knives').
inf('paperknife', 'paperknives').
inf('library', 'libraries').
inf('depository library', 'depository libraries').
inf('library', 'libraries').
inf('library', 'libraries').
inf('lido', 'lidoes').
inf('likeness', 'likenesses').
inf('Lilo', 'Liloes').
inf('limo', 'limoes').
inf('line of products', 'lines of products').
inf('line of merchandise', 'lines of merchandise').
inf('line of business', 'lines of business').
inf('line of defense', 'lines of defense').
inf('line of defence', 'lines of defence').
inf('line-at-a-time printer', 'lines-at-a-time printer').
inf('linoleum knife', 'linoleum knives').
inf('lip-gloss', 'lip-glosses').
inf('liqueur glass', 'liqueur glasses').
inf('livery', 'liveries').
inf('Lochaber ax', 'Lochaber axes').
inf('lockstitch', 'lockstitches').
inf('looking glass', 'looking glasses').
inf('glass', 'glasses').
inf('observatory', 'observatories').
inf('Lorraine cross', 'Lorraine crosses').
inf('cross of Lorraine', 'crosses of Lorraine').
inf('lorry', 'lorries').
inf('lorry', 'lorries').
inf('lug wrench', 'lug wrenches').
inf('machinery', 'machineries').
inf('machine stitch', 'machine stitches').
inf('sewing-machine stitch', 'sewing-machine stitches').
inf('macintosh', 'macintoshes').
inf('mackintosh', 'mackintoshes').
inf('mackintosh', 'mackintoshes').
inf('macintosh', 'macintoshes').
inf('magnetic bubble memory', 'magnetic bubble memories').
inf('magnetic compass', 'magnetic compasses').
inf('magnetic core memory', 'magnetic core memories').
inf('core memory', 'core memories').
inf('magnetic mine', 'magnetic ours').
inf('magneto', 'magnetoes').
inf('magnum opus', 'magnum opera').
inf('magnus hitch', 'magnus hitches').
inf('mail pouch', 'mail pouches').
inf('mailbox', 'mailboxes').
inf('letter box', 'letter boxes').
inf('Maltese cross', 'Maltese crosses').
inf('man', 'men').
inf('man-of-war', 'men-of-war').
inf('ship of the line', 'ships of the line').
inf('marquetry', 'marquetries').
inf('masonry', 'masonries').
inf('master copy', 'master copies').
inf('match', 'matches').
inf('friction match', 'friction matches').
inf('match', 'matches').
inf('match', 'matches').
inf('matchbox', 'matchboxes').
inf('matrix', 'matrices').
inf('mattress', 'mattresses').
inf('Mausoleum at Halicarnasus', 'Mausoleums at Halicarnasus').
inf('Meccano', 'Meccanoes').
inf('mechanical piano', 'mechanical pianoes').
inf('player piano', 'player pianoes').
inf('health facility', 'health facilities').
inf('healthcare facility', 'healthcare facilities').
inf('memory', 'memories').
inf('computer memory', 'computer memories').
inf('patch', 'patches').
inf('mercury-in-glass thermometer', 'mercuries-in-glass thermometer').
inf('mess', 'messes').
inf('glass', 'glasses').
inf('trash', 'trashes').
inf('Urex', 'Urexes').
inf('metro', 'metroes').
inf('first balcony', 'first balconies').
inf('mezzo-relievo', 'mezzo-relievoes').
inf('mezzo-rilievo', 'mezzo-rilievoes').
inf('microbrewery', 'microbreweries').
inf('middy', 'middies').
inf('middy blouse', 'middy blice').
inf('milk of magnesia', 'milks of magnesia').
inf('milling machinery', 'milling machineries').
inf('millinery', 'millineries').
inf('millinery', 'millineries').
inf('mine', 'ours').
inf('mine', 'ours').
inf('ministry', 'ministries').
inf('Minuteman', 'Minutemen').
inf('miter box', 'miter boxes').
inf('mitre box', 'mitre boxes').
inf('monastery', 'monasteries').
inf('monkey-wrench', 'monkey-wrenches').
inf('monkey wrench', 'monkey wrenches').
inf('eyeglass', 'eyeglasses').
inf('Moorish arch', 'Moorish arches').
inf('horseshoe arch', 'horseshoe arches').
inf('mortuary', 'mortuaries').
inf('morning-after pill', 'mornings-after pill').
inf('morning dress', 'morning dresses').
inf('morning dress', 'morning dresses').
inf('mouse', 'mice').
inf('computer mouse', 'computer mice').
inf('mulch', 'mulches').
inf('multiplex', 'multiplexes').
inf('music box', 'music boxes').
inf('musical box', 'musical boxes').
inf('nailbrush', 'nailbrushes').
inf('nail polish', 'nail polishes').
inf('nail varnish', 'nail varnishes').
inf('Anaprox', 'Anaproxes').
inf('narrow-body', 'narrow-bodies').
inf('narthex', 'narthexes').
inf('narthex', 'narthexes').
inf('naumachy', 'naumachies').
inf('naval weaponry', 'naval weaponries').
inf('needlepoint embroidery', 'needlepoint embroideries').
inf('mesh', 'meshes').
inf('nickel-iron battery', 'nickel-iron batteries').
inf('nightdress', 'nightdresses').
inf('night latch', 'night latches').
inf('nonsteroidal anti-inflammatory', 'nonsteroidal anti-inflammatories').
inf('notch', 'notches').
inf('nunnery', 'nunneries').
inf('nursery', 'nurseries').
inf('object glass', 'object glasses').
inf('hautbois', 'hautbois').
inf('observatory', 'observatories').
inf('sweet potato', 'sweet potatoes').
inf('body of work', 'bodies of work').
inf('officer\'s mess', 'officer\'s messes').
inf('ogee arch', 'ogee arches').
inf('keel arch', 'keel arches').
inf('oil refinery', 'oil refineries').
inf('petroleum refinery', 'petroleum refineries').
inf('open-end wrench', 'open-end wrenches').
inf('tappet wrench', 'tappet wrenches').
inf('surgery', 'surgeries').
inf('optical bench', 'optical benches').
inf('ordinary', 'ordinaries').
inf('ordinary', 'ordinaries').
inf('organdy', 'organdies').
inf('Norflex', 'Norflexes').
inf('orrery', 'orreries').
inf('ossuary', 'ossuaries').
inf('ottoman', 'ottomen').
inf('privy', 'privies').
inf('outlet box', 'outlet boxes').
inf('outside clinch', 'outside clinches').
inf('overhand stitch', 'overhand stitches').
inf('overpass', 'overpasses').
inf('ovolo', 'ovoloes').
inf('Daypro', 'Dayproes').
inf('Serax', 'Seraxes').
inf('oxyacetylene torch', 'oxyacetylene torches').
inf('packing box', 'packing boxes').
inf('paddle box', 'paddle boxes').
inf('paddle-box', 'paddle-boxes').
inf('page-at-a-time printer', 'pages-at-a-time printer').
inf('paintbox', 'paintboxes').
inf('paintbrush', 'paintbrushes').
inf('palette knife', 'palette knives').
inf('Pandora\'s box', 'Pandora\'s boxes').
inf('pane of glass', 'panes of glass').
inf('window glass', 'window glasses').
inf('syrinx', 'syringes').
inf('panty', 'panties').
inf('scanty', 'scanties').
inf('pantry', 'pantries').
inf('buttery', 'butteries').
inf('papal cross', 'papal crosses').
inf('paring knife', 'paring knives').
inf('parfait glass', 'parfait glasses').
inf('pargetry', 'pargetries').
inf('University of Paris', 'Universitys of Paris').
inf('park bench', 'park benches').
inf('parquetry', 'parquetries').
inf('rectory', 'rectories').
inf('coach', 'coaches').
inf('pasty', 'pasties').
inf('patch', 'patches').
inf('patchouly', 'patchoulies').
inf('patriarchal cross', 'patriarchal crosses').
inf('pearl fishery', 'pearl fisheries').
inf('peavy', 'peavies').
inf('penal colony', 'penal colonies').
inf('penal facility', 'penal facilities').
inf('penalty box', 'penalty boxes').
inf('pencil box', 'pencil boxes').
inf('pendulum watch', 'pendulum watches').
inf('penitentiary', 'penitentiaries').
inf('penknife', 'penknives').
inf('pentimento', 'pentimentoes').
inf('pepper box', 'pepper boxes').
inf('perch', 'perches').
inf('perfumery', 'perfumeries').
inf('perfumery', 'perfumeries').
inf('perfumery', 'perfumeries').
inf('permanent press', 'permanent presses').
inf('durable press', 'durable presses').
inf('tent stitch', 'tent stitches').
inf('Petri dish', 'Petri dishes').
inf('church bench', 'church benches').
inf('Vasomax', 'Vasomaxes').
inf('photocopy', 'photocopies').
inf('photo', 'photoes').
inf('phrontistery', 'phrontisteries').
inf('physics laboratory', 'physics laboratories').
inf('piano', 'pianoes').
inf('forte-piano', 'forte-pianoes').
inf('piccolo', 'piccoloes').
inf('pickax', 'pickaxes').
inf('piece of cloth', 'pieces of cloth').
inf('piece of material', 'pieces of material').
inf('piece of leather', 'pieces of leather').
inf('wharf', 'wharves').
inf('pier arch', 'pier arches').
inf('pier glass', 'pier glasses').
inf('piggery', 'piggeries').
inf('pillar box', 'pillar boxes').
inf('pillbox', 'pillboxes').
inf('pillbox', 'pillboxes').
inf('pillbox', 'pillboxes').
inf('pillory', 'pillories').
inf('Pimlico', 'Pimlicoes').
inf('pair of pincers', 'pairs of pincers').
inf('pair of tweezers', 'pairs of tweezers').
inf('pin wrench', 'pin wrenches').
inf('pipe wrench', 'pipe wrenches').
inf('tube wrench', 'tube wrenches').
inf('quarry', 'quarries').
inf('placebo', 'placeboes').
inf('place of business', 'places of business').
inf('place of worship', 'places of worship').
inf('house of prayer', 'houses of prayer').
inf('house of God', 'houses of God').
inf('house of worship', 'houses of worship').
inf('plate glass', 'plate glasses').
inf('sheet glass', 'sheet glasses').
inf('playbox', 'playboxes').
inf('play-box', 'play-boxes').
inf('pair of pliers', 'pairs of pliers').
inf('plush', 'plushes').
inf('ply', 'plies').
inf('ply', 'plies').
inf('Pneumovax', 'Pneumovaxes').
inf('pocketknife', 'pocketknives').
inf('pocket knife', 'pocket knives').
inf('pocket watch', 'pocket watches').
inf('pointed arch', 'pointed arches').
inf('poleax', 'poleaxes').
inf('poleax', 'poleaxes').
inf('poncho', 'ponchoes').
inf('poor box', 'poor boxes').
inf('alms box', 'alms boxes').
inf('mite box', 'mite boxes').
inf('porch', 'porches').
inf('portico', 'porticoes').
inf('portmanteau', 'portmanteaux').
inf('postbox', 'postboxes').
inf('mailbox', 'mailboxes').
inf('letter box', 'letter boxes').
inf('Post-Office box', 'Post-Office boxes').
inf('PO Box', 'PO Boxes').
inf('call box', 'call boxes').
inf('letter box', 'letter boxes').
inf('grass', 'grasses').
inf('green goddess', 'green goddesses').
inf('sess', 'sesses').
inf('potbelly', 'potbellies').
inf('pottery', 'potteries').
inf('pottery', 'potteries').
inf('pouch', 'pouches').
inf('Minipress', 'Minipresses').
inf('presbytery', 'presbyteries').
inf('press', 'presses').
inf('mechanical press', 'mechanical presses').
inf('press', 'presses').
inf('printing press', 'printing presses').
inf('press', 'presses').
inf('press box', 'press boxes').
inf('press gallery', 'press galleries').
inf('press of sail', 'presses of sail').
inf('press of canvas', 'presses of canvas').
inf('prie-dieu', 'prie-dieu').
inf('primary', 'primaries').
inf('priory', 'priories').
inf('prisoner of war camp', 'prisoners of war camp').
inf('prompt box', 'prompt boxes').
inf('prompter\'s box', 'prompter\'s boxes').
inf('property', 'properties').
inf('proscenium arch', 'proscenium arches').
inf('prosthesis', 'prostheses').
inf('pruning knife', 'pruning knives').
inf('psaltery', 'psalteries').
inf('public lavatory', 'public lavatories').
inf('toilet facility', 'toilet facilities').
inf('layby', 'laybies').
inf('lay-by', 'lay-bies').
inf('Pullman', 'Pullmen').
inf('punch', 'punches').
inf('punch press', 'punch presses').
inf('purl stitch', 'purl stitches').
inf('push', 'pushes').
inf('putty knife', 'putty knives').
inf('Pyramids of Egypt', 'Pyramidss of Egypt').
inf('pyx', 'pyxes').
inf('pix', 'pixes').
inf('pyx', 'pyxes').
inf('pix', 'pixes').
inf('pyxis', 'pyxes').
inf('quadraphony', 'quadraphonies').
inf('quartz battery', 'quartz batteries').
inf('Quinidex', 'Quinidexes').
inf('rophy', 'rophies').
inf('roofy', 'roofies').
inf('roach', 'roaches').
inf('rabato', 'rabatoes').
inf('rebato', 'rebatoes').
inf('rabbit hutch', 'rabbit hutches').
inf('wireless', 'wirelesses').
inf('radio chassis', 'radio chasses').
inf('radio compass', 'radio compasses').
inf('radiophoto', 'radiophotoes').
inf('wireless', 'wirelesses').
inf('radiotelegraphy', 'radiotelegraphies').
inf('wireless telegraphy', 'wireless telegraphies').
inf('rampant arch', 'rampant arches').
inf('ranch', 'ranches').
inf('cattle ranch', 'cattle ranches').
inf('random-access memory', 'random-access memories').
inf('random access memory', 'random access memories').
inf('random memory', 'random memories').
inf('read/write memory', 'read/write memories').
inf('ratch', 'ratches').
inf('read-only memory', 'read-only memories').
inf('ready-to-wear', 'readies-to-wear').
inf('Edronax', 'Edronaxes').
inf('rebozo', 'rebozoes').
inf('recess', 'recesses').
inf('recreational facility', 'recreational facilities').
inf('recreation facility', 'recreation facilities').
inf('redbrick university', 'redbrick universities').
inf('refectory', 'refectories').
inf('refinery', 'refineries').
inf('reformatory', 'reformatories').
inf('icebox', 'iceboxes').
inf('sanctuary', 'sanctuaries').
inf('relievo', 'relievoes').
inf('rilievo', 'rilievoes').
inf('reliquary', 'reliquaries').
inf('remedy', 'remedies').
inf('repertory', 'repertories').
inf('repository', 'repositories').
inf('research facility', 'research facilities').
inf('reseau', 'reseaux').
inf('reseau', 'reseaux').
inf('eatery', 'eateries').
inf('verso', 'versoes').
inf('rigger brush', 'rigger brushes').
inf('right of way', 'rights of way').
inf('roach', 'roaches').
inf('rockery', 'rockeries').
inf('Vioxx', 'Vioxxes').
inf('rolling hitch', 'rolling hitches').
inf('roll of tobacco', 'rolls of tobacco').
inf('roll-on roll-off', 'rolls-on rolls-off').
inf('Rolodex', 'Rolodexes').
inf('Roman arch', 'Roman arches').
inf('semicircular arch', 'semicircular arches').
inf('rosary', 'rosaries').
inf('bed of roses', 'beds of roses').
inf('rotary press', 'rotary presses').
inf('rouleau', 'rouleaux').
inf('rouleau', 'rouleaux').
inf('unit of ammunition', 'units of ammunitsion').
inf('round arch', 'round arches').
inf('rowlock arch', 'rowlock arches').
inf('row of bricks', 'rows of bricks').
inf('running stitch', 'running stitches').
inf('sable brush', 'sable brushes').
inf('saddlery', 'saddleries').
inf('saddle stitch', 'saddle stitches').
inf('safe-deposit box', 'safe-deposit boxes').
inf('safety deposit box', 'safety deposit boxes').
inf('deposit box', 'deposit boxes').
inf('lockbox', 'lockboxes').
inf('safety arch', 'safety arches').
inf('safety harness', 'safety harnesses').
inf('safety catch', 'safety catches').
inf('safety match', 'safety matches').
inf('canvass', 'canvasses').
inf('saltbox', 'saltboxes').
inf('salt mine', 'salt ours').
inf('sanctuary', 'sanctuaries').
inf('sandbox', 'sandboxes').
inf('sandbox', 'sandboxes').
inf('sandglass', 'sandglasses').
inf('Kotex', 'Kotexes').
inf('sash', 'sashes').
inf('window sash', 'window sashes').
inf('satin stitch', 'satin stitches').
inf('money box', 'money boxes').
inf('sawed-off shotgun', 'saweds-off shotgun').
inf('sawtooth', 'sawteeth').
inf('sax', 'saxes').
inf('scapulary', 'scapularies').
inf('straw man', 'straw men').
inf('strawman', 'strawmen').
inf('scarf', 'scarves').
inf('scarf', 'scarves').
inf('scenery', 'sceneries').
inf('pair of scissors', 'pairs of scissors').
inf('scoinson arch', 'scoinson arches').
inf('sconcheon arch', 'sconcheon arches').
inf('screw wrench', 'screw wrenches').
inf('scrub brush', 'scrub brushes').
inf('scrubbing brush', 'scrubbing brushes').
inf('scullery', 'sculleries').
inf('sealing wax', 'sealing waxes').
inf('secondary', 'secondaries').
inf('second balcony', 'second balconies').
inf('upper balcony', 'upper balconies').
inf('peanut gallery', 'peanut galleries').
inf('secretary', 'secretaries').
inf('security', 'securities').
inf('segmental arch', 'segmental arches').
inf('selector switch', 'selector switches').
inf('synchro', 'synchroes').
inf('semigloss', 'semiglosses').
inf('sentry box', 'sentry boxes').
inf('serving dish', 'serving dishes').
inf('servo', 'servoes').
inf('stitchery', 'stitcheries').
inf('sewing stitch', 'sewing stitches').
inf('embroidery stitch', 'embroidery stitches').
inf('sgraffito', 'sgraffiti').
inf('shadow box', 'shadow boxes').
inf('shaving brush', 'shaving brushes').
inf('weaver\'s hitch', 'weaver\'s hitches').
inf('shelf', 'shelves').
inf('shellac varnish', 'shellac varnishes').
inf('shell stitch', 'shell stitches').
inf('shirtdress', 'shirtdresses').
inf('shoebox', 'shoeboxes').
inf('shoebox', 'shoeboxes').
inf('shoofly', 'shooflies').
inf('shooting gallery', 'shooting galleries').
inf('shooting gallery', 'shooting galleries').
inf('shooting box', 'shooting boxes').
inf('shot glass', 'shot glasses').
inf('pony', 'ponies').
inf('shouldered arch', 'shouldered arches').
inf('shoulder patch', 'shoulder patches').
inf('bypass', 'bypasses').
inf('signal box', 'signal boxes').
inf('Silex', 'Silexes').
inf('silo', 'siloes').
inf('silo', 'siloes').
inf('silver mine', 'silver ours').
inf('single stitch', 'single stitches').
inf('six', 'sixes').
inf('skeen arch', 'skeen arches').
inf('skene arch', 'skene arches').
inf('scheme arch', 'scheme arches').
inf('diminished arch', 'diminished arches').
inf('sketch', 'sketches').
inf('study', 'studies').
inf('skew arch', 'skew arches').
inf('skirt of tasses', 'skirts of tasses').
inf('skybox', 'skyboxes').
inf('slip of paper', 'slips of paper').
inf('slip clutch', 'slip clutches').
inf('slip friction clutch', 'slip friction clutches').
inf('slip coach', 'slip coaches').
inf('slip stitch', 'slip stitches').
inf('slit trench', 'slit trenches').
inf('sloop of war', 'sloops of war').
inf('slow match', 'slow matches').
inf('smeltery', 'smelteries').
inf('brandy glass', 'brandy glasses').
inf('snowman', 'snowmen').
inf('snuffbox', 'snuffboxes').
inf('max', 'maxes').
inf('liquid ecstasy', 'liquid ecstasies').
inf('soapbox', 'soapboxes').
inf('soap dish', 'soap dishes').
inf('socket wrench', 'socket wrenches').
inf('soddy', 'soddies').
inf('couch', 'couches').
inf('solar battery', 'solar batteries').
inf('solar dish', 'solar dishes').
inf('sombrero', 'sombreroes').
inf('soundbox', 'soundboxes').
inf('body', 'bodies').
inf('source of illumination', 'sources of illumination').
inf('sordino', 'sordinoes').
inf('space laboratory', 'space laboratories').
inf('spandex', 'spandexes').
inf('sparkplug wrench', 'sparkplug wrenches').
inf('speakeasy', 'speakeasies').
inf('sphinx', 'sphinges').
inf('spinning jenny', 'spinning jennies').
inf('sport utility', 'sport utilities').
inf('spring mattress', 'spring mattresses').
inf('squawk box', 'squawk boxes').
inf('squelch', 'squelches').
inf('squinch', 'squinches').
inf('saddlery', 'saddleries').
inf('stagecoach', 'stagecoaches').
inf('standby', 'standbies').
inf('standing press', 'standing presses').
inf('St. Andrew\'s cross', 'St. Andrew\'s crosses').
inf('Station of the Cross', 'Stations of the Cross').
inf('Statue of Liberty', 'Statues of Liberty').
inf('steak knife', 'steak knives').
inf('steamship company', 'steamship companies').
inf('steel factory', 'steel factories').
inf('stereophony', 'stereophonies').
inf('stiletto', 'stilettoes').
inf('still life', 'still lives').
inf('Stillson wrench', 'Stillson wrenches').
inf('stitch', 'stitches').
inf('inventory', 'inventories').
inf('stockinette stitch', 'stockinette stitches').
inf('stock-in-trade', 'stocks-in-trade').
inf('stogy', 'stogies').
inf('stopwatch', 'stopwatches').
inf('stop watch', 'stop watches').
inf('storage battery', 'storage batteries').
inf('storm sash', 'storm sashes').
inf('strapless', 'straplesses').
inf('streamer fly', 'streamer flies').
inf('stretch', 'stretches').
inf('strip mine', 'strip ours').
inf('strongbox', 'strongboxes').
inf('deedbox', 'deedboxes').
inf('fastness', 'fastnesses').
inf('studio couch', 'studio couches').
inf('study', 'studies').
inf('stuffing box', 'stuffing boxes').
inf('packing box', 'packing boxes').
inf('sty', 'sties').
inf('pigsty', 'pigsties').
inf('sub-assembly', 'sub-assemblies').
inf('submarine torpedo', 'submarine torpedoes').
inf('sudatory', 'sudatories').
inf('sudatory', 'sudatories').
inf('sugar refinery', 'sugar refineries').
inf('suit of clothes', 'suits of clothes').
inf('sulky', 'sulkies').
inf('sulphur mine', 'sulphur ours').
inf('sulfur mine', 'sulfur ours').
inf('totality', 'totalities').
inf('sundress', 'sundresses').
inf('sunglass', 'sunglasses').
inf('sun porch', 'sun porches').
inf('sunporch', 'sunporches').
inf('suppository', 'suppositories').
inf('surface-to-air missile', 'surfaces-to-air missile').
inf('surface-to-air missile system', 'surfaces-to-air missile system').
inf('surgery', 'surgeries').
inf('surgical knife', 'surgical knives').
inf('suspensory', 'suspensories').
inf('swamp buggy', 'swamp buggies').
inf('marsh buggy', 'marsh buggies').
inf('swatch', 'swatches').
inf('sweatbox', 'sweatboxes').
inf('sweatbox', 'sweatboxes').
inf('switch', 'switches').
inf('switch', 'switches').
inf('electric switch', 'electric switches').
inf('electrical switch', 'electrical switches').
inf('switch', 'switches').
inf('switch', 'switches').
inf('switchblade knife', 'switchblade knives').
inf('flick-knife', 'flick-knives').
inf('flick knife', 'flick knives').
inf('S wrench', 'S wrenches').
inf('synchroflash', 'synchroflashes').
inf('synchromesh', 'synchromeshes').
inf('rotary', 'rotaries').
inf('table knife', 'table knives').
inf('napery', 'naperies').
inf('tach', 'taches').
inf('tail assembly', 'tail assemblies').
inf('tammy', 'tammies').
inf('tammy', 'tammies').
inf('Tampax', 'Tampaxes').
inf('tannery', 'tanneries').
inf('tapestry', 'tapestries').
inf('tapestry', 'tapestries').
inf('tap wrench', 'tap wrenches').
inf('tau cross', 'tau crosses').
inf('St. Anthony\'s cross', 'St. Anthony\'s crosses').
inf('teddy', 'teddies').
inf('telegraphy', 'telegraphies').
inf('call box', 'call boxes').
inf('telephone box', 'telephone boxes').
inf('telephoto', 'telephotoes').
inf('telex', 'telexes').
inf('idiot box', 'idiot boxes').
inf('telly', 'tellies').
inf('goggle box', 'goggle boxes').
inf('Temple of Apollo', 'Temples of Apollo').
inf('Oracle of Apollo', 'Oracles of Apollo').
inf('oracle of Delphi', 'oracles of Delphi').
inf('Temple of Artemis', 'Temples of Artemis').
inf('Temple of Jerusalem', 'Temples of Jerusalem').
inf('Temple of Solomon', 'Temples of Solomon').
inf('patch', 'patches').
inf('tent-fly', 'tent-flies').
inf('rainfly', 'rainflies').
inf('fly', 'flies').
inf('terry', 'terries').
inf('testudo', 'testudoes').
inf('thatch', 'thatches').
inf('three-centered arch', 'three-centered arches').
inf('basket-handle arch', 'basket-handle arches').
inf('three-way switch', 'three-way switches').
inf('three-point switch', 'three-point switches').
inf('thumb index', 'thumb indexes').
inf('tidy', 'tidies').
inf('timber hitch', 'timber hitches').
inf('time-switch', 'time-switches').
inf('tincture of iodine', 'tinctures of iodine').
inf('tinderbox', 'tinderboxes').
inf('tobacco', 'tobaccoes').
inf('baccy', 'baccies').
inf('tobacco pouch', 'tobacco pouches').
inf('toby', 'tobies').
inf('toe box', 'toe boxes').
inf('toggle switch', 'toggle switches').
inf('on-off switch', 'ons-off switch').
inf('on/off switch', 'on/off switches').
inf('lavatory', 'lavatories').
inf('privy', 'privies').
inf('potty', 'potties').
inf('toiletry', 'toiletries').
inf('pair of tongs', 'pairs of tongs').
inf('toolbox', 'toolboxes').
inf('tooth', 'teeth').
inf('tooth', 'teeth').
inf('toothbrush', 'toothbrushes').
inf('topiary', 'topiaries').
inf('top of the line', 'tops of the line').
inf('torch', 'torches').
inf('torpedo', 'torpedoes').
inf('torpedo', 'torpedoes').
inf('torpedo', 'torpedoes').
inf('torpedo', 'torpedoes').
inf('torque wrench', 'torque wrenches').
inf('tourist class', 'tourist classes').
inf('third class', 'third classes').
inf('Tower of Babel', 'Towers of Babel').
inf('Tower of London', 'Towers of London').
inf('Tower of Pharos', 'Towers of Pharos').
inf('toy box', 'toy boxes').
inf('tracery', 'traceries').
inf('rotary', 'rotaries').
inf('articulated lorry', 'articulated lorries').
inf('transdermal patch', 'transdermal patches').
inf('skin patch', 'skin patches').
inf('treasury', 'treasuries').
inf('trefoil arch', 'trefoil arches').
inf('trench', 'trenches').
inf('trench', 'trenches').
inf('trench knife', 'trench knives').
inf('trimmer arch', 'trimmer arches').
inf('triptych', 'triptyches').
inf('triumphal arch', 'triumphal arches').
inf('triviality', 'trivialities').
inf('trolley coach', 'trolley coaches').
inf('trophy', 'trophies').
inf('trouser press', 'trouser presses').
inf('trousseau', 'trousseaux').
inf('trumpet arch', 'trumpet arches').
inf('billy', 'billies').
inf('truss', 'trusses').
inf('truss', 'trusses').
inf('tubeless', 'tubelesses').
inf('tuck box', 'tuck boxes').
inf('Tudor arch', 'Tudor arches').
inf('four-centered arch', 'four-centered arches').
inf('turnery', 'turneries').
inf('turnery', 'turneries').
inf('twenty-two', 'twenty-twoes').
inf('two-by-four', 'twoes-by-four').
inf('underpass', 'underpasses').
inf('university', 'universities').
inf('University of California at Berkeley', 'Universitys of California at Berkeley').
inf('University of Chicago', 'Universitys of Chicago').
inf('University of Michigan', 'Universitys of Michigan').
inf('University of Nebraska', 'Universitys of Nebraska').
inf('University of North Carolina', 'Universitys of North Carolina').
inf('University of Pennsylvania', 'Universitys of Pennsylvania').
inf('University of Pittsburgh', 'Universitys of Pittsburgh').
inf('University of Sussex', 'Universitys of Sussex').
inf('University of Texas', 'Universitys of Texas').
inf('University of Vermont', 'Universitys of Vermont').
inf('University of Washington', 'Universitys of Washington').
inf('University of West Virginia', 'Universitys of West Virginia').
inf('University of Wisconsin', 'Universitys of Wisconsin').
inf('upholstery', 'upholsteries').
inf('upright piano', 'upright pianoes').
inf('utility', 'utilities').
inf('valve-in-head engine', 'valves-in-head engine').
inf('varnish', 'varnishes').
inf('Velcro', 'Velcroes').
inf('Venetian glass', 'Venetian glasses').
inf('ventriloquist\'s dummy', 'ventriloquist\'s dummies').
inf('gallery', 'galleries').
inf('Palace of Versailles', 'Palaces of Versailles').
inf('vestry', 'vestries').
inf('sacristy', 'sacristies').
inf('vinery', 'vineries').
inf('pair of virginals', 'pairs of virginals').
inf('virtual memory', 'virtual memories').
inf('visible speech', 'visible speeches').
inf('voltaic battery', 'voltaic batteries').
inf('galvanic battery', 'galvanic batteries').
inf('vomitory', 'vomitories').
inf('dado', 'dadoes').
inf('walky-talky', 'walky-talkies').
inf('Walkman', 'Walkmen').
inf('press', 'presses').
inf('wash', 'washes').
inf('wash', 'washes').
inf('lavabo', 'lavaboes').
inf('lavatory', 'lavatories').
inf('watch', 'watches').
inf('watch glass', 'watch glasses').
inf('water glass', 'water glasses').
inf('water glass', 'water glasses').
inf('water glass', 'water glasses').
inf('water supply', 'water supplies').
inf('weapon of mass destruction', 'weapons of mass destruction').
inf('weaponry', 'weaponries').
inf('implements of war', 'implementss of war').
inf('weatherglass', 'weatherglasses').
inf('wet fly', 'wet flies').
inf('wherry', 'wherries').
inf('wherry', 'wherries').
inf('Norfolk wherry', 'Norfolk wherries').
inf('whipstitch', 'whipstitches').
inf('whispering gallery', 'whispering galleries').
inf('flag of truce', 'flags of truce').
inf('whitewash', 'whitewashes').
inf('bordello', 'bordelloes').
inf('house of prostitution', 'houses of prostitution').
inf('house of ill repute', 'houses of ill repute').
inf('wide-body', 'wide-bodies').
inf('winch', 'winches').
inf('windlass', 'windlasses').
inf('wind energy facility', 'wind energy facilities').
inf('window box', 'window boxes').
inf('wineglass', 'wineglasses').
inf('winepress', 'winepresses').
inf('winery', 'wineries').
inf('wireless fidelity', 'wireless fidelities').
inf('witness box', 'witness boxes').
inf('piece of work', 'pieces of work').
inf('workbox', 'workboxes').
inf('workbench', 'workbenches').
inf('work bench', 'work benches').
inf('bench', 'benches').
inf('work in progress', 'works in progress').
inf('work of art', 'works of art').
inf('wrench', 'wrenches').
inf('wristwatch', 'wristwatches').
inf('wrist watch', 'wrist watches').
inf('Xerox', 'Xeroxes').
inf('xerox', 'xeroxes').
inf('xerox copy', 'xerox copies').
inf('dandy', 'dandies').
inf('yo-yo', 'yo-yoes').
inf('zarf', 'zarves').
inf('zero', 'zeroes').
inf('personality', 'personalities').
inf('identity', 'identities').
inf('personal identity', 'personal identities').
inf('individuality', 'individualities').
inf('gender identity', 'gender identities').
inf('personableness', 'personablenesses').
inf('anal personality', 'anal personalities').
inf('anal retentive personality', 'anal retentive personalities').
inf('genital personality', 'genital personalities').
inf('narcissistic personality', 'narcissistic personalities').
inf('obsessive-compulsive personality', 'obsessive-compulsive personalities').
inf('oral personality', 'oral personalities').
inf('outwardness', 'outwardnesses').
inf('inwardness', 'inwardnesses').
inf('internality', 'internalities').
inf('spirituality', 'spiritualities').
inf('otherworldliness', 'otherworldlinesses').
inf('worldliness', 'worldlinesses').
inf('aloneness', 'alonenesses').
inf('loneliness', 'lonelinesses').
inf('lonesomeness', 'lonesomenesses').
inf('solitariness', 'solitarinesses').
inf('friendlessness', 'friendlessnesses').
inf('reclusiveness', 'reclusivenesses').
inf('privacy', 'privacies').
inf('privateness', 'privatenesses').
inf('animality', 'animalities').
inf('physicality', 'physicalities').
inf('bloodiness', 'bloodinesses').
inf('bloodthirstiness', 'bloodthirstinesses').
inf('nervousness', 'nervousnesses').
inf('restlessness', 'restlessnesses').
inf('uneasiness', 'uneasinesses').
inf('queasiness', 'queasinesses').
inf('skittishness', 'skittishnesses').
inf('restiveness', 'restivenesses').
inf('compulsiveness', 'compulsivenesses').
inf('compulsivity', 'compulsivities').
inf('obsessiveness', 'obsessivenesses').
inf('obsessivity', 'obsessivities').
inf('emotionality', 'emotionalities').
inf('demonstrativeness', 'demonstrativenesses').
inf('affectionateness', 'affectionatenesses').
inf('fondness', 'fondnesses').
inf('lovingness', 'lovingnesses').
inf('tenderness', 'tendernesses').
inf('uxoriousness', 'uxoriousnesses').
inf('mawkishness', 'mawkishnesses').
inf('sentimentality', 'sentimentalities').
inf('drippiness', 'drippinesses').
inf('mushiness', 'mushinesses').
inf('soupiness', 'soupinesses').
inf('sloppiness', 'sloppinesses').
inf('fieriness', 'fierinesses').
inf('moodiness', 'moodinesses').
inf('excitability', 'excitabilities').
inf('excitableness', 'excitablenesses').
inf('volatility', 'volatilities').
inf('unemotionality', 'unemotionalities').
inf('emotionlessness', 'emotionlessnesses').
inf('blandness', 'blandnesses').
inf('coldness', 'coldnesses').
inf('coolness', 'coolnesses').
inf('frigidity', 'frigidities').
inf('frigidness', 'frigidnesses').
inf('iciness', 'icinesses').
inf('chilliness', 'chillinesses').
inf('dispassionateness', 'dispassionatenesses').
inf('dryness', 'drynesses').
inf('stolidity', 'stolidities').
inf('stolidness', 'stolidnesses').
inf('tepidness', 'tepidnesses').
inf('lukewarmness', 'lukewarmnesses').
inf('cheerfulness', 'cheerfulnesses').
inf('sunniness', 'sunninesses').
inf('good-temperedness', 'good-temperednesses').
inf('good-humoredness', 'good-humorednesses').
inf('good-humouredness', 'good-humourednesses').
inf('good-naturedness', 'good-naturednesses').
inf('uncheerfulness', 'uncheerfulnesses').
inf('gloominess', 'gloominesses').
inf('lugubriousness', 'lugubriousnesses').
inf('sadness', 'sadnesses').
inf('spiritedness', 'spiritednesses').
inf('chirpiness', 'chirpinesses').
inf('liveliness', 'livelinesses').
inf('life', 'lives').
inf('sprightliness', 'sprightlinesses').
inf('pertness', 'pertnesses').
inf('airiness', 'airinesses').
inf('delicacy', 'delicacies').
inf('alacrity', 'alacrities').
inf('briskness', 'brisknesses').
inf('smartness', 'smartnesses').
inf('energy', 'energies').
inf('muscularity', 'muscularities').
inf('vitality', 'vitalities').
inf('breeziness', 'breezinesses').
inf('jauntiness', 'jauntinesses').
inf('irrepressibility', 'irrepressibilities').
inf('buoyancy', 'buoyancies').
inf('high-spiritedness', 'high-spiritednesses').
inf('vivacity', 'vivacities').
inf('mettlesomeness', 'mettlesomenesses').
inf('peppiness', 'peppinesses').
inf('activeness', 'activenesses').
inf('activity', 'activities').
inf('inactiveness', 'inactivenesses').
inf('inactivity', 'inactivities').
inf('lethargy', 'lethargies').
inf('sluggishness', 'sluggishnesses').
inf('flatness', 'flatnesses').
inf('restfulness', 'restfulnesses').
inf('passivity', 'passivities').
inf('passiveness', 'passivenesses').
inf('apathy', 'apathies').
inf('numbness', 'numbnesses').
inf('spiritlessness', 'spiritlessnesses').
inf('listlessness', 'listlessnesses').
inf('torpidity', 'torpidities').
inf('torpidness', 'torpidnesses').
inf('laziness', 'lazinesses').
inf('idleness', 'idlenesses').
inf('slothfulness', 'slothfulnesses').
inf('shiftlessness', 'shiftlessnesses').
inf('permissiveness', 'permissivenesses').
inf('leniency', 'leniencies').
inf('softness', 'softnesses').
inf('unpermissiveness', 'unpermissivenesses').
inf('restrictiveness', 'restrictivenesses').
inf('sternness', 'sternnesses').
inf('strictness', 'strictnesses').
inf('severity', 'severities').
inf('severeness', 'severenesses').
inf('harshness', 'harshnesses').
inf('rigorousness', 'rigorousnesses').
inf('rigourousness', 'rigourousnesses').
inf('inclemency', 'inclemencies').
inf('hardness', 'hardnesses').
inf('stiffness', 'stiffnesses').
inf('longanimity', 'longanimities').
inf('easygoingness', 'easygoingnesses').
inf('risibility', 'risibilities').
inf('agreeableness', 'agreeablenesses').
inf('agreeability', 'agreeabilities').
inf('compliancy', 'compliancies').
inf('obligingness', 'obligingnesses').
inf('crabbiness', 'crabbinesses').
inf('crabbedness', 'crabbednesses').
inf('crossness', 'crossnesses').
inf('crankiness', 'crankinesses').
inf('crotchetiness', 'crotchetinesses').
inf('contrariness', 'contrarinesses').
inf('grumpiness', 'grumpinesses').
inf('sulkiness', 'sulkinesses').
inf('sullenness', 'sullennesses').
inf('moroseness', 'morosenesses').
inf('sourness', 'sournesses').
inf('biliousness', 'biliousnesses').
inf('irritability', 'irritabilities').
inf('peevishness', 'peevishnesses').
inf('pettishness', 'pettishnesses').
inf('snappishness', 'snappishnesses').
inf('surliness', 'surlinesses').
inf('shrewishness', 'shrewishnesses').
inf('querulousness', 'querulousnesses').
inf('asperity', 'asperities').
inf('sharpness', 'sharpnesses').
inf('disagreeableness', 'disagreeablenesses').
inf('bitterness', 'bitternesses').
inf('acrimony', 'acrimonies').
inf('acerbity', 'acerbities').
inf('tartness', 'tartnesses').
inf('thorniness', 'thorninesses').
inf('aggressiveness', 'aggressivenesses').
inf('pugnacity', 'pugnacities').
inf('bellicosity', 'bellicosities').
inf('bellicoseness', 'bellicosenesses').
inf('quarrelsomeness', 'quarrelsomenesses').
inf('contentiousness', 'contentiousnesses').
inf('truculency', 'truculencies').
inf('litigiousness', 'litigiousnesses').
inf('willingness', 'willingnesses').
inf('readiness', 'readinesses').
inf('eagerness', 'eagernesses').
inf('forwardness', 'forwardnesses').
inf('receptiveness', 'receptivenesses').
inf('receptivity', 'receptivities').
inf('openness', 'opennesses').
inf('wholeheartedness', 'wholeheartednesses').
inf('unwillingness', 'unwillingnesses').
inf('involuntariness', 'involuntarinesses').
inf('hesitancy', 'hesitancies').
inf('seriousness', 'seriousnesses').
inf('earnestness', 'earnestnesses').
inf('serious-mindedness', 'serious-mindednesses').
inf('sincerity', 'sincerities').
inf('committedness', 'committednesses').
inf('graveness', 'gravenesses').
inf('gravity', 'gravities').
inf('sobriety', 'sobrieties').
inf('soberness', 'sobernesses').
inf('somberness', 'sombernesses').
inf('sombreness', 'sombrenesses').
inf('sedateness', 'sedatenesses').
inf('staidness', 'staidnesses').
inf('solemnity', 'solemnities').
inf('solemness', 'solemnesses').
inf('stodginess', 'stodginesses').
inf('stuffiness', 'stuffinesses').
inf('frivolity', 'frivolities').
inf('frivolousness', 'frivolousnesses').
inf('giddiness', 'giddinesses').
inf('silliness', 'sillinesses').
inf('lightsomeness', 'lightsomenesses').
inf('lightness', 'lightnesses').
inf('levity', 'levities').
inf('flippancy', 'flippancies').
inf('light-mindedness', 'light-mindednesses').
inf('jocoseness', 'jocosenesses').
inf('jocosity', 'jocosities').
inf('merriness', 'merrinesses').
inf('humorousness', 'humorousnesses').
inf('playfulness', 'playfulnesses').
inf('facetiousness', 'facetiousnesses').
inf('perkiness', 'perkinesses').
inf('pertness', 'pertnesses').
inf('sauciness', 'saucinesses').
inf('archness', 'archnesses').
inf('friskiness', 'friskinesses').
inf('frolicsomeness', 'frolicsomenesses').
inf('sportiveness', 'sportivenesses').
inf('impishness', 'impishnesses').
inf('mischievousness', 'mischievousnesses').
inf('puckishness', 'puckishnesses').
inf('whimsicality', 'whimsicalities').
inf('sense of humor', 'senses of humor').
inf('sense of humour', 'senses of humour').
inf('communicativeness', 'communicativenesses').
inf('frankness', 'franknesses').
inf('outspokenness', 'outspokennesses').
inf('bluffness', 'bluffnesses').
inf('effusiveness', 'effusivenesses').
inf('expansiveness', 'expansivenesses').
inf('expansivity', 'expansivities').
inf('fluency', 'fluencies').
inf('volubility', 'volubilities').
inf('articulateness', 'articulatenesses').
inf('garrulity', 'garrulities').
inf('garrulousness', 'garrulousnesses').
inf('loquaciousness', 'loquaciousnesses').
inf('loquacity', 'loquacities').
inf('talkativeness', 'talkativenesses').
inf('leresis', 'lereses').
inf('uncommunicativeness', 'uncommunicativenesses').
inf('muteness', 'mutenesses').
inf('secrecy', 'secrecies').
inf('secretiveness', 'secretivenesses').
inf('taciturnity', 'taciturnities').
inf('sociality', 'socialities').
inf('sociability', 'sociabilities').
inf('sociableness', 'sociablenesses').
inf('conviviality', 'convivialities').
inf('joviality', 'jovialities').
inf('companionability', 'companionabilities').
inf('companionableness', 'companionablenesses').
inf('chumminess', 'chumminesses').
inf('comradeliness', 'comradelinesses').
inf('comradery', 'comraderies').
inf('gregariousness', 'gregariousnesses').
inf('openness', 'opennesses').
inf('nakedness', 'nakednesses').
inf('friendliness', 'friendlinesses').
inf('affability', 'affabilities').
inf('affableness', 'affablenesses').
inf('amiability', 'amiabilities').
inf('amiableness', 'amiablenesses').
inf('geniality', 'genialities').
inf('amicability', 'amicabilities').
inf('amicableness', 'amicablenesses').
inf('condescendingness', 'condescendingnesses').
inf('familiarity', 'familiarities').
inf('intimacy', 'intimacies').
inf('closeness', 'closenesses').
inf('approachability', 'approachabilities').
inf('accessibility', 'accessibilities').
inf('congeniality', 'congenialities').
inf('amity', 'amities').
inf('cordiality', 'cordialities').
inf('neighborliness', 'neighborlinesses').
inf('neighbourliness', 'neighbourlinesses').
inf('good-neighborliness', 'good-neighborlinesses').
inf('good-neighbourliness', 'good-neighbourlinesses').
inf('hospitableness', 'hospitablenesses').
inf('mellowness', 'mellownesses').
inf('unsociability', 'unsociabilities').
inf('unsociableness', 'unsociablenesses').
inf('aloofness', 'aloofnesses').
inf('remoteness', 'remotenesses').
inf('standoffishness', 'standoffishnesses').
inf('withdrawnness', 'withdrawnnesses').
inf('unapproachability', 'unapproachabilities').
inf('closeness', 'closenesses').
inf('secretiveness', 'secretivenesses').
inf('furtiveness', 'furtivenesses').
inf('sneakiness', 'sneakinesses').
inf('stealthiness', 'stealthinesses').
inf('unfriendliness', 'unfriendlinesses').
inf('hostility', 'hostilities').
inf('virulency', 'virulencies').
inf('misanthropy', 'misanthropies').
inf('uncongeniality', 'uncongenialities').
inf('unneighborliness', 'unneighborlinesses').
inf('inhospitableness', 'inhospitablenesses').
inf('adaptability', 'adaptabilities').
inf('flexibility', 'flexibilities').
inf('flexibleness', 'flexiblenesses').
inf('pliability', 'pliabilities').
inf('pliancy', 'pliancies').
inf('pliantness', 'pliantnesses').
inf('suppleness', 'supplenesses').
inf('unadaptability', 'unadaptabilities').
inf('inflexibility', 'inflexibilities').
inf('rigidity', 'rigidities').
inf('rigidness', 'rigidnesses').
inf('thoughtfulness', 'thoughtfulnesses').
inf('pensiveness', 'pensivenesses').
inf('meditativeness', 'meditativenesses').
inf('contemplativeness', 'contemplativenesses').
inf('introspectiveness', 'introspectivenesses').
inf('deliberateness', 'deliberatenesses').
inf('intentionality', 'intentionalities').
inf('reflectiveness', 'reflectivenesses').
inf('reflectivity', 'reflectivities').
inf('unthoughtfulness', 'unthoughtfulnesses').
inf('thoughtlessness', 'thoughtlessnesses').
inf('recklessness', 'recklessnesses').
inf('foolhardiness', 'foolhardinesses').
inf('rashness', 'rashnesses').
inf('brashness', 'brashnesses').
inf('impulsiveness', 'impulsivenesses').
inf('impetuousness', 'impetuousnesses').
inf('impetuosity', 'impetuosities').
inf('hastiness', 'hastinesses').
inf('attentiveness', 'attentivenesses').
inf('attentiveness', 'attentivenesses').
inf('inattentiveness', 'inattentivenesses').
inf('carefulness', 'carefulnesses').
inf('mindfulness', 'mindfulnesses').
inf('heedfulness', 'heedfulnesses').
inf('cautiousness', 'cautiousnesses').
inf('carefulness', 'carefulnesses').
inf('wariness', 'warinesses').
inf('chariness', 'charinesses').
inf('alertness', 'alertnesses').
inf('sharp-sightedness', 'sharp-sightednesses').
inf('watchfulness', 'watchfulnesses').
inf('carelessness', 'carelessnesses').
inf('sloppiness', 'sloppinesses').
inf('incautiousness', 'incautiousnesses').
inf('unwariness', 'unwarinesses').
inf('unmindfulness', 'unmindfulnesses').
inf('heedlessness', 'heedlessnesses').
inf('inadvertency', 'inadvertencies').
inf('neglectfulness', 'neglectfulnesses').
inf('delinquency', 'delinquencies').
inf('laxness', 'laxnesses').
inf('laxity', 'laxities').
inf('remissness', 'remissnesses').
inf('slackness', 'slacknesses').
inf('masculinity', 'masculinities').
inf('manfulness', 'manfulnesses').
inf('manliness', 'manlinesses').
inf('virility', 'virilities').
inf('boyishness', 'boyishnesses').
inf('machismo', 'machismoes').
inf('tomboyishness', 'tomboyishnesses').
inf('femininity', 'femininities').
inf('muliebrity', 'muliebrities').
inf('womanliness', 'womanlinesses').
inf('ladylikeness', 'ladylikenesses').
inf('maidenliness', 'maidenlinesses').
inf('girlishness', 'girlishnesses').
inf('effeminacy', 'effeminacies').
inf('effeminateness', 'effeminatenesses').
inf('sissiness', 'sissinesses').
inf('softness', 'softnesses').
inf('womanishness', 'womanishnesses').
inf('unmanliness', 'unmanlinesses').
inf('trustworthiness', 'trustworthinesses').
inf('trustiness', 'trustinesses').
inf('creditworthiness', 'creditworthinesses').
inf('responsibility', 'responsibilities').
inf('responsibleness', 'responsiblenesses').
inf('accountability', 'accountabilities').
inf('answerability', 'answerabilities').
inf('answerableness', 'answerablenesses').
inf('dependability', 'dependabilities').
inf('dependableness', 'dependablenesses').
inf('reliability', 'reliabilities').
inf('reliableness', 'reliablenesses').
inf('untrustworthiness', 'untrustworthinesses').
inf('untrustiness', 'untrustinesses').
inf('irresponsibility', 'irresponsibilities').
inf('irresponsibleness', 'irresponsiblenesses').
inf('solidity', 'solidities').
inf('solidness', 'solidnesses').
inf('undependability', 'undependabilities').
inf('undependableness', 'undependablenesses').
inf('unreliability', 'unreliabilities').
inf('unreliableness', 'unreliablenesses').
inf('flightiness', 'flightinesses').
inf('arbitrariness', 'arbitrarinesses').
inf('whimsicality', 'whimsicalities').
inf('whimsy', 'whimsies').
inf('capriciousness', 'capriciousnesses').
inf('carefreeness', 'carefreenesses').
inf('conscientiousness', 'conscientiousnesses').
inf('painstakingness', 'painstakingnesses').
inf('meticulousness', 'meticulousnesses').
inf('meticulosity', 'meticulosities').
inf('punctiliousness', 'punctiliousnesses').
inf('scrupulousness', 'scrupulousnesses').
inf('thoroughness', 'thoroughnesses').
inf('strictness', 'strictnesses').
inf('stringency', 'stringencies').
inf('unconscientiousness', 'unconscientiousnesses').
inf('gloss', 'glosses').
inf('color of law', 'colors of law').
inf('colour of law', 'colours of law').
inf('crisscross', 'crisscrosses').
inf('cross', 'crosses').
inf('hatch', 'hatches').
inf('crosshatch', 'crosshatches').
inf('splash', 'splashes').
inf('patch', 'patches').
inf('hairiness', 'hairinesses').
inf('pilosity', 'pilosities').
inf('hirsuteness', 'hirsutenesses').
inf('hairlessness', 'hairlessnesses').
inf('beauty', 'beauties').
inf('raw beauty', 'raw beauties').
inf('glory', 'glories').
inf('resplendency', 'resplendencies').
inf('exquisiteness', 'exquisitenesses').
inf('picturesqueness', 'picturesquenesses').
inf('pleasingness', 'pleasingnesses').
inf('comeliness', 'comelinesses').
inf('fairness', 'fairnesses').
inf('loveliness', 'lovelinesses').
inf('beauteousness', 'beauteousnesses').
inf('prettiness', 'prettinesses').
inf('cuteness', 'cutenesses').
inf('handsomeness', 'handsomenesses').
inf('attractiveness', 'attractivenesses').
inf('adorability', 'adorabilities').
inf('adorableness', 'adorablenesses').
inf('bewitchery', 'bewitcheries').
inf('curvaceousness', 'curvaceousnesses').
inf('shapeliness', 'shapelinesses').
inf('voluptuousness', 'voluptuousnesses').
inf('desirability', 'desirabilities').
inf('desirableness', 'desirablenesses').
inf('sultriness', 'sultrinesses').
inf('appealingness', 'appealingnesses').
inf('winsomeness', 'winsomenesses').
inf('associability', 'associabilities').
inf('associableness', 'associablenesses').
inf('attractiveness', 'attractivenesses').
inf('affinity', 'affinities').
inf('temptingness', 'temptingnesses').
inf('ugliness', 'uglinesses').
inf('unsightliness', 'unsightlinesses').
inf('grotesqueness', 'grotesquenesses').
inf('grotesquery', 'grotesqueries').
inf('garishness', 'garishnesses').
inf('gaudiness', 'gaudinesses').
inf('unpleasingness', 'unpleasingnesses').
inf('hideousness', 'hideousnesses').
inf('deformity', 'deformities').
inf('unattractiveness', 'unattractivenesses').
inf('homeliness', 'homelinesses').
inf('plainness', 'plainnesses').
inf('shapelessness', 'shapelessnesses').
inf('blemish', 'blemishes').
inf('scratch', 'scratches').
inf('smirch', 'smirches').
inf('blotch', 'blotches').
inf('splotch', 'splotches').
inf('scorch', 'scorches').
inf('tarnish', 'tarnishes').
inf('strawberry', 'strawberries').
inf('hemangioma simplex', 'hemangioma simplexes').
inf('plainness', 'plainnesses').
inf('chasteness', 'chastenesses').
inf('simplicity', 'simplicities').
inf('simpleness', 'simplenesses').
inf('austereness', 'austerenesses').
inf('severity', 'severities').
inf('severeness', 'severenesses').
inf('bareness', 'barenesses').
inf('starkness', 'starknesses').
inf('ornateness', 'ornatenesses').
inf('elaborateness', 'elaboratenesses').
inf('baroqueness', 'baroquenesses').
inf('rococo', 'rococoes').
inf('floridness', 'floridnesses').
inf('floridity', 'floridities').
inf('showiness', 'showinesses').
inf('fussiness', 'fussinesses').
inf('decorativeness', 'decorativenesses').
inf('finish', 'finishes').
inf('clearness', 'clearnesses').
inf('clarity', 'clarities').
inf('uncloudedness', 'uncloudednesses').
inf('pellucidness', 'pellucidnesses').
inf('pellucidity', 'pellucidities').
inf('limpidity', 'limpidities').
inf('transparency', 'transparencies').
inf('transparentness', 'transparentnesses').
inf('translucency', 'translucencies').
inf('semitransparency', 'semitransparencies').
inf('visibility', 'visibilities').
inf('distinctness', 'distinctnesses').
inf('sharpness', 'sharpnesses').
inf('discernability', 'discernabilities').
inf('legibility', 'legibilities').
inf('opacity', 'opacities').
inf('opaqueness', 'opaquenesses').
inf('cloudiness', 'cloudinesses').
inf('murkiness', 'murkinesses').
inf('muddiness', 'muddinesses').
inf('turbidity', 'turbidities').
inf('turbidness', 'turbidnesses').
inf('haziness', 'hazinesses').
inf('mistiness', 'mistinesses').
inf('steaminess', 'steaminesses').
inf('vaporousness', 'vaporousnesses').
inf('vapourousness', 'vapourousnesses').
inf('indistinctness', 'indistinctnesses').
inf('softness', 'softnesses').
inf('blurriness', 'blurrinesses').
inf('fogginess', 'fogginesses').
inf('fuzziness', 'fuzzinesses').
inf('dimness', 'dimnesses').
inf('faintness', 'faintnesses').
inf('vagueness', 'vaguenesses').
inf('divisibility', 'divisibilities').
inf('fissiparity', 'fissiparities').
inf('sharpness', 'sharpnesses').
inf('keenness', 'keennesses').
inf('acuteness', 'acutenesses').
inf('dullness', 'dullnesses').
inf('bluntness', 'bluntnesses').
inf('obtuseness', 'obtusenesses').
inf('conspicuousness', 'conspicuousnesses').
inf('obviousness', 'obviousnesses').
inf('noticeability', 'noticeabilities').
inf('noticeableness', 'noticeablenesses').
inf('patency', 'patencies').
inf('apparentness', 'apparentnesses').
inf('apparency', 'apparencies').
inf('blatancy', 'blatancies').
inf('obtrusiveness', 'obtrusivenesses').
inf('boldness', 'boldnesses').
inf('strikingness', 'strikingnesses').
inf('inconspicuousness', 'inconspicuousnesses').
inf('unnoticeableness', 'unnoticeablenesses').
inf('unobtrusiveness', 'unobtrusivenesses').
inf('easiness', 'easinesses').
inf('simplicity', 'simplicities').
inf('simpleness', 'simplenesses').
inf('effortlessness', 'effortlessnesses').
inf('facility', 'facilities').
inf('readiness', 'readinesses').
inf('smoothness', 'smoothnesses').
inf('difficulty', 'difficulties').
inf('difficultness', 'difficultnesses').
inf('effortfulness', 'effortfulnesses').
inf('arduousness', 'arduousnesses').
inf('strenuousness', 'strenuousnesses').
inf('laboriousness', 'laboriousnesses').
inf('operoseness', 'operosenesses').
inf('toilsomeness', 'toilsomenesses').
inf('asperity', 'asperities').
inf('grimness', 'grimnesses').
inf('severity', 'severities').
inf('severeness', 'severenesses').
inf('rigorousness', 'rigorousnesses').
inf('rigourousness', 'rigourousnesses').
inf('sternness', 'sternnesses').
inf('hardness', 'hardnesses').
inf('ruggedness', 'ruggednesses').
inf('formidability', 'formidabilities').
inf('toughness', 'toughnesses').
inf('burdensomeness', 'burdensomenesses').
inf('heaviness', 'heavinesses').
inf('onerousness', 'onerousnesses').
inf('oppressiveness', 'oppressivenesses').
inf('subtlety', 'subtleties').
inf('niceness', 'nicenesses').
inf('troublesomeness', 'troublesomenesses').
inf('awkwardness', 'awkwardnesses').
inf('cumbersomeness', 'cumbersomenesses').
inf('unwieldiness', 'unwieldinesses').
inf('fly in the ointment', 'flies in the ointment').
inf('unwieldiness', 'unwieldinesses').
inf('combustibility', 'combustibilities').
inf('combustibleness', 'combustiblenesses').
inf('burnability', 'burnabilities').
inf('flammability', 'flammabilities').
inf('inflammability', 'inflammabilities').
inf('compatibility', 'compatibilities').
inf('congenialness', 'congenialnesses').
inf('congeniality', 'congenialities').
inf('harmony', 'harmonies').
inf('harmoniousness', 'harmoniousnesses').
inf('conformity', 'conformities').
inf('justness', 'justnesses').
inf('rightness', 'rightnesses').
inf('nicety', 'niceties').
inf('normality', 'normalities').
inf('congruity', 'congruities').
inf('congruousness', 'congruousnesses').
inf('incompatibility', 'incompatibilities').
inf('incongruity', 'incongruities').
inf('incongruousness', 'incongruousnesses').
inf('irony', 'ironies').
inf('Socratic irony', 'Socratic ironies').
inf('suitability', 'suitabilities').
inf('suitableness', 'suitablenesses').
inf('arability', 'arabilities').
inf('appropriateness', 'appropriatenesses').
inf('felicity', 'felicities').
inf('felicitousness', 'felicitousnesses').
inf('aptness', 'aptnesses').
inf('appositeness', 'appositenesses').
inf('fitness', 'fitnesses').
inf('fittingness', 'fittingnesses').
inf('eligibility', 'eligibilities').
inf('insurability', 'insurabilities').
inf('marriageability', 'marriageabilities').
inf('ineligibility', 'ineligibilities').
inf('uninsurability', 'uninsurabilities').
inf('opportuneness', 'opportunenesses').
inf('patness', 'patnesses').
inf('timeliness', 'timelinesses').
inf('handiness', 'handinesses').
inf('accessibility', 'accessibilities').
inf('availability', 'availabilities').
inf('availableness', 'availablenesses').
inf('impressiveness', 'impressivenesses').
inf('navigability', 'navigabilities').
inf('neediness', 'needinesses').
inf('painfulness', 'painfulnesses').
inf('distressingness', 'distressingnesses').
inf('sharpness', 'sharpnesses').
inf('piquancy', 'piquancies').
inf('piquantness', 'piquantnesses').
inf('publicity', 'publicities').
inf('spinnability', 'spinnabilities').
inf('unsuitability', 'unsuitabilities').
inf('unsuitableness', 'unsuitablenesses').
inf('ineptness', 'ineptnesses').
inf('inaptness', 'inaptnesses').
inf('inappositeness', 'inappositenesses').
inf('inappropriateness', 'inappropriatenesses').
inf('unworthiness', 'unworthinesses').
inf('infelicity', 'infelicities').
inf('habitability', 'habitabilities').
inf('habitableness', 'habitablenesses').
inf('unfitness', 'unfitnesses').
inf('inaccessibility', 'inaccessibilities').
inf('unavailability', 'unavailabilities').
inf('inopportuneness', 'inopportunenesses').
inf('untimeliness', 'untimelinesses').
inf('protectiveness', 'protectivenesses').
inf('quality', 'qualities').
inf('humanness', 'humannesses').
inf('humanity', 'humanities').
inf('quality', 'qualities').
inf('superiority', 'superiorities').
inf('high quality', 'high qualities').
inf('fineness', 'finenesses').
inf('choiceness', 'choicenesses').
inf('admirability', 'admirabilities').
inf('admirableness', 'admirablenesses').
inf('wonderfulness', 'wonderfulnesses').
inf('impressiveness', 'impressivenesses').
inf('grandness', 'grandnesses').
inf('richness', 'richnesses').
inf('expansiveness', 'expansivenesses').
inf('expansivity', 'expansivities').
inf('stateliness', 'statelinesses').
inf('majesty', 'majesties').
inf('loftiness', 'loftinesses').
inf('first class', 'first classes').
inf('ingenuity', 'ingenuities').
inf('ingeniousness', 'ingeniousnesses').
inf('cleverness', 'clevernesses').
inf('inferiority', 'inferiorities').
inf('low quality', 'low qualities').
inf('poorness', 'poornesses').
inf('scrawniness', 'scrawninesses').
inf('scrubbiness', 'scrubbinesses').
inf('second class', 'second classes').
inf('wretchedness', 'wretchednesses').
inf('salability', 'salabilities').
inf('salableness', 'salablenesses').
inf('gaseousness', 'gaseousnesses').
inf('bubbliness', 'bubblinesses').
inf('frothiness', 'frothinesses').
inf('foaminess', 'foaminesses').
inf('changeableness', 'changeablenesses').
inf('changeability', 'changeabilities').
inf('commutability', 'commutabilities').
inf('transmutability', 'transmutabilities').
inf('fluidity', 'fluidities').
inf('fluidness', 'fluidnesses').
inf('reversibility', 'reversibilities').
inf('shiftiness', 'shiftinesses').
inf('inconstancy', 'inconstancies').
inf('changefulness', 'changefulnesses').
inf('capriciousness', 'capriciousnesses').
inf('unpredictability', 'unpredictabilities').
inf('variability', 'variabilities').
inf('variableness', 'variablenesses').
inf('variedness', 'variednesses').
inf('diversity', 'diversities').
inf('exchangeability', 'exchangeabilities').
inf('interchangeability', 'interchangeabilities').
inf('interchangeableness', 'interchangeablenesses').
inf('fungibility', 'fungibilities').
inf('duality', 'dualities').
inf('transferability', 'transferabilities').
inf('convertibility', 'convertibilities').
inf('inconvertibility', 'inconvertibilities').
inf('replaceability', 'replaceabilities').
inf('substitutability', 'substitutabilities').
inf('commutability', 'commutabilities').
inf('liquidity', 'liquidities').
inf('permutability', 'permutabilities').
inf('permutableness', 'permutablenesses').
inf('transposability', 'transposabilities').
inf('progressiveness', 'progressivenesses').
inf('progressivity', 'progressivities').
inf('changelessness', 'changelessnesses').
inf('unchangeability', 'unchangeabilities').
inf('unchangeableness', 'unchangeablenesses').
inf('unchangingness', 'unchangingnesses').
inf('absoluteness', 'absolutenesses').
inf('constancy', 'constancies').
inf('stability', 'stabilities').
inf('metastability', 'metastabilities').
inf('monotony', 'monotonies').
inf('innateness', 'innatenesses').
inf('irreversibility', 'irreversibilities').
inf('invariability', 'invariabilities').
inf('invariableness', 'invariablenesses').
inf('unvariedness', 'unvariednesses').
inf('monotony', 'monotonies').
inf('sameness', 'samenesses').
inf('fixedness', 'fixednesses').
inf('unalterability', 'unalterabilities').
inf('unexchangeability', 'unexchangeabilities').
inf('incommutability', 'incommutabilities').
inf('irreplaceableness', 'irreplaceablenesses').
inf('mutability', 'mutabilities').
inf('mutableness', 'mutablenesses').
inf('alterability', 'alterabilities').
inf('immutability', 'immutabilities').
inf('immutableness', 'immutablenesses').
inf('fixity', 'fixities').
inf('unalterability', 'unalterabilities').
inf('incurability', 'incurabilities').
inf('agelessness', 'agelessnesses').
inf('sameness', 'samenesses').
inf('otherness', 'othernesses').
inf('distinctness', 'distinctnesses').
inf('separateness', 'separatenesses').
inf('identity', 'identities').
inf('identicalness', 'identicalnesses').
inf('indistinguishability', 'indistinguishabilities').
inf('oneness', 'onenesses').
inf('unity', 'unities').
inf('selfsameness', 'selfsamenesses').
inf('similarity', 'similarities').
inf('homogeny', 'homogenies').
inf('homology', 'homologies').
inf('homomorphy', 'homomorphies').
inf('isomorphy', 'isomorphies').
inf('likeness', 'likenesses').
inf('alikeness', 'alikenesses').
inf('uniformity', 'uniformities').
inf('uniformness', 'uniformnesses').
inf('homogeneity', 'homogeneities').
inf('homogeneousness', 'homogeneousnesses').
inf('consistency', 'consistencies').
inf('approach', 'approaches').
inf('echo', 'echoes').
inf('comparability', 'comparabilities').
inf('naturalness', 'naturalnesses').
inf('affinity', 'affinities').
inf('equality', 'equalities').
inf('equatability', 'equatabilities').
inf('parity', 'parities').
inf('evenness', 'evennesses').
inf('isometry', 'isometries').
inf('discrepancy', 'discrepancies').
inf('dissimilarity', 'dissimilarities').
inf('unsimilarity', 'unsimilarities').
inf('disparateness', 'disparatenesses').
inf('distinctiveness', 'distinctivenesses').
inf('heterology', 'heterologies').
inf('unlikeness', 'unlikenesses').
inf('nonuniformity', 'nonuniformities').
inf('heterogeneity', 'heterogeneities').
inf('heterogeneousness', 'heterogeneousnesses').
inf('diverseness', 'diversenesses').
inf('diversity', 'diversities').
inf('multifariousness', 'multifariousnesses').
inf('variety', 'varieties').
inf('biodiversity', 'biodiversities').
inf('inconsistency', 'inconsistencies').
inf('variety', 'varieties').
inf('inequality', 'inequalities').
inf('disparity', 'disparities').
inf('far cry', 'far cries').
inf('unevenness', 'unevennesses').
inf('certainty', 'certainties').
inf('ineluctability', 'ineluctabilities').
inf('unavoidability', 'unavoidabilities').
inf('inevitability', 'inevitabilities').
inf('inevitableness', 'inevitablenesses').
inf('determinateness', 'determinatenesses').
inf('definiteness', 'definitenesses').
inf('finality', 'finalities').
inf('conclusiveness', 'conclusivenesses').
inf('decisiveness', 'decisivenesses').
inf('surety', 'sureties').
inf('indisputability', 'indisputabilities').
inf('indubitability', 'indubitabilities').
inf('unquestionability', 'unquestionabilities').
inf('unquestionableness', 'unquestionablenesses').
inf('incontrovertibility', 'incontrovertibilities').
inf('incontrovertibleness', 'incontrovertiblenesses').
inf('positivity', 'positivities').
inf('positiveness', 'positivenesses').
inf('demonstrability', 'demonstrabilities').
inf('provability', 'provabilities').
inf('givenness', 'givennesses').
inf('moral certainty', 'moral certainties').
inf('predictability', 'predictabilities').
inf('probability', 'probabilities').
inf('likeliness', 'likelinesses').
inf('uncertainty', 'uncertainties').
inf('uncertainness', 'uncertainnesses').
inf('precariousness', 'precariousnesses').
inf('dubiousness', 'dubiousnesses').
inf('doubtfulness', 'doubtfulnesses').
inf('indefiniteness', 'indefinitenesses').
inf('indeterminateness', 'indeterminatenesses').
inf('indefinity', 'indefinities').
inf('indeterminacy', 'indeterminacies').
inf('inconclusiveness', 'inconclusivenesses').
inf('unpredictability', 'unpredictabilities').
inf('improbability', 'improbabilities').
inf('improbableness', 'improbablenesses').
inf('unlikeliness', 'unlikelinesses').
inf('fortuitousness', 'fortuitousnesses').
inf('speculativeness', 'speculativenesses').
inf('factuality', 'factualities').
inf('factualness', 'factualnesses').
inf('counterfactuality', 'counterfactualities').
inf('concreteness', 'concretenesses').
inf('tangibility', 'tangibilities').
inf('tangibleness', 'tangiblenesses').
inf('palpability', 'palpabilities').
inf('intangibility', 'intangibilities').
inf('intangibleness', 'intangiblenesses').
inf('impalpability', 'impalpabilities').
inf('literalness', 'literalnesses').
inf('materiality', 'materialities').
inf('physicalness', 'physicalnesses').
inf('corporeality', 'corporealities').
inf('corporality', 'corporalities').
inf('substantiality', 'substantialities').
inf('substantialness', 'substantialnesses').
inf('solidness', 'solidnesses').
inf('immateriality', 'immaterialities').
inf('incorporeality', 'incorporealities').
inf('insubstantiality', 'insubstantialities').
inf('abstractness', 'abstractnesses').
inf('reality', 'realities').
inf('unreality', 'unrealities').
inf('particularity', 'particularities').
inf('specialness', 'specialnesses').
inf('specificity', 'specificities').
inf('specificity', 'specificities').
inf('individuality', 'individualities').
inf('singularity', 'singularities').
inf('uniqueness', 'uniquenesses').
inf('peculiarity', 'peculiarities').
inf('specialness', 'specialnesses').
inf('specialty', 'specialties').
inf('speciality', 'specialities').
inf('distinctiveness', 'distinctivenesses').
inf('idiosyncrasy', 'idiosyncrasies').
inf('generality', 'generalities').
inf('commonality', 'commonalities').
inf('commonness', 'commonnesses').
inf('solidarity', 'solidarities').
inf('pervasiveness', 'pervasivenesses').
inf('currency', 'currencies').
inf('universality', 'universalities').
inf('catholicity', 'catholicities').
inf('totality', 'totalities').
inf('simplicity', 'simplicities').
inf('simpleness', 'simplenesses').
inf('complexity', 'complexities').
inf('complexness', 'complexnesses').
inf('complicatedness', 'complicatednesses').
inf('knottiness', 'knottinesses').
inf('tortuousness', 'tortuousnesses').
inf('elaborateness', 'elaboratenesses').
inf('intricacy', 'intricacies').
inf('tapestry', 'tapestries').
inf('trickiness', 'trickinesses').
inf('regularity', 'regularities').
inf('cyclicity', 'cyclicities').
inf('periodicity', 'periodicities').
inf('orderliness', 'orderlinesses').
inf('methodicalness', 'methodicalnesses').
inf('uniformity', 'uniformities').
inf('homogeneity', 'homogeneities').
inf('inhomogeneity', 'inhomogeneities').
inf('evenness', 'evennesses').
inf('invariability', 'invariabilities').
inf('smoothness', 'smoothnesses').
inf('steadiness', 'steadinesses').
inf('irregularity', 'irregularities').
inf('unregularity', 'unregularities').
inf('fitfulness', 'fitfulnesses').
inf('jerkiness', 'jerkinesses').
inf('intermittency', 'intermittencies').
inf('randomness', 'randomnesses').
inf('haphazardness', 'haphazardnesses').
inf('stochasticity', 'stochasticities').
inf('ergodicity', 'ergodicities').
inf('spasticity', 'spasticities').
inf('unevenness', 'unevennesses').
inf('variability', 'variabilities').
inf('rockiness', 'rockinesses').
inf('ruggedness', 'ruggednesses').
inf('hilliness', 'hillinesses').
inf('jaggedness', 'jaggednesses').
inf('patchiness', 'patchinesses').
inf('waviness', 'wavinesses').
inf('unsteadiness', 'unsteadinesses').
inf('mobility', 'mobilities').
inf('motivity', 'motivities').
inf('motility', 'motilities').
inf('movability', 'movabilities').
inf('movableness', 'movablenesses').
inf('maneuverability', 'maneuverabilities').
inf('manoeuvrability', 'manoeuvrabilities').
inf('manipulability', 'manipulabilities').
inf('looseness', 'loosenesses').
inf('restlessness', 'restlessnesses').
inf('weatherliness', 'weatherlinesses').
inf('wiggliness', 'wigglinesses').
inf('slackness', 'slacknesses').
inf('unsteadiness', 'unsteadinesses').
inf('ricketiness', 'ricketinesses').
inf('instability', 'instabilities').
inf('unstableness', 'unstablenesses').
inf('shakiness', 'shakinesses').
inf('portability', 'portabilities').
inf('immobility', 'immobilities').
inf('immotility', 'immotilities').
inf('inertness', 'inertnesses').
inf('immovability', 'immovabilities').
inf('immovableness', 'immovablenesses').
inf('tightness', 'tightnesses').
inf('tautness', 'tautnesses').
inf('fastness', 'fastnesses').
inf('fixedness', 'fixednesses').
inf('fixity', 'fixities').
inf('secureness', 'securenesses').
inf('looseness', 'loosenesses').
inf('steadiness', 'steadinesses').
inf('firmness', 'firmnesses').
inf('sureness', 'surenesses').
inf('stability', 'stabilities').
inf('stableness', 'stablenesses').
inf('pleasantness', 'pleasantnesses').
inf('sweetness', 'sweetnesses').
inf('agreeableness', 'agreeablenesses').
inf('amenity', 'amenities').
inf('enjoyableness', 'enjoyablenesses').
inf('niceness', 'nicenesses').
inf('unpleasantness', 'unpleasantnesses').
inf('disagreeableness', 'disagreeablenesses').
inf('abrasiveness', 'abrasivenesses').
inf('acridity', 'acridities').
inf('acridness', 'acridnesses').
inf('unpalatability', 'unpalatabilities').
inf('unpalatableness', 'unpalatablenesses').
inf('disgustingness', 'disgustingnesses').
inf('unsavoriness', 'unsavorinesses').
inf('nastiness', 'nastinesses').
inf('offensiveness', 'offensivenesses').
inf('odiousness', 'odiousnesses').
inf('distastefulness', 'distastefulnesses').
inf('loathsomeness', 'loathsomenesses').
inf('repulsiveness', 'repulsivenesses').
inf('sliminess', 'sliminesses').
inf('vileness', 'vilenesses').
inf('lousiness', 'lousinesses').
inf('wickedness', 'wickednesses').
inf('hatefulness', 'hatefulnesses').
inf('obnoxiousness', 'obnoxiousnesses').
inf('objectionableness', 'objectionablenesses').
inf('beastliness', 'beastlinesses').
inf('awfulness', 'awfulnesses').
inf('dreadfulness', 'dreadfulnesses').
inf('horridness', 'horridnesses').
inf('terribleness', 'terriblenesses').
inf('frightfulness', 'frightfulnesses').
inf('ghastliness', 'ghastlinesses').
inf('grimness', 'grimnesses').
inf('gruesomeness', 'gruesomenesses').
inf('luridness', 'luridnesses').
inf('credibility', 'credibilities').
inf('credibleness', 'crediblenesses').
inf('believability', 'believabilities').
inf('authenticity', 'authenticities').
inf('genuineness', 'genuinenesses').
inf('legitimacy', 'legitimacies').
inf('cogency', 'cogencies').
inf('validity', 'validities').
inf('plausibility', 'plausibilities').
inf('plausibleness', 'plausiblenesses').
inf('reasonableness', 'reasonablenesses').
inf('tenability', 'tenabilities').
inf('tenableness', 'tenablenesses').
inf('incredibility', 'incredibilities').
inf('incredibleness', 'incrediblenesses').
inf('implausibility', 'implausibilities').
inf('implausibleness', 'implausiblenesses').
inf('street credibility', 'street credibilities').
inf('logicality', 'logicalities').
inf('logicalness', 'logicalnesses').
inf('rationality', 'rationalities').
inf('rationalness', 'rationalnesses').
inf('consistency', 'consistencies').
inf('completeness', 'completenesses').
inf('illogicality', 'illogicalities').
inf('illogicalness', 'illogicalnesses').
inf('naturalness', 'naturalnesses').
inf('unaffectedness', 'unaffectednesses').
inf('simplicity', 'simplicities').
inf('simmpleness', 'simmplenesses').
inf('sincerity', 'sincerities').
inf('unassumingness', 'unassumingnesses').
inf('spontaneity', 'spontaneities').
inf('spontaneousness', 'spontaneousnesses').
inf('informality', 'informalities').
inf('unpretentiousness', 'unpretentiousnesses').
inf('unnaturalness', 'unnaturalnesses').
inf('affectedness', 'affectednesses').
inf('coyness', 'coynesses').
inf('demureness', 'demurenesses').
inf('preciosity', 'preciosities').
inf('preciousness', 'preciousnesses').
inf('artificiality', 'artificialities').
inf('staginess', 'staginesses').
inf('theatricality', 'theatricalities').
inf('pretentiousness', 'pretentiousnesses').
inf('largeness', 'largenesses').
inf('supernaturalness', 'supernaturalnesses').
inf('wholesomeness', 'wholesomenesses').
inf('nutritiousness', 'nutritiousnesses').
inf('nutritiveness', 'nutritivenesses').
inf('healthfulness', 'healthfulnesses').
inf('salubrity', 'salubrities').
inf('salubriousness', 'salubriousnesses').
inf('unwholesomeness', 'unwholesomenesses').
inf('morbidness', 'morbidnesses').
inf('morbidity', 'morbidities').
inf('harmfulness', 'harmfulnesses').
inf('noisomeness', 'noisomenesses').
inf('noxiousness', 'noxiousnesses').
inf('perniciousness', 'perniciousnesses').
inf('toxicity', 'toxicities').
inf('deadliness', 'deadlinesses').
inf('lethality', 'lethalities').
inf('fatality', 'fatalities').
inf('jejunity', 'jejunities').
inf('jejuneness', 'jejunenesses').
inf('rottenness', 'rottennesses').
inf('unhealthfulness', 'unhealthfulnesses').
inf('insalubrity', 'insalubrities').
inf('insalubriousness', 'insalubriousnesses').
inf('satisfactoriness', 'satisfactorinesses').
inf('adequacy', 'adequacies').
inf('adequateness', 'adequatenesses').
inf('acceptability', 'acceptabilities').
inf('acceptableness', 'acceptablenesses').
inf('admissibility', 'admissibilities').
inf('permissibility', 'permissibilities').
inf('unsatisfactoriness', 'unsatisfactorinesses').
inf('inadequacy', 'inadequacies').
inf('inadequateness', 'inadequatenesses').
inf('perishability', 'perishabilities').
inf('perishableness', 'perishablenesses').
inf('unacceptability', 'unacceptabilities').
inf('unacceptableness', 'unacceptablenesses').
inf('inadmissibility', 'inadmissibilities').
inf('impermissibility', 'impermissibilities').
inf('palatability', 'palatabilities').
inf('palatableness', 'palatablenesses').
inf('ordinariness', 'ordinarinesses').
inf('mundaneness', 'mundanenesses').
inf('mundanity', 'mundanities').
inf('averageness', 'averagenesses').
inf('mediocrity', 'mediocrities').
inf('expectedness', 'expectednesses').
inf('normality', 'normalities').
inf('normalcy', 'normalcies').
inf('commonness', 'commonnesses').
inf('commonplaceness', 'commonplacenesses').
inf('everydayness', 'everydaynesses').
inf('prosiness', 'prosinesses').
inf('prosaicness', 'prosaicnesses').
inf('usualness', 'usualnesses').
inf('familiarity', 'familiarities').
inf('extraordinariness', 'extraordinarinesses').
inf('unexpectedness', 'unexpectednesses').
inf('surprisingness', 'surprisingnesses').
inf('uncommonness', 'uncommonnesses').
inf('uncommonness', 'uncommonnesses').
inf('unusualness', 'unusualnesses').
inf('unfamiliarity', 'unfamiliarities').
inf('strangeness', 'strangenesses').
inf('oddity', 'oddities').
inf('queerness', 'queernesses').
inf('quirkiness', 'quirkinesses').
inf('eeriness', 'eerinesses').
inf('ghostliness', 'ghostlinesses').
inf('abnormality', 'abnormalities').
inf('freakishness', 'freakishnesses').
inf('singularity', 'singularities').
inf('outlandishness', 'outlandishnesses').
inf('bizarreness', 'bizarrenesses').
inf('weirdness', 'weirdnesses').
inf('quaintness', 'quaintnesses').
inf('eccentricity', 'eccentricities').
inf('oddity', 'oddities').
inf('oddness', 'oddnesses').
inf('ethnicity', 'ethnicities').
inf('foreignness', 'foreignnesses').
inf('strangeness', 'strangenesses').
inf('curiousness', 'curiousnesses').
inf('exoticness', 'exoticnesses').
inf('nativeness', 'nativenesses').
inf('indigenousness', 'indigenousnesses').
inf('autochthony', 'autochthonies').
inf('originality', 'originalities').
inf('freshness', 'freshnesses').
inf('novelty', 'novelties').
inf('unorthodoxy', 'unorthodoxies').
inf('heterodoxy', 'heterodoxies').
inf('unconventionality', 'unconventionalities').
inf('nonconformity', 'nonconformities').
inf('unoriginality', 'unoriginalities').
inf('orthodoxy', 'orthodoxies').
inf('conventionality', 'conventionalities').
inf('conformity', 'conformities').
inf('traditionality', 'traditionalities').
inf('correctness', 'correctnesses').
inf('rightness', 'rightnesses').
inf('incorrectness', 'incorrectnesses').
inf('wrongness', 'wrongnesses').
inf('erroneousness', 'erroneousnesses').
inf('accuracy', 'accuracies').
inf('accuracy', 'accuracies').
inf('exactness', 'exactnesses').
inf('minuteness', 'minutenesses').
inf('preciseness', 'precisenesses').
inf('trueness', 'truenesses').
inf('fidelity', 'fidelities').
inf('inaccuracy', 'inaccuracies').
inf('inexactness', 'inexactnesses').
inf('impreciseness', 'imprecisenesses').
inf('looseness', 'loosenesses').
inf('infallibility', 'infallibilities').
inf('inerrancy', 'inerrancies').
inf('errancy', 'errancies').
inf('papal infallibility', 'papal infallibilities').
inf('errancy', 'errancies').
inf('instability', 'instabilities').
inf('reproducibility', 'reproducibilities').
inf('duplicability', 'duplicabilities').
inf('irreproducibility', 'irreproducibilities').
inf('fallibility', 'fallibilities').
inf('worthiness', 'worthinesses').
inf('deservingness', 'deservingnesses').
inf('meritoriousness', 'meritoriousnesses').
inf('praiseworthiness', 'praiseworthinesses').
inf('laudability', 'laudabilities').
inf('laudableness', 'laudablenesses').
inf('quotability', 'quotabilities').
inf('roadworthiness', 'roadworthinesses').
inf('unworthiness', 'unworthinesses').
inf('baseness', 'basenesses').
inf('sordidness', 'sordidnesses').
inf('contemptibility', 'contemptibilities').
inf('despicableness', 'despicablenesses').
inf('despicability', 'despicabilities').
inf('shamefulness', 'shamefulnesses').
inf('disgracefulness', 'disgracefulnesses').
inf('ignominiousness', 'ignominiousnesses').
inf('scandalousness', 'scandalousnesses').
inf('popularity', 'popularities').
inf('unpopularity', 'unpopularities').
inf('legality', 'legalities').
inf('validity', 'validities').
inf('validness', 'validnesses').
inf('lawfulness', 'lawfulnesses').
inf('legitimacy', 'legitimacies').
inf('licitness', 'licitnesses').
inf('illegality', 'illegalities').
inf('invalidity', 'invalidities').
inf('invalidness', 'invalidnesses').
inf('fallaciousness', 'fallaciousnesses').
inf('unlawfulness', 'unlawfulnesses').
inf('lawlessness', 'lawlessnesses').
inf('outlawry', 'outlawries').
inf('illegitimacy', 'illegitimacies').
inf('illicitness', 'illicitnesses').
inf('shadiness', 'shadinesses').
inf('dash', 'dashes').
inf('daintiness', 'daintinesses').
inf('delicacy', 'delicacies').
inf('fineness', 'finenesses').
inf('courtliness', 'courtlinesses').
inf('tastefulness', 'tastefulnesses').
inf('genteelness', 'genteelnesses').
inf('gentility', 'gentilities').
inf('chicness', 'chicnesses').
inf('modishness', 'modishnesses').
inf('smartness', 'smartnesses').
inf('stylishness', 'stylishnesses').
inf('jauntiness', 'jauntinesses').
inf('nattiness', 'nattinesses').
inf('dapperness', 'dappernesses').
inf('rakishness', 'rakishnesses').
inf('grandness', 'grandnesses').
inf('class', 'classes').
inf('awkwardness', 'awkwardnesses').
inf('clumsiness', 'clumsinesses').
inf('gracelessness', 'gracelessnesses').
inf('stiffness', 'stiffnesses').
inf('woodenness', 'woodennesses').
inf('rusticity', 'rusticities').
inf('urbanity', 'urbanities').
inf('dowdiness', 'dowdinesses').
inf('drabness', 'drabnesses').
inf('homeliness', 'homelinesses').
inf('shabbiness', 'shabbinesses').
inf('seediness', 'seedinesses').
inf('manginess', 'manginesses').
inf('sleaziness', 'sleazinesses').
inf('tweediness', 'tweedinesses').
inf('raggedness', 'raggednesses').
inf('coarseness', 'coarsenesses').
inf('commonness', 'commonnesses').
inf('grossness', 'grossnesses').
inf('vulgarity', 'vulgarities').
inf('raunch', 'raunches').
inf('crudeness', 'crudenesses').
inf('roughness', 'roughnesses').
inf('boorishness', 'boorishnesses').
inf('uncouthness', 'uncouthnesses').
inf('ostentatiousness', 'ostentatiousnesses').
inf('pomposity', 'pomposities').
inf('pompousness', 'pompousnesses').
inf('pretentiousness', 'pretentiousnesses').
inf('puffiness', 'puffinesses').
inf('splashiness', 'splashinesses').
inf('tastelessness', 'tastelessnesses').
inf('cheapness', 'cheapnesses').
inf('tackiness', 'tackinesses').
inf('flashiness', 'flashinesses').
inf('garishness', 'garishnesses').
inf('gaudiness', 'gaudinesses').
inf('loudness', 'loudnesses').
inf('brashness', 'brashnesses').
inf('meretriciousness', 'meretriciousnesses').
inf('tawdriness', 'tawdrinesses').
inf('comprehensibility', 'comprehensibilities').
inf('understandability', 'understandabilities').
inf('legibility', 'legibilities').
inf('readability', 'readabilities').
inf('intelligibility', 'intelligibilities').
inf('expressiveness', 'expressivenesses').
inf('picturesqueness', 'picturesquenesses').
inf('readability', 'readabilities').
inf('speech intelligibility', 'speech intelligibilities').
inf('clarity', 'clarities').
inf('lucidity', 'lucidities').
inf('lucidness', 'lucidnesses').
inf('pellucidity', 'pellucidities').
inf('clearness', 'clearnesses').
inf('limpidity', 'limpidities').
inf('monosemy', 'monosemies').
inf('coherency', 'coherencies').
inf('preciseness', 'precisenesses').
inf('clearcutness', 'clearcutnesses').
inf('perspicuity', 'perspicuities').
inf('perspicuousness', 'perspicuousnesses').
inf('plainness', 'plainnesses').
inf('unambiguity', 'unambiguities').
inf('unequivocalness', 'unequivocalnesses').
inf('explicitness', 'explicitnesses').
inf('incomprehensibility', 'incomprehensibilities').
inf('inscrutability', 'inscrutabilities').
inf('illegibility', 'illegibilities').
inf('impenetrability', 'impenetrabilities').
inf('impenetrableness', 'impenetrablenesses').
inf('opacity', 'opacities').
inf('opaqueness', 'opaquenesses').
inf('obscureness', 'obscurenesses').
inf('obscurity', 'obscurities').
inf('abstruseness', 'abstrusenesses').
inf('reconditeness', 'reconditenesses').
inf('unintelligibility', 'unintelligibilities').
inf('unclearness', 'unclearnesses').
inf('elusiveness', 'elusivenesses').
inf('vagueness', 'vaguenesses').
inf('haziness', 'hazinesses').
inf('inexplicitness', 'inexplicitnesses').
inf('implicitness', 'implicitnesses').
inf('ambiguity', 'ambiguities').
inf('equivocalness', 'equivocalnesses').
inf('evasiveness', 'evasivenesses').
inf('polysemy', 'polysemies').
inf('lexical ambiguity', 'lexical ambiguities').
inf('righteousness', 'righteousnesses').
inf('impeccability', 'impeccabilities').
inf('uprightness', 'uprightnesses').
inf('piety', 'pieties').
inf('piousness', 'piousnesses').
inf('devoutness', 'devoutnesses').
inf('religiousness', 'religiousnesses').
inf('religiosity', 'religiosities').
inf('dutifulness', 'dutifulnesses').
inf('godliness', 'godlinesses').
inf('unrighteousness', 'unrighteousnesses').
inf('sinfulness', 'sinfulnesses').
inf('wickedness', 'wickednesses').
inf('mark of Cain', 'marks of Cain').
inf('impiety', 'impieties').
inf('impiousness', 'impiousnesses').
inf('undutifulness', 'undutifulnesses').
inf('irreligiousness', 'irreligiousnesses').
inf('ungodliness', 'ungodlinesses').
inf('godlessness', 'godlessnesses').
inf('humaneness', 'humanenesses').
inf('humanity', 'humanities').
inf('mercifulness', 'mercifulnesses').
inf('mercy', 'mercies').
inf('pity', 'pities').
inf('forgivingness', 'forgivingnesses').
inf('kindness', 'kindnesses').
inf('leniency', 'leniencies').
inf('mildness', 'mildnesses').
inf('lenity', 'lenities').
inf('inhumaneness', 'inhumanenesses').
inf('inhumanity', 'inhumanities').
inf('atrocity', 'atrocities').
inf('atrociousness', 'atrociousnesses').
inf('barbarity', 'barbarities').
inf('barbarousness', 'barbarousnesses').
inf('heinousness', 'heinousnesses').
inf('bestiality', 'bestialities').
inf('ferociousness', 'ferociousnesses').
inf('brutality', 'brutalities').
inf('viciousness', 'viciousnesses').
inf('savagery', 'savageries').
inf('murderousness', 'murderousnesses').
inf('mercilessness', 'mercilessnesses').
inf('unmercifulness', 'unmercifulnesses').
inf('pitilessness', 'pitilessnesses').
inf('ruthlessness', 'ruthlessnesses').
inf('relentlessness', 'relentlessnesses').
inf('inexorability', 'inexorabilities').
inf('inexorableness', 'inexorablenesses').
inf('generosity', 'generosities').
inf('generousness', 'generousnesses').
inf('charitableness', 'charitablenesses').
inf('bounty', 'bounties').
inf('bounteousness', 'bounteousnesses').
inf('bigheartedness', 'bigheartednesses').
inf('liberality', 'liberalities').
inf('liberalness', 'liberalnesses').
inf('largess', 'largesses').
inf('magnanimity', 'magnanimities').
inf('openhandedness', 'openhandednesses').
inf('unselfishness', 'unselfishnesses').
inf('selflessness', 'selflessnesses').
inf('stinginess', 'stinginesses').
inf('meanness', 'meannesses').
inf('minginess', 'minginesses').
inf('niggardliness', 'niggardlinesses').
inf('niggardness', 'niggardnesses').
inf('parsimony', 'parsimonies').
inf('parsimoniousness', 'parsimoniousnesses').
inf('tightness', 'tightnesses').
inf('tightfistedness', 'tightfistednesses').
inf('closeness', 'closenesses').
inf('pettiness', 'pettinesses').
inf('littleness', 'littlenesses').
inf('smallness', 'smallnesses').
inf('miserliness', 'miserlinesses').
inf('penuriousness', 'penuriousnesses').
inf('illiberality', 'illiberalities').
inf('selfishness', 'selfishnesses').
inf('greediness', 'greedinesses').
inf('voraciousness', 'voraciousnesses').
inf('rapaciousness', 'rapaciousnesses').
inf('self-centeredness', 'self-centerednesses').
inf('enterprisingness', 'enterprisingnesses').
inf('ambitiousness', 'ambitiousnesses').
inf('energy', 'energies').
inf('push', 'pushes').
inf('get-up-and-go', 'get-up-and-goes').
inf('aggressiveness', 'aggressivenesses').
inf('competitiveness', 'competitivenesses').
inf('combativeness', 'combativenesses').
inf('militancy', 'militancies').
inf('scrappiness', 'scrappinesses').
inf('intrusiveness', 'intrusivenesses').
inf('meddlesomeness', 'meddlesomenesses').
inf('officiousness', 'officiousnesses').
inf('boldness', 'boldnesses').
inf('brass', 'brasses').
inf('audacity', 'audacities').
inf('audaciousness', 'audaciousnesses').
inf('presumptuousness', 'presumptuousnesses').
inf('effrontery', 'effronteries').
inf('uppityness', 'uppitynesses').
inf('uppishness', 'uppishnesses').
inf('fairness', 'fairnesses').
inf('equity', 'equities').
inf('unfairness', 'unfairnesses').
inf('inequity', 'inequities').
inf('kindness', 'kindnesses').
inf('charity', 'charities').
inf('grace of God', 'graces of God').
inf('benignity', 'benignities').
inf('benignancy', 'benignancies').
inf('graciousness', 'graciousnesses').
inf('loving-kindness', 'loving-kindnesses').
inf('considerateness', 'consideratenesses').
inf('thoughtfulness', 'thoughtfulnesses').
inf('kindliness', 'kindlinesses').
inf('helpfulness', 'helpfulnesses').
inf('tactfulness', 'tactfulnesses').
inf('delicacy', 'delicacies').
inf('diplomacy', 'diplomacies').
inf('discreetness', 'discreetnesses').
inf('address', 'addresses').
inf('malevolency', 'malevolencies').
inf('cattiness', 'cattinesses').
inf('bitchiness', 'bitchinesses').
inf('spitefulness', 'spitefulnesses').
inf('nastiness', 'nastinesses').
inf('malignity', 'malignities').
inf('malignancy', 'malignancies').
inf('sensitivity', 'sensitivities').
inf('sensitiveness', 'sensitivenesses').
inf('defensiveness', 'defensivenesses').
inf('bunker mentality', 'bunker mentalities').
inf('perceptiveness', 'perceptivenesses').
inf('insensitivity', 'insensitivities').
inf('insensitiveness', 'insensitivenesses').
inf('crassness', 'crassnesses').
inf('unfeelingness', 'unfeelingnesses').
inf('callousness', 'callousnesses').
inf('callosity', 'callosities').
inf('hardness', 'hardnesses').
inf('insensibility', 'insensibilities').
inf('dullness', 'dullnesses').
inf('unperceptiveness', 'unperceptivenesses').
inf('unkindness', 'unkindnesses').
inf('cruelty', 'cruelties').
inf('cruelness', 'cruelnesses').
inf('harshness', 'harshnesses').
inf('beastliness', 'beastlinesses').
inf('meanness', 'meannesses').
inf('unhelpfulness', 'unhelpfulnesses').
inf('inconsiderateness', 'inconsideratenesses').
inf('thoughtlessness', 'thoughtlessnesses').
inf('tactlessness', 'tactlessnesses').
inf('bluntness', 'bluntnesses').
inf('balefulness', 'balefulnesses').
inf('morality', 'moralities').
inf('rightness', 'rightnesses').
inf('virtuousness', 'virtuousnesses').
inf('saintliness', 'saintlinesses').
inf('conscientiousness', 'conscientiousnesses').
inf('religiousness', 'religiousnesses').
inf('unconscientiousness', 'unconscientiousnesses').
inf('goodness', 'goodnesses').
inf('chastity', 'chastities').
inf('sexual morality', 'sexual moralities').
inf('purity', 'purities').
inf('pureness', 'purenesses').
inf('justness', 'justnesses').
inf('rightfulness', 'rightfulnesses').
inf('immorality', 'immoralities').
inf('degeneracy', 'degeneracies').
inf('depravity', 'depravities').
inf('corruptibility', 'corruptibilities').
inf('licentiousness', 'licentiousnesses').
inf('wantonness', 'wantonnesses').
inf('anomy', 'anomies').
inf('wrongness', 'wrongnesses').
inf('evilness', 'evilnesses').
inf('nefariousness', 'nefariousnesses').
inf('wickedness', 'wickednesses').
inf('vileness', 'vilenesses').
inf('ugliness', 'uglinesses').
inf('filthiness', 'filthinesses').
inf('enormity', 'enormities').
inf('reprehensibility', 'reprehensibilities').
inf('villainy', 'villainies').
inf('villainousness', 'villainousnesses').
inf('perversity', 'perversities').
inf('perverseness', 'perversenesses').
inf('frailty', 'frailties').
inf('corruptness', 'corruptnesses').
inf('venality', 'venalities').
inf('unjustness', 'unjustnesses').
inf('wrongfulness', 'wrongfulnesses').
inf('amorality', 'amoralities').
inf('divinity', 'divinities').
inf('holiness', 'holinesses').
inf('sanctity', 'sanctities').
inf('sacredness', 'sacrednesses').
inf('ideality', 'idealities').
inf('holy of holies', 'holies of holies').
inf('unholiness', 'unholinesses').
inf('profaneness', 'profanenesses').
inf('sacrilegiousness', 'sacrilegiousnesses').
inf('safeness', 'safenesses').
inf('dangerousness', 'dangerousnesses').
inf('precariousness', 'precariousnesses').
inf('curability', 'curabilities').
inf('curableness', 'curablenesses').
inf('incurability', 'incurabilities').
inf('incurableness', 'incurablenesses').
inf('courageousness', 'courageousnesses').
inf('bravery', 'braveries').
inf('braveness', 'bravenesses').
inf('gallantry', 'gallantries').
inf('valorousness', 'valorousnesses').
inf('valiancy', 'valiancies').
inf('dauntlessness', 'dauntlessnesses').
inf('intrepidity', 'intrepidities').
inf('stoutheartedness', 'stoutheartednesses').
inf('fearlessness', 'fearlessnesses').
inf('coolness', 'coolnesses').
inf('nervelessness', 'nervelessnesses').
inf('boldness', 'boldnesses').
inf('hardiness', 'hardinesses').
inf('adventurousness', 'adventurousnesses').
inf('venturesomeness', 'venturesomenesses').
inf('daredevilry', 'daredevilries').
inf('daredeviltry', 'daredeviltries').
inf('audacity', 'audacities').
inf('audaciousness', 'audaciousnesses').
inf('temerity', 'temerities').
inf('shamelessness', 'shamelessnesses').
inf('brazenness', 'brazennesses').
inf('gutsiness', 'gutsinesses').
inf('pluckiness', 'pluckinesses').
inf('cowardliness', 'cowardlinesses').
inf('cravenness', 'cravennesses').
inf('faintheartedness', 'faintheartednesses').
inf('faintness', 'faintnesses').
inf('fearfulness', 'fearfulnesses').
inf('timidity', 'timidities').
inf('timorousness', 'timorousnesses').
inf('pusillanimity', 'pusillanimities').
inf('pusillanimousness', 'pusillanimousnesses').
inf('poltroonery', 'poltrooneries').
inf('dastardliness', 'dastardlinesses').
inf('gutlessness', 'gutlessnesses').
inf('resoluteness', 'resolutenesses').
inf('firmness', 'firmnesses').
inf('firmness of purpose', 'firmnesses of purpose').
inf('steadiness', 'steadinesses').
inf('sturdiness', 'sturdinesses').
inf('presence of mind', 'presences of mind').
inf('stiffness', 'stiffnesses').
inf('stubbornness', 'stubbornnesses').
inf('bullheadedness', 'bullheadednesses').
inf('obstinacy', 'obstinacies').
inf('pigheadedness', 'pigheadednesses').
inf('impenitency', 'impenitencies').
inf('intransigency', 'intransigencies').
inf('single-mindedness', 'single-mindednesses').
inf('obduracy', 'obduracies').
inf('unyieldingness', 'unyieldingnesses').
inf('decisiveness', 'decisivenesses').
inf('doggedness', 'doggednesses').
inf('persistency', 'persistencies').
inf('tenacity', 'tenacities').
inf('tenaciousness', 'tenaciousnesses').
inf('pertinacity', 'pertinacities').
inf('indefatigability', 'indefatigabilities').
inf('indefatigableness', 'indefatigablenesses').
inf('tirelessness', 'tirelessnesses').
inf('steadfastness', 'steadfastnesses').
inf('industriousness', 'industriousnesses').
inf('industry', 'industries').
inf('assiduity', 'assiduities').
inf('assiduousness', 'assiduousnesses').
inf('intentness', 'intentnesses').
inf('singleness', 'singlenesses').
inf('sedulity', 'sedulities').
inf('sedulousness', 'sedulousnesses').
inf('studiousness', 'studiousnesses').
inf('bookishness', 'bookishnesses').
inf('irresoluteness', 'irresolutenesses').
inf('volatility', 'volatilities').
inf('unpredictability', 'unpredictabilities').
inf('indecisiveness', 'indecisivenesses').
inf('sincerity', 'sincerities').
inf('heartiness', 'heartinesses').
inf('wholeheartedness', 'wholeheartednesses').
inf('singleness', 'singlenesses').
inf('straightforwardness', 'straightforwardnesses').
inf('insincerity', 'insincerities').
inf('falseness', 'falsenesses').
inf('hollowness', 'hollownesses').
inf('hypocrisy', 'hypocrisies').
inf('sanctimoniousness', 'sanctimoniousnesses').
inf('sanctimony', 'sanctimonies').
inf('fulsomeness', 'fulsomenesses').
inf('oiliness', 'oilinesses').
inf('oleaginousness', 'oleaginousnesses').
inf('smarminess', 'smarminesses').
inf('unctuousness', 'unctuousnesses').
inf('honorableness', 'honorablenesses').
inf('honourableness', 'honourablenesses').
inf('scrupulousness', 'scrupulousnesses').
inf('venerability', 'venerabilities').
inf('venerableness', 'venerablenesses').
inf('integrity', 'integrities').
inf('probity', 'probities').
inf('incorruptness', 'incorruptnesses').
inf('incorruptibility', 'incorruptibilities').
inf('nobility', 'nobilities').
inf('nobleness', 'noblenesses').
inf('magnanimousness', 'magnanimousnesses').
inf('high-mindedness', 'high-mindednesses').
inf('noble-mindedness', 'noble-mindednesses').
inf('sublimity', 'sublimities').
inf('respectability', 'respectabilities').
inf('reputability', 'reputabilities').
inf('decency', 'decencies').
inf('honesty', 'honesties').
inf('honestness', 'honestnesses').
inf('candidness', 'candidnesses').
inf('frankness', 'franknesses').
inf('directness', 'directnesses').
inf('forthrightness', 'forthrightnesses').
inf('straightness', 'straightnesses').
inf('truthfulness', 'truthfulnesses').
inf('veracity', 'veracities').
inf('ingenuousness', 'ingenuousnesses').
inf('artlessness', 'artlessnesses').
inf('parental quality', 'parental qualities').
inf('motherliness', 'motherlinesses').
inf('maternal quality', 'maternal qualities').
inf('maternity', 'maternities').
inf('fatherliness', 'fatherlinesses').
inf('paternal quality', 'paternal qualities').
inf('dishonorableness', 'dishonorablenesses').
inf('dishonourableness', 'dishonourablenesses').
inf('ignobleness', 'ignoblenesses').
inf('ignobility', 'ignobilities').
inf('unscrupulousness', 'unscrupulousnesses').
inf('sleaziness', 'sleazinesses').
inf('unrespectability', 'unrespectabilities').
inf('disreputability', 'disreputabilities').
inf('disreputableness', 'disreputablenesses').
inf('dishonesty', 'dishonesties').
inf('deceptiveness', 'deceptivenesses').
inf('obliquity', 'obliquities').
inf('speciousness', 'speciousnesses').
inf('meretriciousness', 'meretriciousnesses').
inf('jobbery', 'jobberies').
inf('crookedness', 'crookednesses').
inf('deviousness', 'deviousnesses').
inf('rascality', 'rascalities').
inf('shiftiness', 'shiftinesses').
inf('slipperiness', 'slipperinesses').
inf('trickiness', 'trickinesses').
inf('thievishness', 'thievishnesses').
inf('untruthfulness', 'untruthfulnesses').
inf('mendacity', 'mendacities').
inf('disingenuousness', 'disingenuousnesses').
inf('craftiness', 'craftinesses').
inf('deceitfulness', 'deceitfulnesses').
inf('artfulness', 'artfulnesses').
inf('fidelity', 'fidelities').
inf('faithfulness', 'faithfulnesses').
inf('constancy', 'constancies').
inf('loyalty', 'loyalties').
inf('trueness', 'truenesses').
inf('steadfastness', 'steadfastnesses').
inf('staunchness', 'staunchnesses').
inf('fealty', 'fealties').
inf('infidelity', 'infidelities').
inf('unfaithfulness', 'unfaithfulnesses').
inf('faithlessness', 'faithlessnesses').
inf('falseness', 'falsenesses').
inf('fickleness', 'ficklenesses').
inf('inconstancy', 'inconstancies').
inf('disloyalty', 'disloyalties').
inf('subversiveness', 'subversivenesses').
inf('traitorousness', 'traitorousnesses').
inf('perfidy', 'perfidies').
inf('perfidiousness', 'perfidiousnesses').
inf('treachery', 'treacheries').
inf('insidiousness', 'insidiousnesses').
inf('worldliness', 'worldlinesses').
inf('mundaneness', 'mundanenesses').
inf('mundanity', 'mundanities').
inf('naivety', 'naiveties').
inf('naiveness', 'naivenesses').
inf('artlessness', 'artlessnesses').
inf('ingenuousness', 'ingenuousnesses').
inf('naturalness', 'naturalnesses').
inf('innocency', 'innocencies').
inf('credulousness', 'credulousnesses').
inf('gullibility', 'gullibilities').
inf('simplicity', 'simplicities').
inf('simpleness', 'simplenesses').
inf('simple mindedness', 'simple mindednesses').
inf('austerity', 'austerities').
inf('continency', 'continencies').
inf('temperateness', 'temperatenesses').
inf('sobriety', 'sobrieties').
inf('dryness', 'drynesses').
inf('abstemiousness', 'abstemiousnesses').
inf('dissoluteness', 'dissolutenesses').
inf('rakishness', 'rakishnesses').
inf('wantonness', 'wantonnesses').
inf('looseness', 'loosenesses').
inf('madness', 'madnesses').
inf('rabidity', 'rabidities').
inf('rabidness', 'rabidnesses').
inf('sottishness', 'sottishnesses').
inf('gluttony', 'gluttonies').
inf('greediness', 'greedinesses').
inf('hoggishness', 'hoggishnesses').
inf('piggishness', 'piggishnesses').
inf('edacity', 'edacities').
inf('rapaciousness', 'rapaciousnesses').
inf('rapacity', 'rapacities').
inf('voracity', 'voracities').
inf('voraciousness', 'voraciousnesses').
inf('dignity', 'dignities').
inf('conceitedness', 'conceitednesses').
inf('vanity', 'vanities').
inf('boastfulness', 'boastfulnesses').
inf('vainglory', 'vainglories').
inf('superiority complex', 'superiority complexes').
inf('haughtiness', 'haughtinesses').
inf('high-handedness', 'high-handednesses').
inf('lordliness', 'lordlinesses').
inf('superciliousness', 'superciliousnesses').
inf('disdainfulness', 'disdainfulnesses').
inf('contemptuousness', 'contemptuousnesses').
inf('imperiousness', 'imperiousnesses').
inf('domineeringness', 'domineeringnesses').
inf('overbearingness', 'overbearingnesses').
inf('superiority', 'superiorities').
inf('snobbery', 'snobberies').
inf('snobbishness', 'snobbishnesses').
inf('clannishness', 'clannishnesses').
inf('cliquishness', 'cliquishnesses').
inf('exclusiveness', 'exclusivenesses').
inf('humility', 'humilities').
inf('humbleness', 'humblenesses').
inf('meekness', 'meeknesses').
inf('subduedness', 'subduednesses').
inf('spinelessness', 'spinelessnesses').
inf('wiseness', 'wisenesses').
inf('judiciousness', 'judiciousnesses').
inf('sagacity', 'sagacities').
inf('sagaciousness', 'sagaciousnesses').
inf('knowledgeability', 'knowledgeabilities').
inf('knowledgeableness', 'knowledgeablenesses').
inf('diplomacy', 'diplomacies').
inf('folly', 'follies').
inf('foolishness', 'foolishnesses').
inf('unwiseness', 'unwisenesses').
inf('injudiciousness', 'injudiciousnesses').
inf('absurdity', 'absurdities').
inf('fatuity', 'fatuities').
inf('fatuousness', 'fatuousnesses').
inf('silliness', 'sillinesses').
inf('asininity', 'asininities').
inf('perspicacity', 'perspicacities').
inf('objectivity', 'objectivities').
inf('objectiveness', 'objectivenesses').
inf('subjectivity', 'subjectivities').
inf('subjectiveness', 'subjectivenesses').
inf('foresightedness', 'foresightednesses').
inf('foresightfulness', 'foresightfulnesses').
inf('frugality', 'frugalities').
inf('frugalness', 'frugalnesses').
inf('parsimony', 'parsimonies').
inf('parsimoniousness', 'parsimoniousnesses').
inf('economy', 'economies').
inf('thriftiness', 'thriftinesses').
inf('heedlessness', 'heedlessnesses').
inf('mindlessness', 'mindlessnesses').
inf('rashness', 'rashnesses').
inf('lightheadedness', 'lightheadednesses').
inf('shortsightedness', 'shortsightednesses').
inf('prodigality', 'prodigalities').
inf('profligacy', 'profligacies').
inf('thriftlessness', 'thriftlessnesses').
inf('wastefulness', 'wastefulnesses').
inf('trustingness', 'trustingnesses').
inf('trustfulness', 'trustfulnesses').
inf('credulity', 'credulities').
inf('overcredulity', 'overcredulities').
inf('distrustfulness', 'distrustfulnesses').
inf('suspiciousness', 'suspiciousnesses').
inf('cleanliness', 'cleanlinesses').
inf('fastidiousness', 'fastidiousnesses').
inf('tidiness', 'tidinesses').
inf('neatness', 'neatnesses').
inf('uncleanliness', 'uncleanlinesses').
inf('slovenliness', 'slovenlinesses').
inf('slatternliness', 'slatternlinesses').
inf('sluttishness', 'sluttishnesses').
inf('squeamishness', 'squeamishnesses').
inf('untidiness', 'untidinesses').
inf('messiness', 'messinesses').
inf('disorderliness', 'disorderlinesses').
inf('propriety', 'proprieties').
inf('properness', 'propernesses').
inf('decorousness', 'decorousnesses').
inf('appropriateness', 'appropriatenesses').
inf('rightness', 'rightnesses').
inf('correctness', 'correctnesses').
inf('faultlessness', 'faultlessnesses').
inf('impeccability', 'impeccabilities').
inf('political correctness', 'political correctnesses').
inf('priggishness', 'priggishnesses').
inf('primness', 'primnesses').
inf('modesty', 'modesties').
inf('demureness', 'demurenesses').
inf('seemliness', 'seemlinesses').
inf('becomingness', 'becomingnesses').
inf('decency', 'decencies').
inf('modesty', 'modesties').
inf('modestness', 'modestnesses').
inf('primness', 'primnesses').
inf('prudishness', 'prudishnesses').
inf('prudery', 'pruderies').
inf('impropriety', 'improprieties').
inf('improperness', 'impropernesses').
inf('incorrectness', 'incorrectnesses').
inf('political incorrectness', 'political incorrectnesses').
inf('inappropriateness', 'inappropriatenesses').
inf('wrongness', 'wrongnesses').
inf('indelicacy', 'indelicacies').
inf('gaminess', 'gaminesses').
inf('raciness', 'racinesses').
inf('ribaldry', 'ribaldries').
inf('spiciness', 'spicinesses').
inf('indecorousness', 'indecorousnesses').
inf('unseemliness', 'unseemlinesses').
inf('unbecomingness', 'unbecomingnesses').
inf('indecency', 'indecencies').
inf('immodesty', 'immodesties').
inf('outrageousness', 'outrageousnesses').
inf('enormity', 'enormities').
inf('obscenity', 'obscenities').
inf('lewdness', 'lewdnesses').
inf('bawdiness', 'bawdinesses').
inf('salaciousness', 'salaciousnesses').
inf('salacity', 'salacities').
inf('smuttiness', 'smuttinesses').
inf('dirtiness', 'dirtinesses').
inf('calmness', 'calmnesses').
inf('equanimity', 'equanimities').
inf('assuredness', 'assurednesses').
inf('placidity', 'placidities').
inf('serenity', 'serenities').
inf('tranquillity', 'tranquillities').
inf('tranquility', 'tranquilities').
inf('uneasiness', 'uneasinesses').
inf('tractability', 'tractabilities').
inf('tractableness', 'tractablenesses').
inf('flexibility', 'flexibilities').
inf('manageability', 'manageabilities').
inf('manageableness', 'manageablenesses').
inf('docility', 'docilities').
inf('tameness', 'tamenesses').
inf('amenability', 'amenabilities').
inf('amenableness', 'amenablenesses').
inf('cooperativeness', 'cooperativenesses').
inf('submissiveness', 'submissivenesses').
inf('obsequiousness', 'obsequiousnesses').
inf('servility', 'servilities').
inf('sycophancy', 'sycophancies').
inf('passivity', 'passivities').
inf('passiveness', 'passivenesses').
inf('intractability', 'intractabilities').
inf('intractableness', 'intractablenesses').
inf('refractoriness', 'refractorinesses').
inf('unmanageableness', 'unmanageablenesses').
inf('recalcitrancy', 'recalcitrancies').
inf('wildness', 'wildnesses').
inf('rebelliousness', 'rebelliousnesses').
inf('obstreperousness', 'obstreperousnesses').
inf('unruliness', 'unrulinesses').
inf('fractiousness', 'fractiousnesses').
inf('willfulness', 'willfulnesses').
inf('wilfulness', 'wilfulnesses').
inf('balkiness', 'balkinesses').
inf('stubbornness', 'stubbornnesses').
inf('obstinacy', 'obstinacies').
inf('mulishness', 'mulishnesses').
inf('contrariness', 'contrarinesses').
inf('perversity', 'perversities').
inf('perverseness', 'perversenesses').
inf('cussedness', 'cussednesses').
inf('orneriness', 'ornerinesses').
inf('naughtiness', 'naughtinesses').
inf('mischievousness', 'mischievousnesses').
inf('badness', 'badnesses').
inf('prankishness', 'prankishnesses').
inf('rascality', 'rascalities').
inf('roguishness', 'roguishnesses').
inf('wildness', 'wildnesses').
inf('dignity', 'dignities').
inf('lordliness', 'lordlinesses').
inf('foppishness', 'foppishnesses').
inf('gentleness', 'gentlenesses').
inf('softness', 'softnesses').
inf('mildness', 'mildnesses').
inf('formality', 'formalities').
inf('formalness', 'formalnesses').
inf('ceremoniousness', 'ceremoniousnesses').
inf('stateliness', 'statelinesses').
inf('informality', 'informalities').
inf('casualness', 'casualnesses').
inf('familiarity', 'familiarities').
inf('slanginess', 'slanginesses').
inf('unceremoniousness', 'unceremoniousnesses').
inf('courtesy', 'courtesies').
inf('politeness', 'politenesses').
inf('niceness', 'nicenesses').
inf('urbanity', 'urbanities').
inf('suavity', 'suavities').
inf('suaveness', 'suavenesses').
inf('blandness', 'blandnesses').
inf('smoothness', 'smoothnesses').
inf('graciousness', 'graciousnesses').
inf('chivalry', 'chivalries').
inf('gallantry', 'gallantries').
inf('respectfulness', 'respectfulnesses').
inf('civility', 'civilities').
inf('discourtesy', 'discourtesies').
inf('rudeness', 'rudenesses').
inf('boorishness', 'boorishnesses').
inf('impoliteness', 'impolitenesses').
inf('ungraciousness', 'ungraciousnesses').
inf('crudeness', 'crudenesses').
inf('crudity', 'crudities').
inf('gaucheness', 'gauchenesses').
inf('incivility', 'incivilities').
inf('abruptness', 'abruptnesses').
inf('brusqueness', 'brusquenesses').
inf('curtness', 'curtnesses').
inf('gruffness', 'gruffnesses').
inf('shortness', 'shortnesses').
inf('cheekiness', 'cheekinesses').
inf('freshness', 'freshnesses').
inf('property', 'properties').
inf('isotropy', 'isotropies').
inf('symmetry', 'symmetries').
inf('anisotropy', 'anisotropies').
inf('connectivity', 'connectivities').
inf('directness', 'directnesses').
inf('straightness', 'straightnesses').
inf('downrightness', 'downrightnesses').
inf('straightforwardness', 'straightforwardnesses').
inf('immediacy', 'immediacies').
inf('immediateness', 'immediatenesses').
inf('pointedness', 'pointednesses').
inf('indirectness', 'indirectnesses').
inf('allusiveness', 'allusivenesses').
inf('mediacy', 'mediacies').
inf('mediateness', 'mediatenesses').
inf('deviousness', 'deviousnesses').
inf('obliqueness', 'obliquenesses').
inf('discursiveness', 'discursivenesses').
inf('robustness', 'robustnesses').
inf('rurality', 'ruralities').
inf('duality', 'dualities').
inf('wave-particle duality', 'wave-particle dualities').
inf('heredity', 'heredities').
inf('heterosis', 'heteroses').
inf('ancestry', 'ancestries').
inf('oldness', 'oldnesses').
inf('obsoleteness', 'obsoletenesses').
inf('ancientness', 'ancientnesses').
inf('antiquity', 'antiquities').
inf('old-fashionedness', 'old-fashionednesses').
inf('quaintness', 'quaintnesses').
inf('time of origin', 'times of origin').
inf('hoariness', 'hoarinesses').
inf('newness', 'newnesses').
inf('brand-newness', 'brand-newnesses').
inf('freshness', 'freshnesses').
inf('crispness', 'crispnesses').
inf('recency', 'recencies').
inf('recentness', 'recentnesses').
inf('oldness', 'oldnesses').
inf('agedness', 'agednesses').
inf('senility', 'senilities').
inf('longevity', 'longevities').
inf('seniority', 'seniorities').
inf('staleness', 'stalenesses').
inf('mustiness', 'mustinesses').
inf('moldiness', 'moldinesses').
inf('youngness', 'youngnesses').
inf('youthfulness', 'youthfulnesses').
inf('juvenility', 'juvenilities').
inf('childishness', 'childishnesses').
inf('puerility', 'puerilities').
inf('touch', 'touches').
inf('common touch', 'common touches').
inf('consistency', 'consistencies').
inf('body', 'bodies').
inf('viscosity', 'viscosities').
inf('viscousness', 'viscousnesses').
inf('stickiness', 'stickinesses').
inf('sliminess', 'sliminesses').
inf('adhesiveness', 'adhesivenesses').
inf('cohesiveness', 'cohesivenesses').
inf('glueyness', 'glueynesses').
inf('gluiness', 'gluinesses').
inf('gumminess', 'gumminesses').
inf('tackiness', 'tackinesses').
inf('ropiness', 'ropinesses').
inf('viscidity', 'viscidities').
inf('viscidness', 'viscidnesses').
inf('gelatinousness', 'gelatinousnesses').
inf('glutinosity', 'glutinosities').
inf('glutinousness', 'glutinousnesses').
inf('thickness', 'thicknesses').
inf('semifluidity', 'semifluidities').
inf('creaminess', 'creaminesses').
inf('soupiness', 'soupinesses').
inf('thinness', 'thinnesses').
inf('fluidity', 'fluidities').
inf('fluidness', 'fluidnesses').
inf('liquidity', 'liquidities').
inf('liquidness', 'liquidnesses').
inf('runniness', 'runninesses').
inf('wateriness', 'waterinesses').
inf('hardness', 'hardnesses').
inf('hardness', 'hardnesses').
inf('firmness', 'firmnesses').
inf('softness', 'softnesses').
inf('compressibility', 'compressibilities').
inf('squeezability', 'squeezabilities').
inf('sponginess', 'sponginesses').
inf('incompressibility', 'incompressibilities').
inf('downiness', 'downinesses').
inf('featheriness', 'featherinesses').
inf('fluffiness', 'fluffinesses').
inf('flabbiness', 'flabbinesses').
inf('limpness', 'limpnesses').
inf('flaccidity', 'flaccidities').
inf('mushiness', 'mushinesses').
inf('pulpiness', 'pulpinesses').
inf('breakableness', 'breakablenesses').
inf('brittleness', 'brittlenesses').
inf('crispness', 'crispnesses').
inf('crispiness', 'crispinesses').
inf('crumbliness', 'crumblinesses').
inf('friability', 'friabilities').
inf('flakiness', 'flakinesses').
inf('unbreakableness', 'unbreakablenesses').
inf('porosity', 'porosities').
inf('porousness', 'porousnesses').
inf('sponginess', 'sponginesses').
inf('permeability', 'permeabilities').
inf('permeableness', 'permeablenesses').
inf('penetrability', 'penetrabilities').
inf('perviousness', 'perviousnesses').
inf('absorbency', 'absorbencies').
inf('solidity', 'solidities').
inf('solidness', 'solidnesses').
inf('compactness', 'compactnesses').
inf('density', 'densities').
inf('denseness', 'densenesses').
inf('specific gravity', 'specific gravities').
inf('vapor density', 'vapor densities').
inf('vapour density', 'vapour densities').
inf('impermeability', 'impermeabilities').
inf('impermeableness', 'impermeablenesses').
inf('retentiveness', 'retentivenesses').
inf('retentivity', 'retentivities').
inf('impenetrability', 'impenetrabilities').
inf('imperviousness', 'imperviousnesses').
inf('nonabsorbency', 'nonabsorbencies').
inf('aptness', 'aptnesses').
inf('propensity', 'propensities').
inf('mordacity', 'mordacities').
inf('proneness', 'pronenesses').
inf('tendency', 'tendencies').
inf('buoyancy', 'buoyancies').
inf('electronegativity', 'electronegativities').
inf('negativity', 'negativities').
inf('stainability', 'stainabilities').
inf('hungriness', 'hungrinesses').
inf('thirstiness', 'thirstinesses').
inf('avariciousness', 'avariciousnesses').
inf('covetousness', 'covetousnesses').
inf('cupidity', 'cupidities').
inf('possessiveness', 'possessivenesses').
inf('acquisitiveness', 'acquisitivenesses').
inf('retentiveness', 'retentivenesses').
inf('retentivity', 'retentivities').
inf('tactile property', 'tactile properties').
inf('touch', 'touches').
inf('smoothness', 'smoothnesses').
inf('silkiness', 'silkinesses').
inf('sleekness', 'sleeknesses').
inf('slickness', 'slicknesses').
inf('slipperiness', 'slipperinesses').
inf('soapiness', 'soapinesses').
inf('fineness', 'finenesses').
inf('powderiness', 'powderinesses').
inf('roughness', 'roughnesses').
inf('raggedness', 'raggednesses').
inf('scaliness', 'scalinesses').
inf('coarseness', 'coarsenesses').
inf('nubbiness', 'nubbinesses').
inf('tweediness', 'tweedinesses').
inf('harshness', 'harshnesses').
inf('abrasiveness', 'abrasivenesses').
inf('scratchiness', 'scratchinesses').
inf('coarseness', 'coarsenesses').
inf('graininess', 'graininesses').
inf('granularity', 'granularities').
inf('sandiness', 'sandinesses').
inf('shagginess', 'shagginesses').
inf('bumpiness', 'bumpinesses').
inf('prickliness', 'pricklinesses').
inf('bristliness', 'bristlinesses').
inf('spininess', 'spininesses').
inf('thorniness', 'thorninesses').
inf('visual property', 'visual properties').
inf('sleekness', 'sleeknesses').
inf('woodiness', 'woodinesses').
inf('lightness', 'lightnesses').
inf('halo', 'haloes').
inf('glory', 'glories').
inf('sunniness', 'sunninesses').
inf('cloudlessness', 'cloudlessnesses').
inf('brightness', 'brightnesses').
inf('flash', 'flashes').
inf('radiancy', 'radiancies').
inf('refulgency', 'refulgencies').
inf('radio brightness', 'radio brightnesses').
inf('lambency', 'lambencies').
inf('shininess', 'shininesses').
inf('brilliancy', 'brilliancies').
inf('polish', 'polishes').
inf('gloss', 'glosses').
inf('glossiness', 'glossinesses').
inf('burnish', 'burnishes').
inf('French polish', 'French polishes').
inf('dullness', 'dullnesses').
inf('dimness', 'dimnesses').
inf('subduedness', 'subduednesses').
inf('flatness', 'flatnesses').
inf('lusterlessness', 'lusterlessnesses').
inf('lustrelessness', 'lustrelessnesses').
inf('softness', 'softnesses').
inf('mellowness', 'mellownesses').
inf('richness', 'richnesses').
inf('colorlessness', 'colorlessnesses').
inf('colourlessness', 'colourlessnesses').
inf('achromaticity', 'achromaticities').
inf('blackness', 'blacknesses').
inf('inkiness', 'inkinesses').
inf('ebony', 'ebonies').
inf('whiteness', 'whitenesses').
inf('bleach', 'bleaches').
inf('ivory', 'ivories').
inf('frostiness', 'frostinesses').
inf('hoariness', 'hoarinesses').
inf('grayness', 'graynesses').
inf('greyness', 'greynesses').
inf('redness', 'rednesses').
inf('ruby', 'rubies').
inf('burgundy', 'burgundies').
inf('cherry', 'cherries').
inf('orangeness', 'orangenesses').
inf('yellowness', 'yellownesses').
inf('canary', 'canaries').
inf('greenness', 'greennesses').
inf('viridity', 'viridities').
inf('greenishness', 'greenishnesses').
inf('blueness', 'bluenesses').
inf('navy', 'navies').
inf('purpleness', 'purplenesses').
inf('indigo', 'indigoes').
inf('pinkness', 'pinknesses').
inf('rosiness', 'rosinesses').
inf('solferino', 'solferinoes').
inf('peach', 'peaches').
inf('brownness', 'brownnesses').
inf('mahogany', 'mahoganies').
inf('color property', 'color properties').
inf('chromaticity', 'chromaticities').
inf('intensity', 'intensities').
inf('vividness', 'vividnesses').
inf('paleness', 'palenesses').
inf('pallidity', 'pallidities').
inf('complementary', 'complementaries').
inf('poliosis', 'polioses').
inf('paleness', 'palenesses').
inf('blondness', 'blondnesses').
inf('fairness', 'fairnesses').
inf('ruddiness', 'ruddinesses').
inf('rosiness', 'rosinesses').
inf('lividness', 'lividnesses').
inf('lividity', 'lividities').
inf('luridness', 'luridnesses').
inf('paleness', 'palenesses').
inf('pallidness', 'pallidnesses').
inf('wanness', 'wannesses').
inf('sallowness', 'sallownesses').
inf('tawniness', 'tawninesses').
inf('darkness', 'darknesses').
inf('duskiness', 'duskinesses').
inf('swarthiness', 'swarthinesses').
inf('whiteness', 'whitenesses').
inf('lightness', 'lightnesses').
inf('darkness', 'darknesses').
inf('olfactory property', 'olfactory properties').
inf('fragrancy', 'fragrancies').
inf('sweetness', 'sweetnesses').
inf('malodorousness', 'malodorousnesses').
inf('stinkiness', 'stinkinesses').
inf('foulness', 'foulnesses').
inf('rankness', 'ranknesses').
inf('fetidness', 'fetidnesses').
inf('muskiness', 'muskinesses').
inf('noisiness', 'noisinesses').
inf('racketiness', 'racketinesses').
inf('hush', 'hushes').
inf('stillness', 'stillnesses').
inf('speechlessness', 'speechlessnesses').
inf('quietness', 'quietnesses').
inf('soundlessness', 'soundlessnesses').
inf('noiselessness', 'noiselessnesses').
inf('sound property', 'sound properties').
inf('musicality', 'musicalities').
inf('musicalness', 'musicalnesses').
inf('lyricality', 'lyricalities').
inf('songfulness', 'songfulnesses').
inf('melodiousness', 'melodiousnesses').
inf('tunefulness', 'tunefulnesses').
inf('harmony', 'harmonies').
inf('harmoniousness', 'harmoniousnesses').
inf('disharmony', 'disharmonies').
inf('inharmoniousness', 'inharmoniousnesses').
inf('cacophony', 'cacophonies').
inf('boisterousness', 'boisterousnesses').
inf('pitch', 'pitches').
inf('concert pitch', 'concert pitches').
inf('philharmonic pitch', 'philharmonic pitches').
inf('international pitch', 'international pitches').
inf('high pitch', 'high pitches').
inf('high frequency', 'high frequencies').
inf('soprano', 'sopranoes').
inf('low pitch', 'low pitches').
inf('low frequency', 'low frequencies').
inf('deepness', 'deepnesses').
inf('alto', 'altoes').
inf('alto', 'altoes').
inf('bass', 'basses').
inf('quality', 'qualities').
inf('harshness', 'harshnesses').
inf('roughness', 'roughnesses').
inf('gruffness', 'gruffnesses').
inf('hoarseness', 'hoarsenesses').
inf('huskiness', 'huskinesses').
inf('fullness', 'fullnesses').
inf('mellowness', 'mellownesses').
inf('richness', 'richnesses').
inf('nasality', 'nasalities').
inf('plangency', 'plangencies').
inf('sonorousness', 'sonorousnesses').
inf('sonority', 'sonorities').
inf('vibrancy', 'vibrancies').
inf('shrillness', 'shrillnesses').
inf('stridency', 'stridencies').
inf('loudness', 'loudnesses').
inf('intensity', 'intensities').
inf('crescendo', 'crescendoes').
inf('fortissimo', 'fortissimoes').
inf('softness', 'softnesses').
inf('faintness', 'faintnesses').
inf('decrescendo', 'decrescendoes').
inf('diminuendo', 'diminuendoes').
inf('piano', 'pianoes').
inf('pianissimo', 'pianissimoes').
inf('rhythmicity', 'rhythmicities').
inf('cadency', 'cadencies').
inf('taste property', 'taste properties').
inf('rancidness', 'rancidnesses').
inf('spiciness', 'spicinesses').
inf('spicery', 'spiceries').
inf('pungency', 'pungencies').
inf('sharpness', 'sharpnesses').
inf('raciness', 'racinesses').
inf('piquancy', 'piquancies').
inf('piquantness', 'piquantnesses').
inf('tanginess', 'tanginesses').
inf('hotness', 'hotnesses').
inf('pepperiness', 'pepperinesses').
inf('saltiness', 'saltinesses').
inf('brininess', 'brininesses').
inf('salinity', 'salinities').
inf('brackishness', 'brackishnesses').
inf('sourness', 'sournesses').
inf('acidity', 'acidities').
inf('acerbity', 'acerbities').
inf('tartness', 'tartnesses').
inf('vinegariness', 'vinegarinesses').
inf('vinegarishness', 'vinegarishnesses').
inf('sweetness', 'sweetnesses').
inf('saccharinity', 'saccharinities').
inf('sugariness', 'sugarinesses').
inf('bitterness', 'bitternesses').
inf('acerbity', 'acerbities').
inf('acridity', 'acridities').
inf('acridness', 'acridnesses').
inf('palatability', 'palatabilities').
inf('palatableness', 'palatablenesses').
inf('pleasingness', 'pleasingnesses').
inf('tastiness', 'tastinesses').
inf('appetizingness', 'appetizingnesses').
inf('appetisingness', 'appetisingnesses').
inf('delectability', 'delectabilities').
inf('deliciousness', 'deliciousnesses').
inf('lusciousness', 'lusciousnesses').
inf('toothsomeness', 'toothsomenesses').
inf('flavorsomeness', 'flavorsomenesses').
inf('flavoursomeness', 'flavoursomenesses').
inf('savoriness', 'savorinesses').
inf('sapidity', 'sapidities').
inf('sapidness', 'sapidnesses').
inf('succulency', 'succulencies').
inf('juiciness', 'juicinesses').
inf('unpalatability', 'unpalatabilities').
inf('unpalatableness', 'unpalatablenesses').
inf('disgustingness', 'disgustingnesses').
inf('distastefulness', 'distastefulnesses').
inf('nauseatingness', 'nauseatingnesses').
inf('sickeningness', 'sickeningnesses').
inf('unsavoriness', 'unsavorinesses').
inf('unappetizingness', 'unappetizingnesses').
inf('unappetisingness', 'unappetisingnesses').
inf('flavorlessness', 'flavorlessnesses').
inf('flavourlessness', 'flavourlessnesses').
inf('savorlessness', 'savorlessnesses').
inf('savourlessness', 'savourlessnesses').
inf('tastelessness', 'tastelessnesses').
inf('blandness', 'blandnesses').
inf('insipidity', 'insipidities').
inf('insipidness', 'insipidnesses').
inf('edibility', 'edibilities').
inf('edibleness', 'ediblenesses').
inf('digestibility', 'digestibilities').
inf('digestibleness', 'digestiblenesses').
inf('indigestibility', 'indigestibilities').
inf('indigestibleness', 'indigestiblenesses').
inf('bodily property', 'bodily properties').
inf('laterality', 'lateralities').
inf('lankiness', 'lankinesses').
inf('dumpiness', 'dumpinesses').
inf('squattiness', 'squattinesses').
inf('ectomorphy', 'ectomorphies').
inf('endomorphy', 'endomorphies').
inf('mesomorphy', 'mesomorphies').
inf('fatness', 'fatnesses').
inf('avoirdupois', 'avoirdupois').
inf('adiposity', 'adiposities').
inf('adiposeness', 'adiposenesses').
inf('fattiness', 'fattinesses').
inf('abdominousness', 'abdominousnesses').
inf('paunchiness', 'paunchinesses').
inf('greasiness', 'greasinesses').
inf('oiliness', 'oilinesses').
inf('oleaginousness', 'oleaginousnesses').
inf('fleshiness', 'fleshinesses').
inf('obesity', 'obesities').
inf('corpulency', 'corpulencies').
inf('stoutness', 'stoutnesses').
inf('adiposis', 'adiposes').
inf('exogenous obesity', 'exogenous obesities').
inf('plumpness', 'plumpnesses').
inf('roundness', 'roundnesses').
inf('chubbiness', 'chubbinesses').
inf('pudginess', 'pudginesses').
inf('tubbiness', 'tubbinesses').
inf('rolypoliness', 'rolypolinesses').
inf('buxomness', 'buxomnesses').
inf('leanness', 'leannesses').
inf('thinness', 'thinnesses').
inf('spareness', 'sparenesses').
inf('skinniness', 'skinninesses').
inf('scrawniness', 'scrawninesses').
inf('bonyness', 'bonynesses').
inf('boniness', 'boninesses').
inf('gauntness', 'gauntnesses').
inf('slenderness', 'slendernesses').
inf('slightness', 'slightnesses').
inf('slimness', 'slimnesses').
inf('tallness', 'tallnesses').
inf('shortness', 'shortnesses').
inf('manner of walking', 'manners of walking').
inf('slouch', 'slouches').
inf('gracefulness', 'gracefulnesses').
inf('gracility', 'gracilities').
inf('agility', 'agilities').
inf('legerity', 'legerities').
inf('lightness', 'lightnesses').
inf('lightsomeness', 'lightsomenesses').
inf('nimbleness', 'nimblenesses').
inf('lissomeness', 'lissomenesses').
inf('litheness', 'lithenesses').
inf('suppleness', 'supplenesses').
inf('awkwardness', 'awkwardnesses').
inf('clumsiness', 'clumsinesses').
inf('gracelessness', 'gracelessnesses').
inf('ungracefulness', 'ungracefulnesses').
inf('gawkiness', 'gawkinesses').
inf('ungainliness', 'ungainlinesses').
inf('stiffness', 'stiffnesses').
inf('physiology', 'physiologies').
inf('physiological property', 'physiological properties').
inf('animateness', 'animatenesses').
inf('aliveness', 'alivenesses').
inf('liveness', 'livenesses').
inf('vitality', 'vitalities').
inf('inanimateness', 'inanimatenesses').
inf('lifelessness', 'lifelessnesses').
inf('deadness', 'deadnesses').
inf('sex', 'sexes').
inf('sexuality', 'sexualities').
inf('asexuality', 'asexualities').
inf('sexlessness', 'sexlessnesses').
inf('maleness', 'malenesses').
inf('masculinity', 'masculinities').
inf('virility', 'virilities').
inf('androgyny', 'androgynies').
inf('bisexuality', 'bisexualities').
inf('femaleness', 'femalenesses').
inf('feminineness', 'femininenesses').
inf('physical property', 'physical properties').
inf('chemical property', 'chemical properties').
inf('volatility', 'volatilities').
inf('absorptivity', 'absorptivities').
inf('dissolubility', 'dissolubilities').
inf('solubleness', 'solublenesses').
inf('drippiness', 'drippinesses').
inf('reflectivity', 'reflectivities').
inf('echo', 'echoes').
inf('re-echo', 're-echoes').
inf('echo', 'echoes').
inf('refractivity', 'refractivities').
inf('refractiveness', 'refractivenesses').
inf('enthalpy', 'enthalpies').
inf('randomness', 'randomnesses').
inf('entropy', 'entropies').
inf('conformational entropy', 'conformational entropies').
inf('absolute zero', 'absolute zeroes').
inf('mercury', 'mercuries').
inf('coldness', 'coldnesses').
inf('frigidity', 'frigidities').
inf('frigidness', 'frigidnesses').
inf('iciness', 'icinesses').
inf('gelidity', 'gelidities').
inf('chilliness', 'chillinesses').
inf('coolness', 'coolnesses').
inf('frostiness', 'frostinesses').
inf('hotness', 'hotnesses').
inf('fieriness', 'fierinesses').
inf('torridity', 'torridities').
inf('warmness', 'warmnesses').
inf('lukewarmness', 'lukewarmnesses').
inf('tepidity', 'tepidities').
inf('tepidness', 'tepidnesses').
inf('perceptibility', 'perceptibilities').
inf('visibility', 'visibilities').
inf('visibleness', 'visiblenesses').
inf('invisibility', 'invisibilities').
inf('invisibleness', 'invisiblenesses').
inf('luminosity', 'luminosities').
inf('brightness', 'brightnesses').
inf('luminousness', 'luminousnesses').
inf('audibility', 'audibilities').
inf('audibleness', 'audiblenesses').
inf('inaudibility', 'inaudibilities').
inf('inaudibleness', 'inaudiblenesses').
inf('imperceptibility', 'imperceptibilities').
inf('reluctivity', 'reluctivities').
inf('sensitivity', 'sensitivities').
inf('sensitiveness', 'sensitivenesses').
inf('elasticity', 'elasticities').
inf('resiliency', 'resiliencies').
inf('bounciness', 'bouncinesses').
inf('springiness', 'springinesses').
inf('stretch', 'stretches').
inf('stretchiness', 'stretchinesses').
inf('stretchability', 'stretchabilities').
inf('toughness', 'toughnesses').
inf('elasticity of shear', 'elasticities of shear').
inf('malleability', 'malleabilities').
inf('plasticity', 'plasticities').
inf('ductility', 'ductilities').
inf('ductileness', 'ductilenesses').
inf('fibrosity', 'fibrosities').
inf('fibrousness', 'fibrousnesses').
inf('flexibility', 'flexibilities').
inf('flexibleness', 'flexiblenesses').
inf('bendability', 'bendabilities').
inf('pliability', 'pliabilities').
inf('pliancy', 'pliancies').
inf('pliantness', 'pliantnesses').
inf('suppleness', 'supplenesses').
inf('inelasticity', 'inelasticities').
inf('deadness', 'deadnesses').
inf('stiffness', 'stiffnesses').
inf('rigidity', 'rigidities').
inf('rigidness', 'rigidnesses').
inf('unmalleability', 'unmalleabilities').
inf('inflexibility', 'inflexibilities').
inf('inflexibleness', 'inflexiblenesses').
inf('mass', 'masses').
inf('body', 'bodies').
inf('biomass', 'biomasses').
inf('critical mass', 'critical masses').
inf('rest mass', 'rest masses').
inf('relativistic mass', 'relativistic masses').
inf('gravitational mass', 'gravitational masses').
inf('inertial mass', 'inertial masses').
inf('atomic mass', 'atomic masses').
inf('relative atomic mass', 'relative atomic masses').
inf('mass energy', 'mass energies').
inf('relative molecular mass', 'relative molecular masses').
inf('heaviness', 'heavinesses').
inf('weightiness', 'weightinesses').
inf('heftiness', 'heftinesses').
inf('massiveness', 'massivenesses').
inf('ponderousness', 'ponderousnesses').
inf('ponderosity', 'ponderosities').
inf('lightness', 'lightnesses').
inf('weightlessness', 'weightlessnesses').
inf('airiness', 'airinesses').
inf('buoyancy', 'buoyancies').
inf('sustainability', 'sustainabilities').
inf('brawniness', 'brawninesses').
inf('muscularity', 'muscularities').
inf('heftiness', 'heftinesses').
inf('mightiness', 'mightinesses').
inf('heartiness', 'heartinesses').
inf('robustness', 'robustnesses').
inf('hardiness', 'hardinesses').
inf('lustiness', 'lustinesses').
inf('validity', 'validities').
inf('huskiness', 'huskinesses').
inf('ruggedness', 'ruggednesses').
inf('toughness', 'toughnesses').
inf('smallness', 'smallnesses').
inf('littleness', 'littlenesses').
inf('stoutness', 'stoutnesses').
inf('stalwartness', 'stalwartnesses').
inf('sturdiness', 'sturdinesses').
inf('firmness', 'firmnesses').
inf('soundness', 'soundnesses').
inf('indomitability', 'indomitabilities').
inf('invincibility', 'invincibilities').
inf('toughness', 'toughnesses').
inf('wiriness', 'wirinesses').
inf('capacity', 'capacities').
inf('invulnerability', 'invulnerabilities').
inf('immunity', 'immunities').
inf('power of appointment', 'powers of appointment').
inf('potency', 'potencies').
inf('effectiveness', 'effectivenesses').
inf('valency', 'valencies').
inf('covalency', 'covalencies').
inf('valency', 'valencies').
inf('forcefulness', 'forcefulnesses').
inf('energy', 'energies').
inf('strenuosity', 'strenuosities').
inf('intensity', 'intensities').
inf('intensiveness', 'intensivenesses').
inf('badness', 'badnesses').
inf('severity', 'severities').
inf('severeness', 'severenesses').
inf('foulness', 'foulnesses').
inf('raininess', 'raininesses').
inf('seriousness', 'seriousnesses').
inf('distressfulness', 'distressfulnesses').
inf('emphasis', 'emphases').
inf('overemphasis', 'overemphases').
inf('ferocity', 'ferocities').
inf('fierceness', 'fiercenesses').
inf('furiousness', 'furiousnesses').
inf('fury', 'furies').
inf('wildness', 'wildnesses').
inf('savageness', 'savagenesses').
inf('savagery', 'savageries').
inf('acidity', 'acidities').
inf('hyperacidity', 'hyperacidities').
inf('alkalinity', 'alkalinities').
inf('neutrality', 'neutralities').
inf('molality', 'molalities').
inf('molarity', 'molarities').
inf('weakness', 'weaknesses').
inf('feebleness', 'feeblenesses').
inf('tenuity', 'tenuities').
inf('faintness', 'faintnesses').
inf('flimsiness', 'flimsinesses').
inf('shoddiness', 'shoddinesses').
inf('fragility', 'fragilities').
inf('delicacy', 'delicacies').
inf('insubstantiality', 'insubstantialities').
inf('fatigability', 'fatigabilities').
inf('lethargy', 'lethargies').
inf('slackness', 'slacknesses').
inf('underbelly', 'underbellies').
inf('vulnerability', 'vulnerabilities').
inf('defenselessness', 'defenselessnesses').
inf('defencelessness', 'defencelessnesses').
inf('unprotectedness', 'unprotectednesses').
inf('assailability', 'assailabilities').
inf('destructibility', 'destructibilities').
inf('indestructibility', 'indestructibilities').
inf('fragility', 'fragilities').
inf('breakability', 'breakabilities').
inf('frangibleness', 'frangiblenesses').
inf('frangibility', 'frangibilities').
inf('temporal property', 'temporal properties').
inf('successiveness', 'successivenesses').
inf('approach', 'approaches').
inf('earliness', 'earlinesses').
inf('forwardness', 'forwardnesses').
inf('lateness', 'latenesses').
inf('priority', 'priorities').
inf('antecedency', 'antecedencies').
inf('anteriority', 'anteriorities').
inf('precedency', 'precedencies').
inf('posteriority', 'posteriorities').
inf('subsequentness', 'subsequentnesses').
inf('punctuality', 'punctualities').
inf('promptness', 'promptnesses').
inf('tardiness', 'tardinesses').
inf('simultaneity', 'simultaneities').
inf('simultaneousness', 'simultaneousnesses').
inf('contemporaneity', 'contemporaneities').
inf('contemporaneousness', 'contemporaneousnesses').
inf('seasonableness', 'seasonablenesses').
inf('timeliness', 'timelinesses').
inf('unseasonableness', 'unseasonablenesses').
inf('untimeliness', 'untimelinesses').
inf('pastness', 'pastnesses').
inf('recency', 'recencies').
inf('recentness', 'recentnesses').
inf('futurity', 'futurities').
inf('presentness', 'presentnesses').
inf('nowness', 'nownesses').
inf('currentness', 'currentnesses').
inf('currency', 'currencies').
inf('up-to-dateness', 'ups-to-dateness').
inf('modernity', 'modernities').
inf('modernness', 'modernnesses').
inf('contemporaneity', 'contemporaneities').
inf('contemporaneousness', 'contemporaneousnesses').
inf('longness', 'longnesses').
inf('longevity', 'longevities').
inf('length of service', 'lengths of service').
inf('lengthiness', 'lengthinesses').
inf('endlessness', 'endlessnesses').
inf('continuousness', 'continuousnesses').
inf('ceaselessness', 'ceaselessnesses').
inf('incessancy', 'incessancies').
inf('incessantness', 'incessantnesses').
inf('shortness', 'shortnesses').
inf('brevity', 'brevities').
inf('briefness', 'briefnesses').
inf('permanency', 'permanencies').
inf('perpetuity', 'perpetuities').
inf('sempiternity', 'sempiternities').
inf('lastingness', 'lastingnesses').
inf('durability', 'durabilities').
inf('enduringness', 'enduringnesses').
inf('continuity', 'continuities').
inf('changelessness', 'changelessnesses').
inf('everlastingness', 'everlastingnesses').
inf('imperishability', 'imperishabilities').
inf('imperishableness', 'imperishablenesses').
inf('imperishingness', 'imperishingnesses').
inf('perdurability', 'perdurabilities').
inf('impermanency', 'impermanencies').
inf('temporariness', 'temporarinesses').
inf('transiency', 'transiencies').
inf('transitoriness', 'transitorinesses').
inf('fugacity', 'fugacities').
inf('fugaciousness', 'fugaciousnesses').
inf('ephemerality', 'ephemeralities').
inf('ephemeralness', 'ephemeralnesses').
inf('fleetingness', 'fleetingnesses').
inf('fugacity', 'fugacities').
inf('mortality', 'mortalities').
inf('immortality', 'immortalities').
inf('viability', 'viabilities').
inf('audio frequency', 'audio frequencies').
inf('radio frequency', 'radio frequencies').
inf('infrared frequency', 'infrared frequencies').
inf('extremely low frequency', 'extremely low frequencies').
inf('very low frequency', 'very low frequencies').
inf('low frequency', 'low frequencies').
inf('medium frequency', 'medium frequencies').
inf('high frequency', 'high frequencies').
inf('very high frequency', 'very high frequencies').
inf('ultrahigh frequency', 'ultrahigh frequencies').
inf('superhigh frequency', 'superhigh frequencies').
inf('extremely high frequency', 'extremely high frequencies').
inf('swiftness', 'swiftnesses').
inf('fastness', 'fastnesses').
inf('fleetness', 'fleetnesses').
inf('celerity', 'celerities').
inf('quickness', 'quicknesses').
inf('rapidity', 'rapidities').
inf('rapidness', 'rapidnesses').
inf('speediness', 'speedinesses').
inf('immediacy', 'immediacies').
inf('immediateness', 'immediatenesses').
inf('instantaneousness', 'instantaneousnesses').
inf('instancy', 'instancies').
inf('dispatch', 'dispatches').
inf('despatch', 'despatches').
inf('expeditiousness', 'expeditiousnesses').
inf('promptness', 'promptnesses').
inf('hastiness', 'hastinesses').
inf('hurry', 'hurries').
inf('hurriedness', 'hurriednesses').
inf('abruptness', 'abruptnesses').
inf('precipitateness', 'precipitatenesses').
inf('precipitousness', 'precipitousnesses').
inf('precipitancy', 'precipitancies').
inf('suddenness', 'suddennesses').
inf('graduality', 'gradualities').
inf('gradualness', 'gradualnesses').
inf('slowness', 'slownesses').
inf('deliberateness', 'deliberatenesses').
inf('unhurriedness', 'unhurriednesses').
inf('leisureliness', 'leisurelinesses').
inf('dilatoriness', 'dilatorinesses').
inf('sluggishness', 'sluggishnesses').
inf('spatial property', 'spatial properties').
inf('spatiality', 'spatialities').
inf('dimensionality', 'dimensionalities').
inf('one-dimensionality', 'one-dimensionalities').
inf('linearity', 'linearities').
inf('two-dimensionality', 'two-dimensionalities').
inf('flatness', 'flatnesses').
inf('planeness', 'planenesses').
inf('three-dimensionality', 'three-dimensionalities').
inf('third-dimensionality', 'third-dimensionalities').
inf('cubicity', 'cubicities').
inf('directionality', 'directionalities').
inf('topography', 'topographies').
inf('lobularity', 'lobularities').
inf('symmetry', 'symmetries').
inf('symmetricalness', 'symmetricalnesses').
inf('regularity', 'regularities').
inf('geometrical regularity', 'geometrical regularities').
inf('bilaterality', 'bilateralities').
inf('bilateral symmetry', 'bilateral symmetries').
inf('radial symmetry', 'radial symmetries').
inf('asymmetry', 'asymmetries').
inf('dissymmetry', 'dissymmetries').
inf('irregularity', 'irregularities').
inf('geometrical irregularity', 'geometrical irregularities').
inf('lopsidedness', 'lopsidednesses').
inf('skewness', 'skewnesses').
inf('obliqueness', 'obliquenesses').
inf('radial asymmetry', 'radial asymmetries').
inf('directivity', 'directivities').
inf('directionality', 'directionalities').
inf('directivity', 'directivities').
inf('directiveness', 'directivenesses').
inf('handedness', 'handednesses').
inf('laterality', 'lateralities').
inf('ambidexterity', 'ambidexterities').
inf('ambidextrousness', 'ambidextrousnesses').
inf('left-handedness', 'left-handednesses').
inf('sinistrality', 'sinistralities').
inf('right-handedness', 'right-handednesses').
inf('dextrality', 'dextralities').
inf('footedness', 'footednesses').
inf('eyedness', 'eyednesses').
inf('pitch', 'pitches').
inf('abruptness', 'abruptnesses').
inf('precipitousness', 'precipitousnesses').
inf('steepness', 'steepnesses').
inf('gradualness', 'gradualnesses').
inf('gentleness', 'gentlenesses').
inf('concavity', 'concavities').
inf('concaveness', 'concavenesses').
inf('hollowness', 'hollownesses').
inf('convexity', 'convexities').
inf('convexness', 'convexnesses').
inf('roundedness', 'roundednesses').
inf('bulginess', 'bulginesses').
inf('oblateness', 'oblatenesses').
inf('ellipticity', 'ellipticities').
inf('angularity', 'angularities').
inf('pointedness', 'pointednesses').
inf('unpointedness', 'unpointednesses').
inf('rectangularity', 'rectangularities').
inf('oblongness', 'oblongnesses').
inf('orthogonality', 'orthogonalities').
inf('perpendicularity', 'perpendicularities').
inf('squareness', 'squarenesses').
inf('triangularity', 'triangularities').
inf('roundness', 'roundnesses').
inf('sphericity', 'sphericities').
inf('sphericalness', 'sphericalnesses').
inf('globosity', 'globosities').
inf('globularness', 'globularnesses').
inf('rotundity', 'rotundities').
inf('rotundness', 'rotundnesses').
inf('cylindricality', 'cylindricalities').
inf('cylindricalness', 'cylindricalnesses').
inf('circularity', 'circularities').
inf('concentricity', 'concentricities').
inf('eccentricity', 'eccentricities').
inf('straightness', 'straightnesses').
inf('crookedness', 'crookednesses').
inf('curliness', 'curlinesses').
inf('waviness', 'wavinesses').
inf('straightness', 'straightnesses').
inf('point of view', 'points of view').
inf('proportionality', 'proportionalities').
inf('centrality', 'centralities').
inf('marginality', 'marginalities').
inf('anteriority', 'anteriorities').
inf('posteriority', 'posteriorities').
inf('outwardness', 'outwardnesses').
inf('externality', 'externalities').
inf('inwardness', 'inwardnesses').
inf('northernness', 'northernnesses').
inf('southernness', 'southernnesses').
inf('horizontality', 'horizontalities').
inf('verticality', 'verticalities').
inf('verticalness', 'verticalnesses').
inf('erectness', 'erectnesses').
inf('uprightness', 'uprightnesses').
inf('address', 'addresses').
inf('erectness', 'erectnesses').
inf('uprightness', 'uprightnesses').
inf('openness', 'opennesses').
inf('patency', 'patencies').
inf('farness', 'farnesses').
inf('remoteness', 'remotenesses').
inf('farawayness', 'farawaynesses').
inf('far cry', 'far cries').
inf('nearness', 'nearnesses').
inf('closeness', 'closenesses').
inf('proximity', 'proximities').
inf('propinquity', 'propinquities').
inf('adjacency', 'adjacencies').
inf('contiguity', 'contiguities').
inf('contiguousness', 'contiguousnesses').
inf('diffuseness', 'diffusenesses').
inf('density', 'densities').
inf('denseness', 'densenesses').
inf('tightness', 'tightnesses').
inf('compactness', 'compactnesses').
inf('bits per inch', 'bits per inches').
inf('flux density', 'flux densities').
inf('flux', 'fluxes').
inf('optical density', 'optical densities').
inf('transmission density', 'transmission densities').
inf('photographic density', 'photographic densities').
inf('rarity', 'rarities').
inf('tenuity', 'tenuities').
inf('low density', 'low densities').
inf('relative density', 'relative densities').
inf('order of magnitude', 'orders of magnitude').
inf('entropy', 'entropies').
inf('probability', 'probabilities').
inf('conditional probability', 'conditional probabilities').
inf('contingent probability', 'contingent probabilities').
inf('joint probability', 'joint probabilities').
inf('risk of exposure', 'risks of exposure').
inf('risk of infection', 'risks of infection').
inf('profundity', 'profundities').
inf('profoundness', 'profoundnesses').
inf('superficiality', 'superficialities').
inf('shallowness', 'shallownesses').
inf('glibness', 'glibnesses').
inf('slickness', 'slicknesses').
inf('octavo', 'octavoes').
inf('eightvo', 'eightvoes').
inf('8vo', '8voes').
inf('quarto', 'quartoes').
inf('4to', '4toes').
inf('highness', 'highnesses').
inf('lowness', 'lownesses').
inf('extremeness', 'extremenesses').
inf('multiplicity', 'multiplicities').
inf('triplicity', 'triplicities').
inf('mass', 'masses').
inf('muchness', 'muchnesses').
inf('intensity', 'intensities').
inf('field intensity', 'field intensities').
inf('magnetic intensity', 'magnetic intensities').
inf('magnetic flux density', 'magnetic flux densities').
inf('light intensity', 'light intensities').
inf('half-intensity', 'half-intensities').
inf('radius of curvature', 'radii of curvature').
inf('center of curvature', 'centers of curvature').
inf('centre of curvature', 'centres of curvature').
inf('circle of curvature', 'circles of curvature').
inf('thickness', 'thicknesses').
inf('thinness', 'thinnesses').
inf('tenuity', 'tenuities').
inf('slenderness', 'slendernesses').
inf('largeness', 'largenesses').
inf('bigness', 'bignesses').
inf('ampleness', 'amplenesses').
inf('bulkiness', 'bulkinesses').
inf('massiveness', 'massivenesses').
inf('enormousness', 'enormousnesses').
inf('grandness', 'grandnesses').
inf('greatness', 'greatnesses').
inf('immenseness', 'immensenesses').
inf('immensity', 'immensities').
inf('sizeableness', 'sizeablenesses').
inf('vastness', 'vastnesses').
inf('wideness', 'widenesses').
inf('enormity', 'enormities').
inf('capaciousness', 'capaciousnesses').
inf('roominess', 'roominesses').
inf('spaciousness', 'spaciousnesses').
inf('commodiousness', 'commodiousnesses').
inf('airiness', 'airinesses').
inf('seating capacity', 'seating capacities').
inf('fullness', 'fullnesses').
inf('voluminosity', 'voluminosities').
inf('voluminousness', 'voluminousnesses').
inf('largeness', 'largenesses').
inf('extensiveness', 'extensivenesses').
inf('smallness', 'smallnesses').
inf('littleness', 'littlenesses').
inf('diminutiveness', 'diminutivenesses').
inf('minuteness', 'minutenesses').
inf('petiteness', 'petitenesses').
inf('tininess', 'tininesses').
inf('weeness', 'weenesses').
inf('delicacy', 'delicacies').
inf('slightness', 'slightnesses').
inf('puniness', 'puninesses').
inf('runtiness', 'runtinesses').
inf('stuntedness', 'stuntednesses').
inf('dwarfishness', 'dwarfishnesses').
inf('positivity', 'positivities').
inf('positiveness', 'positivenesses').
inf('negativity', 'negativities').
inf('negativeness', 'negativenesses').
inf('critical mass', 'critical masses').
inf('quantity', 'quantities').
inf('smallness', 'smallnesses').
inf('stuffiness', 'stuffinesses').
inf('closeness', 'closenesses').
inf('sufficiency', 'sufficiencies').
inf('adequacy', 'adequacies').
inf('ampleness', 'amplenesses').
inf('insufficiency', 'insufficiencies').
inf('inadequacy', 'inadequacies').
inf('deficiency', 'deficiencies').
inf('meagerness', 'meagernesses').
inf('meagreness', 'meagrenesses').
inf('leanness', 'leannesses').
inf('poorness', 'poornesses').
inf('scantiness', 'scantinesses').
inf('scantness', 'scantnesses').
inf('exiguity', 'exiguities').
inf('wateriness', 'waterinesses').
inf('abstemiousness', 'abstemiousnesses').
inf('sparseness', 'sparsenesses').
inf('spareness', 'sparenesses').
inf('sparsity', 'sparsities').
inf('thinness', 'thinnesses').
inf('copiousness', 'copiousnesses').
inf('teemingness', 'teemingnesses').
inf('bountifulness', 'bountifulnesses').
inf('bounty', 'bounties').
inf('plenty', 'plenties').
inf('plentifulness', 'plentifulnesses').
inf('plenteousness', 'plenteousnesses').
inf('profuseness', 'profusenesses').
inf('richness', 'richnesses').
inf('lushness', 'lushnesses').
inf('voluptuousness', 'voluptuousnesses').
inf('greenness', 'greennesses').
inf('verdancy', 'verdancies').
inf('wilderness', 'wildernesses').
inf('scarcity', 'scarcities').
inf('scarceness', 'scarcenesses').
inf('paucity', 'paucities').
inf('rarity', 'rarities').
inf('rareness', 'rarenesses').
inf('infrequency', 'infrequencies').
inf('slenderness', 'slendernesses').
inf('moderateness', 'moderatenesses').
inf('reasonableness', 'reasonablenesses').
inf('immoderateness', 'immoderatenesses').
inf('excess', 'excesses').
inf('excessiveness', 'excessivenesses').
inf('inordinateness', 'inordinatenesses').
inf('extravagancy', 'extravagancies').
inf('outrageousness', 'outrageousnesses').
inf('luxury', 'luxuries').
inf('overmuch', 'overmuches').
inf('overmuchness', 'overmuchnesses').
inf('excess', 'excesses').
inf('nimiety', 'nimieties').
inf('oversupply', 'oversupplies').
inf('superfluity', 'superfluities').
inf('redundancy', 'redundancies').
inf('margin of safety', 'margins of safety').
inf('margin of error', 'margins of error').
inf('narrowness', 'narrownesses').
inf('slimness', 'slimnesses').
inf('numerousness', 'numerousnesses').
inf('numerosity', 'numerosities').
inf('multiplicity', 'multiplicities').
inf('multitudinousness', 'multitudinousnesses').
inf('innumerableness', 'innumerablenesses').
inf('countlessness', 'countlessnesses').
inf('majority', 'majorities').
inf('minority', 'minorities').
inf('fewness', 'fewnesses').
inf('roundness', 'roundnesses').
inf('boundary', 'boundaries').
inf('starkness', 'starknesses').
inf('absoluteness', 'absolutenesses').
inf('utterness', 'utternesses').
inf('reach', 'reaches').
inf('compass', 'compasses').
inf('internationality', 'internationalities').
inf('falsetto', 'falsettoes').
inf('isometry', 'isometries').
inf('longness', 'longnesses').
inf('lengthiness', 'lengthinesses').
inf('shortness', 'shortnesses').
inf('briefness', 'briefnesses').
inf('deepness', 'deepnesses').
inf('deepness', 'deepnesses').
inf('profundity', 'profundities').
inf('profoundness', 'profoundnesses').
inf('bottomlessness', 'bottomlessnesses').
inf('shallowness', 'shallownesses').
inf('superficiality', 'superficialities').
inf('wideness', 'widenesses').
inf('broadness', 'broadnesses').
inf('thickness', 'thicknesses').
inf('heaviness', 'heavinesses').
inf('narrowness', 'narrownesses').
inf('fineness', 'finenesses').
inf('thinness', 'thinnesses').
inf('tallness', 'tallnesses').
inf('highness', 'highnesses').
inf('loftiness', 'loftinesses').
inf('lowness', 'lownesses').
inf('squatness', 'squatnesses').
inf('stubbiness', 'stubbinesses').
inf('shortness', 'shortnesses').
inf('praisworthiness', 'praisworthinesses').
inf('worthwhileness', 'worthwhilenesses').
inf('worthlessness', 'worthlessnesses').
inf('fecklessness', 'fecklessnesses').
inf('groundlessness', 'groundlessnesses').
inf('idleness', 'idlenesses').
inf('paltriness', 'paltrinesses').
inf('sorriness', 'sorrinesses').
inf('valuelessness', 'valuelessnesses').
inf('shoddiness', 'shoddinesses').
inf('trashiness', 'trashinesses').
inf('vanity', 'vanities').
inf('emptiness', 'emptinesses').
inf('invaluableness', 'invaluablenesses').
inf('preciousness', 'preciousnesses').
inf('pricelessness', 'pricelessnesses').
inf('valuableness', 'valuablenesses').
inf('desirability', 'desirabilities').
inf('desirableness', 'desirablenesses').
inf('undesirability', 'undesirabilities').
inf('goodness', 'goodnesses').
inf('behalf', 'behalves').
inf('badness', 'badnesses').
inf('expensiveness', 'expensivenesses').
inf('costliness', 'costlinesses').
inf('dearness', 'dearnesses').
inf('preciousness', 'preciousnesses').
inf('lavishness', 'lavishnesses').
inf('luxury', 'luxuries').
inf('sumptuosity', 'sumptuosities').
inf('sumptuousness', 'sumptuousnesses').
inf('inexpensiveness', 'inexpensivenesses').
inf('reasonableness', 'reasonablenesses').
inf('moderateness', 'moderatenesses').
inf('modestness', 'modestnesses').
inf('cheapness', 'cheapnesses').
inf('fruitfulness', 'fruitfulnesses').
inf('fecundity', 'fecundities').
inf('richness', 'richnesses').
inf('rankness', 'ranknesses').
inf('prolificacy', 'prolificacies').
inf('fertility', 'fertilities').
inf('productiveness', 'productivenesses').
inf('productivity', 'productivities').
inf('fruitlessness', 'fruitlessnesses').
inf('aridity', 'aridities').
inf('barrenness', 'barrennesses').
inf('poorness', 'poornesses').
inf('unproductiveness', 'unproductivenesses').
inf('utility', 'utilities').
inf('usefulness', 'usefulnesses').
inf('detergency', 'detergencies').
inf('helpfulness', 'helpfulnesses').
inf('serviceability', 'serviceabilities').
inf('serviceableness', 'serviceablenesses').
inf('usableness', 'usablenesses').
inf('useableness', 'useablenesses').
inf('usability', 'usabilities').
inf('instrumentality', 'instrumentalities').
inf('inutility', 'inutilities').
inf('uselessness', 'uselessnesses').
inf('unusefulness', 'unusefulnesses').
inf('futility', 'futilities').
inf('worthlessness', 'worthlessnesses').
inf('practicality', 'practicalities').
inf('functionality', 'functionalities').
inf('viability', 'viabilities').
inf('sensibleness', 'sensiblenesses').
inf('practicability', 'practicabilities').
inf('practicableness', 'practicablenesses').
inf('feasibility', 'feasibilities').
inf('feasibleness', 'feasiblenesses').
inf('impracticality', 'impracticalities').
inf('knight errantry', 'knight errantries').
inf('impracticability', 'impracticabilities').
inf('impracticableness', 'impracticablenesses').
inf('infeasibility', 'infeasibilities').
inf('unfeasibility', 'unfeasibilities').
inf('competency', 'competencies').
inf('fitness', 'fitnesses').
inf('proficiency', 'proficiencies').
inf('incompetency', 'incompetencies').
inf('resourcefulness', 'resourcefulnesses').
inf('profitableness', 'profitablenesses').
inf('profitability', 'profitabilities').
inf('gainfulness', 'gainfulnesses').
inf('lucrativeness', 'lucrativenesses').
inf('expediency', 'expediencies').
inf('superiority', 'superiorities').
inf('specialty', 'specialties').
inf('speciality', 'specialities').
inf('wiseness', 'wisenesses').
inf('soundness', 'soundnesses').
inf('unsoundness', 'unsoundnesses').
inf('advisability', 'advisabilities').
inf('reasonableness', 'reasonablenesses').
inf('favorableness', 'favorablenesses').
inf('favourableness', 'favourablenesses').
inf('advantageousness', 'advantageousnesses').
inf('positivity', 'positivities').
inf('positiveness', 'positivenesses').
inf('profitableness', 'profitablenesses').
inf('auspiciousness', 'auspiciousnesses').
inf('propitiousness', 'propitiousnesses').
inf('liability', 'liabilities').
inf('unfavorableness', 'unfavorablenesses').
inf('unfavourableness', 'unfavourablenesses').
inf('inauspiciousness', 'inauspiciousnesses').
inf('unpropitiousness', 'unpropitiousnesses').
inf('awkwardness', 'awkwardnesses').
inf('loss', 'losses').
inf('penalty', 'penalties').
inf('scratch', 'scratches').
inf('richness', 'richnesses').
inf('catch', 'catches').
inf('penalty', 'penalties').
inf('inadvisability', 'inadvisabilities').
inf('inferiority', 'inferiorities').
inf('inexpediency', 'inexpediencies').
inf('unprofitableness', 'unprofitablenesses').
inf('unprofitability', 'unprofitabilities').
inf('constructiveness', 'constructivenesses').
inf('destructiveness', 'destructivenesses').
inf('harmfulness', 'harmfulnesses').
inf('injuriousness', 'injuriousnesses').
inf('insidiousness', 'insidiousnesses').
inf('virulency', 'virulencies').
inf('positivity', 'positivities').
inf('positiveness', 'positivenesses').
inf('affirmativeness', 'affirmativenesses').
inf('assertiveness', 'assertivenesses').
inf('self-assertiveness', 'self-assertivenesses').
inf('bumptiousness', 'bumptiousnesses').
inf('cockiness', 'cockinesses').
inf('pushiness', 'pushinesses').
inf('forwardness', 'forwardnesses').
inf('negativity', 'negativities').
inf('negativeness', 'negativenesses').
inf('momentousness', 'momentousnesses').
inf('greatness', 'greatnesses').
inf('illustriousness', 'illustriousnesses').
inf('historicalness', 'historicalnesses').
inf('meaningfulness', 'meaningfulnesses').
inf('purposefulness', 'purposefulnesses').
inf('sense of purpose', 'senses of purpose').
inf('hell to pay', 'hells to pay').
inf('essentiality', 'essentialities').
inf('essentialness', 'essentialnesses').
inf('vitalness', 'vitalnesses').
inf('indispensability', 'indispensabilities').
inf('indispensableness', 'indispensablenesses').
inf('vitalness', 'vitalnesses').
inf('urgency', 'urgencies').
inf('sharpness', 'sharpnesses').
inf('imperativeness', 'imperativenesses').
inf('instancy', 'instancies').
inf('weightiness', 'weightinesses').
inf('inessentiality', 'inessentialities').
inf('dispensability', 'dispensabilities').
inf('dispensableness', 'dispensablenesses').
inf('pettiness', 'pettinesses').
inf('triviality', 'trivialities').
inf('slightness', 'slightnesses').
inf('puniness', 'puninesses').
inf('meaninglessness', 'meaninglessnesses').
inf('inanity', 'inanities').
inf('senselessness', 'senselessnesses').
inf('mindlessness', 'mindlessnesses').
inf('vacuity', 'vacuities').
inf('pointlessness', 'pointlessnesses').
inf('purposelessness', 'purposelessnesses').
inf('aimlessness', 'aimlessnesses').
inf('access', 'accesses').
inf('access', 'accesses').
inf('authority', 'authorities').
inf('compulsory process', 'compulsory processes').
inf('privilege of the floor', 'privileges of the floor').
inf('right to privacy', 'rights to privacy').
inf('right to life', 'rights to life').
inf('right to liberty', 'rights to liberty').
inf('right to the pursuit of happiness', 'rights to the pursuit of happiness').
inf('freedom of thought', 'freedoms of thought').
inf('equality before the law', 'equalities before the law').
inf('civil liberty', 'civil liberties').
inf('habeas corpus', 'habeas corpora').
inf('freedom of religion', 'freedoms of religion').
inf('freedom of speech', 'freedoms of speech').
inf('freedom of the press', 'freedoms of the press').
inf('freedom of assembly', 'freedoms of assembly').
inf('freedom to bear arms', 'freedoms to bear arms').
inf('freedom from search and seizure', 'freedoms from search and seizure').
inf('right to due process', 'rights to due process').
inf('freedom from self-incrimination', 'freedoms from self-incrimination').
inf('freedom from double jeopardy', 'freedoms from double jeopardy').
inf('right to speedy and public trial by jury', 'rights to speedy and public trial by jury').
inf('right to an attorney', 'rights to an attorney').
inf('right to confront accusors', 'rights to confront accusors').
inf('freedom from cruel and unusual punishment', 'freedoms from cruel and unusual punishment').
inf('freedom from involuntary servitude', 'freedoms from involuntary servitude').
inf('right to vote', 'rights to vote').
inf('freedom from discrimination', 'freedoms from discrimination').
inf('equal opportunity', 'equal opportunities').
inf('right of action', 'rights of action').
inf('right of search', 'rights of search').
inf('right of way', 'rights of way').
inf('right of election', 'rights of election').
inf('right of entry', 'rights of entry').
inf('right of re-entry', 'rights of re-entry').
inf('right of offset', 'rights of offset').
inf('right of privacy', 'rights of privacy').
inf('right of way', 'rights of way').
inf('powerfulness', 'powerfulnesses').
inf('persuasiveness', 'persuasivenesses').
inf('convincingness', 'convincingnesses').
inf('irresistibility', 'irresistibilities').
inf('irresistibleness', 'irresistiblenesses').
inf('interestingness', 'interestingnesses').
inf('newsworthiness', 'newsworthinesses').
inf('topicality', 'topicalities').
inf('vividness', 'vividnesses').
inf('shrillness', 'shrillnesses').
inf('duress', 'duresses').
inf('hydrophobicity', 'hydrophobicities').
inf('authority', 'authorities').
inf('potency', 'potencies').
inf('say-so', 'say-soes').
inf('sovereignty', 'sovereignties').
inf('veto', 'vetoes').
inf('effectiveness', 'effectivenesses').
inf('effectivity', 'effectivities').
inf('effectualness', 'effectualnesses').
inf('effectuality', 'effectualities').
inf('incisiveness', 'incisivenesses').
inf('trenchancy', 'trenchancies').
inf('efficacy', 'efficacies').
inf('efficaciousness', 'efficaciousnesses').
inf('ability', 'abilities').
inf('interoperability', 'interoperabilities').
inf('magical ability', 'magical abilities').
inf('lycanthropy', 'lycanthropies').
inf('Midas touch', 'Midas touches').
inf('physical ability', 'physical abilities').
inf('contractility', 'contractilities').
inf('astringency', 'astringencies').
inf('stypsis', 'stypses').
inf('capability', 'capabilities').
inf('capableness', 'capablenesses').
inf('defensibility', 'defensibilities').
inf('executability', 'executabilities').
inf('capacity', 'capacities').
inf('military capability', 'military capabilities').
inf('operating capability', 'operating capabilities').
inf('performance capability', 'performance capabilities').
inf('powerlessness', 'powerlessnesses').
inf('impotency', 'impotencies').
inf('helplessness', 'helplessnesses').
inf('weakness', 'weaknesses').
inf('unpersuasiveness', 'unpersuasivenesses').
inf('uninterestingness', 'uninterestingnesses').
inf('voicelessness', 'voicelessnesses').
inf('dullness', 'dullnesses').
inf('boringness', 'boringnesses').
inf('dreariness', 'drearinesses').
inf('insipidness', 'insipidnesses').
inf('insipidity', 'insipidities').
inf('tediousness', 'tediousnesses').
inf('tiresomeness', 'tiresomenesses').
inf('jejunity', 'jejunities').
inf('jejuneness', 'jejunenesses').
inf('tameness', 'tamenesses').
inf('vapidity', 'vapidities').
inf('vapidness', 'vapidnesses').
inf('ponderousness', 'ponderousnesses').
inf('heaviness', 'heavinesses').
inf('inability', 'inabilities').
inf('unfitness', 'unfitnesses').
inf('incapability', 'incapabilities').
inf('incapableness', 'incapablenesses').
inf('incapacity', 'incapacities').
inf('ineffectiveness', 'ineffectivenesses').
inf('ineffectualness', 'ineffectualnesses').
inf('ineffectuality', 'ineffectualities').
inf('inefficacy', 'inefficacies').
inf('inefficaciousness', 'inefficaciousnesses').
inf('analyticity', 'analyticities').
inf('compositeness', 'compositenesses').
inf('primality', 'primalities').
inf('selectivity', 'selectivities').
inf('domesticity', 'domesticities').
inf('infiniteness', 'infinitenesses').
inf('unboundedness', 'unboundednesses').
inf('boundlessness', 'boundlessnesses').
inf('limitlessness', 'limitlessnesses').
inf('finiteness', 'finitenesses').
inf('boundedness', 'boundednesses').
inf('quantifiability', 'quantifiabilities').
inf('measurability', 'measurabilities').
inf('ratability', 'ratabilities').
inf('scalability', 'scalabilities').
inf('solubility', 'solubilities').
inf('insolubility', 'insolubilities').
inf('comicality', 'comicalities').
inf('voluptuousness', 'voluptuousnesses').
inf('poignancy', 'poignancies').
inf('brachycephaly', 'brachycephalies').
inf('dolichocephaly', 'dolichocephalies').
inf('relativity', 'relativities').
inf('responsiveness', 'responsivenesses').
inf('unresponsiveness', 'unresponsivenesses').
inf('deadness', 'deadnesses').
inf('frigidity', 'frigidities').
inf('frigidness', 'frigidnesses').
inf('vascularity', 'vascularities').
inf('snootiness', 'snootinesses').
inf('totipotency', 'totipotencies').
inf('ulteriority', 'ulteriorities').
inf('solvability', 'solvabilities').
inf('solubility', 'solubilities').
inf('unsolvability', 'unsolvabilities').
inf('insolubility', 'insolubilities').
inf('memorability', 'memorabilities').
inf('woodiness', 'woodinesses').
inf('woodsiness', 'woodsinesses').
inf('waxiness', 'waxinesses').
inf('body', 'bodies').
inf('human body', 'human bodies').
inf('physical body', 'physical bodies').
inf('material body', 'material bodies').
inf('anatomy', 'anatomies').
inf('chassis', 'chasses').
inf('flesh', 'fleshes').
inf('person', 'people').
inf('body', 'bodies').
inf('dead body', 'dead bodies').
inf('mummy', 'mummies').
inf('live body', 'live bodies').
inf('juvenile body', 'juvenile bodies').
inf('child\'s body', 'child\'s bodies').
inf('adult body', 'adult bodies').
inf('male body', 'male bodies').
inf('female body', 'female bodies').
inf('adult female body', 'adult female bodies').
inf('woman\'s body', 'woman\'s bodies').
inf('adult male body', 'adult male bodies').
inf('man\'s body', 'man\'s bodies').
inf('corpus', 'corpora').
inf('fissure of Rolando', 'fissures of Rolando').
inf('fissure of Sylvius', 'fissures of Sylvius').
inf('carina fornicis', 'carina fornices').
inf('fornix', 'fornixes').
inf('fornix', 'fornixes').
inf('mamillary body', 'mamillary bodies').
inf('mammillary body', 'mammillary bodies').
inf('mandibular notch', 'mandibular notches').
inf('lentigo', 'lentigoes').
inf('comedo', 'comedoes').
inf('salpinx', 'salpinges').
inf('malpighian body', 'malpighian bodies').
inf('deltoid tuberosity', 'deltoid tuberosities').
inf('anastomosis', 'anastomoses').
inf('canal of Schlemm', 'canals of Schlemm').
inf('lanugo', 'lanugoes').
inf('head of hair', 'heads of hair').
inf('hairdo', 'hairdoes').
inf('Afro', 'Afroes').
inf('Afro hairdo', 'Afro hairdoes').
inf('tress', 'tresses').
inf('roach', 'roaches').
inf('thatch', 'thatches').
inf('toothbrush', 'toothbrushes').
inf('soul patch', 'soul patches').
inf('bush', 'bushes').
inf('flesh', 'fleshes').
inf('coccyx', 'coccyxes').
inf('phalanx', 'phalanges').
inf('glenoid cavity', 'glenoid cavities').
inf('acromial process', 'acromial processes').
inf('xiphoid process', 'xiphoid processes').
inf('tooth', 'teeth').
inf('pulp cavity', 'pulp cavities').
inf('pearly', 'pearlies').
inf('carnassial tooth', 'carnassial teeth').
inf('maxillary', 'maxillaries').
inf('zygomatic arch', 'zygomatic arches').
inf('orbital cavity', 'orbital cavities').
inf('musculus abductor hallucis', 'musculus abductor halluces').
inf('musculus abductor pollicis', 'musculus abductor pollices').
inf('musculus adductor hallucis', 'musculus adductor halluces').
inf('musculus articularis genus', 'musculus articularis genera').
inf('ganglion', 'ganglia').
inf('autonomic ganglion', 'autonomic ganglia').
inf('otic ganglion', 'otic ganglia').
inf('otoganglion', 'otoganglia').
inf('oral cavity', 'oral cavities').
inf('buccal cavity', 'buccal cavities').
inf('incompetent cervix', 'incompetent cervixes').
inf('cervix', 'cervixes').
inf('uterine cervix', 'uterine cervixes').
inf('cavity', 'cavities').
inf('bodily cavity', 'bodily cavities').
inf('vestibule of the ear', 'vestibules of the ear').
inf('organ of speech', 'organs of speech').
inf('front tooth', 'front teeth').
inf('bucktooth', 'buckteeth').
inf('back tooth', 'back teeth').
inf('malposed tooth', 'malposed teeth').
inf('permanent tooth', 'permanent teeth').
inf('adult tooth', 'adult teeth').
inf('primary tooth', 'primary teeth').
inf('deciduous tooth', 'deciduous teeth').
inf('baby tooth', 'baby teeth').
inf('milk tooth', 'milk teeth').
inf('canine tooth', 'canine teeth').
inf('eyetooth', 'eyeteeth').
inf('eye tooth', 'eye teeth').
inf('dogtooth', 'dogteeth').
inf('wisdom tooth', 'wisdom teeth').
inf('roof of the mouth', 'roofs of the mouth').
inf('alveolar arch', 'alveolar arches').
inf('alveolar process', 'alveolar processes').
inf('ciliary body', 'ciliary bodies').
inf('eyelash', 'eyelashes').
inf('lash', 'lashes').
inf('vitreous body', 'vitreous bodies').
inf('lens of the eye', 'lenses of the eye').
inf('lens cortex', 'lens cortexes').
inf('cortex', 'cortexes').
inf('organ of hearing', 'organs of hearing').
inf('organ of Corti', 'organs of Corti').
inf('umbo', 'umboes').
inf('tympanic cavity', 'tympanic cavities').
inf('fenestra of the vestibule', 'fenestras of the vestibule').
inf('fenestra of the cochlea', 'fenestras of the cochlea').
inf('meninx', 'meninges').
inf('artery', 'arteries').
inf('alveolar artery', 'alveolar arteries').
inf('inferior alveolar artery', 'inferior alveolar arteries').
inf('superior alveolar artery', 'superior alveolar arteries').
inf('angular artery', 'angular arteries').
inf('aortic arch', 'aortic arches').
inf('appendicular artery', 'appendicular arteries').
inf('arcuate artery', 'arcuate arteries').
inf('capillary artery', 'capillary arteries').
inf('artery of the penis bulb', 'arteries of the penis bulb').
inf('arteria bulbi penis', 'arteria bulbi penes').
inf('artery of the vestibule bulb', 'arteries of the vestibule bulb').
inf('ascending artery', 'ascending arteries').
inf('auricular artery', 'auricular arteries').
inf('axillary artery', 'axillary arteries').
inf('basilar artery', 'basilar arteries').
inf('brachial artery', 'brachial arteries').
inf('radial artery', 'radial arteries').
inf('bronchial artery', 'bronchial arteries').
inf('buccal artery', 'buccal arteries').
inf('carotid artery', 'carotid arteries').
inf('common carotid artery', 'common carotid arteries').
inf('external carotid artery', 'external carotid arteries').
inf('internal carotid artery', 'internal carotid arteries').
inf('carotid body', 'carotid bodies').
inf('celiac artery', 'celiac arteries').
inf('cerebellar artery', 'cerebellar arteries').
inf('inferior cerebellar artery', 'inferior cerebellar arteries').
inf('superior cerebellar artery', 'superior cerebellar arteries').
inf('cerebral artery', 'cerebral arteries').
inf('anterior cerebral artery', 'anterior cerebral arteries').
inf('middle cerebral artery', 'middle cerebral arteries').
inf('posterior cerebral artery', 'posterior cerebral arteries').
inf('cervical artery', 'cervical arteries').
inf('choroidal artery', 'choroidal arteries').
inf('ciliary artery', 'ciliary arteries').
inf('circle of Willis', 'circles of Willis').
inf('circumflex artery', 'circumflex arteries').
inf('circumflex humeral artery', 'circumflex humeral arteries').
inf('circumflex iliac artery', 'circumflex iliac arteries').
inf('circumflex scapular artery', 'circumflex scapular arteries').
inf('colic artery', 'colic arteries').
inf('communicating artery', 'communicating arteries').
inf('coronary artery', 'coronary arteries').
inf('atrial artery', 'atrial arteries').
inf('right coronary artery', 'right coronary arteries').
inf('left coronary artery', 'left coronary arteries').
inf('cystic artery', 'cystic arteries').
inf('epigastric artery', 'epigastric arteries').
inf('ethmoidal artery', 'ethmoidal arteries').
inf('facial artery', 'facial arteries').
inf('external maxillary artery', 'external maxillary arteries').
inf('femoral artery', 'femoral arteries').
inf('popliteal artery', 'popliteal arteries').
inf('gastric artery', 'gastric arteries').
inf('right gastric artery', 'right gastric arteries').
inf('left gastric artery', 'left gastric arteries').
inf('short gastric artery', 'short gastric arteries').
inf('gluteal artery', 'gluteal arteries').
inf('hepatic artery', 'hepatic arteries').
inf('ileal artery', 'ileal arteries').
inf('intestinal artery', 'intestinal arteries').
inf('ileocolic artery', 'ileocolic arteries').
inf('iliac artery', 'iliac arteries').
inf('common iliac artery', 'common iliac arteries').
inf('external iliac artery', 'external iliac arteries').
inf('internal iliac artery', 'internal iliac arteries').
inf('hypogastric artery', 'hypogastric arteries').
inf('iliolumbar artery', 'iliolumbar arteries').
inf('infraorbital artery', 'infraorbital arteries').
inf('innominate artery', 'innominate arteries').
inf('intercostal artery', 'intercostal arteries').
inf('jejunal artery', 'jejunal arteries').
inf('intestinal artery', 'intestinal arteries').
inf('labial artery', 'labial arteries').
inf('inferior labial artery', 'inferior labial arteries').
inf('superior labial artery', 'superior labial arteries').
inf('labyrinthine artery', 'labyrinthine arteries').
inf('artery of the labyrinth', 'arteries of the labyrinth').
inf('internal auditory artery', 'internal auditory arteries').
inf('lacrimal artery', 'lacrimal arteries').
inf('laryngeal artery', 'laryngeal arteries').
inf('lienal artery', 'lienal arteries').
inf('splenic artery', 'splenic arteries').
inf('lingual artery', 'lingual arteries').
inf('lumbar artery', 'lumbar arteries').
inf('maxillary artery', 'maxillary arteries').
inf('internal maxillary artery', 'internal maxillary arteries').
inf('meningeal artery', 'meningeal arteries').
inf('anterior meningeal artery', 'anterior meningeal arteries').
inf('middle meningeal artery', 'middle meningeal arteries').
inf('posterior meningeal artery', 'posterior meningeal arteries').
inf('mesenteric artery', 'mesenteric arteries').
inf('inferior mesenteric artery', 'inferior mesenteric arteries').
inf('superior mesenteric artery', 'superior mesenteric arteries').
inf('metacarpal artery', 'metacarpal arteries').
inf('metatarsal artery', 'metatarsal arteries').
inf('musculophrenic artery', 'musculophrenic arteries').
inf('nutrient artery', 'nutrient arteries').
inf('ophthalmic artery', 'ophthalmic arteries').
inf('ovarian artery', 'ovarian arteries').
inf('palatine artery', 'palatine arteries').
inf('pancreatic artery', 'pancreatic arteries').
inf('perineal artery', 'perineal arteries').
inf('pudendal artery', 'pudendal arteries').
inf('pulmonary artery', 'pulmonary arteries').
inf('rectal artery', 'rectal arteries').
inf('renal artery', 'renal arteries').
inf('subclavian artery', 'subclavian arteries').
inf('temporal artery', 'temporal arteries').
inf('anterior temporal artery', 'anterior temporal arteries').
inf('intermediate temporal artery', 'intermediate temporal arteries').
inf('posterior temporal artery', 'posterior temporal arteries').
inf('testicular artery', 'testicular arteries').
inf('internal spermatic artery', 'internal spermatic arteries').
inf('ulnar artery', 'ulnar arteries').
inf('uterine artery', 'uterine arteries').
inf('vaginal artery', 'vaginal arteries').
inf('vertebral artery', 'vertebral arteries').
inf('vena genus', 'vena genera').
inf('glans penis', 'glans penes').
inf('lobe of the lung', 'lobes of the lung').
inf('pleural cavity', 'pleural cavities').
inf('bundle of His', 'bundles of His').
inf('area of cardiac dullness', 'areas of cardiac dullness').
inf('auricular appendix', 'auricular appendixes').
inf('cranial cavity', 'cranial cavities').
inf('intracranial cavity', 'intracranial cavities').
inf('atrium of the heart', 'atriums of the heart').
inf('stomach', 'stomaches').
inf('tummy', 'tummies').
inf('amniotic cavity', 'amniotic cavities').
inf('melancholy', 'melancholies').
inf('vena bulbi penis', 'vena bulbi penes').
inf('vein of penis', 'veins of penis').
inf('vena profunda penis', 'vena profunda penes').
inf('capillary', 'capillaries').
inf('peritoneal cavity', 'peritoneal cavities').
inf('lesser peritoneal cavity', 'lesser peritoneal cavities').
inf('pericardial cavity', 'pericardial cavities').
inf('mesentery', 'mesenteries').
inf('Peyer\'s patch', 'Peyer\'s patches').
inf('Golgi body', 'Golgi bodies').
inf('Golgi complex', 'Golgi complexes').
inf('homeobox', 'homeoboxes').
inf('central body', 'central bodies').
inf('spermatozoon', 'spermatozoa').
inf('polar body', 'polar bodies').
inf('system of macrophages', 'systems of macrophages').
inf('nodes of Ranvier', 'nodess of Ranvier').
inf('process', 'processes').
inf('condylar process', 'condylar processes').
inf('condyloid process', 'condyloid processes').
inf('coronoid process', 'coronoid processes').
inf('apophysis', 'apophyses').
inf('spinal accessory', 'spinal accessories').
inf('neocortex', 'neocortexes').
inf('paleocortex', 'paleocortexes').
inf('pituitary', 'pituitaries').
inf('pituitary body', 'pituitary bodies').
inf('hypophysis', 'hypophyses').
inf('anterior pituitary', 'anterior pituitaries').
inf('adenohypophysis', 'adenohypophyses').
inf('posterior pituitary', 'posterior pituitaries').
inf('neurohypophysis', 'neurohypophyses').
inf('pineal body', 'pineal bodies').
inf('epiphysis', 'epiphyses').
inf('islands of Langerhans', 'islandss of Langerhans').
inf('isles of Langerhans', 'isless of Langerhans').
inf('islets of Langerhans', 'isletss of Langerhans').
inf('cerebral cortex', 'cerebral cortexes').
inf('cortex', 'cortexes').
inf('association cortex', 'association cortexes').
inf('geniculate body', 'geniculate bodies').
inf('lateral geniculate body', 'lateral geniculate bodies').
inf('medial geniculate body', 'medial geniculate bodies').
inf('auditory cortex', 'auditory cortexes').
inf('convolution of Broca', 'convolutions of Broca').
inf('motor cortex', 'motor cortexes').
inf('visual cortex', 'visual cortexes').
inf('cortex', 'cortexes').
inf('adrenal cortex', 'adrenal cortexes').
inf('renal cortex', 'renal cortexes').
inf('frontal cortex', 'frontal cortexes').
inf('prefrontal cortex', 'prefrontal cortexes').
inf('parietal cortex', 'parietal cortexes').
inf('occipital cortex', 'occipital cortexes').
inf('striate cortex', 'striate cortexes').
inf('temporal ccortex', 'temporal ccortexes').
inf('basal ganglion', 'basal ganglia').
inf('striate body', 'striate bodies').
inf('pulmonary plexis', 'pulmonary plexes').
inf('pit of the stomach', 'pits of the stomach').
inf('crotch', 'crotches').
inf('fanny', 'fannies').
inf('ovary', 'ovaries').
inf('ovotestis', 'ovotestes').
inf('pouch', 'pouches').
inf('cheek pouch', 'cheek pouches').
inf('uterine cavity', 'uterine cavities').
inf('puss', 'pusses').
inf('pussy', 'pussies').
inf('snatch', 'snatches').
inf('vestibule of the vagina', 'vestibules of the vagina').
inf('testis', 'testes').
inf('undescended testis', 'undescended testes').
inf('rete testis', 'rete testes').
inf('penis', 'penes').
inf('micropenis', 'micropenes').
inf('nasal cavity', 'nasal cavities').
inf('nasopharynx', 'nasopharynges').
inf('oropharynx', 'oropharynges').
inf('laryngopharynx', 'laryngopharynges').
inf('larynx', 'larynges').
inf('voice box', 'voice boxes').
inf('appendix', 'appendixes').
inf('vermiform appendix', 'vermiform appendixes').
inf('vermiform process', 'vermiform processes').
inf('occiput', 'occipita').
inf('mastoid process', 'mastoid processes').
inf('styloid process', 'styloid processes').
inf('pterygoid process', 'pterygoid processes').
inf('tuberosity', 'tuberosities').
inf('diarthrosis', 'diarthroses').
inf('foramen of Monro', 'foramina of Monro').
inf('zygomatic process', 'zygomatic processes').
inf('cervix', 'cervixes').
inf('pharynx', 'pharynges').
inf('bypass', 'bypasses').
inf('tubular cavity', 'tubular cavities').
inf('axillary cavity', 'axillary cavities').
inf('torso', 'torsoes').
inf('body', 'bodies').
inf('thorax', 'thoraxes').
inf('chest cavity', 'chest cavities').
inf('thoracic cavity', 'thoracic cavities').
inf('thorax', 'thoraxes').
inf('titty', 'titties').
inf('ring of color', 'rings of color').
inf('belly', 'bellies').
inf('paunch', 'paunches').
inf('potbelly', 'potbellies').
inf('tummy', 'tummies').
inf('haunch', 'haunches').
inf('stomach', 'stomaches').
inf('belly', 'bellies').
inf('underbelly', 'underbellies').
inf('underbody', 'underbodies').
inf('abdominal cavity', 'abdominal cavities').
inf('tush', 'tushes').
inf('fanny', 'fannies').
inf('ass', 'asses').
inf('extremity', 'extremities').
inf('shank\'s pony', 'shank\'s ponies').
inf('shanks\' pony', 'shanks\' ponies').
inf('vertebrate foot', 'vertebrate feet').
inf('pedal extremity', 'pedal extremities').
inf('foot', 'feet').
inf('human foot', 'human feet').
inf('extremity', 'extremities').
inf('pollex', 'pollexes').
inf('index', 'indexes').
inf('annualry', 'annualries').
inf('pinky', 'pinkies').
inf('articulatio genus', 'articulatio genera').
inf('calf', 'calves').
inf('mid-calf', 'mid-calves').
inf('flatfoot', 'flatfeet').
inf('splayfoot', 'splayfeet').
inf('arch', 'arches').
inf('metatarsal arch', 'metatarsal arches').
inf('sunken arch', 'sunken arches').
inf('fallen arch', 'fallen arches').
inf('hallux', 'halluxes').
inf('tendon of Achilles', 'tendons of Achilles').
inf('matrix', 'matrices').
inf('matrix', 'matrices').
inf('aponeurosis', 'aponeuroses').
inf('autoplasty', 'autoplasties').
inf('onyxis', 'onyxes').
inf('pectoral arch', 'pectoral arches').
inf('transverse process', 'transverse processes').
inf('hemal arch', 'hemal arches').
inf('haemal arch', 'haemal arches').
inf('neural arch', 'neural arches').
inf('vertebral arch', 'vertebral arches').
inf('atlas', 'atlantes').
inf('axis', 'axes').
inf('odontoid process', 'odontoid processes').
inf('epiphysis', 'epiphyses').
inf('diaphysis', 'diaphyses').
inf('metaphysis', 'metaphyses').
inf('olecranon process', 'olecranon processes').
inf('enarthrosis', 'enarthroses').
inf('cotyloid cavity', 'cotyloid cavities').
inf('pelvic arch', 'pelvic arches').
inf('pelvic cavity', 'pelvic cavities').
inf('crotch', 'crotches').
inf('proboscis', 'probosces').
inf('physiognomy', 'physiognomies').
inf('superciliary arch', 'superciliary arches').
inf('pharyngeal recess', 'pharyngeal recesses').
inf('ancient history', 'ancient histories').
inf('ego', 'egoes').
inf('superego', 'superegoes').
inf('astuteness', 'astutenesses').
inf('profundity', 'profundities').
inf('profoundness', 'profoundnesses').
inf('deepness', 'deepnesses').
inf('sagacity', 'sagacities').
inf('sagaciousness', 'sagaciousnesses').
inf('judiciousness', 'judiciousnesses').
inf('discreetness', 'discreetnesses').
inf('confidentiality', 'confidentialities').
inf('injudiciousness', 'injudiciousnesses').
inf('indiscreetness', 'indiscreetnesses').
inf('ability', 'abilities').
inf('bag of tricks', 'bags of tricks').
inf('learning ability', 'learning abilities').
inf('mental capacity', 'mental capacities').
inf('mentality', 'mentalities').
inf('comprehensiveness', 'comprehensivenesses').
inf('largeness', 'largenesses').
inf('capaciousness', 'capaciousnesses').
inf('roominess', 'roominesses').
inf('mental quickness', 'mental quicknesses').
inf('quickness', 'quicknesses').
inf('quick-wittedness', 'quick-wittednesses').
inf('nimbleness', 'nimblenesses').
inf('mental dexterity', 'mental dexterities').
inf('precociousness', 'precociousnesses').
inf('precocity', 'precocities').
inf('acuteness', 'acutenesses').
inf('acuity', 'acuities').
inf('sharpness', 'sharpnesses').
inf('keenness', 'keennesses').
inf('brightness', 'brightnesses').
inf('cleverness', 'clevernesses').
inf('smartness', 'smartnesses').
inf('craftiness', 'craftinesses').
inf('foxiness', 'foxinesses').
inf('slyness', 'slynesses').
inf('wiliness', 'wilinesses').
inf('shrewdness', 'shrewdnesses').
inf('astuteness', 'astutenesses').
inf('perspicacity', 'perspicacities').
inf('perspicaciousness', 'perspicaciousnesses').
inf('insightfulness', 'insightfulnesses').
inf('knowingness', 'knowingnesses').
inf('capacity', 'capacities').
inf('mental ability', 'mental abilities').
inf('capability', 'capabilities').
inf('capableness', 'capablenesses').
inf('potentiality', 'potentialities').
inf('perfectibility', 'perfectibilities').
inf('compass', 'compasses').
inf('reach', 'reaches').
inf('natural ability', 'natural abilities').
inf('creativity', 'creativities').
inf('creativeness', 'creativenesses').
inf('fecundity', 'fecundities').
inf('fruitfulness', 'fruitfulnesses').
inf('wizardry', 'wizardries').
inf('imaginativeness', 'imaginativenesses').
inf('El Dorado', 'El Doradoes').
inf('eldorado', 'eldoradoes').
inf('faery', 'faeries').
inf('bosom of Abraham', 'bosoms of Abraham').
inf('City of God', 'Citys of God').
inf('Garden of Eden', 'Gardens of Eden').
inf('Inferno', 'Infernoes').
inf('limbo', 'limboes').
inf('limbo', 'limboes').
inf('purgatory', 'purgatories').
inf('fancy', 'fancies').
inf('fantasy', 'fantasies').
inf('phantasy', 'phantasies').
inf('fantasy life', 'fantasy lives').
inf('phantasy life', 'phantasy lives').
inf('inventiveness', 'inventivenesses').
inf('ingeniousness', 'ingeniousnesses').
inf('ingenuity', 'ingenuities').
inf('cleverness', 'clevernesses').
inf('resourcefulness', 'resourcefulnesses').
inf('armory', 'armories').
inf('armoury', 'armouries').
inf('inventory', 'inventories').
inf('originality', 'originalities').
inf('innovativeness', 'innovativenesses').
inf('unconventionality', 'unconventionalities').
inf('novelty', 'novelties').
inf('freshness', 'freshnesses').
inf('falconry', 'falconries').
inf('horology', 'horologies').
inf('minstrelsy', 'minstrelsies').
inf('enology', 'enologies').
inf('oenology', 'oenologies').
inf('puppetry', 'puppetries').
inf('taxidermy', 'taxidermies').
inf('telescopy', 'telescopies').
inf('virtuosity', 'virtuosities').
inf('literacy', 'literacies').
inf('mixology', 'mixologies').
inf('artistry', 'artistries').
inf('prowess', 'prowesses').
inf('numeracy', 'numeracies').
inf('skillfulness', 'skillfulnesses').
inf('expertness', 'expertnesses').
inf('handiness', 'handinesses').
inf('versatility', 'versatilities').
inf('mastery', 'masteries').
inf('adeptness', 'adeptnesses').
inf('adroitness', 'adroitnesses').
inf('deftness', 'deftnesses').
inf('facility', 'facilities').
inf('quickness', 'quicknesses').
inf('touch', 'touches').
inf('finishing touch', 'finishing touches').
inf('dexterity', 'dexterities').
inf('manual dexterity', 'manual dexterities').
inf('fluency', 'fluencies').
inf('disfluency', 'disfluencies').
inf('proficiency', 'proficiencies').
inf('musketry', 'musketries').
inf('efficiency', 'efficiencies').
inf('economy', 'economies').
inf('inability', 'inabilities').
inf('stupidity', 'stupidities').
inf('denseness', 'densenesses').
inf('dumbness', 'dumbnesses').
inf('slow-wittedness', 'slow-wittednesses').
inf('dullness', 'dullnesses').
inf('obtuseness', 'obtusenesses').
inf('backwardness', 'backwardnesses').
inf('slowness', 'slownesses').
inf('subnormality', 'subnormalities').
inf('abnormality', 'abnormalities').
inf('mental defectiveness', 'mental defectivenesses').
inf('feeblemindedness', 'feeblemindednesses').
inf('moronity', 'moronities').
inf('mental deficiency', 'mental deficiencies').
inf('idiocy', 'idiocies').
inf('imbecility', 'imbecilities').
inf('folly', 'follies').
inf('foolishness', 'foolishnesses').
inf('craziness', 'crazinesses').
inf('madness', 'madnesses').
inf('vacuousness', 'vacuousnesses').
inf('talentlessness', 'talentlessnesses').
inf('incapability', 'incapabilities').
inf('incapableness', 'incapablenesses').
inf('imperfectibility', 'imperfectibilities').
inf('incapacity', 'incapacities').
inf('unskillfulness', 'unskillfulnesses').
inf('awkwardness', 'awkwardnesses').
inf('clumsiness', 'clumsinesses').
inf('ineptness', 'ineptnesses').
inf('maladroitness', 'maladroitnesses').
inf('slowness', 'slownesses').
inf('rustiness', 'rustinesses').
inf('inefficiency', 'inefficiencies').
inf('amateurishness', 'amateurishnesses').
inf('illiteracy', 'illiteracies').
inf('uncreativeness', 'uncreativenesses').
inf('fruitlessness', 'fruitlessnesses').
inf('unoriginality', 'unoriginalities').
inf('triteness', 'tritenesses').
inf('staleness', 'stalenesses').
inf('conventionality', 'conventionalities').
inf('faculty', 'faculties').
inf('mental faculty', 'mental faculties').
inf('speech', 'speeches').
inf('lexis', 'lexes').
inf('vocabulary', 'vocabularies').
inf('memory', 'memories').
inf('retentiveness', 'retentivenesses').
inf('retentivity', 'retentivities').
inf('sentiency', 'sentiencies').
inf('sensory faculty', 'sensory faculties').
inf('modality', 'modalities').
inf('sense modality', 'sense modalities').
inf('velleity', 'velleities').
inf('sensitivity', 'sensitivities').
inf('sensitiveness', 'sensitivenesses').
inf('sensibility', 'sensibilities').
inf('acuteness', 'acutenesses').
inf('hypersensitivity', 'hypersensitivities').
inf('responsiveness', 'responsivenesses').
inf('reactivity', 'reactivities').
inf('excitability', 'excitabilities').
inf('irritability', 'irritabilities').
inf('photosensitivity', 'photosensitivities').
inf('radiosensitivity', 'radiosensitivities').
inf('visual modality', 'visual modalities').
inf('touch', 'touches').
inf('sense of touch', 'senses of touch').
inf('touch modality', 'touch modalities').
inf('acuity', 'acuities').
inf('visual acuity', 'visual acuities').
inf('sharp-sightedness', 'sharp-sightednesses').
inf('twenty-twenty', 'twenty-twenties').
inf('trichromacy', 'trichromacies').
inf('sightedness', 'sightednesses').
inf('stereoscopy', 'stereoscopies').
inf('sense of hearing', 'senses of hearing').
inf('auditory modality', 'auditory modalities').
inf('absolute pitch', 'absolute pitches').
inf('perfect pitch', 'perfect pitches').
inf('sense of taste', 'senses of taste').
inf('gustatory modality', 'gustatory modalities').
inf('sense of smell', 'senses of smell').
inf('olfactory modality', 'olfactory modalities').
inf('kinesthesis', 'kinestheses').
inf('kinaesthesis', 'kinaestheses').
inf('sense of movement', 'senses of movement').
inf('sense of balance', 'senses of balance').
inf('sense of equilibrium', 'senses of equilibrium').
inf('somesthesis', 'somestheses').
inf('somaesthesis', 'somaestheses').
inf('somataesthesis', 'somataestheses').
inf('pedagogy', 'pedagogies').
inf('method of choice', 'methods of choice').
inf('methodology', 'methodologies').
inf('system of rules', 'systems of rules').
inf('frame of reference', 'frames of reference').
inf('vocabulary', 'vocabularies').
inf('system of logic', 'systems of logic').
inf('Benday process', 'Benday processes').
inf('code of conduct', 'codes of conduct').
inf('code of behavior', 'codes of behavior').
inf('knight errantry', 'knight errantries').
inf('state of mind', 'states of mind').
inf('interestedness', 'interestednesses').
inf('personal business', 'personal businesses').
inf('dirty laundry', 'dirty laundries').
inf('point of honor', 'points of honor').
inf('cult of personality', 'cults of personality').
inf('memory loss', 'memory losses').
inf('forgetfulness', 'forgetfulnesses').
inf('forgetfulness', 'forgetfulnesses').
inf('obliviousness', 'obliviousnesses').
inf('readiness', 'readinesses').
inf('consciousness', 'consciousnesses').
inf('stream of consciousness', 'streams of consciousness').
inf('self', 'selves').
inf('ego', 'egoes').
inf('awareness', 'awarenesses').
inf('consciousness', 'consciousnesses').
inf('knowingness', 'knowingnesses').
inf('self-awareness', 'self-awarenesses').
inf('self-consciousness', 'self-consciousnesses').
inf('unselfconsciousness', 'unselfconsciousnesses').
inf('sense of direction', 'senses of direction').
inf('sense of responsibility', 'senses of responsibility').
inf('awareness', 'awarenesses').
inf('sensibility', 'sensibilities').
inf('wakefulness', 'wakefulnesses').
inf('unconsciousness', 'unconsciousnesses').
inf('unknowingness', 'unknowingnesses').
inf('unawareness', 'unawarenesses').
inf('grogginess', 'grogginesses').
inf('semiconsciousness', 'semiconsciousnesses').
inf('comatoseness', 'comatosenesses').
inf('insensibility', 'insensibilities').
inf('narcosis', 'narcoses').
inf('nitrogen narcosis', 'nitrogen narcoses').
inf('subconsciousness', 'subconsciousnesses').
inf('curiosity', 'curiosities').
inf('desire to know', 'desires to know').
inf('lust for learning', 'lusts for learning').
inf('thirst for knowledge', 'thirsts for knowledge').
inf('curiousness', 'curiousnesses').
inf('inquisitiveness', 'inquisitivenesses').
inf('nosiness', 'nosinesses').
inf('snoopiness', 'snoopinesses').
inf('confusedness', 'confusednesses').
inf('muddiness', 'muddinesses').
inf('perplexity', 'perplexities').
inf('mystery', 'mysteries').
inf('quandary', 'quandaries').
inf('difficulty', 'difficulties').
inf('can of worms', 'cans of worms').
inf('albatross', 'albatrosses').
inf('hitch', 'hitches').
inf('stymy', 'stymies').
inf('match', 'matches').
inf('mismatch', 'mismatches').
inf('certainty', 'certainties').
inf('authority', 'authorities').
inf('sureness', 'surenesses').
inf('cocksureness', 'cocksurenesses').
inf('uncertainty', 'uncertainties').
inf('dubiety', 'dubieties').
inf('doubtfulness', 'doubtfulnesses').
inf('dubiousness', 'dubiousnesses').
inf('incredulity', 'incredulities').
inf('indecisiveness', 'indecisivenesses').
inf('morbidity', 'morbidities').
inf('morbidness', 'morbidnesses').
inf('preoccupancy', 'preoccupancies').
inf('abstractedness', 'abstractednesses').
inf('revery', 'reveries').
inf('brown study', 'brown studies').
inf('absentmindedness', 'absentmindednesses').
inf('process', 'processes').
inf('cognitive process', 'cognitive processes').
inf('mental process', 'mental processes').
inf('process', 'processes').
inf('unconscious process', 'unconscious processes').
inf('basic cognitive process', 'basic cognitive processes').
inf('attentiveness', 'attentivenesses').
inf('advertency', 'advertencies').
inf('study', 'studies').
inf('watchfulness', 'watchfulnesses').
inf('wakefulness', 'wakefulnesses').
inf('alertness', 'alertnesses').
inf('jealousy', 'jealousies').
inf('inattentiveness', 'inattentivenesses').
inf('heedlessness', 'heedlessnesses').
inf('gnosis', 'gnoses').
inf('immediacy', 'immediacies').
inf('constancy', 'constancies').
inf('perceptual constancy', 'perceptual constancies').
inf('brightness constancy', 'brightness constancies').
inf('color constancy', 'color constancies').
inf('colour constancy', 'colour constancies').
inf('shape constancy', 'shape constancies').
inf('size constancy', 'size constancies').
inf('perceptiveness', 'perceptivenesses').
inf('melody', 'melodies').
inf('esthesis', 'estheses').
inf('aesthesis', 'aestheses').
inf('stench', 'stenches').
inf('mephitis', 'mephitis').
inf('relish', 'relishes').
inf('sapidity', 'sapidities').
inf('sweetness', 'sweetnesses').
inf('sugariness', 'sugarinesses').
inf('sourness', 'sournesses').
inf('tartness', 'tartnesses').
inf('acidity', 'acidities').
inf('acidulousness', 'acidulousnesses').
inf('bitterness', 'bitternesses').
inf('acridity', 'acridities').
inf('saltiness', 'saltinesses').
inf('salinity', 'salinities').
inf('astringency', 'astringencies').
inf('finish', 'finishes').
inf('flatness', 'flatnesses').
inf('mellowness', 'mellownesses').
inf('euphony', 'euphonies').
inf('music of the spheres', 'musics of the spheres').
inf('fundamental frequency', 'fundamental frequencies').
inf('tightness', 'tightnesses').
inf('tactility', 'tactilities').
inf('skin perceptiveness', 'skin perceptivenesses').
inf('feeling of movement', 'feelings of movement').
inf('touch', 'touches').
inf('creepiness', 'creepinesses').
inf('itch', 'itches').
inf('itchiness', 'itchinesses').
inf('topognosis', 'topognoses').
inf('coldness', 'coldnesses').
inf('frame of reference', 'frames of reference').
inf('redundancy', 'redundancies').
inf('topology', 'topologies').
inf('network topology', 'network topologies').
inf('bus topology', 'bus topologies').
inf('loop topology', 'loop topologies').
inf('star topology', 'star topologies').
inf('mesh topology', 'mesh topologies').
inf('mesh', 'meshes').
inf('physical topology', 'physical topologies').
inf('logical topology', 'logical topologies').
inf('critical analysis', 'critical analyses').
inf('biopsy', 'biopsies').
inf('chorionic villus biopsy', 'chorionic villus biopsies').
inf('needle biopsy', 'needle biopsies').
inf('point of no return', 'points of no return').
inf('perceptiveness', 'perceptivenesses').
inf('retro', 'retroes').
inf('delicacy', 'delicacies').
inf('committal to memory', 'committals to memory').
inf('study', 'studies').
inf('transfer of training', 'transfers of training').
inf('memory', 'memories').
inf('short-term memory', 'short-term memories').
inf('immediate memory', 'immediate memories').
inf('working memory', 'working memories').
inf('long-term memory', 'long-term memories').
inf('episodic memory', 'episodic memories').
inf('personal memory', 'personal memories').
inf('semantic memory', 'semantic memories').
inf('motor memory', 'motor memories').
inf('muscle memory', 'muscle memories').
inf('anamnesis', 'anamneses').
inf('reconstructive memory', 'reconstructive memories').
inf('reproductive memory', 'reproductive memories').
inf('identity', 'identities').
inf('representational process', 'representational processes').
inf('imagery', 'imageries').
inf('mental imagery', 'mental imageries').
inf('revery', 'reveries').
inf('castle in the air', 'castles in the air').
inf('castle in Spain', 'castles in Spain').
inf('search', 'searches').
inf('higher cognitive process', 'higher cognitive processes').
inf('thought process', 'thought processes').
inf('mental synthesis', 'mental syntheses').
inf('analysis', 'analyses').
inf('line of reasoning', 'lines of reasoning').
inf('line of thought', 'lines of thought').
inf('train of thought', 'trains of thought').
inf('line of inquiry', 'lines of inquiry').
inf('line of questioning', 'lines of questioning').
inf('synthesis', 'syntheses').
inf('prophecy', 'prophecies').
inf('arithmancy', 'arithmancies').
inf('rhabdomancy', 'rhabdomancies').
inf('geomancy', 'geomancies').
inf('hydromancy', 'hydromancies').
inf('lithomancy', 'lithomancies').
inf('necromancy', 'necromancies').
inf('oneiromancy', 'oneiromancies').
inf('onomancy', 'onomancies').
inf('palmistry', 'palmistries').
inf('chiromancy', 'chiromancies').
inf('chirology', 'chirologies').
inf('pyromancy', 'pyromancies').
inf('astrology', 'astrologies').
inf('horoscopy', 'horoscopies').
inf('alchemy', 'alchemies').
inf('ideology', 'ideologies').
inf('analogy', 'analogies').
inf('corollary', 'corollaries').
inf('cost-benefit analysis', 'cost-benefit analyses').
inf('reasoning by elimination', 'reasonings by elimination').
inf('systems analysis', 'systems analyses').
inf('regress', 'regresses').
inf('synthesis', 'syntheses').
inf('trend analysis', 'trend analyses').
inf('study', 'studies').
inf('thoughtfulness', 'thoughtfulnesses').
inf('self-analysis', 'self-analyses').
inf('inwardness', 'inwardnesses').
inf('outwardness', 'outwardnesses').
inf('omphaloskepsis', 'omphaloskepses').
inf('change of mind', 'changes of mind').
inf('wish', 'wishes').
inf('possibility', 'possibilities').
inf('impossibility', 'impossibilities').
inf('basis', 'bases').
inf('natural history', 'natural histories').
inf('out-of-the-box thinking', 'outs-of-the-box thinking').
inf('inquiry', 'inquiries').
inf('enquiry', 'enquiries').
inf('research', 'researches').
inf('nature study', 'nature studies').
inf('empirical research', 'empirical researches').
inf('canvass', 'canvasses').
inf('heraldry', 'heraldries').
inf('guess', 'guesses').
inf('farsightedness', 'farsightednesses').
inf('savvy', 'savvies').
inf('discovery', 'discoveries').
inf('flash', 'flashes').
inf('linguistic process', 'linguistic processes').
inf('reality', 'realities').
inf('real life', 'real lives').
inf('life', 'lives').
inf('food for thought', 'foods for thought').
inf('antipathy', 'antipathies').
inf('center of attention', 'centers of attention').
inf('centre of attention', 'centres of attention').
inf('thing-in-itself', 'things-in-itself').
inf('memento', 'mementoes').
inf('universe of discourse', 'universes of discourse').
inf('familiarity', 'familiarities').
inf('conversancy', 'conversancies').
inf('matter of fact', 'matters of fact').
inf('verity', 'verities').
inf('nook and cranny', 'nook and crannies').
inf('technicality', 'technicalities').
inf('triviality', 'trivialities').
inf('apology', 'apologies').
inf('case in point', 'cases in point').
inf('pain in the neck', 'pains in the neck').
inf('pain in the ass', 'pains in the ass').
inf('worry', 'worries').
inf('business', 'businesses').
inf('conceptuality', 'conceptualities').
inf('category', 'categories').
inf('variety', 'varieties').
inf('style of architecture', 'styles of architecture').
inf('type of architecture', 'types of architecture').
inf('Moorish', 'Moorishes').
inf('strangeness', 'strangenesses').
inf('genus', 'genera').
inf('narrowness', 'narrownesses').
inf('rule of thumb', 'rules of thumb').
inf('rule of cy pres', 'rules of cy pres').
inf('property', 'properties').
inf('quality', 'qualities').
inf('feature of speech', 'features of speech').
inf('excellency', 'excellencies').
inf('peculiarity', 'peculiarities').
inf('quantity', 'quantities').
inf('numerical quantity', 'numerical quantities').
inf('zero', 'zeroes').
inf('eigenvalue of a matrix', 'eigenvalues of a matrix').
inf('eigenvalue of a square matrix', 'eigenvalues of a square matrix').
inf('characteristic root of a square matrix', 'characteristic root of a square matrices').
inf('variable quantity', 'variable quantities').
inf('arity', 'arities').
inf('degree of freedom', 'degrees of freedom').
inf('constant quantity', 'constant quantities').
inf('parametric quantity', 'parametric quantities').
inf('degree of freedom', 'degrees of freedom').
inf('degree of a term', 'degrees of a term').
inf('degree of a polynomial', 'degrees of a polynomial').
inf('convergency', 'convergencies').
inf('divergency', 'divergencies').
inf('point of intersection', 'points of intersection').
inf('vertex', 'vertexes').
inf('complex', 'complexes').
inf('law of nature', 'laws of nature').
inf('law of Archimedes', 'laws of Archimedes').
inf('Avogadro\'s hypothesis', 'Avogadro\'s hypotheses').
inf('law of large numbers', 'laws of large numbers').
inf('law of partial pressures', 'laws of partial pressures').
inf('law of chemical equilibrium', 'laws of chemical equilibrium').
inf('law of volumes', 'laws of volumes').
inf('law of areas', 'laws of areas').
inf('law of equal areas', 'laws of equal areas').
inf('law of averages', 'laws of averages').
inf('law of constant proportion', 'laws of constant proportion').
inf('law of definite proportions', 'laws of definite proportions').
inf('law of diminishing returns', 'laws of diminishing returns').
inf('law of effect', 'laws of effect').
inf('law of equivalent proportions', 'laws of equivalent proportions').
inf('law of reciprocal proportions', 'laws of reciprocal proportions').
inf('law of gravitation', 'laws of gravitation').
inf('law of multiple proportions', 'laws of multiple proportions').
inf('law of mass action', 'laws of mass action').
inf('law of thermodynamics', 'laws of thermodynamics').
inf('law of segregation', 'laws of segregation').
inf('law of independent assortment', 'laws of independent assortment').
inf('law of motion', 'laws of motion').
inf('law of action and reaction', 'laws of action and reaction').
inf('big-bang theory', 'big-bang theories').
inf('big bang theory', 'big bang theories').
inf('nebular hypothesis', 'nebular hypotheses').
inf('planetesimal hypothesis', 'planetesimal hypotheses').
inf('steady state theory', 'steady state theories').
inf('continuous creation theory', 'continuous creation theories').
inf('hypothesis', 'hypotheses').
inf('possibility', 'possibilities').
inf('theory', 'theories').
inf('M-theory', 'M-theories').
inf('string theory', 'string theories').
inf('fallacy', 'fallacies').
inf('logical fallacy', 'logical fallacies').
inf('pathetic fallacy', 'pathetic fallacies').
inf('sophistry', 'sophistries').
inf('fantasy', 'fantasies').
inf('phantasy', 'phantasies').
inf('fancy', 'fancies').
inf('Flying Dutchman', 'Flying Dutchmen').
inf('defense policy', 'defense policies').
inf('defence policy', 'defence policies').
inf('tax policy', 'tax policies').
inf('policy', 'policies').
inf('beggar-my-neighbor policy', 'beggar-my-neighbor policies').
inf('beggar-my-neighbour policy', 'beggar-my-neighbour policies').
inf('beggar-my-neighbor strategy', 'beggar-my-neighbor strategies').
inf('beggar-my-neighbour strategy', 'beggar-my-neighbour strategies').
inf('plan of action', 'plans of action').
inf('strategy', 'strategies').
inf('itinerary', 'itineraries').
inf('house of cards', 'houses of cards').
inf('conspiracy', 'conspiracies').
inf('cash cow', 'cash kine').
inf('generality', 'generalities').
inf('pillar of Islam', 'pillars of Islam').
inf('first blush', 'first blushes').
inf('hunch', 'hunches').
inf('whimsy', 'whimsies').
inf('inwardness', 'inwardnesses').
inf('nitty-gritty', 'nitty-gritties').
inf('hypostasis', 'hypostases').
inf('quiddity', 'quiddities').
inf('haecceity', 'haecceities').
inf('beauty', 'beauties').
inf('reconditeness', 'reconditenesses').
inf('abstruseness', 'abstrusenesses').
inf('abstrusity', 'abstrusities').
inf('profoundness', 'profoundnesses').
inf('profundity', 'profundities').
inf('heavy', 'heavies').
inf('hero', 'heroes').
inf('psychosexuality', 'psychosexualities').
inf('eye candy', 'eye candies').
inf('field of view', 'fields of view').
inf('field of vision', 'fields of vision').
inf('field of regard', 'fields of regard').
inf('tableau', 'tableaux').
inf('memory', 'memories').
inf('screen memory', 'screen memories').
inf('imago', 'imagoes').
inf('prodigy', 'prodigies').
inf('unsoundness', 'unsoundnesses').
inf('doctrine of analogy', 'doctrines of analogy').
inf('analogy', 'analogies').
inf('article of faith', 'articles of faith').
inf('philosophy', 'philosophies').
inf('school of thought', 'schools of thought').
inf('philosophy', 'philosophies').
inf('irrational hostility', 'irrational hostilities').
inf('possibility', 'possibilities').
inf('expectancy', 'expectancies').
inf('theory', 'theories').
inf('theosophy', 'theosophies').
inf('anthroposophy', 'anthroposophies').
inf('chivalry', 'chivalries').
inf('knightliness', 'knightlinesses').
inf('credo', 'credoes').
inf('gymnosophy', 'gymnosophies').
inf('mimesis', 'mimeses').
inf('democracy', 'democracies').
inf('thaumaturgy', 'thaumaturgies').
inf('mojo', 'mojoes').
inf('empiricist philosophy', 'empiricist philosophies').
inf('existential philosophy', 'existential philosophies').
inf('existentialist philosophy', 'existentialist philosophies').
inf('semiology', 'semiologies').
inf('teleology', 'teleologies').
inf('conjury', 'conjuries').
inf('sorcery', 'sorceries').
inf('necromancy', 'necromancies').
inf('theurgy', 'theurgies').
inf('witchery', 'witcheries').
inf('heresy', 'heresies').
inf('unorthodoxy', 'unorthodoxies').
inf('business', 'businesses').
inf('eruditeness', 'eruditenesses').
inf('learnedness', 'learnednesses').
inf('darkness', 'darknesses').
inf('ignorantness', 'ignorantnesses').
inf('unknowingness', 'unknowingnesses').
inf('rawness', 'rawnesses').
inf('illiteracy', 'illiteracies').
inf('theory', 'theories').
inf('theory of gravitation', 'theories of gravitation').
inf('theory of gravity', 'theories of gravity').
inf('gravitational theory', 'gravitational theories').
inf('principle of relativity', 'principles of relativity').
inf('principle of parsimony', 'principles of parsimony').
inf('law of parsimony', 'laws of parsimony').
inf('principle of equivalence', 'principles of equivalence').
inf('principle of liquid displacement', 'principles of liquid displacement').
inf('principle of superposition', 'principles of superposition').
inf('principle of superposition', 'principles of superposition').
inf('localization of function', 'localizations of function').
inf('localisation of function', 'localisations of function').
inf('laterality', 'lateralities').
inf('blastogenesis', 'blastogeneses').
inf('theory of preformation', 'theories of preformation').
inf('scientific theory', 'scientific theories').
inf('field theory', 'field theories').
inf('economic theory', 'economic theories').
inf('Malthusian theory', 'Malthusian theories').
inf('field of study', 'fields of study').
inf('study', 'studies').
inf('communication theory', 'communication theories').
inf('genealogy', 'genealogies').
inf('allometry', 'allometries').
inf('ology', 'ologies').
inf('symbology', 'symbologies').
inf('territory', 'territories').
inf('geometry', 'geometries').
inf('affine geometry', 'affine geometries').
inf('elementary geometry', 'elementary geometries').
inf('parabolic geometry', 'parabolic geometries').
inf('Euclidean geometry', 'Euclidean geometries').
inf('fractal geometry', 'fractal geometries').
inf('non-Euclidean geometry', 'non-Euclidean geometries').
inf('hyperbolic geometry', 'hyperbolic geometries').
inf('elliptic geometry', 'elliptic geometries').
inf('Riemannian geometry', 'Riemannian geometries').
inf('numerical analysis', 'numerical analyses').
inf('spherical geometry', 'spherical geometries').
inf('spherical trigonometry', 'spherical trigonometries').
inf('analytic geometry', 'analytic geometries').
inf('analytical geometry', 'analytical geometries').
inf('coordinate geometry', 'coordinate geometries').
inf('axis', 'axes').
inf('coordinate axis', 'coordinate axes').
inf('x-axis', 'x-axes').
inf('y-axis', 'y-axes').
inf('z-axis', 'z-axes').
inf('major axis', 'major axes').
inf('semimajor axis', 'semimajor axes').
inf('minor axis', 'minor axes').
inf('semiminor axis', 'semiminor axes').
inf('principal axis', 'principal axes').
inf('optic axis', 'optic axes').
inf('optic axis', 'optic axes').
inf('plane geometry', 'plane geometries').
inf('solid geometry', 'solid geometries').
inf('projective geometry', 'projective geometries').
inf('descriptive geometry', 'descriptive geometries').
inf('trigonometry', 'trigonometries').
inf('analysis', 'analyses').
inf('Fourier analysis', 'Fourier analyses').
inf('harmonic analysis', 'harmonic analyses').
inf('method of fluxions', 'methods of fluxions').
inf('calculus of variations', 'calculuss of variations').
inf('set theory', 'set theories').
inf('group theory', 'group theories').
inf('Galois theory', 'Galois theories').
inf('topology', 'topologies').
inf('method of least squares', 'methods of least squares').
inf('multivariate analysis', 'multivariate analyses').
inf('multicollinearity', 'multicollinearities').
inf('regression analysis', 'regression analyses').
inf('regression of y on x', 'regressions of y on x').
inf('correlational analysis', 'correlational analyses').
inf('correlation matrix', 'correlation matrices').
inf('factor analysis', 'factor analyses').
inf('analysis of variance', 'analyses of variance').
inf('coefficient of correlation', 'coefficients of correlation').
inf('coefficient of concordance', 'coefficients of concordance').
inf('probability theory', 'probability theories').
inf('theory of probability', 'theories of probability').
inf('biology', 'biologies').
inf('biometry', 'biometries').
inf('craniology', 'craniologies').
inf('medical specialty', 'medical specialties').
inf('phrenology', 'phrenologies').
inf('allergology', 'allergologies').
inf('anesthesiology', 'anesthesiologies').
inf('angiology', 'angiologies').
inf('bacteriology', 'bacteriologies').
inf('cardiology', 'cardiologies').
inf('dentistry', 'dentistries').
inf('odontology', 'odontologies').
inf('cosmetic dentistry', 'cosmetic dentistries').
inf('dental surgery', 'dental surgeries').
inf('dermatology', 'dermatologies').
inf('endocrinology', 'endocrinologies').
inf('epidemiology', 'epidemiologies').
inf('forensic pathology', 'forensic pathologies').
inf('gastroenterology', 'gastroenterologies').
inf('gerontology', 'gerontologies').
inf('gynecology', 'gynecologies').
inf('gynaecology', 'gynaecologies').
inf('hematology', 'hematologies').
inf('haematology', 'haematologies').
inf('immunology', 'immunologies').
inf('immunochemistry', 'immunochemistries').
inf('chemoimmunology', 'chemoimmunologies').
inf('immunopathology', 'immunopathologies').
inf('nephrology', 'nephrologies').
inf('neurology', 'neurologies').
inf('clinical neurology', 'clinical neurologies').
inf('neuropsychiatry', 'neuropsychiatries').
inf('nosology', 'nosologies').
inf('tocology', 'tocologies').
inf('midwifery', 'midwiferies').
inf('fetology', 'fetologies').
inf('foetology', 'foetologies').
inf('perinatology', 'perinatologies').
inf('oncology', 'oncologies').
inf('ophthalmology', 'ophthalmologies').
inf('otology', 'otologies').
inf('pharmacology', 'pharmacologies').
inf('pharmacy', 'pharmacies').
inf('posology', 'posologies').
inf('psychopharmacology', 'psychopharmacologies').
inf('psychiatry', 'psychiatries').
inf('psychopathology', 'psychopathologies').
inf('psychotherapy', 'psychotherapies').
inf('clinical psychology', 'clinical psychologies').
inf('Freudian psychology', 'Freudian psychologies').
inf('Jungian psychology', 'Jungian psychologies').
inf('anatomy', 'anatomies').
inf('general anatomy', 'general anatomies').
inf('clinical anatomy', 'clinical anatomies').
inf('applied anatomy', 'applied anatomies').
inf('comparative anatomy', 'comparative anatomies').
inf('dental anatomy', 'dental anatomies').
inf('developmental anatomy', 'developmental anatomies').
inf('functional anatomy', 'functional anatomies').
inf('physiological anatomy', 'physiological anatomies').
inf('morphophysiology', 'morphophysiologies').
inf('gross anatomy', 'gross anatomies').
inf('macroscopic anatomy', 'macroscopic anatomies').
inf('microscopic anatomy', 'microscopic anatomies').
inf('neuroanatomy', 'neuroanatomies').
inf('osteology', 'osteologies').
inf('regional anatomy', 'regional anatomies').
inf('topographic anatomy', 'topographic anatomies').
inf('topology', 'topologies').
inf('audiology', 'audiologies').
inf('audiometry', 'audiometries').
inf('pathology', 'pathologies').
inf('pedology', 'pedologies').
inf('neonatology', 'neonatologies').
inf('podiatry', 'podiatries').
inf('chiropody', 'chiropodies').
inf('proctology', 'proctologies').
inf('radiology', 'radiologies').
inf('rheumatology', 'rheumatologies').
inf('rhinolaryngology', 'rhinolaryngologies').
inf('otorhinolaryngology', 'otorhinolaryngologies').
inf('otolaryngology', 'otolaryngologies').
inf('serology', 'serologies').
inf('surgery', 'surgeries').
inf('toxicology', 'toxicologies').
inf('traumatology', 'traumatologies').
inf('accident surgery', 'accident surgeries').
inf('urology', 'urologies').
inf('virology', 'virologies').
inf('agronomy', 'agronomies').
inf('agrobiology', 'agrobiologies').
inf('agrology', 'agrologies').
inf('biogeography', 'biogeographies').
inf('botany', 'botanies').
inf('phytology', 'phytologies').
inf('mycology', 'mycologies').
inf('pomology', 'pomologies').
inf('cryobiology', 'cryobiologies').
inf('cytology', 'cytologies').
inf('ecology', 'ecologies').
inf('embryology', 'embryologies').
inf('exobiology', 'exobiologies').
inf('space biology', 'space biologies').
inf('astrobiology', 'astrobiologies').
inf('forestry', 'forestries').
inf('entomology', 'entomologies').
inf('bugology', 'bugologies').
inf('lepidopterology', 'lepidopterologies').
inf('lepidoptery', 'lepidopteries').
inf('ethology', 'ethologies').
inf('herpetology', 'herpetologies').
inf('ichthyology', 'ichthyologies').
inf('malacology', 'malacologies').
inf('mammalogy', 'mammalogies').
inf('oology', 'oologies').
inf('ornithology', 'ornithologies').
inf('primatology', 'primatologies').
inf('protozoology', 'protozoologies').
inf('paleontology', 'paleontologies').
inf('palaeontology', 'palaeontologies').
inf('fossilology', 'fossilologies').
inf('paleoanthropology', 'paleoanthropologies').
inf('palaeoanthropology', 'palaeoanthropologies').
inf('human paleontology', 'human paleontologies').
inf('human palaeontology', 'human palaeontologies').
inf('paleobotany', 'paleobotanies').
inf('palaeobotany', 'palaeobotanies').
inf('phycology', 'phycologies').
inf('algology', 'algologies').
inf('pteridology', 'pteridologies').
inf('paleodendrology', 'paleodendrologies').
inf('palaeodendrology', 'palaeodendrologies').
inf('paleozoology', 'paleozoologies').
inf('palaeozoology', 'palaeozoologies').
inf('paleomammalogy', 'paleomammalogies').
inf('paleornithology', 'paleornithologies').
inf('palaeornithology', 'palaeornithologies').
inf('histology', 'histologies').
inf('microbiology', 'microbiologies').
inf('molecular biology', 'molecular biologies').
inf('morphology', 'morphologies').
inf('neurobiology', 'neurobiologies').
inf('paleobiology', 'paleobiologies').
inf('palaeobiology', 'palaeobiologies').
inf('neurology', 'neurologies').
inf('teratology', 'teratologies').
inf('biochemistry', 'biochemistries').
inf('enzymology', 'enzymologies').
inf('zymology', 'zymologies').
inf('zymurgy', 'zymurgies').
inf('physiology', 'physiologies').
inf('neurophysiology', 'neurophysiologies').
inf('kinesiology', 'kinesiologies').
inf('myology', 'myologies').
inf('paleoecology', 'paleoecologies').
inf('palaeoecology', 'palaeoecologies').
inf('radiobiology', 'radiobiologies').
inf('sociobiology', 'sociobiologies').
inf('zoology', 'zoologies').
inf('chemistry', 'chemistries').
inf('organic chemistry', 'organic chemistries').
inf('inorganic chemistry', 'inorganic chemistries').
inf('physical chemistry', 'physical chemistries').
inf('phytochemistry', 'phytochemistries').
inf('electrochemistry', 'electrochemistries').
inf('femtochemistry', 'femtochemistries').
inf('geochemistry', 'geochemistries').
inf('photochemistry', 'photochemistries').
inf('radiochemistry', 'radiochemistries').
inf('nuclear chemistry', 'nuclear chemistries').
inf('surface chemistry', 'surface chemistries').
inf('natural philosophy', 'natural philosophies').
inf('astronomy', 'astronomies').
inf('uranology', 'uranologies').
inf('astrometry', 'astrometries').
inf('radio astronomy', 'radio astronomies').
inf('selenology', 'selenologies').
inf('cosmology', 'cosmologies').
inf('cosmogony', 'cosmogonies').
inf('cosmogeny', 'cosmogenies').
inf('cryogeny', 'cryogenies').
inf('crystallography', 'crystallographies').
inf('holography', 'holographies').
inf('rheology', 'rheologies').
inf('atomic theory', 'atomic theories').
inf('atomist theory', 'atomist theories').
inf('atomistic theory', 'atomistic theories').
inf('holistic theory', 'holistic theories').
inf('atomic theory', 'atomic theories').
inf('Bohr theory', 'Bohr theories').
inf('conservation of charge', 'conservations of charge').
inf('conservation of electricity', 'conservations of electricity').
inf('conservation of energy', 'conservations of energy').
inf('law of conservation of energy', 'laws of conservation of energy').
inf('conservation of mass', 'conservations of mass').
inf('conservation of matter', 'conservations of matter').
inf('law of conservation of mass', 'laws of conservation of mass').
inf('law of conservation of matter', 'laws of conservation of matter').
inf('conservation of momentum', 'conservations of momentum').
inf('parity', 'parities').
inf('conservation of parity', 'conservations of parity').
inf('space-reflection symmetry', 'space-reflection symmetries').
inf('mirror symmetry', 'mirror symmetries').
inf('cell theory', 'cell theories').
inf('wave theory', 'wave theories').
inf('undulatory theory', 'undulatory theories').
inf('corpuscular theory', 'corpuscular theories').
inf('kinetic theory', 'kinetic theories').
inf('relativity', 'relativities').
inf('theory of relativity', 'theories of relativity').
inf('relativity theory', 'relativity theories').
inf('Einstein\'s theory of relativity', 'Einstein\'s theory of relativities').
inf('general relativity', 'general relativities').
inf('general theory of relativity', 'general theory of relativities').
inf('general relativity theory', 'general relativity theories').
inf('Einstein\'s general theory of relativity', 'Einsteins\' general theory of relativity').
inf('special relativity', 'special relativities').
inf('special theory of relativity', 'special theory of relativities').
inf('special relativity theory', 'special relativity theories').
inf('Einstein\'s special theory of relativity', 'Einstein\'s special theory of relativities').
inf('supersymmetry', 'supersymmetries').
inf('quantum theory', 'quantum theories').
inf('germ theory', 'germ theories').
inf('information theory', 'information theories').
inf('theory of dissociation', 'theories of dissociation').
inf('theory of electrolytic dissociation', 'theories of electrolytic dissociation').
inf('theory of evolution', 'theories of evolution').
inf('theory of organic evolution', 'theories of organic evolution').
inf('theory of indicators', 'theories of indicators').
inf('theory of inheritance', 'theories of inheritance').
inf('thermochemistry', 'thermochemistries').
inf('theory of punctuated equilibrium', 'theories of punctuated equilibrium').
inf('quantum field theory', 'quantum field theories').
inf('thermodynamics of equilibrium', 'thermodynamicss of equilibrium').
inf('geology', 'geologies').
inf('hypsography', 'hypsographies').
inf('paleogeology', 'paleogeologies').
inf('palaeogeology', 'palaeogeologies').
inf('morphology', 'morphologies').
inf('geomorphology', 'geomorphologies').
inf('orology', 'orologies').
inf('orography', 'orographies').
inf('stratigraphy', 'stratigraphies').
inf('plate tectonic theory', 'plate tectonic theories').
inf('meteorology', 'meteorologies').
inf('aerology', 'aerologies').
inf('climatology', 'climatologies').
inf('bioclimatology', 'bioclimatologies').
inf('nephology', 'nephologies').
inf('hydrology', 'hydrologies').
inf('oceanography', 'oceanographies').
inf('oceanology', 'oceanologies').
inf('hydrography', 'hydrographies').
inf('limnology', 'limnologies').
inf('seismology', 'seismologies').
inf('volcanology', 'volcanologies').
inf('vulcanology', 'vulcanologies').
inf('geodesy', 'geodesies').
inf('mineralogy', 'mineralogies').
inf('petrology', 'petrologies').
inf('lithology', 'lithologies').
inf('speleology', 'speleologies').
inf('spelaeology', 'spelaeologies').
inf('petroleum geology', 'petroleum geologies').
inf('economic geology', 'economic geologies').
inf('mining geology', 'mining geologies').
inf('geography', 'geographies').
inf('physical geography', 'physical geographies').
inf('physiography', 'physiographies').
inf('topography', 'topographies').
inf('topology', 'topologies').
inf('economic geography', 'economic geographies').
inf('cosmography', 'cosmographies').
inf('technology', 'technologies').
inf('metallurgy', 'metallurgies').
inf('powder metallurgy', 'powder metallurgies').
inf('biotechnology', 'biotechnologies').
inf('biotechnology', 'biotechnologies').
inf('biotech', 'bioteches').
inf('recombinant DNA technology', 'recombinant DNA technologies').
inf('information technology', 'information technologies').
inf('nanotechnology', 'nanotechnologies').
inf('tribology', 'tribologies').
inf('rocketry', 'rocketries').
inf('metrology', 'metrologies').
inf('futurology', 'futurologies').
inf('psychology', 'psychologies').
inf('abnormal psychology', 'abnormal psychologies').
inf('psychopathology', 'psychopathologies').
inf('association theory', 'association theories').
inf('applied psychology', 'applied psychologies').
inf('industrial psychology', 'industrial psychologies').
inf('cognitive psychology', 'cognitive psychologies').
inf('comparative psychology', 'comparative psychologies').
inf('animal psychology', 'animal psychologies').
inf('developmental psychology', 'developmental psychologies').
inf('genetic psychology', 'genetic psychologies').
inf('child psychology', 'child psychologies').
inf('differential psychology', 'differential psychologies').
inf('experimental psychology', 'experimental psychologies').
inf('behavioristic psychology', 'behavioristic psychologies').
inf('behaviouristic psychology', 'behaviouristic psychologies').
inf('memory', 'memories').
inf('physiological psychology', 'physiological psychologies').
inf('neuropsychology', 'neuropsychologies').
inf('psychophysiology', 'psychophysiologies').
inf('psychometry', 'psychometries').
inf('reflexology', 'reflexologies').
inf('Gestalt psychology', 'Gestalt psychologies').
inf('social psychology', 'social psychologies').
inf('human language technology', 'human language technologies').
inf('anthropology', 'anthropologies').
inf('archeology', 'archeologies').
inf('archaeology', 'archaeologies').
inf('micropaleontology', 'micropaleontologies').
inf('marine archeology', 'marine archeologies').
inf('marine archaeology', 'marine archaeologies').
inf('underwater archeology', 'underwater archeologies').
inf('underwater archaeology', 'underwater archaeologies').
inf('paleoclimatology', 'paleoclimatologies').
inf('palaeoclimatology', 'palaeoclimatologies').
inf('paleogeography', 'paleogeographies').
inf('palaeogeography', 'palaeogeographies').
inf('paleography', 'paleographies').
inf('paleopathology', 'paleopathologies').
inf('palaeopathology', 'palaeopathologies').
inf('paletiology', 'paletiologies').
inf('palaetiology', 'palaetiologies').
inf('epigraphy', 'epigraphies').
inf('paleology', 'paleologies').
inf('palaeology', 'palaeologies').
inf('protohistory', 'protohistories').
inf('protoanthropology', 'protoanthropologies').
inf('protoarcheology', 'protoarcheologies').
inf('protoarchaeology', 'protoarchaeologies').
inf('ethnography', 'ethnographies').
inf('descriptive anthropology', 'descriptive anthropologies').
inf('paleoethnography', 'paleoethnographies').
inf('palaeoethnography', 'palaeoethnographies').
inf('ethnology', 'ethnologies').
inf('physical anthropology', 'physical anthropologies').
inf('craniometry', 'craniometries').
inf('social anthropology', 'social anthropologies').
inf('cultural anthropology', 'cultural anthropologies').
inf('garbology', 'garbologies').
inf('mythology', 'mythologies').
inf('geostrategy', 'geostrategies').
inf('political economy', 'political economies').
inf('game theory', 'game theories').
inf('theory of games', 'theories of games').
inf('sociology', 'sociologies').
inf('criminology', 'criminologies').
inf('demography', 'demographies').
inf('human ecology', 'human ecologies').
inf('psephology', 'psephologies').
inf('penology', 'penologies').
inf('poenology', 'poenologies').
inf('sociometry', 'sociometries').
inf('biosystematy', 'biosystematies').
inf('taxonomy', 'taxonomies').
inf('cladistic analysis', 'cladistic analyses').
inf('thanatology', 'thanatologies').
inf('English', 'Englishes').
inf('history', 'histories').
inf('art history', 'art histories').
inf('iconology', 'iconologies').
inf('chronology', 'chronologies').
inf('glottochronology', 'glottochronologies').
inf('history', 'histories').
inf('philosophy', 'philosophies').
inf('moral philosophy', 'moral philosophies').
inf('casuistry', 'casuistries').
inf('casuistry', 'casuistries').
inf('etiology', 'etiologies').
inf('aetiology', 'aetiologies').
inf('axiology', 'axiologies').
inf('legal philosophy', 'legal philosophies').
inf('ontology', 'ontologies').
inf('ontology', 'ontologies').
inf('cosmology', 'cosmologies').
inf('epistemology', 'epistemologies').
inf('methodology', 'methodologies').
inf('methodological analysis', 'methodological analyses').
inf('phenomenology', 'phenomenologies').
inf('philosophical theory', 'philosophical theories').
inf('structural sociology', 'structural sociologies').
inf('structural anthropology', 'structural anthropologies').
inf('dialect geography', 'dialect geographies').
inf('linguistic geography', 'linguistic geographies').
inf('etymology', 'etymologies').
inf('diachrony', 'diachronies').
inf('literary study', 'literary studies').
inf('prosody', 'prosodies').
inf('philology', 'philologies').
inf('dialectology', 'dialectologies').
inf('musicology', 'musicologies').
inf('stemmatology', 'stemmatologies').
inf('cryptanalysis', 'cryptanalyses').
inf('cryptography', 'cryptographies').
inf('cryptology', 'cryptologies').
inf('syntax', 'syntaxes').
inf('syntax', 'syntaxes').
inf('orthoepy', 'orthoepies').
inf('phonology', 'phonologies').
inf('morphology', 'morphologies').
inf('morphology', 'morphologies').
inf('inflectional morphology', 'inflectional morphologies').
inf('derivational morphology', 'derivational morphologies').
inf('compound morphology', 'compound morphologies').
inf('lexicology', 'lexicologies').
inf('toponymy', 'toponymies').
inf('toponomy', 'toponomies').
inf('deixis', 'deixes').
inf('semasiology', 'semasiologies').
inf('theology', 'theologies').
inf('divinity', 'divinities').
inf('angelology', 'angelologies').
inf('ecclesiology', 'ecclesiologies').
inf('eschatology', 'eschatologies').
inf('liturgiology', 'liturgiologies').
inf('theodicy', 'theodicies').
inf('theology', 'theologies').
inf('Christian theology', 'Christian theologies').
inf('liberation theology', 'liberation theologies').
inf('natural theology', 'natural theologies').
inf('patrology', 'patrologies').
inf('soteriology', 'soteriologies').
inf('total depravity', 'total depravities').
inf('transcendental philosophy', 'transcendental philosophies').
inf('vertebrate paleontology', 'vertebrate paleontologies').
inf('mentality', 'mentalities').
inf('tendency', 'tendencies').
inf('neoteny', 'neotenies').
inf('sympathy', 'sympathies').
inf('proclivity', 'proclivities').
inf('propensity', 'propensities').
inf('partiality', 'partialities').
inf('anthropocentricity', 'anthropocentricities').
inf('unfairness', 'unfairnesses').
inf('impartiality', 'impartialities').
inf('disinterestedness', 'disinterestednesses').
inf('fairness', 'fairnesses').
inf('fair-mindedness', 'fair-mindednesses').
inf('white supremacy', 'white supremacies').
inf('tendentiousness', 'tendentiousnesses').
inf('broad-mindedness', 'broad-mindednesses').
inf('liberality', 'liberalities').
inf('liberalness', 'liberalnesses').
inf('neutrality', 'neutralities').
inf('narrow-mindedness', 'narrow-mindednesses').
inf('narrowness', 'narrownesses').
inf('pettiness', 'pettinesses').
inf('bigotry', 'bigotries').
inf('zealotry', 'zealotries').
inf('profaneness', 'profanenesses').
inf('point of view', 'points of view').
inf('orthodoxy', 'orthodoxies').
inf('conformity', 'conformities').
inf('conventionality', 'conventionalities').
inf('unorthodoxy', 'unorthodoxies').
inf('heterodoxy', 'heterodoxies').
inf('heresy', 'heresies').
inf('nonconformity', 'nonconformities').
inf('ideology', 'ideologies').
inf('political theory', 'political theories').
inf('autocracy', 'autocracies').
inf('democracy', 'democracies').
inf('social democracy', 'social democracies').
inf('domino theory', 'domino theories').
inf('meritocracy', 'meritocracies').
inf('theocracy', 'theocracies').
inf('dovishness', 'dovishnesses').
inf('peace advocacy', 'peace advocacies').
inf('hawkishness', 'hawkishnesses').
inf('war advocacy', 'war advocacies').
inf('godlessness', 'godlessnesses').
inf('Shinto', 'Shintoes').
inf('escapology', 'escapologies').
inf('graphology', 'graphologies').
inf('numerology', 'numerologies').
inf('protology', 'protologies').
inf('theogony', 'theogonies').
inf('strategy', 'strategies').
inf('law of closure', 'laws of closure').
inf('law of common fate', 'laws of common fate').
inf('law of continuation', 'laws of continuation').
inf('proximity', 'proximities').
inf('law of proximity', 'laws of proximity').
inf('similarity', 'similarities').
inf('law of similarity', 'laws of similarity').
inf('piece of paper', 'pieces of paper').
inf('sheet of paper', 'sheets of paper').
inf('leaf', 'leaves').
inf('flyleaf', 'flyleaves').
inf('interleaf', 'interleaves').
inf('recto', 'rectoes').
inf('verso', 'versoes').
inf('stationery', 'stationeries').
inf('gutter press', 'gutter presses').
inf('free press', 'free presses').
inf('press', 'presses').
inf('public press', 'public presses').
inf('rural free delivery', 'rural free deliveries').
inf('first class', 'first classes').
inf('1st class', '1st classes').
inf('express', 'expresses').
inf('pony express', 'pony expresses').
inf('news photography', 'news photographies').
inf('daily', 'dailies').
inf('news story', 'news stories').
inf('morceau', 'morceaux').
inf('article of faith', 'articles of faith').
inf('lead story', 'lead stories').
inf('underground press', 'underground presses').
inf('telephony', 'telephonies').
inf('telegraphy', 'telegraphies').
inf('wireless', 'wirelesses').
inf('radiotelegraphy', 'radiotelegraphies').
inf('wireless telegraphy', 'wireless telegraphies').
inf('third class', 'third classes').
inf('radiotelephony', 'radiotelephonies').
inf('multiplex', 'multiplexes').
inf('wireless', 'wirelesses').
inf('context of use', 'contexts of use').
inf('portmanteau', 'portmanteaux').
inf('neology', 'neologies').
inf('lexical entry', 'lexical entries').
inf('dictionary entry', 'dictionary entries').
inf('terminology', 'terminologies').
inf('affix', 'affixes').
inf('prefix', 'prefixes').
inf('suffix', 'suffixes').
inf('postfix', 'postfixes').
inf('inflectional suffix', 'inflectional suffixes').
inf('infix', 'infixes').
inf('grammatical category', 'grammatical categories').
inf('syntactic category', 'syntactic categories').
inf('substitution class', 'substitution classes').
inf('object of a preposition', 'objects of a preposition').
inf('object of the verb', 'objects of the verb').
inf('part of speech', 'parts of speech').
inf('form class', 'form classes').
inf('word class', 'word classes').
inf('major form class', 'major form classes').
inf('modal auxiliary', 'modal auxiliaries').
inf('person', 'people').
inf('first person', 'first people').
inf('second person', 'second people').
inf('third person', 'third people').
inf('beneficiary', 'beneficiaries').
inf('title of respect', 'titles of respect').
inf('form of address', 'forms of address').
inf('Defender of the Faith', 'Defenders of the Faith').
inf('Miss', 'Misses').
inf('baronetcy', 'baronetcies').
inf('viscountcy', 'viscountcies').
inf('pony', 'ponies').
inf('line of poetry', 'lines of poetry').
inf('line of verse', 'lines of verse').
inf('stenography', 'stenographies').
inf('tachygraphy', 'tachygraphies').
inf('orthography', 'orthographies').
inf('access', 'accesses').
inf('address', 'addresses').
inf('computer address', 'computer addresses').
inf('syllabary', 'syllabaries').
inf('ideography', 'ideographies').
inf('piece of writing', 'pieces of writing').
inf('patrology', 'patrologies').
inf('historiography', 'historiographies').
inf('allegory', 'allegories').
inf('fantasy', 'fantasies').
inf('phantasy', 'phantasies').
inf('story', 'stories').
inf('adventure story', 'adventure stories').
inf('mystery', 'mysteries').
inf('mystery story', 'mystery stories').
inf('detective story', 'detective stories').
inf('murder mystery', 'murder mysteries').
inf('love story', 'love stories').
inf('short story', 'short stories').
inf('allegory', 'allegories').
inf('Pilgrim\'s Progress', 'Pilgrim\'s Progresses').
inf('Twilight of the Gods', 'Twilights of the Gods').
inf('climax', 'climaxes').
inf('anticlimax', 'anticlimaxes').
inf('stream of consciousness', 'streams of consciousness').
inf('analysis', 'analyses').
inf('hagiology', 'hagiologies').
inf('elegy', 'elegies').
inf('rondeau', 'rondeaux').
inf('rhapsody', 'rhapsodies').
inf('canto', 'cantoes').
inf('tushery', 'tusheries').
inf('fair copy', 'fair copies').
inf('copy', 'copies').
inf('draft copy', 'draft copies').
inf('soft copy', 'soft copies').
inf('hard copy', 'hard copies').
inf('double indemnity', 'double indemnities').
inf('body', 'bodies').
inf('appendix', 'appendixes').
inf('chrestomathy', 'chrestomathies').
inf('diary', 'diaries').
inf('calligraphy', 'calligraphies').
inf('chirography', 'chirographies').
inf('scratch', 'scratches').
inf('cacography', 'cacographies').
inf('chicken scratch', 'chicken scratches').
inf('endorsement in blank', 'endorsements in blank').
inf('codex', 'codexes').
inf('thesis', 'theses').
inf('thanatopsis', 'thanatopses').
inf('authority', 'authorities').
inf('formulary', 'formularies').
inf('bestiary', 'bestiaries').
inf('breviary', 'breviaries').
inf('Book of Psalms', 'Books of Psalms').
inf('book of facts', 'books of facts').
inf('review copy', 'review copies').
inf('hymnary', 'hymnaries').
inf('dictionary', 'dictionaries').
inf('bilingual dictionary', 'bilingual dictionaries').
inf('desk dictionary', 'desk dictionaries').
inf('collegiate dictionary', 'collegiate dictionaries').
inf('etymological dictionary', 'etymological dictionaries').
inf('learner\'s dictionary', 'learner\'s dictionaries').
inf('school dictionary', 'school dictionaries').
inf('pocket dictionary', 'pocket dictionaries').
inf('little dictionary', 'little dictionaries').
inf('unabridged dictionary', 'unabridged dictionaries').
inf('vocabulary', 'vocabularies').
inf('glossary', 'glossaries').
inf('gloss', 'glosses').
inf('consuetudinary', 'consuetudinaries').
inf('book of instructions', 'books of instructions').
inf('itinerary', 'itineraries').
inf('directory', 'directories').
inf('telephone directory', 'telephone directories').
inf('yearly', 'yearlies').
inf('ephemeris', 'ephemerides').
inf('atlas', 'atlantes').
inf('book of maps', 'books of maps').
inf('dialect atlas', 'dialect atlantes').
inf('linguistic atlas', 'linguistic atlantes').
inf('book of knowledge', 'books of knowledge').
inf('paraphrasis', 'paraphrases').
inf('Word of God', 'Words of God').
inf('Genesis', 'Geneses').
inf('Book of Genesis', 'Books of Genesis').
inf('Book of Exodus', 'Books of Exodus').
inf('Book of Leviticus', 'Books of Leviticus').
inf('Book of Numbers', 'Books of Numbers').
inf('Book of Deuteronomy', 'Books of Deuteronomy').
inf('Book of Joshua', 'Books of Joshua').
inf('Book of Judges', 'Books of Judges').
inf('Book of Ruth', 'Books of Ruth').
inf('Book of Ezra', 'Books of Ezra').
inf('Book of Nehemiah', 'Books of Nehemiah').
inf('Book of Esther', 'Books of Esther').
inf('Book of Job', 'Books of Job').
inf('Book of Psalms', 'Books of Psalms').
inf('Book of Proverbs', 'Books of Proverbs').
inf('Book of Ecclesiastes', 'Books of Ecclesiastes').
inf('Song of Songs', 'Songs of Songss').
inf('Song of Solomon', 'Songs of Solomon').
inf('Canticle of Canticles', 'Canticles of Canticless').
inf('Book of Isaiah', 'Books of Isaiah').
inf('Book of Jeremiah', 'Books of Jeremiah').
inf('Book of Lamentations', 'Books of Lamentations').
inf('Book of Ezekiel', 'Books of Ezekiel').
inf('Book of Daniel', 'Books of Daniel').
inf('Book of the Prophet Daniel', 'Books of the Prophet Daniel').
inf('Book of Hosea', 'Books of Hosea').
inf('Book of Joel', 'Books of Joel').
inf('Book of Amos', 'Books of Amos').
inf('Book of Obadiah', 'Books of Obadiah').
inf('Book of Jonah', 'Books of Jonah').
inf('Book of Micah', 'Books of Micah').
inf('Book of Nahum', 'Books of Nahum').
inf('Book of Habakkuk', 'Books of Habakkuk').
inf('Book of Zephaniah', 'Books of Zephaniah').
inf('Book of Haggai', 'Books of Haggai').
inf('Book of Zachariah', 'Books of Zachariah').
inf('Book of Malachi', 'Books of Malachi').
inf('Gospel of Luke', 'Gospels of Luke').
inf('Acts of the Apostles', 'Actss of the Apostles').
inf('Epistle of Paul the Apostle to the Romans', 'Epistles of Paul the Apostle to the Romans').
inf('Epistle to the Romans', 'Epistles to the Romans').
inf('Epistle of Paul the Apostle to the Galatians', 'Epistles of Paul the Apostle to the Galatians').
inf('Epistle to the Galatians', 'Epistles to the Galatians').
inf('Epistle of Paul the Apostle to the Ephesians', 'Epistles of Paul the Apostle to the Ephesians').
inf('Epistle to the Ephesians', 'Epistles to the Ephesians').
inf('Epistle of Paul the Apostle to the Philippians', 'Epistles of Paul the Apostle to the Philippians').
inf('Epistle to the Philippians', 'Epistles to the Philippians').
inf('Epistle of Paul the Apostle to the Colossians', 'Epistles of Paul the Apostle to the Colossians').
inf('Epistle to the Colossians', 'Epistles to the Colossians').
inf('Epistle of Paul the Apostle to Titus', 'Epistles of Paul the Apostle to Titus').
inf('Epistle to Titus', 'Epistles to Titus').
inf('Epistle of Paul the Apostle to Philemon', 'Epistles of Paul the Apostle to Philemon').
inf('Epistle to Philemon', 'Epistles to Philemon').
inf('Epistle to the Hebrews', 'Epistles to the Hebrews').
inf('Epistle of James', 'Epistles of James').
inf('Epistle of Jude', 'Epistles of Jude').
inf('Revelation of Saint John the Divine', 'Revelations of Saint John the Divine').
inf('Book of Revelation', 'Books of Revelation').
inf('Pentateuch', 'Pentateuches').
inf('Tanach', 'Tanaches').
inf('Word of God', 'Words of God').
inf('Book of Mormon', 'Books of Mormon').
inf('Canticle of Simeon', 'Canticles of Simeon').
inf('Book of Common Prayer', 'Books of Common Prayer').
inf('Additions to Esther', 'Additionss to Esther').
inf('Prayer of Azariah and Song of the Three Children', 'Prayers of Azariah and Song of the Three Children').
inf('Book of Susanna', 'Books of Susanna').
inf('Baruch', 'Baruches').
inf('Book of Baruch', 'Books of Baruch').
inf('Letter of Jeremiah', 'Letters of Jeremiah').
inf('Epistle of Jeremiah', 'Epistles of Jeremiah').
inf('Book of Tobit', 'Books of Tobit').
inf('Book of Judith', 'Books of Judith').
inf('Sirach', 'Siraches').
inf('Wisdom of Jesus the Son of Sirach', 'Wisdoms of Jesus the Son of Sirach').
inf('Wisdom of Solomon', 'Wisdoms of Solomon').
inf('summary', 'summaries').
inf('synopsis', 'synopses').
inf('precis', 'preces').
inf('sketch', 'sketches').
inf('articles of incorporation', 'articless of incorporation').
inf('right of first publication', 'rights of first publication').
inf('personality inventory', 'personality inventories').
inf('self-report personality inventory', 'self-report personality inventories').
inf('self-report inventory', 'self-report inventories').
inf('certificate of incorporation', 'certificates of incorporation').
inf('bill of health', 'bills of health').
inf('order of business', 'orders of business').
inf('order of the day', 'orders of the day').
inf('bibliography', 'bibliographies').
inf('bill of entry', 'bills of entry').
inf('bill of goods', 'bills of goods').
inf('discography', 'discographies').
inf('codex', 'codexes').
inf('table of contents', 'tables of contents').
inf('directory', 'directories').
inf('subdirectory', 'subdirectories').
inf('index', 'indexes').
inf('parts inventory', 'parts inventories').
inf('inventory', 'inventories').
inf('bill of fare', 'bills of fare').
inf('necrology', 'necrologies').
inf('visible speech', 'visible speeches').
inf('patent of invention', 'patents of invention').
inf('chronology', 'chronologies').
inf('entry', 'entries').
inf('notebook entry', 'notebook entries').
inf('copy', 'copies').
inf('memo', 'memoes').
inf('registry', 'registries').
inf('Declaration of Independence', 'Declarations of Independence').
inf('history', 'histories').
inf('story', 'stories').
inf('ancient history', 'ancient histories').
inf('etymology', 'etymologies').
inf('folk etymology', 'folk etymologies').
inf('case history', 'case histories').
inf('family history', 'family histories').
inf('medical history', 'medical histories').
inf('anamnesis', 'anamneses').
inf('biography', 'biographies').
inf('life', 'lives').
inf('life story', 'life stories').
inf('life history', 'life histories').
inf('autobiography', 'autobiographies').
inf('hagiography', 'hagiographies').
inf('tally', 'tallies').
inf('pass', 'passes').
inf('bill of lading', 'bills of lading').
inf('contract of adhesion', 'contracts of adhesion').
inf('policy', 'policies').
inf('insurance policy', 'insurance policies').
inf('contract under seal', 'contracts under seal').
inf('conspiracy', 'conspiracies').
inf('confederacy', 'confederacies').
inf('conspiracy of silence', 'conspiracies of silence').
inf('floating policy', 'floating policies').
inf('articles of agreement', 'articless of agreement').
inf('sale in gross', 'sales in gross').
inf('contract of hazard', 'contracts of hazard').
inf('power of attorney', 'powers of attorney').
inf('proxy', 'proxies').
inf('letters of administration', 'letterss of administration').
inf('letters testamentary', 'letters testamentaries').
inf('nullity', 'nullities').
inf('statute of limitations', 'statutes of limitations').
inf('Articles of Confederation', 'Articless of Confederation').
inf('Constitution of the United States', 'Constitutions of the United States').
inf('bill of attainder', 'bills of attainder').
inf('stay of execution', 'stays of execution').
inf('deed of conveyance', 'deeds of conveyance').
inf('bill of sale', 'bills of sale').
inf('deed of trust', 'deeds of trust').
inf('declaration of estimated tax', 'declarations of estimated tax').
inf('letter of marque', 'letters of marque').
inf('letters of marque', 'letterss of marque').
inf('letter of mark and reprisal', 'letters of mark and reprisal').
inf('amnesty', 'amnesties').
inf('writ of certiorari', 'writs of certiorari').
inf('writ of execution', 'writs of execution').
inf('execution of instrument', 'executions of instrument').
inf('habeas corpus', 'habeas corpora').
inf('writ of habeas corpus', 'writs of habeas corpus').
inf('writ of mandamus', 'writs of mandamus').
inf('writ of detinue', 'writs of detinue').
inf('writ of election', 'writs of election').
inf('writ of error', 'writs of error').
inf('writ of prohibition', 'writs of prohibition').
inf('writ of right', 'writs of right').
inf('process', 'processes').
inf('process of monition', 'processes of monition').
inf('bill of Particulars', 'bills of Particulars').
inf('pleading in the alternative', 'pleadings in the alternative').
inf('plea of insanity', 'pleas of insanity').
inf('Linux', 'Linuxes').
inf('binary', 'binaries').
inf('lynx', 'lynges').
inf('patch', 'patches').
inf('part-of-speech tagger', 'parts-of-speech tagger').
inf('utility', 'utilities').
inf('macro', 'macroes').
inf('test copy', 'test copies').
inf('anthology', 'anthologies').
inf('miscellany', 'miscellanies').
inf('semiweekly', 'semiweeklies').
inf('weekly', 'weeklies').
inf('semimonthly', 'semimonthlies').
inf('monthly', 'monthlies').
inf('quarterly', 'quarterlies').
inf('bimonthly', 'bimonthlies').
inf('biweekly', 'biweeklies').
inf('glossy', 'glossies').
inf('bone of contention', 'bones of contention').
inf('parenthesis', 'parentheses').
inf('run-on sentence', 'runs-on sentence').
inf('ambiguity', 'ambiguities').
inf('amphibology', 'amphibologies').
inf('amphiboly', 'amphibolies').
inf('parisology', 'parisologies').
inf('nicety', 'niceties').
inf('subtlety', 'subtleties').
inf('crux', 'cruxes').
inf('crux of the matter', 'cruxes of the matter').
inf('nonsensicality', 'nonsensicalities').
inf('meaninglessness', 'meaninglessnesses').
inf('absurdity', 'absurdities').
inf('absurdness', 'absurdnesses').
inf('ridiculousness', 'ridiculousnesses').
inf('amphigory', 'amphigories').
inf('balderdash', 'balderdashes').
inf('gibberish', 'gibberishes').
inf('incoherency', 'incoherencies').
inf('unintelligibility', 'unintelligibilities').
inf('jabberwocky', 'jabberwockies').
inf('mummery', 'mummeries').
inf('flummery', 'flummeries').
inf('double Dutch', 'double Dutches').
inf('bill of goods', 'bills of goods').
inf('bosh', 'boshes').
inf('tosh', 'toshes').
inf('hogwash', 'hogwashes').
inf('rubbish', 'rubbishes').
inf('trumpery', 'trumperies').
inf('trash', 'trashes').
inf('wish-wash', 'wish-washes').
inf('mumbo jumbo', 'mumbo jumboes').
inf('mimesis', 'mimeses').
inf('documentary', 'documentaries').
inf('tetralogy', 'tetralogies').
inf('special delivery', 'special deliveries').
inf('billet doux', 'billet douxes').
inf('letter of intent', 'letters of intent').
inf('word of farewell', 'words of farewell').
inf('adieu', 'adieu').
inf('good-by', 'good-bies').
inf('goodby', 'goodbies').
inf('wish', 'wishes').
inf('pax', 'paxes').
inf('kiss of peace', 'kisses of peace').
inf('hospitality', 'hospitalities').
inf('inhospitality', 'inhospitalities').
inf('hello', 'helloes').
inf('hullo', 'hulloes').
inf('howdy', 'howdies').
inf('how-do-you-do', 'how-do-you-does').
inf('apology', 'apologies').
inf('info', 'infoes').
inf('rehash', 'rehashes').
inf('the skinny', 'the skinnies').
inf('machine readable dictionary', 'machine readable dictionaries').
inf('electronic dictionary', 'electronic dictionaries').
inf('index', 'indexes').
inf('body mass index', 'body mass indexes').
inf('business index', 'business indexes').
inf('Standard and Poor\'s Index', 'Standard and Poor\'s Indexes').
inf('price index', 'price indexes').
inf('retail price index', 'retail price indexes').
inf('producer price index', 'producer price indexes').
inf('wholesale price index', 'wholesale price indexes').
inf('consumer price index', 'consumer price indexes').
inf('cost-of-living index', 'costs-of-living index').
inf('stock index', 'stock indexes').
inf('stock market index', 'stock market indexes').
inf('testimony', 'testimonies').
inf('good authority', 'good authorities').
inf('pro', 'proes').
inf('rules of order', 'ruless of order').
inf('rule of evidence', 'rules of evidence').
inf('fruit of the poisonous tree', 'fruit of the poisonous tree').
inf('policy', 'policies').
inf('economic policy', 'economic policies').
inf('fiscal policy', 'fiscal policies').
inf('social policy', 'social policies').
inf('embargo', 'embargoes').
inf('trade embargo', 'trade embargoes').
inf('foreign policy', 'foreign policies').
inf('manifest destiny', 'manifest destinies').
inf('trade policy', 'trade policies').
inf('national trading policy', 'national trading policies').
inf('open-door policy', 'open-door policies').
inf('zero-tolerance policy', 'zero-tolerance policies').
inf('closure by compartment', 'closures by compartment').
inf('point of order', 'points of order').
inf('Bushido', 'Bushidoes').
inf('word of advice', 'words of advice').
inf('copy', 'copies').
inf('course of study', 'courses of study').
inf('printing process', 'printing processes').
inf('typography', 'typographies').
inf('letterpress', 'letterpresses').
inf('planography', 'planographies').
inf('photogelatin process', 'photogelatin processes').
inf('lithography', 'lithographies').
inf('photolithography', 'photolithographies').
inf('chromolithography', 'chromolithographies').
inf('offset lithography', 'offset lithographies').
inf('carbon process', 'carbon processes').
inf('story', 'stories').
inf('newsflash', 'newsflashes').
inf('flash', 'flashes').
inf('dispatch', 'dispatches').
inf('despatch', 'despatches').
inf('stop press', 'stop presses').
inf('warranty', 'warranties').
inf('security', 'securities').
inf('surety', 'sureties').
inf('pass', 'passes').
inf('pass', 'passes').
inf('boarding pass', 'boarding passes').
inf('hall pass', 'hall passes').
inf('ticket-of-leave', 'tickets-of-leave').
inf('pass', 'passes').
inf('bravo', 'bravoes').
inf('salvo', 'salvoes').
inf('eulogy', 'eulogies').
inf('eulogy', 'eulogies').
inf('flattery', 'flatteries').
inf('cajolery', 'cajoleries').
inf('puffery', 'pufferies').
inf('fulsomeness', 'fulsomenesses').
inf('Associate in Arts', 'Associates in Arts').
inf('Associate in Applied Science', 'Associates in Applied Science').
inf('Associate in Nursing', 'Associates in Nursing').
inf('Bachelor of Arts', 'Bachelors of Arts').
inf('Bachelor of Arts in Library Science', 'Bachelors of Arts in Library Science').
inf('Bachelor of Arts in Nursing', 'Bachelors of Arts in Nursing').
inf('Bachelor of Divinity', 'Bachelors of Divinity').
inf('Bachelor of Literature', 'Bachelors of Literature').
inf('Bachelor of Medicine', 'Bachelors of Medicine').
inf('Bachelor of Music', 'Bachelors of Music').
inf('Bachelor of Naval Science', 'Bachelors of Naval Science').
inf('Bachelor of Science', 'Bachelors of Science').
inf('Bachelor of Science in Architecture', 'Bachelors of Science in Architecture').
inf('BSArch', 'BSArches').
inf('Bachelor of Science in Engineering', 'Bachelors of Science in Engineering').
inf('Bachelor of Theology', 'Bachelors of Theology').
inf('Master of Architecture', 'Masters of Architecture').
inf('MArch', 'MArches').
inf('Master of Arts', 'Masters of Arts').
inf('Master of Arts in Library Science', 'Masters of Arts in Library Science').
inf('Master of Arts in Teaching', 'Masters of Arts in Teaching').
inf('Master in Business', 'Masters in Business').
inf('Master in Business Administration', 'Masters in Business Administration').
inf('Master of Divinity', 'Masters of Divinity').
inf('Master of Education', 'Masters of Education').
inf('Master of Fine Arts', 'Masters of Fine Arts').
inf('Master of Literature', 'Masters of Literature').
inf('Master of Library Science', 'Masters of Library Science').
inf('Master in Public Affairs', 'Masters in Public Affairs').
inf('Master of Science', 'Masters of Science').
inf('Master of Science in Engineering', 'Masters of Science in Engineering').
inf('Master of Theology', 'Masters of Theology').
inf('Doctor of Dental Medicine', 'Doctors of Dental Medicine').
inf('Doctor of Dental Surgery', 'Doctors of Dental Surgery').
inf('Doctor of Divinity', 'Doctors of Divinity').
inf('Doctor of Education', 'Doctors of Education').
inf('Doctor of Medicine', 'Doctors of Medicine').
inf('Doctor of Music', 'Doctors of Music').
inf('Doctor of Musical Arts', 'Doctors of Musical Arts').
inf('Doctor of Optometry', 'Doctors of Optometry').
inf('Doctor of Osteopathy', 'Doctors of Osteopathy').
inf('Doctor of Arts', 'Doctors of Arts').
inf('Doctor of Philosophy', 'Doctors of Philosophy').
inf('Doctor of Public Health', 'Doctors of Public Health').
inf('Doctor of Theology', 'Doctors of Theology').
inf('Doctor of Sacred Theology', 'Doctors of Sacred Theology').
inf('Bachelor of Laws', 'Bachelors of Laws').
inf('Master of Laws', 'Masters of Laws').
inf('Doctor of Arts', 'Doctors of Arts').
inf('Doctor of Fine Arts', 'Doctors of Fine Arts').
inf('Doctor of Humane Letters', 'Doctors of Humane Letters').
inf('Doctor of Humanities', 'Doctors of Humanities').
inf('Doctor of Laws', 'Doctors of Laws').
inf('Doctor of Science', 'Doctors of Science').
inf('seal of approval', 'seals of approval').
inf('Medal of Honor', 'Medals of Honor').
inf('Distinguished Service Cross', 'Distinguished Service Crosses').
inf('Navy Cross', 'Navy Crosses').
inf('Distinguished Flying Cross', 'Distinguished Flying Crosses').
inf('Order of the Purple Heart', 'Orders of the Purple Heart').
inf('Victoria Cross', 'Victoria Crosses').
inf('trophy', 'trophies').
inf('reproach', 'reproaches').
inf('self-reproach', 'self-reproaches').
inf('speech', 'speeches').
inf('talking to', 'talking toes').
inf('courtesy', 'courtesies').
inf('discourtesy', 'discourtesies').
inf('contumely', 'contumelies').
inf('mockery', 'mockeries').
inf('squelch', 'squelches').
inf('calumny', 'calumnies').
inf('blackwash', 'blackwashes').
inf('scurrility', 'scurrilities').
inf('sass', 'sasses').
inf('antinomy', 'antinomies').
inf('paradox', 'paradoxes').
inf('manifesto', 'manifestoes').
inf('pronunciamento', 'pronunciamentoes').
inf('Communist Manifesto', 'Communist Manifestoes').
inf('say-so', 'say-soes').
inf('Bill of Rights', 'Bills of Rights').
inf('cause of action', 'causes of action').
inf('testimony', 'testimonies').
inf('testimony', 'testimonies').
inf('witness', 'witnesses').
inf('apology', 'apologies').
inf('gloss', 'glosses').
inf('reply', 'replies').
inf('advisory', 'advisories').
inf('obituary', 'obituaries').
inf('necrology', 'necrologies').
inf('extropy', 'extropies').
inf('meteorology', 'meteorologies').
inf('prognosis', 'prognoses').
inf('prophecy', 'prophecies').
inf('premiss', 'premisses').
inf('major premiss', 'major premisses').
inf('minor premiss', 'minor premisses').
inf('thesis', 'theses').
inf('proviso', 'provisoes').
inf('falsity', 'falsities').
inf('story', 'stories').
inf('fairy story', 'fairy stories').
inf('cock-and-bull story', 'cock-and-bull stories').
inf('hypocrisy', 'hypocrisies').
inf('trickery', 'trickeries').
inf('slickness', 'slicknesses').
inf('hanky panky', 'hanky pankies').
inf('jiggery-pokery', 'jiggery-pokeries').
inf('skulduggery', 'skulduggeries').
inf('skullduggery', 'skullduggeries').
inf('duplicity', 'duplicities').
inf('quiddity', 'quiddities').
inf('commentary', 'commentaries').
inf('Midrash', 'Midrashes').
inf('cross-index', 'cross-indexes').
inf('sally', 'sallies').
inf('typo', 'typoes').
inf('slip of the tongue', 'slips of the tongue').
inf('treaty', 'treaties').
inf('commercial treaty', 'commercial treaties').
inf('peace treaty', 'peace treaties').
inf('Peace of Westphalia', 'Peaces of Westphalia').
inf('SALT I', 'SALT we').
inf('Treaty of Versailles', 'Treatys of Versailles').
inf('mush', 'mushes').
inf('wittiness', 'wittinesses').
inf('pungency', 'pungencies').
inf('irony', 'ironies').
inf('raillery', 'railleries').
inf('dirty story', 'dirty stories').
inf('blue story', 'blue stories').
inf('funny story', 'funny stories').
inf('good story', 'good stories').
inf('funny', 'funnies').
inf('shaggy dog story', 'shaggy dog stories').
inf('parody', 'parodies').
inf('mockery', 'mockeries').
inf('travesty', 'travesties').
inf('sketch', 'sketches').
inf('jocosity', 'jocosities').
inf('jocularity', 'jocularities').
inf('waggery', 'waggeries').
inf('waggishness', 'waggishnesses').
inf('drollery', 'drolleries').
inf('comedy', 'comedies').
inf('funniness', 'funninesses').
inf('ribaldry', 'ribaldries').
inf('guess', 'guesses').
inf('hypothesis', 'hypotheses').
inf('question of fact', 'questions of fact').
inf('matter of fact', 'matters of fact').
inf('question of law', 'questions of law').
inf('matter of law', 'matters of law').
inf('address', 'addresses').
inf('name and address', 'name and addresses').
inf('return address', 'return addresses').
inf('toponymy', 'toponymies').
inf('toponomy', 'toponomies').
inf('address', 'addresses').
inf('cloven foot', 'cloven feet').
inf('dash', 'dashes').
inf('horn of plenty', 'horns of plenty').
inf('system of numeration', 'systems of numeration').
inf('index', 'indexes').
inf('obbligato', 'obbligatoes').
inf('obligato', 'obligatoes').
inf('choreography', 'choreographies').
inf('ditto', 'dittoes').
inf('circumflex', 'circumflexes').
inf('dieresis', 'diereses').
inf('diaeresis', 'diaereses').
inf('type family', 'type families').
inf('roman', 'romen').
inf('diesis', 'dieses').
inf('letter of the alphabet', 'letters of the alphabet').
inf('a', 'some').
inf('I', 'we').
inf('o', 'oes').
inf('x', 'xes').
inf('ex', 'exes').
inf('y', 'ies').
inf('rho', 'rhoes').
inf('he', 'they').
inf('resh', 'reshes').
inf('dash', 'dashes').
inf('parenthesis', 'parentheses').
inf('slash', 'slashes').
inf('separatrix', 'separatrices').
inf('swung dash', 'swung dashes').
inf('phylactery', 'phylacteries').
inf('flourish', 'flourishes').
inf('glissando', 'glissandoes').
inf('tonality', 'tonalities').
inf('atonality', 'atonalities').
inf('scale of C major', 'scales of C major').
inf('do', 'does').
inf('so', 'soes').
inf('segno', 'segnoes').
inf('sforzando', 'sforzandoes').
inf('sforzando', 'sforzandoes').
inf('bass', 'basses').
inf('basso', 'bassoes').
inf('basso profundo', 'basso profundoes').
inf('alto', 'altoes').
inf('contralto', 'contraltoes').
inf('alto', 'altoes').
inf('mezzo-soprano', 'mezzo-sopranoes').
inf('mezzo', 'mezzoes').
inf('soprano', 'sopranoes').
inf('flash', 'flashes').
inf('demo', 'demoes').
inf('gaudery', 'gauderies').
inf('allegory', 'allegories').
inf('Star of David', 'Stars of David').
inf('Shield of David', 'Shields of David').
inf('maple-leaf', 'maple-leaves').
inf('insignia of rank', 'insignias of rank').
inf('shoulder flash', 'shoulder flashes').
inf('flash', 'flashes').
inf('bravado', 'bravadoes').
inf('pedantry', 'pedantries').
inf('flourish', 'flourishes').
inf('brandish', 'brandishes').
inf('flourish', 'flourishes').
inf('flourish', 'flourishes').
inf('flourish', 'flourishes').
inf('Antido', 'Antidoes').
inf('Arulo', 'Aruloes').
inf('Basic English', 'Basic Englishes').
inf('Esperantido', 'Esperantidoes').
inf('Esperanto', 'Esperantoes').
inf('Ido', 'Idoes').
inf('Latino', 'Latinoes').
inf('Nov-Esperanto', 'Nov-Esperantoes').
inf('Ro', 'Roes').
inf('Sango', 'Sangoes').
inf('Arapaho', 'Arapahoes').
inf('Blackfoot', 'Blackfeet').
inf('Oto', 'Otoes').
inf('Fox', 'Foxes').
inf('Illinois', 'Illinois').
inf('Ofo', 'Ofoes').
inf('Pamlico', 'Pamlicoes').
inf('Tutelo', 'Tuteloes').
inf('Winnebago', 'Winnebagoes').
inf('Salish', 'Salishes').
inf('Caddo', 'Caddoes').
inf('Iroquois', 'Iroquois').
inf('Maraco', 'Maracoes').
inf('Eskimo', 'Eskimoes').
inf('Navaho', 'Navahoes').
inf('Navajo', 'Navajoes').
inf('Chimariko', 'Chimarikoes').
inf('Pomo', 'Pomoes').
inf('Yuman', 'Yumen').
inf('Diegueno', 'Dieguenoes').
inf('Takilman', 'Takilmen').
inf('Yucateco', 'Yucatecoes').
inf('Turkish', 'Turkishes').
inf('Turkoman', 'Turkomen').
inf('Turcoman', 'Turcomen').
inf('Chuvash', 'Chuvashes').
inf('Japanese', 'Japanese').
inf('Chinese', 'Chinese').
inf('Mandarin Chinese', 'Mandarin Chinese').
inf('Cantonese', 'Cantonese').
inf('Fukkianese', 'Fukkianese').
inf('Hokkianese', 'Hokkianese').
inf('Taiwanese', 'Taiwanese').
inf('Tibeto-Burman', 'Tibeto-Burmen').
inf('Himalayish', 'Himalayishes').
inf('Lolo-Burmese', 'Lolo-Burmese').
inf('Burmese', 'Burmese').
inf('Loloish', 'Loloishes').
inf('Lolo', 'Loloes').
inf('Jinghpo', 'Jinghpoes').
inf('Chingpo', 'Chingpoes').
inf('Bodo-Garo', 'Bodo-Garoes').
inf('Barish', 'Barishes').
inf('Mirish', 'Mirishes').
inf('Tho', 'Thoes').
inf('Siamese', 'Siamese').
inf('Vietnamese', 'Vietnamese').
inf('Annamese', 'Annamese').
inf('Javanese', 'Javanese').
inf('Sundanese', 'Sundanese').
inf('Balinese', 'Balinese').
inf('Filipino', 'Filipinoes').
inf('Cebuano', 'Cebuanoes').
inf('Polish', 'Polishes').
inf('Czech', 'Czeches').
inf('Lettish', 'Lettishes').
inf('English', 'Englishes').
inf('American English', 'American Englishes').
inf('African American Vernacular English', 'African American Vernacular Englishes').
inf('African American English', 'African American Englishes').
inf('Black English', 'Black Englishes').
inf('Black Vernacular English', 'Black Vernacular Englishes').
inf('King\'s English', 'King\'s Englishes').
inf('Queen\'s English', 'Queen\'s Englishes').
inf('Middle English', 'Middle Englishes').
inf('Kentish', 'Kentishes').
inf('Modern English', 'Modern Englishes').
inf('Old English', 'Old Englishes').
inf('Kentish', 'Kentishes').
inf('Jutish', 'Jutishes').
inf('Oxford English', 'Oxford Englishes').
inf('Scottish', 'Scottishes').
inf('Scots English', 'Scots Englishes').
inf('German', 'Germen').
inf('High German', 'High Germen').
inf('Old High German', 'Old High Germen').
inf('Middle High German', 'Middle High Germen').
inf('Yiddish', 'Yiddishes').
inf('Pennsylvania Dutch', 'Pennsylvania Dutches').
inf('Low German', 'Low Germen').
inf('Plattdeutsch', 'Plattdeutsches').
inf('Middle Low German', 'Middle Low Germen').
inf('Dutch', 'Dutches').
inf('Flemish', 'Flemishes').
inf('South African Dutch', 'South African Dutches').
inf('Danish', 'Danishes').
inf('Swedish', 'Swedishes').
inf('Faroese', 'Faroese').
inf('Faeroese', 'Faeroese').
inf('Cheremiss', 'Cheremisses').
inf('Finnish', 'Finnishes').
inf('Lappish', 'Lappishes').
inf('Irish', 'Irishes').
inf('Old Irish', 'Old Irishes').
inf('Middle Irish', 'Middle Irishes').
inf('Manx', 'Manges').
inf('Welsh', 'Welshes').
inf('Cornish', 'Cornishes').
inf('French', 'Frenches').
inf('Langue d\'oil French', 'Langue d\'oil Frenches').
inf('Langue d\'oc French', 'Langue d\'oc Frenches').
inf('Old French', 'Old Frenches').
inf('Norman-French', 'Norman-Frenches').
inf('Norman French', 'Norman Frenches').
inf('Old North French', 'Old North Frenches').
inf('Anglo-French', 'Anglo-Frenches').
inf('Anglo-Norman', 'Anglo-Normen').
inf('Canadian French', 'Canadian Frenches').
inf('Portuguese', 'Portuguese').
inf('Spanish', 'Spanishes').
inf('Judeo-Spanish', 'Judeo-Spanishes').
inf('Ladino', 'Ladinoes').
inf('Mexican Spanish', 'Mexican Spanishes').
inf('Romansh', 'Romanshes').
inf('Rumansh', 'Rumanshes').
inf('Assamese', 'Assamese').
inf('Sinhalese', 'Sinhalese').
inf('Singhalese', 'Singhalese').
inf('Kurdish', 'Kurdishes').
inf('Pashto', 'Pashtoes').
inf('Paxto', 'Paxtoes').
inf('Kanarese', 'Kanarese').
inf('Pengo', 'Pengoes').
inf('Kurux', 'Kuruxes').
inf('Malto', 'Maltoes').
inf('Daffo', 'Daffoes').
inf('Kotoko', 'Kotokoes').
inf('Sokoro', 'Sokoroes').
inf('Maltese', 'Maltese').
inf('Niger-Congo', 'Niger-Congoes').
inf('Herero', 'Hereroes').
inf('Kongo', 'Kongoes').
inf('Pokomo', 'Pokomoes').
inf('Sotho', 'Sothoes').
inf('Sesotho', 'Sesothoes').
inf('Basuto', 'Basutoes').
inf('dramaturgy', 'dramaturgies').
inf('snatch', 'snatches').
inf('theater of the absurd', 'theaters of the absurd').
inf('prompt copy', 'prompt copies').
inf('continuity', 'continuities').
inf('speech', 'speeches').
inf('soliloquy', 'soliloquies').
inf('libretto', 'librettoes').
inf('line of gab', 'lines of gab').
inf('string of words', 'strings of words').
inf('gush', 'gushes').
inf('cry', 'cries').
inf('comedy', 'comedies').
inf('black comedy', 'black comedies').
inf('dark comedy', 'dark comedies').
inf('farce comedy', 'farce comedies').
inf('travesty', 'travesties').
inf('high comedy', 'high comedies').
inf('low comedy', 'low comedies').
inf('seriocomedy', 'seriocomedies').
inf('tragicomedy', 'tragicomedies').
inf('tragedy', 'tragedies').
inf('tragicomedy', 'tragicomedies').
inf('situation comedy', 'situation comedies').
inf('situation comedy', 'situation comedies').
inf('musical comedy', 'musical comedies').
inf('variety', 'varieties').
inf('choreography', 'choreographies').
inf('pizzicato', 'pizzicatoes').
inf('monophony', 'monophonies').
inf('monody', 'monodies').
inf('polyphony', 'polyphonies').
inf('polytonality', 'polytonalities').
inf('harmony', 'harmonies').
inf('musical harmony', 'musical harmonies').
inf('four-part harmony', 'four-part harmonies').
inf('melody', 'melodies').
inf('obbligato', 'obbligatoes').
inf('obligato', 'obligatoes').
inf('homophony', 'homophonies').
inf('primo', 'primoes').
inf('secondo', 'secondoes').
inf('canto', 'cantoes').
inf('bass', 'basses').
inf('ground bass', 'ground basses').
inf('figured bass', 'figured basses').
inf('thorough bass', 'thorough basses').
inf('antiphony', 'antiphonies').
inf('Mass', 'Masses').
inf('Mass', 'Masses').
inf('antiphonary', 'antiphonaries').
inf('doxology', 'doxologies').
inf('opus', 'opera').
inf('piece of music', 'pieces of music').
inf('intermezzo', 'intermezzoes').
inf('allegro', 'allegroes').
inf('allegretto', 'allegrettoes').
inf('intermezzo', 'intermezzoes').
inf('solo', 'soloes').
inf('voluntary', 'voluntaries').
inf('divertimento', 'divertimentoes').
inf('concerto', 'concertoes').
inf('concerto grosso', 'concerto grossoes').
inf('rondo', 'rondoes').
inf('rondeau', 'rondeaux').
inf('symphony', 'symphonies').
inf('intro', 'introes').
inf('ostinato', 'ostinatoes').
inf('largo', 'largoes').
inf('larghetto', 'larghettoes').
inf('scherzo', 'scherzoes').
inf('notturno', 'notturnoes').
inf('study', 'studies').
inf('antiphony', 'antiphonies').
inf('minstrelsy', 'minstrelsies').
inf('chanty', 'chanties').
inf('shanty', 'shanties').
inf('ditty', 'ditties').
inf('coronach', 'coronaches').
inf('threnody', 'threnodies').
inf('fado', 'fadoes').
inf('lullaby', 'lullabies').
inf('bolero', 'boleroes').
inf('flamenco', 'flamencoes').
inf('tango', 'tangoes').
inf('techno', 'technoes').
inf('march', 'marches').
inf('military march', 'military marches').
inf('pibroch', 'pibroches').
inf('processional march', 'processional marches').
inf('recessional march', 'recessional marches').
inf('funeral march', 'funeral marches').
inf('dead march', 'dead marches').
inf('wedding march', 'wedding marches').
inf('disco', 'discoes').
inf('bluegrass', 'bluegrasses').
inf('zydeco', 'zydecoes').
inf('rockabilly', 'rockabillies').
inf('address', 'addresses').
inf('catch', 'catches').
inf('analysis', 'analyses').
inf('fluency', 'fluencies').
inf('smoothness', 'smoothnesses').
inf('flatness', 'flatnesses').
inf('grandiosity', 'grandiosities').
inf('ornateness', 'ornatenesses').
inf('manner of speaking', 'manners of speaking').
inf('speech', 'speeches').
inf('delivery', 'deliveries').
inf('saltiness', 'saltinesses').
inf('coarseness', 'coarsenesses').
inf('phraseology', 'phraseologies').
inf('choice of words', 'choices of words').
inf('tone of voice', 'tones of voice').
inf('roundness', 'roundnesses').
inf('rotundity', 'rotundities').
inf('prosody', 'prosodies').
inf('stress', 'stresses').
inf('emphasis', 'emphases').
inf('word stress', 'word stresses').
inf('sentence stress', 'sentence stresses').
inf('arioso', 'ariosoes').
inf('sesquipedality', 'sesquipedalities').
inf('luridness', 'luridnesses').
inf('terseness', 'tersenesses').
inf('turn of phrase', 'turns of phrase').
inf('turn of expression', 'turns of expression').
inf('conciseness', 'concisenesses').
inf('pithiness', 'pithinesses').
inf('succinctness', 'succinctnesses').
inf('crispness', 'crispnesses').
inf('brevity', 'brevities').
inf('verboseness', 'verbosenesses').
inf('verbosity', 'verbosities').
inf('prolixity', 'prolixities').
inf('prolixness', 'prolixnesses').
inf('windiness', 'windinesses').
inf('long-windedness', 'long-windednesses').
inf('wordiness', 'wordinesses').
inf('periphrasis', 'periphrases').
inf('turgidity', 'turgidities').
inf('turgidness', 'turgidnesses').
inf('repetitiveness', 'repetitivenesses').
inf('repetitiousness', 'repetitiousnesses').
inf('redundancy', 'redundancies').
inf('tautology', 'tautologies').
inf('tautology', 'tautologies').
inf('poetry', 'poetries').
inf('poesy', 'poesies').
inf('heroic poetry', 'heroic poetries').
inf('epic poetry', 'epic poetries').
inf('poetry', 'poetries').
inf('prosody', 'prosodies').
inf('catalexis', 'catalexes').
inf('metrical foot', 'metrical feet').
inf('foot', 'feet').
inf('amphibrach', 'amphibraches').
inf('dibrach', 'dibraches').
inf('anadiplosis', 'anadiploses').
inf('epanalepsis', 'epanalepses').
inf('antiphrasis', 'antiphrases').
inf('antithesis', 'antitheses').
inf('apophasis', 'apophases').
inf('aposiopesis', 'aposiopeses').
inf('catachresis', 'catachreses').
inf('climax', 'climaxes').
inf('dramatic irony', 'dramatic ironies').
inf('ecphonesis', 'ecphoneses').
inf('emphasis', 'emphases').
inf('epanorthosis', 'epanorthoses').
inf('epiplexis', 'epiplexes').
inf('hypozeuxis', 'hypozeuxes').
inf('meiosis', 'meioses').
inf('paralepsis', 'paralepses').
inf('paraleipsis', 'paraleipses').
inf('paralipsis', 'paralipses').
inf('prolepsis', 'prolepses').
inf('figure of speech', 'figures of speech').
inf('irony', 'ironies').
inf('metonymy', 'metonymies').
inf('metalepsis', 'metalepses').
inf('syllepsis', 'syllepses').
inf('speech', 'speeches').
inf('speech', 'speeches').
inf('vox', 'voxes').
inf('syllabicity', 'syllabicities').
inf('glottal catch', 'glottal catches').
inf('epenthesis', 'epentheses').
inf('cry', 'cries').
inf('outcry', 'outcries').
inf('cry', 'cries').
inf('hollo', 'holloes').
inf('hue and cry', 'hue and cries').
inf('screech', 'screeches').
inf('war cry', 'war cries').
inf('rallying cry', 'rallying cries').
inf('battle cry', 'battle cries').
inf('hiss', 'hisses').
inf('raspberry', 'raspberries').
inf('blasphemy', 'blasphemies').
inf('obscenity', 'obscenities').
inf('bawdry', 'bawdries').
inf('bawdy', 'bawdies').
inf('scatology', 'scatologies').
inf('cuss', 'cusses').
inf('profanity', 'profanities').
inf('orthoepy', 'orthoepies').
inf('speech', 'speeches').
inf('voicelessness', 'voicelessnesses').
inf('homophony', 'homophonies').
inf('thickness', 'thicknesses').
inf('phatic speech', 'phatic speeches').
inf('heart-to-heart', 'hearts-to-hearts').
inf('malarky', 'malarkies').
inf('nothingness', 'nothingnesses').
inf('logomachy', 'logomachies').
inf('second-hand speech', 'second-hand speeches').
inf('diplomacy', 'diplomacies').
inf('dollar diplomacy', 'dollar diplomacies').
inf('gunboat diplomacy', 'gunboat diplomacies').
inf('shuttle diplomacy', 'shuttle diplomacies').
inf('motto', 'mottoes').
inf('war cry', 'war cries').
inf('rallying cry', 'rallying cries').
inf('battle cry', 'battle cries').
inf('cry', 'cries').
inf('banality', 'banalities').
inf('non-standard speech', 'non-standard speeches').
inf('patois', 'patois').
inf('lingo', 'lingoes').
inf('patois', 'patois').
inf('hex', 'hexes').
inf('jinx', 'jinges').
inf('whammy', 'whammies').
inf('soliloquy', 'soliloquies').
inf('proposal of marriage', 'proposals of marriage').
inf('hypothesis', 'hypotheses').
inf('touch', 'touches').
inf('approach', 'approaches').
inf('olive branch', 'olive branches').
inf('entry', 'entries').
inf('eisegesis', 'eisegeses').
inf('exegesis', 'exegeses').
inf('embroidery', 'embroideries').
inf('pass', 'passes').
inf('concurrency', 'concurrencies').
inf('conformity', 'conformities').
inf('out-of-court settlement', 'outs-of-court settlement').
inf('harmony', 'harmonies').
inf('nonconformity', 'nonconformities').
inf('difference of opinion', 'differences of opinion').
inf('straw man', 'straw men').
inf('strawman', 'strawmen').
inf('argy-bargy', 'argy-bargies').
inf('controversy', 'controversies').
inf('pettifoggery', 'pettifoggeries').
inf('fuss', 'fusses').
inf('wish', 'wishes').
inf('entreaty', 'entreaties').
inf('demagoguery', 'demagogueries').
inf('demagogy', 'demagogies').
inf('beggary', 'beggaries').
inf('mendicancy', 'mendicancies').
inf('touch', 'touches').
inf('importunity', 'importunities').
inf('urgency', 'urgencies').
inf('inquiry', 'inquiries').
inf('enquiry', 'enquiries').
inf('query', 'queries').
inf('interrogatory', 'interrogatories').
inf('reply', 'replies').
inf('echo', 'echoes').
inf('sketch', 'sketches').
inf('say-so', 'say-soes').
inf('no', 'noes').
inf('contradiction in terms', 'contradictions in terms').
inf('beef', 'beeves').
inf('bitch', 'bitches').
inf('kvetch', 'kvetches').
inf('discovery', 'discoveries').
inf('discovery', 'discoveries').
inf('intro', 'introes').
inf('study', 'studies').
inf('skinny', 'skinnies').
inf('case study', 'case studies').
inf('story', 'stories').
inf('sob story', 'sob stories').
inf('fairy story', 'fairy stories').
inf('word of mouth', 'words of mouth').
inf('talk of the town', 'talks of the town').
inf('warning of attack', 'warnings of attack').
inf('warning of war', 'warnings of war').
inf('word of honor', 'words of honor').
inf('thank you', 'thank you').
inf('acrophony', 'acrophonies').
inf('calling into question', 'callings into question').
inf('demand for explanation', 'demands for explanation').
inf('demand for identification', 'demands for identification').
inf('bill of indictment', 'bills of indictment').
inf('innuendo', 'innuendoes').
inf('address', 'addresses').
inf('speech', 'speeches').
inf('Gettysburg Address', 'Gettysburg Addresses').
inf('inaugural address', 'inaugural addresses').
inf('keynote speech', 'keynote speeches').
inf('keynote address', 'keynote addresses').
inf('litany', 'litanies').
inf('nominating speech', 'nominating speeches').
inf('nominating address', 'nominating addresses').
inf('oratory', 'oratories').
inf('epideictic oratory', 'epideictic oratories').
inf('stump speech', 'stump speeches').
inf('salutatory address', 'salutatory addresses').
inf('salutatory', 'salutatories').
inf('valedictory address', 'valedictory addresses').
inf('valedictory', 'valedictories').
inf('Sermon on the Mount', 'Sermons on the Mount').
inf('homily', 'homilies').
inf('artillery', 'artilleries').
inf('publicity', 'publicities').
inf('sales pitch', 'sales pitches').
inf('pitch', 'pitches').
inf('subornation of perjury', 'subornations of perjury').
inf('vote of confidence', 'votes of confidence').
inf('telepathy', 'telepathies').
inf('telegnosis', 'telegnoses').
inf('parapsychology', 'parapsychologies').
inf('telekinesis', 'telekineses').
inf('psychokinesis', 'psychokineses').
inf('point of reference', 'points of reference').
inf('rule of grammar', 'rules of grammar').
inf('rule of morphology', 'rules of morphology').
inf('radar echo', 'radar echoes').
inf('unknown quantity', 'unknown quantities').
inf('logo', 'logoes').
inf('sign of the cross', 'signs of the cross').
inf('curtsy', 'curtsies').
inf('Bach', 'Baches').
inf('touch', 'touches').
inf('augury', 'auguries').
inf('prodigy', 'prodigies').
inf('flash', 'flashes').
inf('loss', 'losses').
inf('out-of-body experience', 'outs-of-body experience').
inf('eventuality', 'eventualities').
inf('contingency', 'contingencies').
inf('finish', 'finishes').
inf('homestretch', 'homestretches').
inf('corollary', 'corollaries').
inf('matter of course', 'matters of course').
inf('peripety', 'peripeties').
inf('fortuity', 'fortuities').
inf('near miss', 'near misses').
inf('crash', 'crashes').
inf('inferno', 'infernoes').
inf('pity', 'pities').
inf('calvary', 'calvaries').
inf('skeleton in the closet', 'skeletons in the closet').
inf('skeleton in the cupboard', 'skeletons in the cupboard').
inf('approach', 'approaches').
inf('change of location', 'changes of location').
inf('Ascension of Christ', 'Ascensions of Christ').
inf('Resurrection of Christ', 'Resurrections of Christ').
inf('injury', 'injuries').
inf('accidental injury', 'accidental injuries').
inf('breach', 'breaches').
inf('calamity', 'calamities').
inf('tragedy', 'tragedies').
inf('act of God', 'acts of God').
inf('unavoidable casualty', 'unavoidable casualties').
inf('kiss of death', 'kisses of death').
inf('lottery', 'lotteries').
inf('smash', 'smashes').
inf('success', 'successes').
inf('miss', 'misses').
inf('egress', 'egresses').
inf('nativity', 'nativities').
inf('nascency', 'nascencies').
inf('delivery', 'deliveries').
inf('cycle of rebirth', 'cycles of rebirth').
inf('egress', 'egresses').
inf('ingress', 'ingresses').
inf('epiphany', 'epiphanies').
inf('theophany', 'theophanies').
inf('Word of God', 'Words of God').
inf('genesis', 'geneses').
inf('preliminary', 'preliminaries').
inf('etiology', 'etiologies').
inf('aetiology', 'aetiologies').
inf('unknown quantity', 'unknown quantities').
inf('destiny', 'destinies').
inf('fatality', 'fatalities').
inf('finish', 'finishes').
inf('loss', 'losses').
inf('day of reckoning', 'days of reckoning').
inf('end of the world', 'ends of the world').
inf('adversity', 'adversities').
inf('vagary', 'vagaries').
inf('mesh', 'meshes').
inf('equipment casualty', 'equipment casualties').
inf('combat casualty', 'combat casualties').
inf('operational casualty', 'operational casualties').
inf('casualty', 'casualties').
inf('injury', 'injuries').
inf('combat injury', 'combat injuries').
inf('personnel casualty', 'personnel casualties').
inf('loss', 'losses').
inf('pass', 'passes').
inf('rematch', 'rematches').
inf('backwash', 'backwashes').
inf('swash', 'swashes').
inf('backlash', 'backlashes').
inf('resiliency', 'resiliencies').
inf('wrench', 'wrenches').
inf('finish', 'finishes').
inf('photo finish', 'photo finishes').
inf('second-place finish', 'second-place finishes').
inf('runner-up finish', 'runner-up finishes').
inf('third-place finish', 'third-place finishes').
inf('first-place finish', 'first-place finishes').
inf('expiry', 'expiries').
inf('breath of fresh air', 'breaths of fresh air').
inf('Transfiguration of Jesus', 'Transfigurations of Jesus').
inf('pyrolysis', 'pyrolyses').
inf('casualty', 'casualties').
inf('shimmy', 'shimmies').
inf('fiasco', 'fiascoes').
inf('discrepancy', 'discrepancies').
inf('monstrosity', 'monstrosities').
inf('fuss', 'fusses').
inf('amphimixis', 'amphimixes').
inf('mix', 'mixes').
inf('cacophony', 'cacophonies').
inf('clash', 'clashes').
inf('crash', 'crashes').
inf('crunch', 'crunches').
inf('cry', 'cries').
inf('hiss', 'hisses').
inf('whinny', 'whinnies').
inf('scratch', 'scratches').
inf('screech', 'screeches').
inf('scrunch', 'scrunches').
inf('splash', 'splashes').
inf('plash', 'plashes').
inf('squish', 'squishes').
inf('swish', 'swishes').
inf('swoosh', 'swooshes').
inf('whoosh', 'whooshes').
inf('vibrato', 'vibratoes').
inf('tremolo', 'tremoloes').
inf('reflux', 'refluxes').
inf('sea puss', 'sea pusses').
inf('sea-puss', 'sea-pusses').
inf('flow of air', 'flows of air').
inf('flux', 'fluxes').
inf('touch', 'touches').
inf('bash', 'bashes').
inf('smash', 'smashes').
inf('flash', 'flashes').
inf('heat flash', 'heat flashes').
inf('brush', 'brushes').
inf('light touch', 'light touches').
inf('glycogenesis', 'glycogeneses').
inf('climax', 'climaxes').
inf('emergency', 'emergencies').
inf('exigency', 'exigencies').
inf('pinch', 'pinches').
inf('crisis', 'crises').
inf('Fall of Man', 'Falls of Man').
inf('road to Damascus', 'roads to Damascus').
inf('pass', 'passes').
inf('recovery', 'recoveries').
inf('turn of events', 'turns of events').
inf('mutagenesis', 'mutageneses').
inf('insertional mutagenesis', 'insertional mutageneses').
inf('atrophy', 'atrophies').
inf('anticlimax', 'anticlimaxes').
inf('abiotrophy', 'abiotrophies').
inf('eddy', 'eddies').
inf('vortex', 'vortexes').
inf('salvo', 'salvoes').
inf('rush', 'rushes').
inf('allogamy', 'allogamies').
inf('autogamy', 'autogamies').
inf('cleistogamy', 'cleistogamies').
inf('flush', 'flushes').
inf('gush', 'gushes').
inf('rush', 'rushes').
inf('onrush', 'onrushes').
inf('English', 'Englishes').
inf('switch', 'switches').
inf('progress', 'progresses').
inf('party', 'parties').
inf('bash', 'bashes').
inf('do', 'does').
inf('birthday party', 'birthday parties').
inf('cocktail party', 'cocktail parties').
inf('house party', 'house parties').
inf('jolly', 'jollies').
inf('tea party', 'tea parties').
inf('ceremony', 'ceremonies').
inf('wedding ceremony', 'wedding ceremonies').
inf('pageantry', 'pageantries').
inf('military ceremony', 'military ceremonies').
inf('commencement ceremony', 'commencement ceremonies').
inf('formality', 'formalities').
inf('potlatch', 'potlatches').
inf('photo opportunity', 'photo opportunities').
inf('preliminary', 'preliminaries').
inf('Grand Prix', 'Grand Prixes').
inf('rally', 'rallies').
inf('cross country', 'cross countries').
inf('Preakness', 'Preaknesses').
inf('boxing match', 'boxing matches').
inf('chess match', 'chess matches').
inf('cricket match', 'cricket matches').
inf('dash', 'dashes').
inf('match', 'matches').
inf('tennis match', 'tennis matches').
inf('test match', 'test matches').
inf('wrestling match', 'wrestling matches').
inf('sparring match', 'sparring matches').
inf('tug-of-war', 'tugs-of-war').
inf('campaign for governor', 'campaigns for governor').
inf('victory', 'victories').
inf('Pyrrhic victory', 'Pyrrhic victories').
inf('whammy', 'whammies').
inf('lurch', 'lurches').
inf('whitewash', 'whitewashes').
inf('gold rush', 'gold rushes').
inf('gravy', 'gravies').
inf('manna from heaven', 'mannas from heaven').
inf('crash', 'crashes').
inf('loss of consciousness', 'losses of consciousness').
inf('crash', 'crashes').
inf('head crash', 'head crashes').
inf('faintness', 'faintnesses').
inf('soulfulness', 'soulfulnesses').
inf('passionateness', 'passionatenesses').
inf('wildness', 'wildnesses').
inf('fervency', 'fervencies').
inf('fervidness', 'fervidnesses').
inf('storminess', 'storminesses').
inf('sentimentality', 'sentimentalities').
inf('mawkishness', 'mawkishnesses').
inf('razbliuto', 'razbliutoes').
inf('complex', 'complexes').
inf('Oedipus complex', 'Oedipus complexes').
inf('Oedipal complex', 'Oedipal complexes').
inf('Electra complex', 'Electra complexes').
inf('inferiority complex', 'inferiority complexes').
inf('ambivalency', 'ambivalencies').
inf('apathy', 'apathies').
inf('emotionlessness', 'emotionlessnesses').
inf('impassivity', 'impassivities').
inf('impassiveness', 'impassivenesses').
inf('stolidity', 'stolidities').
inf('unemotionality', 'unemotionalities').
inf('listlessness', 'listlessnesses').
inf('appetency', 'appetencies').
inf('stomach', 'stomaches').
inf('sweet tooth', 'sweet teeth').
inf('wish', 'wishes').
inf('velleity', 'velleities').
inf('hungriness', 'hungrinesses').
inf('wishfulness', 'wishfulnesses').
inf('wistfulness', 'wistfulnesses').
inf('lovesickness', 'lovesicknesses').
inf('homesickness', 'homesicknesses').
inf('sex', 'sexes').
inf('sensuality', 'sensualities').
inf('sensualness', 'sensualnesses').
inf('amorousness', 'amorousnesses').
inf('sexiness', 'sexinesses').
inf('amativeness', 'amativenesses').
inf('fetish', 'fetishes').
inf('libido', 'libidoes').
inf('lecherousness', 'lecherousnesses').
inf('lustfulness', 'lustfulnesses').
inf('satyriasis', 'satyriases').
inf('pruriency', 'pruriencies').
inf('lasciviousness', 'lasciviousnesses').
inf('carnality', 'carnalities').
inf('lubricity', 'lubricities').
inf('itch', 'itches').
inf('gusto', 'gustoes').
inf('relish', 'relishes').
inf('zestfulness', 'zestfulnesses').
inf('pleasantness', 'pleasantnesses').
inf('painfulness', 'painfulnesses').
inf('unpleasantness', 'unpleasantnesses').
inf('mental anguish', 'mental anguishes').
inf('agony', 'agonies').
inf('soreness', 'sorenesses').
inf('intertrigo', 'intertrigoes').
inf('distress', 'distresses').
inf('anguish', 'anguishes').
inf('fondness', 'fondnesses').
inf('fancy', 'fancies').
inf('partiality', 'partialities').
inf('weakness', 'weaknesses').
inf('propensity', 'propensities').
inf('tendency', 'tendencies').
inf('stomach', 'stomaches').
inf('friendliness', 'friendlinesses').
inf('amicability', 'amicabilities').
inf('amicableness', 'amicablenesses').
inf('philogyny', 'philogynies').
inf('unfriendliness', 'unfriendlinesses').
inf('antipathy', 'antipathies').
inf('gratefulness', 'gratefulnesses').
inf('thankfulness', 'thankfulnesses').
inf('appreciativeness', 'appreciativenesses').
inf('ungratefulness', 'ungratefulnesses').
inf('solicitousness', 'solicitousnesses').
inf('softheartedness', 'softheartednesses').
inf('tenderness', 'tendernesses').
inf('aloofness', 'aloofnesses').
inf('heartlessness', 'heartlessnesses').
inf('coldheartedness', 'coldheartednesses').
inf('hardheartedness', 'hardheartednesses').
inf('cruelty', 'cruelties').
inf('mercilessness', 'mercilessnesses').
inf('pitilessness', 'pitilessnesses').
inf('ruthlessness', 'ruthlessnesses').
inf('self-consciousness', 'self-consciousnesses').
inf('uneasiness', 'uneasinesses').
inf('uncomfortableness', 'uncomfortablenesses').
inf('shamefacedness', 'shamefacednesses').
inf('sheepishness', 'sheepishnesses').
inf('bashfulness', 'bashfulnesses').
inf('pridefulness', 'pridefulnesses').
inf('ego', 'egoes').
inf('vanity', 'vanities').
inf('humility', 'humilities').
inf('humbleness', 'humblenesses').
inf('meekness', 'meeknesses').
inf('expectancy', 'expectancies').
inf('levity', 'levities').
inf('gaiety', 'gaieties').
inf('playfulness', 'playfulnesses').
inf('gravity', 'gravities').
inf('solemnity', 'solemnities').
inf('earnestness', 'earnestnesses').
inf('seriousness', 'seriousnesses').
inf('sincerity', 'sincerities').
inf('sensitivity', 'sensitivities').
inf('sensitiveness', 'sensitivenesses').
inf('oversensitiveness', 'oversensitivenesses').
inf('sensibility', 'sensibilities').
inf('perceptiveness', 'perceptivenesses').
inf('perceptivity', 'perceptivities').
inf('sensuousness', 'sensuousnesses').
inf('fidgetiness', 'fidgetinesses').
inf('restlessness', 'restlessnesses').
inf('electricity', 'electricities').
inf('calmness', 'calmnesses').
inf('placidity', 'placidities').
inf('placidness', 'placidnesses').
inf('coolness', 'coolnesses').
inf('imperturbability', 'imperturbabilities').
inf('imperturbableness', 'imperturbablenesses').
inf('tranquillity', 'tranquillities').
inf('tranquility', 'tranquilities').
inf('quietness', 'quietnesses').
inf('peacefulness', 'peacefulnesses').
inf('peace of mind', 'peaces of mind').
inf('serenity', 'serenities').
inf('ataraxis', 'ataraxes').
inf('easiness', 'easinesses').
inf('dreaminess', 'dreaminesses').
inf('fury', 'furies').
inf('madness', 'madnesses').
inf('lividity', 'lividities').
inf('huffiness', 'huffinesses').
inf('fearfulness', 'fearfulnesses').
inf('apprehensiveness', 'apprehensivenesses').
inf('timidity', 'timidities').
inf('timidness', 'timidnesses').
inf('timorousness', 'timorousnesses').
inf('shyness', 'shynesses').
inf('hesitancy', 'hesitancies').
inf('unassertiveness', 'unassertivenesses').
inf('anxiety', 'anxieties').
inf('worry', 'worries').
inf('anxiousness', 'anxiousnesses').
inf('insecurity', 'insecurities').
inf('edginess', 'edginesses').
inf('uneasiness', 'uneasinesses').
inf('jitteriness', 'jitterinesses').
inf('jumpiness', 'jumpinesses').
inf('nervousness', 'nervousnesses').
inf('restiveness', 'restivenesses').
inf('fearlessness', 'fearlessnesses').
inf('bravery', 'braveries').
inf('security', 'securities').
inf('happiness', 'happinesses').
inf('gladness', 'gladnesses').
inf('gladfulness', 'gladfulnesses').
inf('gladsomeness', 'gladsomenesses').
inf('joyousness', 'joyousnesses').
inf('joyfulness', 'joyfulnesses').
inf('lightness', 'lightnesses').
inf('jubilancy', 'jubilancies').
inf('rush', 'rushes').
inf('flush', 'flushes').
inf('euphory', 'euphories').
inf('gaiety', 'gaieties').
inf('hilarity', 'hilarities').
inf('mirthfulness', 'mirthfulnesses').
inf('gleefulness', 'gleefulnesses').
inf('jocundity', 'jocundities').
inf('jocularity', 'jocularities').
inf('comfortableness', 'comfortablenesses').
inf('closeness', 'closenesses').
inf('intimacy', 'intimacies').
inf('togetherness', 'togethernesses').
inf('cheerfulness', 'cheerfulnesses').
inf('blitheness', 'blithenesses').
inf('buoyancy', 'buoyancies').
inf('perkiness', 'perkinesses').
inf('carefreeness', 'carefreenesses').
inf('lightheartedness', 'lightheartednesses').
inf('lightsomeness', 'lightsomenesses').
inf('complacency', 'complacencies').
inf('self-complacency', 'self-complacencies').
inf('smugness', 'smugnesses').
inf('sadness', 'sadnesses').
inf('unhappiness', 'unhappinesses').
inf('dolefulness', 'dolefulnesses').
inf('heaviness', 'heavinesses').
inf('melancholy', 'melancholies').
inf('gloominess', 'gloominesses').
inf('somberness', 'sombernesses').
inf('sombreness', 'sombrenesses').
inf('heavyheartedness', 'heavyheartednesses').
inf('pensiveness', 'pensivenesses').
inf('world-weariness', 'world-wearinesses').
inf('woefulness', 'woefulnesses').
inf('misery', 'miseries').
inf('forlornness', 'forlornnesses').
inf('loneliness', 'lonelinesses').
inf('weepiness', 'weepinesses').
inf('tearfulness', 'tearfulnesses').
inf('contriteness', 'contritenesses').
inf('brokenheartedness', 'brokenheartednesses').
inf('mournfulness', 'mournfulnesses').
inf('sorrowfulness', 'sorrowfulnesses').
inf('ruthfulness', 'ruthfulnesses').
inf('plaintiveness', 'plaintivenesses').
inf('ruefulness', 'ruefulnesses').
inf('self-reproach', 'self-reproaches').
inf('cheerlessness', 'cheerlessnesses').
inf('uncheerfulness', 'uncheerfulnesses').
inf('joylessness', 'joylessnesses').
inf('downheartedness', 'downheartednesses').
inf('dejectedness', 'dejectednesses').
inf('low-spiritedness', 'low-spiritednesses').
inf('lowness', 'lownesses').
inf('dispiritedness', 'dispiritednesses').
inf('helplessness', 'helplessnesses').
inf('self-pity', 'self-pities').
inf('despondency', 'despondencies').
inf('heartsickness', 'heartsicknesses').
inf('disconsolateness', 'disconsolatenesses').
inf('oppressiveness', 'oppressivenesses').
inf('discontentedness', 'discontentednesses').
inf('hopefulness', 'hopefulnesses').
inf('sanguinity', 'sanguinities').
inf('sanguineness', 'sanguinenesses').
inf('hopelessness', 'hopelessnesses').
inf('amorousness', 'amorousnesses').
inf('enamoredness', 'enamorednesses').
inf('crush', 'crushes').
inf('devotedness', 'devotednesses').
inf('affectionateness', 'affectionatenesses').
inf('fondness', 'fondnesses').
inf('tenderness', 'tendernesses').
inf('warmness', 'warmnesses').
inf('warmheartedness', 'warmheartednesses').
inf('protectiveness', 'protectivenesses').
inf('lovingness', 'lovingnesses').
inf('warmheartedness', 'warmheartednesses').
inf('loyalty', 'loyalties').
inf('misanthropy', 'misanthropies').
inf('misogamy', 'misogamies').
inf('misogyny', 'misogynies').
inf('misology', 'misologies').
inf('murderousness', 'murderousnesses').
inf('hostility', 'hostilities').
inf('enmity', 'enmities').
inf('animosity', 'animosities').
inf('aggressiveness', 'aggressivenesses').
inf('belligerency', 'belligerencies').
inf('bitterness', 'bitternesses').
inf('sulkiness', 'sulkinesses').
inf('huffishness', 'huffishnesses').
inf('envy', 'envies').
inf('enviousness', 'enviousnesses').
inf('covetousness', 'covetousnesses').
inf('jealousy', 'jealousies').
inf('penis envy', 'penis envies').
inf('malignity', 'malignities').
inf('maliciousness', 'maliciousnesses').
inf('spitefulness', 'spitefulnesses').
inf('vindictiveness', 'vindictivenesses').
inf('vengefulness', 'vengefulnesses').
inf('sulkiness', 'sulkinesses').
inf('amiability', 'amiabilities').
inf('jollity', 'jollities').
inf('jolliness', 'jollinesses').
inf('joviality', 'jovialities').
inf('moodiness', 'moodinesses').
inf('moroseness', 'morosenesses').
inf('glumness', 'glumnesses').
inf('sullenness', 'sullennesses').
inf('irascibility', 'irascibilities').
inf('irritability', 'irritabilities').
inf('crossness', 'crossnesses').
inf('fretfulness', 'fretfulnesses').
inf('fussiness', 'fussinesses').
inf('peevishness', 'peevishnesses').
inf('testiness', 'testinesses').
inf('touchiness', 'touchinesses').
inf('tetchiness', 'tetchinesses').
inf('sympathy', 'sympathies').
inf('kindheartedness', 'kindheartednesses').
inf('kind-heartedness', 'kind-heartednesses').
inf('compassionateness', 'compassionatenesses').
inf('pity', 'pities').
inf('mellowness', 'mellownesses').
inf('tenderness', 'tendernesses').
inf('tenderheartedness', 'tenderheartednesses').
inf('mercifulness', 'mercifulnesses').
inf('mercy', 'mercies').
inf('forgiveness', 'forgivenesses').
inf('compatibility', 'compatibilities').
inf('empathy', 'empathies').
inf('eagerness', 'eagernesses').
inf('avidity', 'avidities').
inf('avidness', 'avidnesses').
inf('keenness', 'keennesses').
inf('dainty', 'dainties').
inf('delicacy', 'delicacies').
inf('goody', 'goodies').
inf('dish', 'dishes').
inf('dietary', 'dietaries').
inf('mess', 'messes').
inf('gastronomy', 'gastronomies').
inf('brunch', 'brunches').
inf('lunch', 'lunches').
inf('business lunch', 'business lunches').
inf('fish fry', 'fish fries').
inf('nosh', 'noshes').
inf('ploughman\'s lunch', 'ploughman\'s lunches').
inf('adobo', 'adoboes').
inf('side dish', 'side dishes').
inf('antipasto', 'antipastoes').
inf('relish', 'relishes').
inf('borsch', 'borsches').
inf('borsh', 'borshes').
inf('borshch', 'borshches').
inf('bortsch', 'bortsches').
inf('cocky-leeky', 'cocky-leekies').
inf('gazpacho', 'gazpachoes').
inf('gumbo', 'gumboes').
inf('mulligatawny', 'mulligatawnies').
inf('goulash', 'goulashes').
inf('Hungarian goulash', 'Hungarian goulashes').
inf('hotchpotch', 'hotchpotches').
inf('beef goulash', 'beef goulashes').
inf('pork-and-veal goulash', 'pork-and-veal goulashes').
inf('ready-mix', 'ready-mixes').
inf('brownie mix', 'brownie mixes').
inf('cake mix', 'cake mixes').
inf('lemonade mix', 'lemonade mixes').
inf('savory', 'savories').
inf('savoury', 'savouries').
inf('calf\'s-foot jelly', 'calf\'s-foot jellies').
inf('sugarloaf', 'sugarloaves').
inf('sugar loaf', 'sugar loaves').
inf('confectionery', 'confectioneries').
inf('candy', 'candies').
inf('hard candy', 'hard candies').
inf('barley candy', 'barley candies').
inf('patty', 'patties').
inf('peppermint patty', 'peppermint patties').
inf('toffy', 'toffies').
inf('butterscotch', 'butterscotches').
inf('chocolate candy', 'chocolate candies').
inf('cotton candy', 'cotton candies').
inf('candyfloss', 'candyflosses').
inf('divinity', 'divinities').
inf('mint candy', 'mint candies').
inf('peppermint candy', 'peppermint candies').
inf('kiss', 'kisses').
inf('candy kiss', 'candy kisses').
inf('molasses kiss', 'molasses kisses').
inf('meringue kiss', 'meringue kisses').
inf('chocolate kiss', 'chocolate kisses').
inf('Scotch kiss', 'Scotch kisses').
inf('rock candy', 'rock candies').
inf('rock candy', 'rock candies').
inf('sugar candy', 'sugar candies').
inf('taffy', 'taffies').
inf('molasses taffy', 'molasses taffies').
inf('jello', 'jelloes').
inf('ice lolly', 'ice lollies').
inf('lolly', 'lollies').
inf('flummery', 'flummeries').
inf('roly-poly', 'roly-polies').
inf('maraschino', 'maraschinoes').
inf('maraschino cherry', 'maraschino cherries').
inf('garnish', 'garnishes').
inf('pastry', 'pastries').
inf('pastry', 'pastries').
inf('dowdy', 'dowdies').
inf('pandowdy', 'pandowdies').
inf('knish', 'knishes').
inf('French pastry', 'French pastries').
inf('patty', 'patties').
inf('toad-in-the-hole', 'toads-in-the-hole').
inf('phyllo', 'phylloes').
inf('pate a choux', 'pate a chouxes').
inf('gateau', 'gateaux').
inf('cooky', 'cookies').
inf('kiss', 'kisses').
inf('gingerbread man', 'gingerbread men').
inf('bliny', 'blinies').
inf('Victoria sandwich', 'Victoria sandwiches').
inf('jelly', 'jellies').
inf('apple jelly', 'apple jellies').
inf('crabapple jelly', 'crabapple jellies').
inf('grape jelly', 'grape jellies').
inf('jelly', 'jellies').
inf('poultry', 'poultries').
inf('goose', 'geese').
inf('loaf', 'loaves').
inf('mess', 'messes').
inf('cut of meat', 'cuts of meat').
inf('side of meat', 'sides of meat').
inf('side of beef', 'sides of beef').
inf('cut of beef', 'cuts of beef').
inf('buffalo', 'buffaloes').
inf('beef', 'beeves').
inf('roast beef', 'roast beeves').
inf('patty', 'patties').
inf('ground beef', 'ground beeves').
inf('beef patty', 'beef patties').
inf('bully beef', 'bully beeves').
inf('corned beef', 'corned beeves').
inf('corn beef', 'corn beeves').
inf('carbonado', 'carbonadoes').
inf('jerky', 'jerkies').
inf('beef jerky', 'beef jerkies').
inf('veau', 'veaux').
inf('cut of veal', 'cuts of veal').
inf('breast of veal', 'breasts of veal').
inf('fricandeau', 'fricandeaux').
inf('horseflesh', 'horsefleshes').
inf('cut of mutton', 'cuts of mutton').
inf('cut of lamb', 'cuts of lamb').
inf('breast of lamb', 'breasts of lamb').
inf('poitrine d\'agneau', 'poitrine d\'agneaux').
inf('saddle of lamb', 'saddles of lamb').
inf('loin of lamb', 'loins of lamb').
inf('rack of lamb', 'racks of lamb').
inf('leg of lamb', 'legs of lamb').
inf('cut of pork', 'cuts of pork').
inf('flitch', 'flitches').
inf('side of bacon', 'sides of bacon').
inf('side of pork', 'sides of pork').
inf('pork belly', 'pork bellies').
inf('prosciutto', 'prosciuttoes').
inf('sowbelly', 'sowbellies').
inf('chorizo', 'chorizoes').
inf('polony', 'polonies').
inf('staff of life', 'staffs of life').
inf('grissino', 'grissinoes').
inf('loaf of bread', 'loaves of bread').
inf('loaf', 'loaves').
inf('French loaf', 'French loaves').
inf('matzo', 'matzoes').
inf('corn tash', 'corn tashes').
inf('hush puppy', 'hush puppies').
inf('hushpuppy', 'hushpuppies').
inf('bialy', 'bialies').
inf('danish', 'danishes').
inf('danish pastry', 'danish pastries').
inf('sandwich', 'sandwiches').
inf('butty', 'butties').
inf('ham sandwich', 'ham sandwiches').
inf('chicken sandwich', 'chicken sandwiches').
inf('club sandwich', 'club sandwiches').
inf('open-face sandwich', 'open-face sandwiches').
inf('open sandwich', 'open sandwiches').
inf('hero', 'heroes').
inf('hero sandwich', 'hero sandwiches').
inf('hoagy', 'hoagies').
inf('Cuban sandwich', 'Cuban sandwiches').
inf('Italian sandwich', 'Italian sandwiches').
inf('submarine sandwich', 'submarine sandwiches').
inf('torpedo', 'torpedoes').
inf('gyro', 'gyroes').
inf('bacon-lettuce-tomato sandwich', 'bacon-lettuce-tomato sandwiches').
inf('western sandwich', 'western sandwiches').
inf('orzo', 'orzoes').
inf('fettuccine Alfredo', 'fettuccine Alfredoes').
inf('mush', 'mushes').
inf('cornmeal mush', 'cornmeal mushes').
inf('skilly', 'skillies').
inf('frumenty', 'frumenties').
inf('potato', 'potatoes').
inf('white potato', 'white potatoes').
inf('Irish potato', 'Irish potatoes').
inf('murphy', 'murphies').
inf('baked potato', 'baked potatoes').
inf('jacket potato', 'jacket potatoes').
inf('mashed potato', 'mashed potatoes').
inf('Uruguay potato', 'Uruguay potatoes').
inf('sweet potato', 'sweet potatoes').
inf('nacho', 'nachoes').
inf('Chinese celery', 'Chinese celeries').
inf('squash', 'squashes').
inf('summer squash', 'summer squashes').
inf('yellow squash', 'yellow squashes').
inf('crookneck squash', 'crookneck squashes').
inf('pattypan squash', 'pattypan squashes').
inf('spaghetti squash', 'spaghetti squashes').
inf('winter squash', 'winter squashes').
inf('acorn squash', 'acorn squashes').
inf('butternut squash', 'butternut squashes').
inf('hubbard squash', 'hubbard squashes').
inf('turban squash', 'turban squashes').
inf('buttercup squash', 'buttercup squashes').
inf('winter crookneck squash', 'winter crookneck squashes').
inf('pimento', 'pimentoes').
inf('pimiento', 'pimientoes').
inf('chilly', 'chillies').
inf('jalapeno', 'jalapenoes').
inf('tabasco', 'tabascoes').
inf('buttercrunch', 'buttercrunches').
inf('garbanzo', 'garbanzoes').
inf('celery', 'celeries').
inf('pascal celery', 'pascal celeries').
inf('Paschal celery', 'Paschal celeries').
inf('chicory', 'chicories').
inf('chicory', 'chicories').
inf('hominy', 'hominies').
inf('lye hominy', 'lye hominies').
inf('pearl hominy', 'pearl hominies').
inf('cress', 'cresses').
inf('watercress', 'watercresses').
inf('garden cress', 'garden cresses').
inf('winter cress', 'winter cresses').
inf('gumbo', 'gumboes').
inf('wild spinach', 'wild spinaches').
inf('wild spinach', 'wild spinaches').
inf('tomato', 'tomatoes').
inf('beefsteak tomato', 'beefsteak tomatoes').
inf('cherry tomato', 'cherry tomatoes').
inf('plum tomato', 'plum tomatoes').
inf('tomatillo', 'tomatilloes').
inf('husk tomato', 'husk tomatoes').
inf('Mexican husk tomato', 'Mexican husk tomatoes').
inf('salsify', 'salsifies').
inf('black salsify', 'black salsifies').
inf('radish', 'radishes').
inf('spinach', 'spinaches').
inf('taro', 'taroes').
inf('McIntosh', 'McIntoshes').
inf('Stayman', 'Staymen').
inf('berry', 'berries').
inf('bilberry', 'bilberries').
inf('whortleberry', 'whortleberries').
inf('European blueberry', 'European blueberries').
inf('huckleberry', 'huckleberries').
inf('blueberry', 'blueberries').
inf('boxberry', 'boxberries').
inf('checkerberry', 'checkerberries').
inf('teaberry', 'teaberries').
inf('spiceberry', 'spiceberries').
inf('cranberry', 'cranberries').
inf('lingonberry', 'lingonberries').
inf('mountain cranberry', 'mountain cranberries').
inf('cowberry', 'cowberries').
inf('lowbush cranberry', 'lowbush cranberries').
inf('gooseberry', 'gooseberries').
inf('blackberry', 'blackberries').
inf('boysenberry', 'boysenberries').
inf('dewberry', 'dewberries').
inf('loganberry', 'loganberries').
inf('raspberry', 'raspberries').
inf('serviceberry', 'serviceberries').
inf('shadberry', 'shadberries').
inf('juneberry', 'juneberries').
inf('strawberry', 'strawberries').
inf('sugarberry', 'sugarberries').
inf('hackberry', 'hackberries').
inf('barbados cherry', 'barbados cherries').
inf('surinam cherry', 'surinam cherries').
inf('West Indian cherry', 'West Indian cherries').
inf('ceriman', 'cerimen').
inf('tangelo', 'tangeloes').
inf('pomelo', 'pomeloes').
inf('peach', 'peaches').
inf('sweet calabash', 'sweet calabashes').
inf('cherry', 'cherries').
inf('sweet cherry', 'sweet cherries').
inf('black cherry', 'black cherries').
inf('bing cherry', 'bing cherries').
inf('heart cherry', 'heart cherries').
inf('oxheart cherry', 'oxheart cherries').
inf('blackheart cherry', 'blackheart cherries').
inf('Mexican black cherry', 'Mexican black cherries').
inf('sour cherry', 'sour cherries').
inf('morello', 'morelloes').
inf('icaco', 'icacoes').
inf('Thompson Seedless', 'Thompson Seedlesses').
inf('Chinese gooseberry', 'Chinese gooseberries').
inf('mango', 'mangoes').
inf('tamarindo', 'tamarindoes').
inf('avocado', 'avocadoes').
inf('elderberry', 'elderberries').
inf('longanberry', 'longanberries').
inf('mulberry', 'mulberries').
inf('Barbados gooseberry', 'Barbados gooseberries').
inf('native peach', 'native peaches').
inf('fish', 'fish').
inf('saltwater fish', 'saltwater fish').
inf('freshwater fish', 'freshwater fish').
inf('freshwater bass', 'freshwater basses').
inf('bass', 'basses').
inf('largemouth bass', 'largemouth basses').
inf('smallmouth bass', 'smallmouth basses').
inf('sea bass', 'sea basses').
inf('bass', 'basses').
inf('striped bass', 'striped basses').
inf('dolphinfish', 'dolphinfish').
inf('buffalofish', 'buffalofish').
inf('monkfish', 'monkfish').
inf('catfish', 'catfish').
inf('perch', 'perches').
inf('sunfish', 'sunfish').
inf('tuna fish', 'tuna fish').
inf('tunny', 'tunnies').
inf('bonito', 'bonitoes').
inf('pompano', 'pompanoes').
inf('calamary', 'calamaries').
inf('blowfish', 'blowfish').
inf('pufferfish', 'pufferfish').
inf('octopus', 'octopodes').
inf('panfish', 'panfish').
inf('stockfish', 'stockfish').
inf('shellfish', 'shellfish').
inf('anchovy', 'anchovies').
inf('kingfish', 'kingfish').
inf('alewife', 'alewives').
inf('bluefish', 'bluefish').
inf('swordfish', 'swordfish').
inf('butterfish', 'butterfish').
inf('crayfish', 'crayfish').
inf('crawfish', 'crawfish').
inf('codfish', 'codfish').
inf('porgy', 'porgies').
inf('flatfish', 'flatfish').
inf('fillet of sole', 'fillets of sole').
inf('flitch', 'flitches').
inf('redfish', 'redfish').
inf('rosefish', 'rosefish').
inf('ocean perch', 'ocean perches').
inf('rockfish', 'rockfish').
inf('sailfish', 'sailfish').
inf('weakfish', 'weakfish').
inf('crayfish', 'crayfish').
inf('scampo', 'scampoes').
inf('whitefish', 'whitefish').
inf('whitefish', 'whitefish').
inf('cisco', 'ciscoes').
inf('coho', 'cohoes').
inf('lox', 'loxes').
inf('Scandinavian lox', 'Scandinavian loxes').
inf('Nova Scotia lox', 'Nova Scotia loxes').
inf('Nova lox', 'Nova loxes').
inf('red rockfish', 'red rockfish').
inf('grass', 'grasses').
inf('timothy', 'timothies').
inf('wheat berry', 'wheat berries').
inf('paddy', 'paddies').
inf('pigwash', 'pigwashes').
inf('mash', 'mashes').
inf('scratch', 'scratches').
inf('oil of wintergreen', 'oils of wintergreen').
inf('bay leaf', 'bay leaves').
inf('cilantro', 'cilantroes').
inf('costmary', 'costmaries').
inf('oregano', 'oreganoes').
inf('rosemary', 'rosemaries').
inf('savory', 'savories').
inf('savoury', 'savouries').
inf('summer savory', 'summer savories').
inf('summer savoury', 'summer savouries').
inf('winter savory', 'winter savories').
inf('winter savoury', 'winter savouries').
inf('sweet cicely', 'sweet cicelies').
inf('Indian relish', 'Indian relishes').
inf('curry', 'curries').
inf('lamb curry', 'lamb curries').
inf('horseradish', 'horseradishes').
inf('pickle relish', 'pickle relishes').
inf('Tabasco', 'Tabascoes').
inf('pesto', 'pestoes').
inf('mayo', 'mayoes').
inf('gravy', 'gravies').
inf('pan gravy', 'pan gravies').
inf('gravy', 'gravies').
inf('roux', 'rouxes').
inf('half-and-half', 'half-and-halves').
inf('sapsago', 'sapsagoes').
inf('miso', 'misoes').
inf('manna from heaven', 'mannas from heaven').
inf('arroz con pollo', 'arroz con polloes').
inf('chicken Marengo', 'chicken Marengoes').
inf('chicken paprikash', 'chicken paprikashes').
inf('Cornish pasty', 'Cornish pasties').
inf('gefilte fish', 'gefilte fish').
inf('hash', 'hashes').
inf('corned beef hash', 'corned beef hashes').
inf('meat loaf', 'meat loaves').
inf('meatloaf', 'meatloaves').
inf('pasty', 'pasties').
inf('osso buco', 'osso bucoes').
inf('pheasant under glass', 'pheasants under glass').
inf('pigs in blankets', 'pigss in blankets').
inf('loblolly', 'loblollies').
inf('risotto', 'risottoes').
inf('fish loaf', 'fish loaves').
inf('salmon loaf', 'salmon loaves').
inf('stuffed tomato', 'stuffed tomatoes').
inf('hot stuffed tomato', 'hot stuffed tomatoes').
inf('stuffed tomato', 'stuffed tomatoes').
inf('cold stuffed tomato', 'cold stuffed tomatoes').
inf('succotash', 'succotashes').
inf('taco', 'tacoes').
inf('chicken taco', 'chicken tacoes').
inf('burrito', 'burritoes').
inf('beef burrito', 'beef burritoes').
inf('wish-wash', 'wish-washes').
inf('mix', 'mixes').
inf('premix', 'premixes').
inf('elixir of life', 'elixirs of life').
inf('round of drinks', 'rounds of drinks').
inf('hooch', 'hooches').
inf('hootch', 'hootches').
inf('Guinness', 'Guinnesses').
inf('kvass', 'kvasses').
inf('vino', 'vinoes').
inf('bubbly', 'bubblies').
inf('Bordeaux', 'Bordeauxes').
inf('red Bordeaux', 'red Bordeauxes').
inf('Rhenish', 'Rhenishes').
inf('liebfraumilch', 'liebfraumilches').
inf('sherry', 'sherries').
inf('Amontillado', 'Amontilladoes').
inf('brandy', 'brandies').
inf('kirsch', 'kirsches').
inf('ouzo', 'ouzoes').
inf('whisky', 'whiskies').
inf('blended whisky', 'blended whiskies').
inf('corn whisky', 'corn whiskies').
inf('Irish', 'Irishes').
inf('Irish whisky', 'Irish whiskies').
inf('rye whisky', 'rye whiskies').
inf('Scotch', 'Scotches').
inf('Scotch whisky', 'Scotch whiskies').
inf('malt whisky', 'malt whiskies').
inf('Scotch malt whisky', 'Scotch malt whiskies').
inf('sour mash', 'sour mashes').
inf('amaretto', 'amarettoes').
inf('anisette de Bordeaux', 'anisette de Bordeauxes').
inf('Galliano', 'Gallianoes').
inf('maraschino', 'maraschinoes').
inf('Dom Pedro', 'Dom Pedroes').
inf('hair of the dog', 'hairs of the dog').
inf('shandy', 'shandies').
inf('gin and it', 'gin and they').
inf('pink lady', 'pink ladies').
inf('whiskey on the rocks', 'whiskeys on the rocks').
inf('whisky on the rocks', 'whiskies on the rocks').
inf('hot toddy', 'hot toddies').
inf('toddy', 'toddies').
inf('espresso', 'espressoes').
inf('cappuccino', 'cappuccinoes').
inf('coffee cappuccino', 'coffee cappuccinoes').
inf('scrumpy', 'scrumpies').
inf('perry', 'perries').
inf('pruno', 'prunoes').
inf('criollo', 'criolloes').
inf('fruit crush', 'fruit crushes').
inf('koumiss', 'koumisses').
inf('coffee berry', 'coffee berries').
inf('fruit punch', 'fruit punches').
inf('milk punch', 'milk punches').
inf('punch', 'punches').
inf('planter\'s punch', 'planter\'s punches').
inf('fish house punch', 'fish house punches').
inf('tea leaf', 'tea leaves').
inf('congo', 'congoes').
inf('rugelach', 'rugelaches').
inf('ruggelach', 'ruggelaches').
inf('dichotomy', 'dichotomies').
inf('duality', 'dualities').
inf('trichotomy', 'trichotomies').
inf('community', 'communities').
inf('biotic community', 'biotic communities').
inf('elderly', 'elderlies').
inf('enemy', 'enemies').
inf('episcopacy', 'episcopacies').
inf('homeless', 'homelesses').
inf('network army', 'network armies').
inf('nationality', 'nationalities').
inf('peanut gallery', 'peanut galleries').
inf('battery', 'batteries').
inf('bunch', 'bunches').
inf('corpus', 'corpora').
inf('tenantry', 'tenantries').
inf('pack of cards', 'packs of cards').
inf('deck of cards', 'decks of cards').
inf('royal flush', 'royal flushes').
inf('straight flush', 'straight flushes').
inf('flush', 'flushes').
inf('statuary', 'statuaries').
inf('gimmickry', 'gimmickries').
inf('bunch', 'bunches').
inf('mass', 'masses').
inf('combination in restraint of trade', 'combinations in restraint of trade').
inf('body', 'bodies').
inf('society', 'societies').
inf('minority', 'minorities').
inf('business', 'businesses').
inf('big business', 'big businesses').
inf('ethnic minority', 'ethnic minorities').
inf('people of color', 'peoples of color').
inf('people of colour', 'peoples of colour').
inf('military-industrial complex', 'military-industrial complexes').
inf('family', 'families').
inf('family', 'families').
inf('phratry', 'phratries').
inf('dynasty', 'dynasties').
inf('patriarchy', 'patriarchies').
inf('matriarchy', 'matriarchies').
inf('meritocracy', 'meritocracies').
inf('nuclear family', 'nuclear families').
inf('conjugal family', 'conjugal families').
inf('extended family', 'extended families').
inf('foster family', 'foster families').
inf('class', 'classes').
inf('social class', 'social classes').
inf('socio-economic class', 'socio-economic classes').
inf('age class', 'age classes').
inf('library', 'libraries').
inf('program library', 'program libraries').
inf('subroutine library', 'subroutine libraries').
inf('library', 'libraries').
inf('public library', 'public libraries').
inf('mythology', 'mythologies').
inf('classical mythology', 'classical mythologies').
inf('Greek mythology', 'Greek mythologies').
inf('Roman mythology', 'Roman mythologies').
inf('Norse mythology', 'Norse mythologies').
inf('trilogy', 'trilogies').
inf('trinity', 'trinities').
inf('triplicity', 'triplicities').
inf('match', 'matches').
inf('man and wife', 'man and wives').
inf('antibiosis', 'antibioses').
inf('bevy', 'bevies').
inf('taxonomic category', 'taxonomic categories').
inf('biology', 'biologies').
inf('zoology', 'zoologies').
inf('wildlife', 'wildlives').
inf('colony', 'colonies').
inf('colony', 'colonies').
inf('class', 'classes').
inf('category', 'categories').
inf('family', 'families').
inf('brass family', 'brass families').
inf('violin family', 'violin families').
inf('woodwind family', 'woodwind families').
inf('sex', 'sexes').
inf('domain of a function', 'domains of a function').
inf('range of a function', 'ranges of a function').
inf('broadcasting company', 'broadcasting companies').
inf('car company', 'car companies').
inf('auto company', 'auto companies').
inf('dot com company', 'dot com companies').
inf('drug company', 'drug companies').
inf('pharmaceutical company', 'pharmaceutical companies').
inf('electronics company', 'electronics companies').
inf('film company', 'film companies').
inf('food company', 'food companies').
inf('furniture company', 'furniture companies').
inf('mining company', 'mining companies').
inf('shipping company', 'shipping companies').
inf('steel company', 'steel companies').
inf('subsidiary company', 'subsidiary companies').
inf('subsidiary', 'subsidiaries').
inf('transportation company', 'transportation companies').
inf('trucking company', 'trucking companies').
inf('histocompatibility complex', 'histocompatibility complexes').
inf('adhocracy', 'adhocracies').
inf('bureaucracy', 'bureaucracies').
inf('Bearer of the Sword', 'Bearers of the Sword').
inf('Martyrs of al-Aqsa', 'Martyrss of al-Aqsa').
inf('Vanguards of Conquest', 'Vanguardss of Conquest').
inf('Followers of the Phrophet', 'Followerss of the Phrophet').
inf('Supporters of Islam', 'Supporterss of Islam').
inf('Army for the Liberation of Rwanda', 'Armys for the Liberation of Rwanda').
inf('Band of Partisans', 'Bands of Partisans').
inf('Aum Shinrikyo', 'Aum Shinrikyoes').
inf('First of October Antifascist Resistance Group', 'Firsts of October Antifascist Resistance Group').
inf('Forces of Umar Al-Mukhtar', 'Forcess of Umar Al-Mukhtar').
inf('Movement of Holy Warriors', 'Movements of Holy Warriors').
inf('Party of God', 'Partys of God').
inf('Organization of the Oppressed on Earth', 'Organizations of the Oppressed on Earth').
inf('Army of Muhammad', 'Armys of Muhammad').
inf('Soldiers of God', 'Soldierss of God').
inf('Kach', 'Kaches').
inf('Association of Islamic Groups and Communities', 'Associations of Islamic Groups and Communities').
inf('Party of Democratic Kampuchea', 'Partys of Democratic Kampuchea').
inf('Army of the Pure', 'Armys of the Pure').
inf('Army of the Righteous', 'Armys of the Righteous').
inf('Association of Orangemen', 'Associations of Orangemen').
inf('Sol Rojo', 'Sol Rojoes').
inf('Sendero Luminoso', 'Sendero Luminosoes').
inf('Movement for Revenge', 'Movements for Revenge').
inf('Red Cross', 'Red Crosses').
inf('line of defense', 'lines of defense').
inf('line of defence', 'lines of defence').
inf('Commonwealth of Nations', 'Commonwealths of Nations').
inf('polity', 'polities').
inf('quango', 'quangoes').
inf('bureaucracy', 'bureaucracies').
inf('Court of Saint James\'s', 'Courts of Saint James\'s').
inf('government-in-exile', 'governments-in-exile').
inf('stratocracy', 'stratocracies').
inf('papacy', 'papacies').
inf('extended care facility', 'extended care facilities').
inf('giro', 'giroes').
inf('charity', 'charities').
inf('agency', 'agencies').
inf('company', 'companies').
inf('manufacturing business', 'manufacturing businesses').
inf('business', 'businesses').
inf('consulting company', 'consulting companies').
inf('publishing company', 'publishing companies').
inf('industry', 'industries').
inf('aluminum business', 'aluminum businesses').
inf('aluminum industry', 'aluminum industries').
inf('apparel industry', 'apparel industries').
inf('garment industry', 'garment industries').
inf('fashion industry', 'fashion industries').
inf('fashion business', 'fashion businesses').
inf('banking industry', 'banking industries').
inf('computer business', 'computer businesses').
inf('automobile industry', 'automobile industries').
inf('chemical industry', 'chemical industries').
inf('coal industry', 'coal industries').
inf('computer industry', 'computer industries').
inf('construction industry', 'construction industries').
inf('housing industry', 'housing industries').
inf('electronics industry', 'electronics industries').
inf('entertainment industry', 'entertainment industries').
inf('show business', 'show businesses').
inf('film industry', 'film industries').
inf('movie industry', 'movie industries').
inf('growth industry', 'growth industries').
inf('lighting industry', 'lighting industries').
inf('munitions industry', 'munitions industries').
inf('arms industry', 'arms industries').
inf('oil industry', 'oil industries').
inf('refining industry', 'refining industries').
inf('oil business', 'oil businesses').
inf('oil company', 'oil companies').
inf('packaging company', 'packaging companies').
inf('pipeline company', 'pipeline companies').
inf('printing business', 'printing businesses').
inf('printing company', 'printing companies').
inf('plastics industry', 'plastics industries').
inf('insurance company', 'insurance companies').
inf('investment company', 'investment companies').
inf('mutual fund company', 'mutual fund companies').
inf('open-end investment company', 'open-end investment companies').
inf('closed-end investment company', 'closed-end investment companies').
inf('face-amount certificate company', 'face-amount certificate companies').
inf('securities industry', 'securities industries').
inf('service industry', 'service industries').
inf('shipbuilding industry', 'shipbuilding industries').
inf('shoe industry', 'shoe industries').
inf('sign industry', 'sign industries').
inf('steel industry', 'steel industries').
inf('sunrise industry', 'sunrise industries').
inf('tobacco industry', 'tobacco industries').
inf('toy industry', 'toy industries').
inf('toy business', 'toy businesses').
inf('trucking industry', 'trucking industries').
inf('fraternity', 'fraternities').
inf('sodality', 'sodalities').
inf('target company', 'target companies').
inf('sleeping beauty', 'sleeping beauties').
inf('battery', 'batteries').
inf('administrative body', 'administrative bodies').
inf('company', 'companies').
inf('family', 'families').
inf('menage a trois', 'menage a trois').
inf('junior varsity', 'junior varsities').
inf('varsity', 'varsities').
inf('church', 'churches').
inf('Christian church', 'Christian churches').
inf('church', 'churches').
inf('Armenian Church', 'Armenian Churches').
inf('Armenian Apostolic Orthodox Church', 'Armenian Apostolic Orthodox Churches').
inf('Catholic Church', 'Catholic Churches').
inf('Western Church', 'Western Churches').
inf('Roman Catholic Church', 'Roman Catholic Churches').
inf('Church of Rome', 'Churches of Rome').
inf('Roman Church', 'Roman Churches').
inf('Nestorian Church', 'Nestorian Churches').
inf('College of Cardinals', 'Colleges of Cardinals').
inf('Old Catholic Church', 'Old Catholic Churches').
inf('Eastern Church', 'Eastern Churches').
inf('Byzantine Church', 'Byzantine Churches').
inf('Orthodox Church', 'Orthodox Churches').
inf('Orthodox Catholic Church', 'Orthodox Catholic Churches').
inf('Eastern Orthodox Church', 'Eastern Orthodox Churches').
inf('Eastern Church', 'Eastern Churches').
inf('Eastern Orthodox', 'Eastern Orthodoxes').
inf('Greek Orthodox Church', 'Greek Orthodox Churches').
inf('Greek Church', 'Greek Churches').
inf('Russian Orthodox Church', 'Russian Orthodox Churches').
inf('Uniat Church', 'Uniat Churches').
inf('Uniate Church', 'Uniate Churches').
inf('Coptic Church', 'Coptic Churches').
inf('Protestant Church', 'Protestant Churches').
inf('Christian Church', 'Christian Churches').
inf('Disciples of Christ', 'Discipless of Christ').
inf('Anglican Church', 'Anglican Churches').
inf('Church of England', 'Churches of England').
inf('Episcopal Church', 'Episcopal Churches').
inf('Protestant Episcopal Church', 'Protestant Episcopal Churches').
inf('Church of Ireland', 'Churches of Ireland').
inf('Episcopal Church', 'Episcopal Churches').
inf('High Church', 'High Churches').
inf('High Anglican Church', 'High Anglican Churches').
inf('Church of Jesus Christ of Latter-Day Saints', 'Churches of Jesus Christ of Latter-Day Saints').
inf('Mormon Church', 'Mormon Churches').
inf('Baptist Church', 'Baptist Churches').
inf('Church of the Brethren', 'Churches of the Brethren').
inf('Church of Christ Scientist', 'Churches of Christ Scientist').
inf('Congregational Church', 'Congregational Churches').
inf('Congregational Christian Church', 'Congregational Christian Churches').
inf('Evangelical and Reformed Church', 'Evangelical and Reformed Churches').
inf('Lutheran Church', 'Lutheran Churches').
inf('Presbyterian Church', 'Presbyterian Churches').
inf('Unitarian Church', 'Unitarian Churches').
inf('Arminian Church', 'Arminian Churches').
inf('Methodist Church', 'Methodist Churches').
inf('Wesleyan Methodist Church', 'Wesleyan Methodist Churches').
inf('Evangelical United Brethren Church', 'Evangelical United Brethren Churches').
inf('United Methodist Church', 'United Methodist Churches').
inf('Mennonite Church', 'Mennonite Churches').
inf('Unification Church', 'Unification Churches').
inf('International Society for Krishna Consciousness', 'International Society for Krishna Consciousnesses').
inf('Church of Scientology', 'Churches of Scientology').
inf('Shinto', 'Shintoes').
inf('Kokka Shinto', 'Kokka Shintoes').
inf('Shuha Shinto', 'Shuha Shintoes').
inf('established church', 'established churches').
inf('variety', 'varieties').
inf('line of descent', 'lines of descent').
inf('ancestry', 'ancestries').
inf('genealogy', 'genealogies').
inf('class', 'classes').
inf('subclass', 'subclasses').
inf('superclass', 'superclasses').
inf('family', 'families').
inf('superfamily', 'superfamilies').
inf('form family', 'form families').
inf('subfamily', 'subfamilies').
inf('genus', 'genera').
inf('subgenus', 'subgenera').
inf('type genus', 'type genera').
inf('form genus', 'form genera').
inf('legal community', 'legal communities').
inf('medical community', 'medical communities').
inf('business community', 'business communities').
inf('community of scholars', 'communities of scholars').
inf('prelacy', 'prelacies').
inf('ministry', 'ministries').
inf('ministry', 'ministries').
inf('Free French', 'Free Frenches').
inf('Fighting French', 'Fighting Frenches').
inf('department of anthropology', 'departments of anthropology').
inf('department of biology', 'departments of biology').
inf('department of chemistry', 'departments of chemistry').
inf('department of computer science', 'departments of computer science').
inf('department of economics', 'departments of economics').
inf('department of English', 'departments of English').
inf('department of history', 'departments of history').
inf('department of linguistics', 'departments of linguistics').
inf('department of mathematics', 'departments of mathematics').
inf('department of philosophy', 'departments of philosophy').
inf('department of physics', 'departments of physics').
inf('department of music', 'departments of music').
inf('department of psychology', 'departments of psychology').
inf('department of sociology', 'departments of sociology').
inf('treasury', 'treasuries').
inf('department of local government', 'departments of local government').
inf('department of corrections', 'departments of corrections').
inf('security', 'securities').
inf('fire company', 'fire companies').
inf('Special Branch', 'Special Branches').
inf('department of the federal government', 'departments of the federal government').
inf('executive agency', 'executive agencies').
inf('Council of Economic Advisors', 'Councils of Economic Advisors').
inf('Center for Disease Control and Prevention', 'Centers for Disease Control and Prevention').
inf('Council on Environmental Policy', 'Councils on Environmental Policy').
inf('Office of Management and Budget', 'Offices of Management and Budget').
inf('Department of Agriculture', 'Departments of Agriculture').
inf('Department of Commerce', 'Departments of Commerce').
inf('Bureau of the Census', 'Bureaux of the Census').
inf('Census Bureau', 'Census Bureaux').
inf('Department of Defense', 'Departments of Defense').
inf('Department of Defense Laboratory System', 'Departments of Defense Laboratory System').
inf('Department of Education', 'Departments of Education').
inf('Department of Energy', 'Departments of Energy').
inf('Department of Energy Intelligence', 'Departments of Energy Intelligence').
inf('Department of Health and Human Services', 'Departments of Health and Human Services').
inf('Department of Homeland Security', 'Departments of Homeland Security').
inf('Department of Housing and Urban Development', 'Departments of Housing and Urban Development').
inf('Department of Justice', 'Departments of Justice').
inf('Bureau of Justice Assistance', 'Bureaux of Justice Assistance').
inf('Bureau of Justice Statistics', 'Bureaux of Justice Statistics').
inf('Department of Labor', 'Departments of Labor').
inf('Department of State', 'Departments of State').
inf('Bureau of Diplomatic Security', 'Bureaux of Diplomatic Security').
inf('Bureau of Intelligence and Research', 'Bureaux of Intelligence and Research').
inf('Department of the Interior', 'Departments of the Interior').
inf('Department of the Treasury', 'Departments of the Treasury').
inf('Bureau of Alcohol Tobacco and Firearms', 'Bureaux of Alcohol Tobacco and Firearms').
inf('Office of Intelligence Support', 'Offices of Intelligence Support').
inf('Comptroller of the Currency', 'Comptrollers of the Currency').
inf('Bureau of Customs', 'Bureaux of Customs').
inf('Customs Bureau', 'Customs Bureaux').
inf('Bureau of Engraving and Printing', 'Bureaux of Engraving and Printing').
inf('Department of Transportation', 'Departments of Transportation').
inf('Department of Veterans Affairs', 'Departments of Veterans Affairs').
inf('Department of Commerce and Labor', 'Departments of Commerce and Labor').
inf('Department of Health Education and Welfare', 'Departments of Health Education and Welfare').
inf('general delivery', 'general deliveries').
inf('instrumentality', 'instrumentalities').
inf('order of Saint Benedict', 'orders of Saint Benedict').
inf('Order of Our Lady of Mount Carmel', 'Orders of Our Lady of Mount Carmel').
inf('Society of Jesus', 'Societys of Jesus').
inf('Society of Friends', 'Societys of Friends').
inf('Assemblies of God', 'Assembliess of God').
inf('Vaudois', 'Vaudois').
inf('clergy', 'clergies').
inf('laity', 'laities').
inf('temporalty', 'temporalties').
inf('royalty', 'royalties').
inf('royal family', 'royal families').
inf('Bourbon dynasty', 'Bourbon dynasties').
inf('Capetian dynasty', 'Capetian dynasties').
inf('Carolingian dynasty', 'Carolingian dynasties').
inf('Carlovingian dynasty', 'Carlovingian dynasties').
inf('Flavian dynasty', 'Flavian dynasties').
inf('Han dynasty', 'Han dynasties').
inf('House of Hanover', 'Houses of Hanover').
inf('House of Lancaster', 'Houses of Lancaster').
inf('Liao dynasty', 'Liao dynasties').
inf('Merovingian dynasty', 'Merovingian dynasties').
inf('Ming dynasty', 'Ming dynasties').
inf('Ottoman', 'Ottomen').
inf('Ottoman dynasty', 'Ottoman dynasties').
inf('Ptolemaic dynasty', 'Ptolemaic dynasties').
inf('Qin dynasty', 'Qin dynasties').
inf('Ch\'in dynasty', 'Ch\'in dynasties').
inf('Qing dynasty', 'Qing dynasties').
inf('Ch\'ing dynasty', 'Ch\'ing dynasties').
inf('Manchu dynasty', 'Manchu dynasties').
inf('Shang dynasty', 'Shang dynasties').
inf('Sung dynasty', 'Sung dynasties').
inf('Song dynasty', 'Song dynasties').
inf('Tang dynasty', 'Tang dynasties').
inf('House of Tudor', 'Houses of Tudor').
inf('Valois', 'Valois').
inf('Wei dynasty', 'Wei dynasties').
inf('House of Windsor', 'Houses of Windsor').
inf('House of York', 'Houses of York').
inf('Yuan dynasty', 'Yuan dynasties').
inf('Mongol dynasty', 'Mongol dynasties').
inf('citizenry', 'citizenries').
inf('Congress', 'Congresses').
inf('United States Congress', 'United States Congresses').
inf('U.S. Congress', 'U.S. Congresses').
inf('US Congress', 'US Congresses').
inf('House of Representatives', 'Houses of Representatives').
inf('House of Burgesses', 'Houses of Burgesses').
inf('House of Commons', 'Houses of Commons').
inf('House of Lords', 'Houses of Lords').
inf('legislative assembly', 'legislative assemblies').
inf('legislative body', 'legislative bodies').
inf('general assembly', 'general assemblies').
inf('assembly', 'assemblies').
inf('governing body', 'governing bodies').
inf('brass', 'brasses').
inf('top brass', 'top brasses').
inf('judiciary', 'judiciaries').
inf('bench', 'benches').
inf('judiciary', 'judiciaries').
inf('judicatory', 'judicatories').
inf('country', 'countries').
inf('commonwealth country', 'commonwealth countries').
inf('developing country', 'developing countries').
inf('estate of the realm', 'estates of the realm').
inf('foreign country', 'foreign countries').
inf('federation of tribes', 'federations of tribes').
inf('country', 'countries').
inf('Reich', 'Reiches').
inf('Second Reich', 'Second Reiches').
inf('Third Reich', 'Third Reiches').
inf('axis', 'axes').
inf('Axis', 'Axes').
inf('Organization for the Prohibition of Chemical Weapons', 'Organizations for the Prohibition of Chemical Weapons').
inf('Organization of American States', 'Organizations of American States').
inf('Organization of Petroleum-Exporting Countries', 'Organizations of Petroleum-Exporting Countries').
inf('rich', 'riches').
inf('mass', 'masses').
inf('admass', 'admasses').
inf('working class', 'working classes').
inf('lower class', 'lower classes').
inf('underclass', 'underclasses').
inf('middle class', 'middle classes').
inf('commonalty', 'commonalties').
inf('commonality', 'commonalities').
inf('petit bourgeois', 'petit bourgeois').
inf('peasantry', 'peasantries').
inf('army', 'armies').
inf('crush', 'crushes').
inf('press', 'presses').
inf('company', 'companies').
inf('limited company', 'limited companies').
inf('holding company', 'holding companies').
inf('bank holding company', 'bank holding companies').
inf('multibank holding company', 'multibank holding companies').
inf('utility', 'utilities').
inf('public utility', 'public utilities').
inf('public utility company', 'public utility companies').
inf('telephone company', 'telephone companies').
inf('phone company', 'phone companies').
inf('telco', 'telcoes').
inf('power company', 'power companies').
inf('electric company', 'electric companies').
inf('light company', 'light companies').
inf('water company', 'water companies').
inf('gas company', 'gas companies').
inf('bus company', 'bus companies').
inf('livery company', 'livery companies').
inf('company', 'companies').
inf('opera company', 'opera companies').
inf('theater company', 'theater companies').
inf('stock company', 'stock companies').
inf('repertory company', 'repertory companies').
inf('ballet company', 'ballet companies').
inf('minstrelsy', 'minstrelsies').
inf('enemy', 'enemies').
inf('army', 'armies').
inf('regular army', 'regular armies').
inf('navy', 'navies').
inf('Office of Naval Intelligence', 'Offices of Naval Intelligence').
inf('League of Nations', 'Leagues of Nations').
inf('military', 'militaries').
inf('paramilitary', 'paramilitaries').
inf('bench', 'benches').
inf('constabulary', 'constabularies').
inf('gendarmery', 'gendarmeries').
inf('Gestapo', 'Gestapoes').
inf('company', 'companies').
inf('yeomanry', 'yeomanries').
inf('brass', 'brasses').
inf('day watch', 'day watches').
inf('ship\'s company', 'ship\'s companies').
inf('company', 'companies').
inf('gallery', 'galleries').
inf('parish', 'parishes').
inf('community', 'communities').
inf('community', 'communities').
inf('speech community', 'speech communities').
inf('municipality', 'municipalities').
inf('city', 'cities').
inf('society', 'societies').
inf('family', 'families').
inf('Veterans of Foreign Wars', 'Veteranss of Foreign Wars').
inf('fraternity', 'fraternities').
inf('sorority', 'sororities').
inf('Congress of Industrial Organizations', 'Congresses of Industrial Organizations').
inf('secret society', 'secret societies').
inf('cast of characters', 'casts of characters').
inf('constituency', 'constituencies').
inf('class', 'classes').
inf('class', 'classes').
inf('graduating class', 'graduating classes').
inf('master class', 'master classes').
inf('senior class', 'senior classes').
inf('junior class', 'junior classes').
inf('sophomore class', 'sophomore classes').
inf('freshman class', 'freshman classes').
inf('class', 'classes').
inf('junto', 'juntoes').
inf('work party', 'work parties').
inf('family', 'families').
inf('yeomanry', 'yeomanries').
inf('symphony', 'symphonies').
inf('combo', 'comboes').
inf('conspiracy', 'conspiracies').
inf('confederacy', 'confederacies').
inf('party', 'parties').
inf('shindy', 'shindies').
inf('masquerade party', 'masquerade parties').
inf('dinner party', 'dinner parties').
inf('gaudy', 'gaudies').
inf('garden party', 'garden parties').
inf('lawn party', 'lawn parties').
inf('bachelor party', 'bachelor parties').
inf('stag party', 'stag parties').
inf('hen party', 'hen parties').
inf('slumber party', 'slumber parties').
inf('wedding party', 'wedding parties').
inf('party', 'parties').
inf('political party', 'political parties').
inf('labor party', 'labor parties').
inf('labour party', 'labour parties').
inf('war party', 'war parties').
inf('third party', 'third parties').
inf('party', 'parties').
inf('company', 'companies').
inf('fatigue party', 'fatigue parties').
inf('landing party', 'landing parties').
inf('party to the action', 'parties to the action').
inf('party to the transaction', 'parties to the transaction').
inf('rescue party', 'rescue parties').
inf('search party', 'search parties').
inf('stretcher party', 'stretcher parties').
inf('war party', 'war parties').
inf('matrix', 'matrices').
inf('dot matrix', 'dot matrices').
inf('square matrix', 'square matrices').
inf('diagonal matrix', 'diagonal matrices').
inf('scalar matrix', 'scalar matrices').
inf('identity matrix', 'identity matrices').
inf('unit matrix', 'unit matrices').
inf('nonsingular matrix', 'nonsingular matrices').
inf('real matrix', 'real matrices').
inf('singular matrix', 'singular matrices').
inf('galaxy', 'galaxies').
inf('galaxy', 'galaxies').
inf('spiral galaxy', 'spiral galaxies').
inf('Andromeda galaxy', 'Andromeda galaxies').
inf('phalanx', 'phalanges').
inf('phalanx', 'phalanges').
inf('bunch', 'bunches').
inf('academy', 'academies').
inf('academy', 'academies').
inf('police academy', 'police academies').
inf('military academy', 'military academies').
inf('naval academy', 'naval academies').
inf('air force academy', 'air force academies').
inf('academy', 'academies').
inf('honorary society', 'honorary societies').
inf('Academy of Motion Picture Arts and Sciences', 'Academys of Motion Picture Arts and Sciences').
inf('Academy of Television Arts and Sciences', 'Academys of Television Arts and Sciences').
inf('school of dentistry', 'schools of dentistry').
inf('school of law', 'schools of law').
inf('school of medicine', 'schools of medicine').
inf('school of music', 'schools of music').
inf('school of nursing', 'schools of nursing').
inf('seminary', 'seminaries').
inf('seminary', 'seminaries').
inf('tech', 'teches').
inf('university', 'universities').
inf('university', 'universities').
inf('multiversity', 'multiversities').
inf('varsity', 'varsities').
inf('conservatory', 'conservatories').
inf('faculty', 'faculties').
inf('open society', 'open societies').
inf('tribal society', 'tribal societies').
inf('hunting and gathering society', 'hunting and gathering societies').
inf('Commonwealth of Independent States', 'Commonwealths of Independent States').
inf('deliberative assembly', 'deliberative assemblies').
inf('Commission on Human Rights', 'Commissions on Human Rights').
inf('Commission on Narcotic Drugs', 'Commissions on Narcotic Drugs').
inf('Commission on the Status of Women', 'Commissions on the Status of Women').
inf('United Nations agency', 'United Nations agencies').
inf('UN agency', 'UN agencies').
inf('Centre for International Crime Prevention', 'Centres for International Crime Prevention').
inf('confederacy', 'confederacies').
inf('enosis', 'enoses').
inf('League of Iroquois', 'Leagues of Iroquois').
inf('Benelux', 'Beneluxes').
inf('ally', 'allies').
inf('brahman', 'brahmen').
inf('congress', 'congresses').
inf('Congress of Racial Equality', 'Congresses of Racial Equality').
inf('Council of Ephesus', 'Councils of Ephesus').
inf('Council of Chalcedon', 'Councils of Chalcedon').
inf('Council of Vienne', 'Councils of Vienne').
inf('Council of Constance', 'Councils of Constance').
inf('Council of Basel-Ferrara-Florence', 'Councils of Basel-Ferrara-Florence').
inf('Council of Trent', 'Councils of Trent').
inf('Vatican I', 'Vatican we').
inf('Continental Congress', 'Continental Congresses').
inf('congress', 'congresses').
inf('chamber of commerce', 'chambers of commerce').
inf('board of appeals', 'boards of appeals').
inf('board of selectmen', 'boards of selectmen').
inf('board of regents', 'boards of regents').
inf('board of trustees', 'boards of trustees').
inf('conservancy', 'conservancies').
inf('politburo', 'politburoes').
inf('Bench', 'Benches').
inf('court of appeals', 'courts of appeals').
inf('court of assize', 'courts of assize').
inf('court of assize and nisi prius', 'courts of assize and nisi prius').
inf('chancery', 'chanceries').
inf('court of chancery', 'courts of chancery').
inf('consistory', 'consistories').
inf('court of domestic relations', 'courts of domestic relations').
inf('Congregation of the Inquisition', 'Congregations of the Inquisition').
inf('repertory', 'repertories').
inf('repertory', 'repertories').
inf('agency', 'agencies').
inf('federal agency', 'federal agencies').
inf('government agency', 'government agencies').
inf('bureau', 'bureaux').
inf('authority', 'authorities').
inf('independent agency', 'independent agencies').
inf('intelligence agency', 'intelligence agencies').
inf('military intelligence agency', 'military intelligence agencies').
inf('United States intelligence agency', 'United States intelligence agencies').
inf('international intelligence agency', 'international intelligence agencies').
inf('Department of Justice Canada', 'Departments of Justice Canada').
inf('Directorate for Inter-Services Intelligence', 'Directorates for Inter-Services Intelligence').
inf('international law enforcement agency', 'international law enforcement agencies').
inf('A\'man', 'A\'men').
inf('law enforcement agency', 'law enforcement agencies').
inf('admiralty', 'admiralties').
inf('Office of Inspector General', 'Offices of Inspector General').
inf('Bank of England', 'Banks of England').
inf('Bank of Japan', 'Banks of Japan').
inf('redevelopment authority', 'redevelopment authorities').
inf('regulatory agency', 'regulatory agencies').
inf('regulatory authority', 'regulatory authorities').
inf('weather bureau', 'weather bureaux').
inf('advertising agency', 'advertising agencies').
inf('ad agency', 'ad agencies').
inf('credit bureau', 'credit bureaux').
inf('detective agency', 'detective agencies').
inf('employment agency', 'employment agencies').
inf('mercantile agency', 'mercantile agencies').
inf('commercial agency', 'commercial agencies').
inf('news agency', 'news agencies').
inf('press agency', 'press agencies').
inf('service agency', 'service agencies').
inf('service bureau', 'service bureaux').
inf('travel agency', 'travel agencies').
inf('executive branch', 'executive branches').
inf('legislative branch', 'legislative branches').
inf('judicial branch', 'judicial branches').
inf('rally', 'rallies').
inf('pep rally', 'pep rallies').
inf('political entity', 'political entities').
inf('amphictyony', 'amphictyonies').
inf('autocracy', 'autocracies').
inf('autarchy', 'autarchies').
inf('democracy', 'democracies').
inf('diarchy', 'diarchies').
inf('dyarchy', 'dyarchies').
inf('gerontocracy', 'gerontocracies').
inf('gynecocracy', 'gynecocracies').
inf('gynarchy', 'gynarchies').
inf('hegemony', 'hegemonies').
inf('mobocracy', 'mobocracies').
inf('ochlocracy', 'ochlocracies').
inf('oligarchy', 'oligarchies').
inf('plutocracy', 'plutocracies').
inf('technocracy', 'technocracies').
inf('theocracy', 'theocracies').
inf('hierocracy', 'hierocracies').
inf('parliamentary democracy', 'parliamentary democracies').
inf('monarchy', 'monarchies').
inf('parliamentary monarchy', 'parliamentary monarchies').
inf('capitalist economy', 'capitalist economies').
inf('black economy', 'black economies').
inf('market economy', 'market economies').
inf('laissez-faire economy', 'laissez-faire economies').
inf('mixed economy', 'mixed economies').
inf('non-market economy', 'non-market economies').
inf('socialist economy', 'socialist economies').
inf('economy', 'economies').
inf('managed economy', 'managed economies').
inf('communist economy', 'communist economies').
inf('form of government', 'forms of government').
inf('posterity', 'posterities').
inf('posterity', 'posterities').
inf('moiety', 'moieties').
inf('tableau', 'tableaux').
inf('Tribes of Israel', 'Tribess of Israel').
inf('colony', 'colonies').
inf('proprietary colony', 'proprietary colonies').
inf('lobby', 'lobbies').
inf('lobby', 'lobbies').
inf('hierarchy', 'hierarchies').
inf('hierarchy', 'hierarchies').
inf('celestial hierarchy', 'celestial hierarchies').
inf('data hierarchy', 'data hierarchies').
inf('taxonomy', 'taxonomies').
inf('board of directors', 'boards of directors').
inf('stock company', 'stock companies').
inf('joint-stock company', 'joint-stock companies').
inf('family business', 'family businesses').
inf('shell entity', 'shell entities').
inf('argosy', 'argosies').
inf('upper class', 'upper classes').
inf('gentry', 'gentries').
inf('aristocracy', 'aristocracies').
inf('clerisy', 'clerisies').
inf('landed gentry', 'landed gentries').
inf('squirearchy', 'squirearchies').
inf('ruling class', 'ruling classes').
inf('people in power', 'peoples in power').
inf('society', 'societies').
inf('high society', 'high societies').
inf('nobility', 'nobilities').
inf('aristocracy', 'aristocracies').
inf('artillery', 'artilleries').
inf('musketry', 'musketries').
inf('battery', 'batteries').
inf('cavalry', 'cavalries').
inf('horse cavalry', 'horse cavalries').
inf('mechanized cavalry', 'mechanized cavalries').
inf('infantry', 'infantries').
inf('foot', 'feet').
inf('National Guard Bureau', 'National Guard Bureaux').
inf('standing army', 'standing armies').
inf('Army of the Confederacy', 'Armys of the Confederacy').
inf('soldiery', 'soldieries').
inf('friendly', 'friendlies').
inf('cavalry', 'cavalries').
inf('horse cavalry', 'horse cavalries').
inf('miscellany', 'miscellanies').
inf('variety', 'varieties').
inf('farrago', 'farragoes').
inf('mishmash', 'mishmashes').
inf('hotchpotch', 'hotchpotches').
inf('gallimaufry', 'gallimaufries').
inf('batch', 'batches').
inf('clutch', 'clutches').
inf('batch', 'batches').
inf('clutch', 'clutches').
inf('branch', 'branches').
inf('business', 'businesses').
inf('smart money', 'smart monies').
inf('trash', 'trashes').
inf('delegacy', 'delegacies').
inf('embassy', 'embassies').
inf('Iraqi National Congress', 'Iraqi National Congresses').
inf('commando', 'commandoes').
inf('rogue\'s gallery', 'rogue\'s galleries').
inf('rogue\'s gallery', 'rogue\'s galleries').
inf('charity', 'charities').
inf('public charity', 'public charities').
inf('expo', 'expoes').
inf('working party', 'working parties').
inf('board of education', 'boards of education').
inf('jury', 'juries').
inf('jury', 'juries').
inf('grand jury', 'grand juries').
inf('hung jury', 'hung juries').
inf('petit jury', 'petit juries').
inf('petty jury', 'petty juries').
inf('special jury', 'special juries').
inf('blue ribbon jury', 'blue ribbon juries').
inf('bevy', 'bevies').
inf('immigrant class', 'immigrant classes').
inf('banking company', 'banking companies').
inf('finance company', 'finance companies').
inf('consumer finance company', 'consumer finance companies').
inf('small loan company', 'small loan companies').
inf('industrial loan company', 'industrial loan companies').
inf('captive finance company', 'captive finance companies').
inf('sales finance company', 'sales finance companies').
inf('commercial finance company', 'commercial finance companies').
inf('commercial credit company', 'commercial credit companies').
inf('building society', 'building societies').
inf('child welfare agency', 'child welfare agencies').
inf('trust company', 'trust companies').
inf('firing party', 'firing parties').
inf('march', 'marches').
inf('hunger march', 'hunger marches').
inf('secondary', 'secondaries').
inf('line of march', 'lines of march').
inf('line of succession', 'lines of succession').
inf('machinery', 'machineries').
inf('syntax', 'syntaxes').
inf('body', 'bodies').
inf('botany', 'botanies').
inf('brush', 'brushes').
inf('bush', 'bushes').
inf('underbrush', 'underbrushes').
inf('shrubbery', 'shrubberies').
inf('monocracy', 'monocracies').
inf('tyranny', 'tyrannies').
inf('law of nations', 'laws of nations').
inf('law of the land', 'laws of the land').
inf('Law of Moses', 'Laws of Moses').
inf('bureaucracy', 'bureaucracies').
inf('rash', 'rashes').
inf('panoply', 'panoplies').
inf('trinketry', 'trinketries').
inf('troponymy', 'troponymies').
inf('troponomy', 'troponomies').
inf('deco', 'decoes').
inf('art deco', 'art decoes').
inf('art nouveau', 'art nouveaux').
inf('needy', 'needies').
inf('Age of Reason', 'Ages of Reason').
inf('Lubavitch', 'Lubavitches').
inf('Chabad-Lubavitch', 'Chabad-Lubavitches').
inf('Nation of Islam', 'Nations of Islam').
inf('woman', 'women').
inf('fair sex', 'fair sexes').
inf('record company', 'record companies').
inf('moving company', 'moving companies').
inf('removal company', 'removal companies').
inf('think factory', 'think factories').
inf('vestry', 'vestries').
inf('Zhou dynasty', 'Zhou dynasties').
inf('Chou dynasty', 'Chou dynasties').
inf('Chow dynasty', 'Chow dynasties').
inf('local authority', 'local authorities').
inf('Flemish', 'Flemishes').
inf('Committee for State Security', 'Committees for State Security').
inf('Federal Security Bureau', 'Federal Security Bureaux').
inf('Russian agency', 'Russian agencies').
inf('Association for the Advancement of Retired Persons', 'Associations for the Advancement of Retired Persons').
inf('Association of Southeast Asian Nations', 'Associations of Southeast Asian Nations').
inf('address', 'addresses').
inf('mailing address', 'mailing addresses').
inf('PO Box No', 'PO Box Noes').
inf('street address', 'street addresses').
inf('aery', 'aeries').
inf('eyry', 'eyries').
inf('territory', 'territories').
inf('approach', 'approaches').
inf('apex', 'apexes').
inf('solar apex', 'solar apexes').
inf('apex of the sun\'s way', 'apices of the sun\'s way').
inf('antapex', 'antapexes').
inf('apoapsis', 'apoapses').
inf('point of apoapsis', 'points of apoapsis').
inf('country', 'countries').
inf('high country', 'high countries').
inf('back of beyond', 'backs of beyond').
inf('colony', 'colonies').
inf('dependency', 'dependencies').
inf('back country', 'back countries').
inf('bush', 'bushes').
inf('oasis', 'oases').
inf('field of battle', 'fields of battle').
inf('field of honor', 'fields of honor').
inf('jumping-off place', 'jumpings-off place').
inf('point of departure', 'points of departure').
inf('mother country', 'mother countries').
inf('country of origin', 'countries of origin').
inf('place of birth', 'places of birth').
inf('place of origin', 'places of origin').
inf('underbelly', 'underbellies').
inf('foot', 'feet').
inf('boundary', 'boundaries').
inf('march', 'marches').
inf('property', 'properties').
inf('colony', 'colonies').
inf('Line of Control', 'Lines of Control').
inf('state boundary', 'state boundaries').
inf('buffer country', 'buffer countries').
inf('equinox', 'equinoxes').
inf('vernal equinox', 'vernal equinoxes').
inf('autumnal equinox', 'autumnal equinoxes').
inf('vault of heaven', 'vaults of heaven').
inf('cemetery', 'cemeteries').
inf('center of buoyancy', 'centers of buoyancy').
inf('centre of buoyancy', 'centres of buoyancy').
inf('center of immersion', 'centers of immersion').
inf('centre of immersion', 'centres of immersion').
inf('center of gravity', 'centers of gravity').
inf('centre of gravity', 'centres of gravity').
inf('center of flotation', 'centers of flotation').
inf('centre of flotation', 'centres of flotation').
inf('center of mass', 'centers of mass').
inf('centre of mass', 'centres of mass').
inf('central city', 'central cities').
inf('city', 'cities').
inf('purlieu', 'purlieu').
inf('city', 'cities').
inf('inner city', 'inner cities').
inf('country', 'countries').
inf('county', 'counties').
inf('county', 'counties').
inf('ghetto', 'ghettoes').
inf('abbacy', 'abbacies').
inf('archdeaconry', 'archdeaconries').
inf('eparchy', 'eparchies').
inf('theater of war', 'theaters of war').
inf('theatre of war', 'theatres of war').
inf('field of operations', 'fields of operations').
inf('theater of operations', 'theaters of operations').
inf('theatre of operations', 'theatres of operations').
inf('zone of interior', 'zones of interior').
inf('territory', 'territories').
inf('community', 'communities').
inf('planned community', 'planned communities').
inf('retirement community', 'retirement communities').
inf('retirement complex', 'retirement complexes').
inf('bedroom community', 'bedroom communities').
inf('archduchy', 'archduchies').
inf('barony', 'baronies').
inf('duchy', 'duchies').
inf('grand duchy', 'grand duchies').
inf('viscounty', 'viscounties').
inf('principality', 'principalities').
inf('Kingdom of God', 'Kingdoms of God').
inf('suzerainty', 'suzerainties').
inf('home from home', 'homes from homes').
inf('business address', 'business addresses').
inf('dude ranch', 'dude ranches').
inf('earreach', 'earreaches').
inf('finish', 'finishes').
inf('extremity', 'extremities').
inf('field of fire', 'fields of fire').
inf('ghetto', 'ghettoes').
inf('ground zero', 'ground zeroes').
inf('ground zero', 'ground zeroes').
inf('hatchery', 'hatcheries').
inf('hell on earth', 'hells on earth').
inf('inferno', 'infernoes').
inf('heronry', 'heronries').
inf('hole-in-the-wall', 'holes-in-the-wall').
inf('holy', 'holies').
inf('home territory', 'home territories').
inf('justiciary', 'justiciaries').
inf('belly', 'bellies').
inf('turf', 'turves').
inf('line of battle', 'lines of battle').
inf('line of flight', 'lines of flight').
inf('line of march', 'lines of march').
inf('line of sight', 'lines of sight').
inf('line of vision', 'lines of vision').
inf('line of latitude', 'lines of latitude').
inf('parallel of latitude', 'parallels of latitude').
inf('mandatory', 'mandatories').
inf('market cross', 'market crosses').
inf('line of longitude', 'lines of longitude').
inf('Gates of the Arctic National Park', 'Gatess of the Arctic National Park').
inf('old country', 'old countries').
inf('out-of-doors', 'outs-of-doors').
inf('paddy', 'paddies').
inf('rice paddy', 'rice paddies').
inf('parish', 'parishes').
inf('itinerary', 'itineraries').
inf('line of fire', 'lines of fire').
inf('periapsis', 'periapses').
inf('point of periapsis', 'points of periapsis').
inf('pride of place', 'prides of place').
inf('anomaly', 'anomalies').
inf('locus of infection', 'locuss of infection').
inf('pitch', 'pitches').
inf('municipality', 'municipalities').
inf('perch', 'perches').
inf('reach', 'reaches').
inf('port of entry', 'ports of entry').
inf('point of entry', 'points of entry').
inf('port of call', 'ports of call').
inf('vicinity', 'vicinities').
inf('locality', 'localities').
inf('neck of the woods', 'necks of the woods').
inf('proximity', 'proximities').
inf('safety', 'safeties').
inf('rookery', 'rookeries').
inf('country', 'countries').
inf('darkness', 'darknesses').
inf('field of honor', 'fields of honor').
inf('scenery', 'sceneries').
inf('shrubbery', 'shrubberies').
inf('line of scrimmage', 'lines of scrimmage').
inf('out of bounds', 'outs of bounds').
inf('vacuity', 'vacuities').
inf('sphere of influence', 'spheres of influence').
inf('scratch', 'scratches').
inf('eparchy', 'eparchies').
inf('ground zero', 'ground zeroes').
inf('laboratory', 'laboratories').
inf('pueblo', 'puebloes').
inf('piece of land', 'pieces of land').
inf('piece of ground', 'pieces of ground').
inf('parcel of land', 'parcels of land').
inf('plot of land', 'plots of land').
inf('plot of ground', 'plots of ground').
inf('patch', 'patches').
inf('Tropic of Cancer', 'Tropics of Cancer').
inf('Tropic of Capricorn', 'Tropics of Capricorn').
inf('trust territory', 'trust territories').
inf('vertex', 'vertexes').
inf('apex', 'apexes').
inf('viceroyalty', 'viceroyalties').
inf('tendency', 'tendencies').
inf('wilderness', 'wildernesses').
inf('sign of the zodiac', 'signs of the zodiac').
inf('Virgo', 'Virgoes').
inf('Fish', 'Fishes').
inf('turf', 'turves').
inf('European country', 'European countries').
inf('Scandinavian country', 'Scandinavian countries').
inf('Balkan country', 'Balkan countries').
inf('African country', 'African countries').
inf('Republic of Namibia', 'Republics of Namibia').
inf('Asian country', 'Asian countries').
inf('South American country', 'South American countries').
inf('North American country', 'North American countries').
inf('Central American country', 'Central American countries').
inf('capital of Afghanistan', 'capitals of Afghanistan').
inf('Republic of Albania', 'Republics of Albania').
inf('Durazzo', 'Durazzoes').
inf('Hippo', 'Hippoes').
inf('Republic of Angola', 'Republics of Angola').
inf('Huambo', 'Huamboes').
inf('Lobito', 'Lobitoes').
inf('capital of Antigua and Barbuda', 'capitals of Antigua and Barbuda').
inf('capital of Argentina', 'capitals of Argentina').
inf('Republic of Bulgaria', 'Republics of Bulgaria').
inf('Dobrich', 'Dobriches').
inf('Union of Burma', 'Unions of Burma').
inf('Republic of Burundi', 'Republics of Burundi').
inf('capital of Burundi', 'capitals of Burundi').
inf('Kingdom of Cambodia', 'Kingdoms of Cambodia').
inf('Republic of Cameroon', 'Republics of Cameroon').
inf('capital of Cameroon', 'capitals of Cameroon').
inf('Republic of Cape Verde', 'Republics of Cape Verde').
inf('capital of Cape Verde', 'capitals of Cape Verde').
inf('capital of Central Africa', 'capitals of Central Africa').
inf('Colombo', 'Colomboes').
inf('capital of Sri Lanka', 'capitals of Sri Lanka').
inf('Republic of Chad', 'Republics of Chad').
inf('capital of Chad', 'capitals of Chad').
inf('Republic of Chile', 'Republics of Chile').
inf('Gran Santiago', 'Gran Santiagoes').
inf('Santiago', 'Santiagoes').
inf('capital of Chile', 'capitals of Chile').
inf('Temuco', 'Temucoes').
inf('Valparaiso', 'Valparaisoes').
inf('Tierra del Fuego', 'Tierra del Fuegoes').
inf('capital of Red China', 'capitals of Red China').
inf('Republic of China', 'Republics of China').
inf('capital of Taiwan', 'capitals of Taiwan').
inf('Republic of Colombia', 'Republics of Colombia').
inf('capital of Colombia', 'capitals of Colombia').
inf('Congo', 'Congoes').
inf('Republic of the Congo', 'Republics of the Congo').
inf('French Congo', 'French Congoes').
inf('Congo', 'Congoes').
inf('Democratic Republic of the Congo', 'Democratic Republic of the Congoes').
inf('Belgian Congo', 'Belgian Congoes').
inf('Republic of Costa Rica', 'Republics of Costa Rica').
inf('capital of Costa Rica', 'capitals of Costa Rica').
inf('Republic of Cote d\'Ivoire', 'Republics of Cote d\'Ivoire').
inf('Yamoussukro', 'Yamoussukroes').
inf('Republic of Guatemala', 'Republics of Guatemala').
inf('capital of Guatemala', 'capitals of Guatemala').
inf('Republic of Honduras', 'Republics of Honduras').
inf('Republic of El Salvador', 'Republics of El Salvador').
inf('Republic of Nicaragua', 'Republics of Nicaragua').
inf('capital of Nicaragua', 'capitals of Nicaragua').
inf('Republic of Panama', 'Republics of Panama').
inf('capital of Panama', 'capitals of Panama').
inf('Mexico', 'Mexicoes').
inf('Acapulco', 'Acapulcoes').
inf('Durango', 'Durangoes').
inf('Victoria de Durango', 'Victoria de Durangoes').
inf('Hermosillo', 'Hermosilloes').
inf('Ciudad de Mexico', 'Ciudad de Mexicoes').
inf('capital of Mexico', 'capitals of Mexico').
inf('Tabasco', 'Tabascoes').
inf('Tampico', 'Tampicoes').
inf('Republic of Cuba', 'Republics of Cuba').
inf('capital of Cuba', 'capitals of Cuba').
inf('Santiago', 'Santiagoes').
inf('Guantanamo', 'Guantanamoes').
inf('Republic of Haiti', 'Republics of Haiti').
inf('Santo Domingo', 'Santo Domingoes').
inf('Ciudad Trujillo', 'Ciudad Trujilloes').
inf('capital of the Dominican Republic', 'capitals of the Dominican Republic').
inf('Santiago', 'Santiagoes').
inf('Puerto Rico', 'Puerto Ricoes').
inf('Porto Rico', 'Porto Ricoes').
inf('Puerto Rico', 'Puerto Ricoes').
inf('Porto Rico', 'Porto Ricoes').
inf('Commonwealth of Puerto Rico', 'Commonwealths of Puerto Rico').
inf('capital of Jamaica', 'capitals of Jamaica').
inf('capital of Barbados', 'capitals of Barbados').
inf('Tobago', 'Tobagoes').
inf('Trinidad and Tobago', 'Trinidad and Tobagoes').
inf('Republic of Trinidad and Tobago', 'Republics of Trinidad and Tobago').
inf('Port of Spain', 'Ports of Spain').
inf('Port-of-Spain', 'Ports-of-Spain').
inf('capital of Trinidad and Tobago', 'capitals of Trinidad and Tobago').
inf('Republic of Cyprus', 'Republics of Cyprus').
inf('capital of Cyprus', 'capitals of Cyprus').
inf('Brno', 'Brnoes').
inf('capital of Slovakia', 'capitals of Slovakia').
inf('Republic of Benin', 'Republics of Benin').
inf('Porto Novo', 'Porto Novoes').
inf('capital of Benin', 'capitals of Benin').
inf('Togo', 'Togoes').
inf('capital of Togo', 'capitals of Togo').
inf('Kingdom of Denmark', 'Kingdoms of Denmark').
inf('Republic of Djibouti', 'Republics of Djibouti').
inf('capital of Djibouti', 'capitals of Djibouti').
inf('Commonwealth of Dominica', 'Commonwealths of Dominica').
inf('Roseau', 'Roseaux').
inf('Republic of Equatorial Guinea', 'Republics of Equatorial Guinea').
inf('Malabo', 'Malaboes').
inf('Bioko', 'Biokoes').
inf('Kingdom of Norway', 'Kingdoms of Norway').
inf('Oslo', 'Osloes').
inf('capital of Norway', 'capitals of Norway').
inf('Kingdom of Sweden', 'Kingdoms of Sweden').
inf('capital of Sweden', 'capitals of Sweden').
inf('Malmo', 'Malmoes').
inf('Frankfurt on the Main', 'Frankfurts on the Main').
inf('Munich', 'Muniches').
inf('Republic of Ecuador', 'Republics of Ecuador').
inf('Quito', 'Quitoes').
inf('capital of Ecuador', 'capitals of Ecuador').
inf('State of Eritrea', 'States of Eritrea').
inf('capital of Ethiopia', 'capitals of Ethiopia').
inf('Republic of Fiji', 'Republics of Fiji').
inf('Republic of Finland', 'Republics of Finland').
inf('capital of Finland', 'capitals of Finland').
inf('Dodecanese', 'Dodecanese').
inf('capital of Greece', 'capitals of Greece').
inf('Peloponnese', 'Peloponnese').
inf('State of Israel', 'States of Israel').
inf('Akko', 'Akkoes').
inf('Accho', 'Acchoes').
inf('capital of Israel', 'capitals of Israel').
inf('Tel Aviv-Yalo', 'Tel Aviv-Yaloes').
inf('Yafo', 'Yafoes').
inf('Ticino', 'Ticinoes').
inf('Bolzano', 'Bolzanoes').
inf('capital of Italy', 'capitals of Italy').
inf('Milano', 'Milanoes').
inf('Torino', 'Torinoes').
inf('Palermo', 'Palermoes').
inf('Cape Passero', 'Cape Passeroes').
inf('Agrigento', 'Agrigentoes').
inf('Trento', 'Trentoes').
inf('Veneto', 'Venetoes').
inf('capital of Romania', 'capitals of Romania').
inf('capital of Rwanda', 'capitals of Rwanda').
inf('Republic of Croatia', 'Republics of Croatia').
inf('Serbia and Montenegro', 'Serbia and Montenegroes').
inf('Union of Serbia and Montenegro', 'Unions of Serbia and Montenegro').
inf('Kosovo', 'Kosovoes').
inf('Montenegro', 'Montenegroes').
inf('capital of Serbia and Montenegro', 'capitals of Serbia and Montenegro').
inf('Republic of Bosnia and Herzegovina', 'Republics of Bosnia and Herzegovina').
inf('Sarajevo', 'Sarajevoes').
inf('Republic of Slovenia', 'Republics of Slovenia').
inf('Laurentian Plateau', 'Laurentian Plateaux').
inf('Nanaimo', 'Nanaimoes').
inf('Arctic Archipelago', 'Arctic Archipelagoes').
inf('Yellowknife', 'Yellowknives').
inf('Halifax', 'Halifaxes').
inf('capital of Canada', 'capitals of Canada').
inf('Toronto', 'Torontoes').
inf('Commonwealth of Australia', 'Commonwealths of Australia').
inf('capital of Australia', 'capitals of Australia').
inf('Republic of the Marshall Islands', 'Republics of the Marshall Islands').
inf('Republic of Kiribati', 'Republics of Kiribati').
inf('Republic of Nauru', 'Republics of Nauru').
inf('Malay Archipelago', 'Malay Archipelagoes').
inf('Bismarck Archipelago', 'Bismarck Archipelagoes').
inf('capital of Papua New Guinea', 'capitals of Papua New Guinea').
inf('Republic of Austria', 'Republics of Austria').
inf('Oesterreich', 'Oesterreiches').
inf('capital of Austria', 'capitals of Austria').
inf('Commonwealth of the Bahamas', 'Commonwealths of the Bahamas').
inf('capital of the Bahamas', 'capitals of the Bahamas').
inf('State of Bahrain', 'States of Bahrain').
inf('capital of Bahrain', 'capitals of Bahrain').
inf('Bangladesh', 'Bangladeshes').
inf('People\'s Republic of Bangladesh', 'People\'s Republic of Bangladeshes').
inf('Bangla Desh', 'Bangla Deshes').
inf('capital of Bangladesh', 'capitals of Bangladesh').
inf('Kingdom of Belgium', 'Kingdoms of Belgium').
inf('capital of Belgium', 'capitals of Belgium').
inf('City of Bridges', 'Citys of Bridges').
inf('Kingdom of Bhutan', 'Kingdoms of Bhutan').
inf('Republic of Botswana', 'Republics of Botswana').
inf('capital of Botswana', 'capitals of Botswana').
inf('Republic of Bolivia', 'Republics of Bolivia').
inf('capital of Bolivia', 'capitals of Bolivia').
inf('capital of Brazil', 'capitals of Brazil').
inf('Osasco', 'Osascoes').
inf('Rio de Janeiro', 'Rio de Janeiroes').
inf('Pernambuco', 'Pernambucoes').
inf('Sao Bernardo do Campo', 'Sao Bernardo do Campoes').
inf('Sao Goncalo', 'Sao Goncaloes').
inf('Sao Paulo', 'Sao Pauloes').
inf('capital of the United Kingdom', 'capitals of the United Kingdom').
inf('City of London', 'Citys of London').
inf('Greenwich', 'Greenwiches').
inf('Soho', 'Sohoes').
inf('City of Westminster', 'Citys of Westminster').
inf('Houses of Parliament', 'Housess of Parliament').
inf('Kingston-upon Hull', 'Kingstons-upon Hull').
inf('Newcastle-upon-Tyne', 'Newcastles-upon-Tyne').
inf('Stratford-on-Avon', 'Stratfords-on-Avon').
inf('Stratford-upon-Avon', 'Stratfords-upon-Avon').
inf('Essex', 'Essexes').
inf('East Sussex', 'East Sussexes').
inf('West Sussex', 'West Sussexes').
inf('Sussex', 'Sussexes').
inf('Wessex', 'Wessexes').
inf('island of Jersey', 'islands of Jersey').
inf('island of Guernsey', 'islands of Guernsey').
inf('Isles of Scilly', 'Isless of Scilly').
inf('Isle of Man', 'Isles of Man').
inf('capital of Northern Ireland', 'capitals of Northern Ireland').
inf('Republic of Ireland', 'Republics of Ireland').
inf('capital of Ireland', 'capitals of Ireland').
inf('Highlands of Scotland', 'Highlandss of Scotland').
inf('Lowlands of Scotland', 'Lowlandss of Scotland').
inf('Isle of Skye', 'Isles of Skye').
inf('Burkina Faso', 'Burkina Fasoes').
inf('Cairo', 'Cairoes').
inf('capital of Egypt', 'capitals of Egypt').
inf('Republic of India', 'Republics of India').
inf('capital of India', 'capitals of India').
inf('Andhra Pradesh', 'Andhra Pradeshes').
inf('Uttar Pradesh', 'Uttar Pradeshes').
inf('Kingdom of Nepal', 'Kingdoms of Nepal').
inf('capital of Nepal', 'capitals of Nepal').
inf('capital of Tibet', 'capitals of Tibet').
inf('Republic of Indonesia', 'Republics of Indonesia').
inf('capital of Indonesia', 'capitals of Indonesia').
inf('capital of Iran', 'capitals of Iran').
inf('Republic of Iraq', 'Republics of Iraq').
inf('capital of Iraq', 'capitals of Iraq').
inf('Japanese Archipelago', 'Japanese Archipelagoes').
inf('Hokkaido', 'Hokkaidoes').
inf('Ezo', 'Ezoes').
inf('Yezo', 'Yezoes').
inf('Hondo', 'Hondoes').
inf('Tokyo', 'Tokyoes').
inf('Yeddo', 'Yeddoes').
inf('Yedo', 'Yedoes').
inf('Edo', 'Edoes').
inf('capital of Japan', 'capitals of Japan').
inf('Nagano', 'Naganoes').
inf('Kyoto', 'Kyotoes').
inf('Sapporo', 'Sapporoes').
inf('Amman', 'Ammen').
inf('capital of Jordan', 'capitals of Jordan').
inf('Jericho', 'Jerichoes').
inf('Republic of Kenya', 'Republics of Kenya').
inf('capital of Kenya', 'capitals of Kenya').
inf('State of Kuwait', 'States of Kuwait').
inf('capital of Kuwait', 'capitals of Kuwait').
inf('City of Light', 'Citys of Light').
inf('capital of France', 'capitals of France').
inf('Bordeaux', 'Bordeauxes').
inf('Toulouse', 'Toulice').
inf('Elsass', 'Elsasses').
inf('Artois', 'Artois').
inf('capital of Gabon', 'capitals of Gabon').
inf('Republic of The Gambia', 'Republics of The Gambia').
inf('capital of Gambia', 'capitals of Gambia').
inf('Republic of Ghana', 'Republics of Ghana').
inf('capital of Ghana', 'capitals of Ghana').
inf('capital of Grenada', 'capitals of Grenada').
inf('Republic of Guinea', 'Republics of Guinea').
inf('capital of Guinea', 'capitals of Guinea').
inf('Republic of Guinea-Bissau', 'Republics of Guinea-Bissau').
inf('capital of Guinea-Bissau', 'capitals of Guinea-Bissau').
inf('Kingdom of The Netherlands', 'Kingdoms of The Netherlands').
inf('capital of The Netherlands', 'capitals of The Netherlands').
inf('Republic of Hungary', 'Republics of Hungary').
inf('capital of Hungary', 'capitals of Hungary').
inf('Republic of Iceland', 'Republics of Iceland').
inf('capital of Iceland', 'capitals of Iceland').
inf('capital of North Korea', 'capitals of North Korea').
inf('Republic of Korea', 'Republics of Korea').
inf('capital of South Korea', 'capitals of South Korea').
inf('Chemulpo', 'Chemulpoes').
inf('capital of Laos', 'capitals of Laos').
inf('capital of Lebanon', 'capitals of Lebanon').
inf('Lesotho', 'Lesothoes').
inf('Kingdom of Lesotho', 'Kingdoms of Lesotho').
inf('capital of Lesotho', 'capitals of Lesotho').
inf('Republic of Liberia', 'Republics of Liberia').
inf('capital of Liberia', 'capitals of Liberia').
inf('capital of Libya', 'capitals of Libya').
inf('Principality of Liechtenstein', 'Principalitys of Liechtenstein').
inf('capital of Liechtenstein', 'capitals of Liechtenstein').
inf('capital of Luxembourg', 'capitals of Luxembourg').
inf('Republic of Madagascar', 'Republics of Madagascar').
inf('Antananarivo', 'Antananarivoes').
inf('capital of Madagascar', 'capitals of Madagascar').
inf('Republic of Malawi', 'Republics of Malawi').
inf('capital of Malawi', 'capitals of Malawi').
inf('capital of Malaysia', 'capitals of Malaysia').
inf('Republic of Maldives', 'Republics of Maldives').
inf('Republic of Mali', 'Republics of Mali').
inf('Bamako', 'Bamakoes').
inf('Republic of Malta', 'Republics of Malta').
inf('capital of Malta', 'capitals of Malta').
inf('Republic of Mauritius', 'Republics of Mauritius').
inf('Monaco', 'Monacoes').
inf('Principality of Monaco', 'Principalitys of Monaco').
inf('Monte Carlo', 'Monte Carloes').
inf('capital of Mongolia', 'capitals of Mongolia').
inf('Morocco', 'Moroccoes').
inf('Kingdom of Morocco', 'Kingdoms of Morocco').
inf('Marrakesh', 'Marrakeshes').
inf('Marrakech', 'Marrakeches').
inf('capital of Morocco', 'capitals of Morocco').
inf('Republic of Mozambique', 'Republics of Mozambique').
inf('Maputo', 'Maputoes').
inf('capital of Mozambique', 'capitals of Mozambique').
inf('Christchurch', 'Christchurches').
inf('capital of New Zealand', 'capitals of New Zealand').
inf('Republic of Niger', 'Republics of Niger').
inf('capital of Niger', 'capitals of Niger').
inf('capital of Nigeria', 'capitals of Nigeria').
inf('Oman', 'Omen').
inf('Sultanate of Oman', 'Sultanates of Oman').
inf('Muscat and Oman', 'Muscat and Omen').
inf('capital of Oman', 'capitals of Oman').
inf('capital of Pakistan', 'capitals of Pakistan').
inf('Republic of Palau', 'Republics of Palau').
inf('Republic of Paraguay', 'Republics of Paraguay').
inf('capital of Paraguay', 'capitals of Paraguay').
inf('Republic of Peru', 'Republics of Peru').
inf('Cuzco', 'Cuzcoes').
inf('Cusco', 'Cuscoes').
inf('capital of Peru', 'capitals of Peru').
inf('Mindoro', 'Mindoroes').
inf('Republic of the Philippines', 'Republics of the Philippines').
inf('capital of the Philippines', 'capitals of the Philippines').
inf('Pinatubo', 'Pinatuboes').
inf('Mount Pinatubo', 'Mount Pinatuboes').
inf('Republic of Poland', 'Republics of Poland').
inf('capital of Poland', 'capitals of Poland').
inf('capital of Portugal', 'capitals of Portugal').
inf('Porto', 'Portoes').
inf('Oporto', 'Oportoes').
inf('State of Qatar', 'States of Qatar').
inf('State of Katar', 'States of Katar').
inf('capital of Qatar', 'capitals of Qatar').
inf('Federation of Saint Kitts and Nevis', 'Federations of Saint Kitts and Nevis').
inf('Sombrero', 'Sombreroes').
inf('Tuamotu Archipelago', 'Tuamotu Archipelagoes').
inf('Paumotu Archipelago', 'Paumotu Archipelagoes').
inf('Low Archipelago', 'Low Archipelagoes').
inf('Samoa i Sisifo', 'Samoa i Sisifoes').
inf('capital of Western Samoa', 'capitals of Western Samoa').
inf('Pago Pago', 'Pago Pagoes').
inf('Pango Pango', 'Pango Pangoes').
inf('San Marino', 'San Marinoes').
inf('Republic of San Marino', 'Republics of San Marino').
inf('San Marino', 'San Marinoes').
inf('capital of San Marino', 'capitals of San Marino').
inf('Kingdom of Saudi Arabia', 'Kingdoms of Saudi Arabia').
inf('capital of Saudi Arabia', 'capitals of Saudi Arabia').
inf('Republic of Senegal', 'Republics of Senegal').
inf('capital of Senegal', 'capitals of Senegal').
inf('Republic of Seychelles', 'Republics of Seychelles').
inf('capital of Seychelles', 'capitals of Seychelles').
inf('Republic of Sierra Leone', 'Republics of Sierra Leone').
inf('capital of Sierra Leone', 'capitals of Sierra Leone').
inf('Republic of Singapore', 'Republics of Singapore').
inf('capital of Singapore', 'capitals of Singapore').
inf('capital of Somalia', 'capitals of Somalia').
inf('Horn of Africa', 'Horns of Africa').
inf('Republic of South Africa', 'Republics of South Africa').
inf('capital of South Africa', 'capitals of South Africa').
inf('Cape of Good Hope Province', 'Capes of Good Hope Province').
inf('Cape of Good Hope', 'Capes of Good Hope').
inf('Cape of Good Hope', 'Capes of Good Hope').
inf('Soweto', 'Sowetoes').
inf('Union of Soviet Socialist Republics', 'Unions of Soviet Socialist Republics').
inf('capital of the Russian Federation', 'capitals of the Russian Federation').
inf('Rostov on Don', 'Rostovs on Don').
inf('Republic of Belarus', 'Republics of Belarus').
inf('capital of Belarus', 'capitals of Belarus').
inf('Lubavitch', 'Lubavitches').
inf('Republic of Estonia', 'Republics of Estonia').
inf('capital of Estonia', 'capitals of Estonia').
inf('Republic of Latvia', 'Republics of Latvia').
inf('capital of Latvia', 'capitals of Latvia').
inf('Republic of Lithuania', 'Republics of Lithuania').
inf('Vilno', 'Vilnoes').
inf('Wilno', 'Wilnoes').
inf('capital of Lithuania', 'capitals of Lithuania').
inf('Kovno', 'Kovnoes').
inf('Republic of Moldova', 'Republics of Moldova').
inf('capital of Moldova', 'capitals of Moldova').
inf('capital of the Ukraine', 'capitals of the Ukraine').
inf('Stalino', 'Stalinoes').
inf('Donbass', 'Donbasses').
inf('Republic of Armenia', 'Republics of Armenia').
inf('capital of Armenia', 'capitals of Armenia').
inf('capital of Azerbaijan', 'capitals of Azerbaijan').
inf('Sakartvelo', 'Sakartveloes').
inf('capital of Georgia', 'capitals of Georgia').
inf('Republic of Kazakhstan', 'Republics of Kazakhstan').
inf('capital of Kazakhstan', 'capitals of Kazakhstan').
inf('capital of Kyrgyzstan', 'capitals of Kyrgyzstan').
inf('Republic of Tajikistan', 'Republics of Tajikistan').
inf('capital of Tajikistan', 'capitals of Tajikistan').
inf('capital of Turkmenistan', 'capitals of Turkmenistan').
inf('Republic of Uzbekistan', 'Republics of Uzbekistan').
inf('capital of Uzbek', 'capitals of Uzbek').
inf('Principality of Andorra', 'Principalitys of Andorra').
inf('Kingdom of Spain', 'Kingdoms of Spain').
inf('capital of Spain', 'capitals of Spain').
inf('Logrono', 'Logronoes').
inf('Oviedo', 'Oviedoes').
inf('Toledo', 'Toledoes').
inf('Rock of Gibraltar', 'Rocks of Gibraltar').
inf('Republic of the Sudan', 'Republics of the Sudan').
inf('capital of Sudan', 'capitals of Sudan').
inf('Omdurman', 'Omdurmen').
inf('Republic of Suriname', 'Republics of Suriname').
inf('Paramaribo', 'Paramariboes').
inf('capital of Suriname', 'capitals of Suriname').
inf('Kingdom of Swaziland', 'Kingdoms of Swaziland').
inf('capital of Swaziland', 'capitals of Swaziland').
inf('capital of Switzerland', 'capitals of Switzerland').
inf('Zurich', 'Zuriches').
inf('Dimash', 'Dimashes').
inf('capital of Syria', 'capitals of Syria').
inf('Aleppo', 'Aleppoes').
inf('capital of Tanzania', 'capitals of Tanzania').
inf('Kingdom of Thailand', 'Kingdoms of Thailand').
inf('capital of Thailand', 'capitals of Thailand').
inf('Kingdom of Tonga', 'Kingdoms of Tonga').
inf('Republic of Tunisia', 'Republics of Tunisia').
inf('capital of Tunisia', 'capitals of Tunisia').
inf('Sfax', 'Sfaxes').
inf('Republic of Turkey', 'Republics of Turkey').
inf('capital of Turkey', 'capitals of Turkey').
inf('Antioch', 'Antioches').
inf('Republic of Uganda', 'Republics of Uganda').
inf('capital of Uganda', 'capitals of Uganda').
inf('Heart of Dixie', 'Hearts of Dixie').
inf('capital of Alabama', 'capitals of Alabama').
inf('Pittsburgh of the South', 'Pittsburghs of the South').
inf('Juneau', 'Juneaux').
inf('capital of Alaska', 'capitals of Alaska').
inf('Alexander Archipelago', 'Alexander Archipelagoes').
inf('Phoenix', 'Phoenixes').
inf('capital of Arizona', 'capitals of Arizona').
inf('Land of Opportunity', 'Lands of Opportunity').
inf('Jonesboro', 'Jonesboroes').
inf('capital of Arkansas', 'capitals of Arkansas').
inf('Fresno', 'Fresnoes').
inf('Long Beach', 'Long Beaches').
inf('City of the Angels', 'Citys of the Angels').
inf('Palo Alto', 'Palo Altoes').
inf('Sacramento', 'Sacramentoes').
inf('capital of California', 'capitals of California').
inf('San Bernardino', 'San Bernardinoes').
inf('San Diego', 'San Diegoes').
inf('San Francisco', 'San Franciscoes').
inf('San Pablo', 'San Pabloes').
inf('Colorado', 'Coloradoes').
inf('capital of Colorado', 'capitals of Colorado').
inf('Pueblo', 'Puebloes').
inf('capital of Connecticut', 'capitals of Connecticut').
inf('capital of Delaware', 'capitals of Delaware').
inf('District of Columbia', 'Districts of Columbia').
inf('capital of the United States', 'capitals of the United States').
inf('Daytona Beach', 'Daytona Beaches').
inf('Miami Beach', 'Miami Beaches').
inf('Orlando', 'Orlandoes').
inf('Palm Beach', 'Palm Beaches').
inf('capital of Florida', 'capitals of Florida').
inf('West Palm Beach', 'West Palm Beaches').
inf('capital of Georgia', 'capitals of Georgia').
inf('Hilo', 'Hiloes').
inf('capital of Hawaii', 'capitals of Hawaii').
inf('Idaho', 'Idahoes').
inf('capital of Idaho', 'capitals of Idaho').
inf('Pocatello', 'Pocatelloes').
inf('Illinois', 'Illinois').
inf('Land of Lincoln', 'Lands of Lincoln').
inf('Cairo', 'Cairoes').
inf('Chicago', 'Chicagoes').
inf('capital of Illinois', 'capitals of Illinois').
inf('capital of Indiana', 'capitals of Indiana').
inf('capital of Iowa', 'capitals of Iowa').
inf('capital of Kansas', 'capitals of Kansas').
inf('capital of Kentucky', 'capitals of Kentucky').
inf('Owensboro', 'Owensboroes').
inf('Bluegrass', 'Bluegrasses').
inf('capital of Louisiana', 'capitals of Louisiana').
inf('capital of Maine', 'capitals of Maine').
inf('Orono', 'Oronoes').
inf('capital of Maryland', 'capitals of Maryland').
inf('Hub of the Universe', 'Hubs of the Universe').
inf('capital of Massachusetts', 'capitals of Massachusetts').
inf('capital of Michigan', 'capitals of Michigan').
inf('Mankato', 'Mankatoes').
inf('capital of Minnesota', 'capitals of Minnesota').
inf('capital of Mississippi', 'capitals of Mississippi').
inf('Tupelo', 'Tupeloes').
inf('Cape Girardeau', 'Cape Girardeaux').
inf('capital of Missouri', 'capitals of Missouri').
inf('Gateway to the West', 'Gateways to the West').
inf('Bozeman', 'Bozemen').
inf('capital of Montana', 'capitals of Montana').
inf('capital of Nebraska', 'capitals of Nebraska').
inf('capital of Nevada', 'capitals of Nevada').
inf('Reno', 'Renoes').
inf('capital of New Hampshire', 'capitals of New Hampshire').
inf('capital of New Jersey', 'capitals of New Jersey').
inf('New Mexico', 'New Mexicoes').
inf('Land of Enchantment', 'Lands of Enchantment').
inf('capital of New Mexico', 'capitals of New Mexico').
inf('capital of New York', 'capitals of New York').
inf('Buffalo', 'Buffaloes').
inf('Bronx', 'Bronxes').
inf('SoHo', 'SoHoes').
inf('South of Houston', 'Souths of Houston').
inf('capital of North Carolina', 'capitals of North Carolina').
inf('Goldsboro', 'Goldsboroes').
inf('Greensboro', 'Greensboroes').
inf('capital of North Dakota', 'capitals of North Dakota').
inf('Fargo', 'Fargoes').
inf('capital of Ohio', 'capitals of Ohio').
inf('Toledo', 'Toledoes').
inf('capital of Oklahoma', 'capitals of Oklahoma').
inf('capital of Oregon', 'capitals of Oregon').
inf('capital of Pennsylvania', 'capitals of Pennsylvania').
inf('City of Brotherly Love', 'Citys of Brotherly Love').
inf('capital of Rhode Island', 'capitals of Rhode Island').
inf('capital of South Carolina', 'capitals of South Carolina').
inf('capital of South Dakota', 'capitals of South Dakota').
inf('capital of Tennessee', 'capitals of Tennessee').
inf('Amarillo', 'Amarilloes').
inf('capital of Texas', 'capitals of Texas').
inf('El Paso', 'El Pasoes').
inf('Laredo', 'Laredoes').
inf('Plano', 'Planoes').
inf('San Angelo', 'San Angeloes').
inf('Sherman', 'Shermen').
inf('Waco', 'Wacoes').
inf('Provo', 'Provoes').
inf('capital of Utah', 'capitals of Utah').
inf('capital of Vermont', 'capitals of Vermont').
inf('Brattleboro', 'Brattleboroes').
inf('capital of Virginia', 'capitals of Virginia').
inf('Virginia Beach', 'Virginia Beaches').
inf('capital of Washington', 'capitals of Washington').
inf('capital of West Virginia', 'capitals of West Virginia').
inf('capital of Wisconsin', 'capitals of Wisconsin').
inf('capital of Wyoming', 'capitals of Wyoming').
inf('capital of Uruguay', 'capitals of Uruguay').
inf('Republic of Vanuatu', 'Republics of Vanuatu').
inf('capital of Vanuatu', 'capitals of Vanuatu').
inf('State of the Vatican City', 'States of the Vatican City').
inf('Citta del Vaticano', 'Citta del Vaticanoes').
inf('Republic of Venezuela', 'Republics of Venezuela').
inf('capital of Venezuela', 'capitals of Venezuela').
inf('Maracaibo', 'Maracaiboes').
inf('capital of Vietnam', 'capitals of Vietnam').
inf('Republic of Yemen', 'Republics of Yemen').
inf('Republic of Zambia', 'Republics of Zambia').
inf('capital of Zambia', 'capitals of Zambia').
inf('Slezsko', 'Slezskoes').
inf('Republic of Zimbabwe', 'Republics of Zimbabwe').
inf('capital of Zimbabwe', 'capitals of Zimbabwe').
inf('Bulawayo', 'Bulawayoes').
inf('Fuego', 'Fuegoes').
inf('Pasto', 'Pastoes').
inf('Nyiragongo', 'Nyiragongoes').
inf('Tupungatito', 'Tupungatitoes').
inf('House of Islam', 'Houses of Islam').
inf('House of War', 'Houses of War').
inf('life', 'lives').
inf('why', 'whies').
inf('death wish', 'death wishes').
inf('morality', 'moralities').
inf('sense of right and wrong', 'senses of right and wrong').
inf('voice of conscience', 'voices of conscience').
inf('sense of shame', 'senses of shame').
inf('sense of duty', 'senses of duty').
inf('psychic energy', 'psychic energies').
inf('mental energy', 'mental energies').
inf('libidinal energy', 'libidinal energies').
inf('cathexis', 'cathexes').
inf('acathexis', 'acathexes').
inf('abyss', 'abysses').
inf('aery', 'aeries').
inf('eyry', 'eyries').
inf('Alpha Crucis', 'Alpha Cruces').
inf('formicary', 'formicaries').
inf('antineutrino', 'antineutrinoes').
inf('archipelago', 'archipelagoes').
inf('Argo', 'Argoes').
inf('Arno', 'Arnoes').
inf('River Arno', 'River Arnoes').
inf('arroyo', 'arroyoes').
inf('acclivity', 'acclivities').
inf('Atacama Trench', 'Atacama Trenches').
inf('Bay of Bengal', 'Bays of Bengal').
inf('Bay of Biscay', 'Bays of Biscay').
inf('Bay of Fundy', 'Bays of Fundy').
inf('Bay of Naples', 'Bays of Naples').
inf('beach', 'beaches').
inf('Beta Crucis', 'Beta Cruces').
inf('Bight of Benin', 'Bights of Benin').
inf('binary', 'binaries').
inf('black body', 'black bodies').
inf('blackbody', 'blackbodies').
inf('blue sky', 'blue skies').
inf('body', 'bodies').
inf('body of water', 'bodies of water').
inf('Bougainville Trench', 'Bougainville Trenches').
inf('branch', 'branches').
inf('breach', 'breaches').
inf('Brenner Pass', 'Brenner Passes').
inf('Callisto', 'Callistoes').
inf('Canopus', 'Canopera').
inf('ness', 'nesses').
inf('catch', 'catches').
inf('celestial body', 'celestial bodies').
inf('heavenly body', 'heavenly bodies').
inf('Chimborazo', 'Chimborazoes').
inf('Colorado', 'Coloradoes').
inf('Colorado', 'Coloradoes').
inf('Colorado Plateau', 'Colorado Plateaux').
inf('Mount Garmo', 'Mount Garmoes').
inf('Congo', 'Congoes').
inf('continental shelf', 'continental shelves').
inf('cranny', 'crannies').
inf('Cumberland Plateau', 'Cumberland Plateaux').
inf('declivity', 'declivities').
inf('distributary', 'distributaries').
inf('ditch', 'ditches').
inf('dog do', 'dog does').
inf('doggy do', 'doggy does').
inf('Donner Pass', 'Donner Passes').
inf('Dorado', 'Doradoes').
inf('Draco', 'Dracoes').
inf('Ebro', 'Ebroes').
inf('El Muerto', 'El Muertoes').
inf('estuary', 'estuaries').
inf('estraterrestrial body', 'estraterrestrial bodies').
inf('tributary', 'tributaries').
inf('Firth of Clyde', 'Firths of Clyde').
inf('Firth of Forth', 'Firths of Forth').
inf('Fornax', 'Fornaxes').
inf('Fountain of Youth', 'Fountains of Youth').
inf('grotto', 'grottoes').
inf('gulch', 'gulches').
inf('Gulf of Aden', 'Gulfs of Aden').
inf('Gulf of Alaska', 'Gulfs of Alaska').
inf('Gulf of Antalya', 'Gulfs of Antalya').
inf('Gulf of Aqaba', 'Gulfs of Aqaba').
inf('Gulf of Akaba', 'Gulfs of Akaba').
inf('Gulf of Bothnia', 'Gulfs of Bothnia').
inf('Gulf of California', 'Gulfs of California').
inf('Sea of Cortes', 'Seas of Cortes').
inf('Gulf of Campeche', 'Gulfs of Campeche').
inf('Bay of Campeche', 'Bays of Campeche').
inf('Gulf of Carpentaria', 'Gulfs of Carpentaria').
inf('Gulf of Corinth', 'Gulfs of Corinth').
inf('Gulf of Lepanto', 'Gulfs of Lepanto').
inf('Gulf of Finland', 'Gulfs of Finland').
inf('Gulf of Guinea', 'Gulfs of Guinea').
inf('Gulf of Martaban', 'Gulfs of Martaban').
inf('Gulf of Mexico', 'Gulfs of Mexico').
inf('Golfo de Mexico', 'Golfo de Mexicoes').
inf('Gulf of Ob', 'Gulfs of Ob').
inf('Bay of Ob', 'Bays of Ob').
inf('Gulf of Oman', 'Gulfs of Oman').
inf('Gulf of Riga', 'Gulfs of Riga').
inf('Gulf of Saint Lawrence', 'Gulfs of Saint Lawrence').
inf('Gulf of St. Lawrence', 'Gulfs of St. Lawrence').
inf('Gulf of Sidra', 'Gulfs of Sidra').
inf('Gulf of Suez', 'Gulfs of Suez').
inf('Gulf of Tehuantepec', 'Gulfs of Tehuantepec').
inf('Gulf of Thailand', 'Gulfs of Thailand').
inf('Gulf of Siam', 'Gulfs of Siam').
inf('Gulf of Venice', 'Gulfs of Venice').
inf('gully', 'gullies').
inf('Hindu Kush', 'Hindu Kushes').
inf('Hook of Holland', 'Hooks of Holland').
inf('Hwang Ho', 'Hwang Hoes').
inf('ice mass', 'ice masses').
inf('inclusion body', 'inclusion bodies').
inf('recess', 'recesses').
inf('Io', 'Ioes').
inf('Irtish', 'Irtishes').
inf('Irtysh', 'Irtyshes').
inf('Isthmus of Corinth', 'Isthmuss of Corinth').
inf('Isthmus of Kra', 'Isthmuss of Kra').
inf('Isthmus of Panama', 'Isthmuss of Panama').
inf('Isthmus of Darien', 'Isthmuss of Darien').
inf('Isthmus of Suez', 'Isthmuss of Suez').
inf('Isthmus of Tehuantepec', 'Isthmuss of Tehuantepec').
inf('Japan Trench', 'Japan Trenches').
inf('Khyber Pass', 'Khyber Passes').
inf('Kilimanjaro', 'Kilimanjaroes').
inf('Mount Kilimanjaro', 'Mount Kilimanjaroes').
inf('Lake Leman', 'Lake Lemen').
inf('landmass', 'landmasses').
inf('land mass', 'land masses').
inf('Lascaux', 'Lascauxes').
inf('Laudo', 'Laudoes').
inf('shelf', 'shelves').
inf('liman', 'limen').
inf('Limpopo', 'Limpopoes').
inf('Little Wabash', 'Little Wabashes').
inf('llano', 'llanoes').
inf('Llano Estacado', 'Llano Estacadoes').
inf('Llullaillaco', 'Llullaillacoes').
inf('loch', 'loches').
inf('loch', 'loches').
inf('Loch Ness', 'Loch Nesses').
inf('mackerel sky', 'mackerel skies').
inf('briny', 'brinies').
inf('Sea of Marmara', 'Seas of Marmara').
inf('Sea of Marmora', 'Seas of Marmora').
inf('marsh', 'marshes').
inf('mass', 'masses').
inf('matrix', 'matrices').
inf('morass', 'morasses').
inf('Mohorovicic discontinuity', 'Mohorovicic discontinuities').
inf('Moho', 'Mohoes').
inf('Monte Bianco', 'Monte Biancoes').
inf('Nacimiento', 'Nacimientoes').
inf('mother-of-pearl cloud', 'mothers-of-pearl cloud').
inf('necessity', 'necessities').
inf('necessary', 'necessaries').
inf('Neosho', 'Neoshoes').
inf('neutrino', 'neutrinoes').
inf('Northern Cross', 'Northern Crosses').
inf('Ojos del Salado', 'Ojos del Saladoes').
inf('Orinoco', 'Orinocoes').
inf('Ozark Plateau', 'Ozark Plateaux').
inf('pass', 'passes').
inf('mountain pass', 'mountain passes').
inf('notch', 'notches').
inf('Paulo Afonso', 'Paulo Afonsoes').
inf('Pavo', 'Pavoes').
inf('Phoenix', 'Phoenixes').
inf('Pillars of Hercules', 'Pillarss of Hercules').
inf('Pissis', 'Pisses').
inf('cavity', 'cavities').
inf('Pluto', 'Plutoes').
inf('Po', 'Poes').
inf('Pollux', 'Polluxes').
inf('primary', 'primaries').
inf('virino', 'virinoes').
inf('promontory', 'promontories').
inf('mush', 'mushes').
inf('Pyxis', 'Pyxes').
inf('range of mountains', 'ranges of mountains').
inf('chain of mountains', 'chains of mountains').
inf('red dwarf', 'red dwarves').
inf('Rio Bravo', 'Rio Bravoes').
inf('Russell\'s body', 'Russell\'s bodies').
inf('cancer body', 'cancer bodies').
inf('Saint Francis', 'Saint Frances').
inf('St. Francis', 'St. Frances').
inf('salt marsh', 'salt marshes').
inf('Sao Francisco', 'Sao Franciscoes').
inf('Gulf of Aegina', 'Gulfs of Aegina').
inf('Sea of Azov', 'Seas of Azov').
inf('Sea of Azof', 'Seas of Azof').
inf('Sea of Azoff', 'Seas of Azoff').
inf('Sea of Japan', 'Seas of Japan').
inf('Sea of Okhotsk', 'Seas of Okhotsk').
inf('Sherman', 'Shermen').
inf('Mount Sherman', 'Mount Shermen').
inf('ice shelf', 'ice shelves').
inf('sky', 'skies').
inf('slash', 'slashes').
inf('Southern Cross', 'Southern Crosses').
inf('Crux', 'Cruxes').
inf('Strait of Georgia', 'Straits of Georgia').
inf('Strait of Gibraltar', 'Straits of Gibraltar').
inf('Strait of Hormuz', 'Straits of Hormuz').
inf('Strait of Ormuz', 'Straits of Ormuz').
inf('Strait of Magellan', 'Straits of Magellan').
inf('Strait of Messina', 'Straits of Messina').
inf('Strait of Dover', 'Straits of Dover').
inf('Strait of Calais', 'Straits of Calais').
inf('stretch', 'stretches').
inf('Styx', 'Styxes').
inf('River Styx', 'River Styxes').
inf('plateau', 'plateaux').
inf('bench', 'benches').
inf('trench', 'trenches').
inf('oceanic abyss', 'oceanic abysses').
inf('Tupungato', 'Tupungatoes').
inf('turf', 'turves').
inf('volcano', 'volcanoes').
inf('Virgo', 'Virgoes').
inf('volcano', 'volcanoes').
inf('Wabash', 'Wabashes').
inf('wash', 'washes').
inf('dry wash', 'dry washes').
inf('white dwarf', 'white dwarves').
inf('Isle of Wight', 'Isles of Wight').
inf('Wilderness', 'Wildernesses').
inf('abominable snowman', 'abominable snowmen').
inf('Bigfoot', 'Bigfeet').
inf('Sasquatch', 'Sasquatches').
inf('bogeyman', 'bogeymen').
inf('boogeyman', 'boogeymen').
inf('merman', 'mermen').
inf('Calypso', 'Calypsoes').
inf('giantess', 'giantesses').
inf('ogress', 'ogresses').
inf('Stheno', 'Sthenoes').
inf('phoenix', 'phoenixes').
inf('Sphinx', 'Sphinges').
inf('werewolf', 'werewolves').
inf('wolfman', 'wolfmen').
inf('witch', 'witches').
inf('theurgy', 'theurgies').
inf('destiny', 'destinies').
inf('deity', 'deities').
inf('divinity', 'divinities').
inf('Alecto', 'Alectoes').
inf('Celtic deity', 'Celtic deities').
inf('Egyptian deity', 'Egyptian deities').
inf('Isis', 'Ises').
inf('Eye of Ra', 'Eyes of Ra').
inf('Semitic deity', 'Semitic deities').
inf('Merodach', 'Merodaches').
inf('Baal Merodach', 'Baal Merodaches').
inf('Bel-Merodach', 'Bel-Merodaches').
inf('Moloch', 'Moloches').
inf('Molech', 'Moleches').
inf('Nebo', 'Neboes').
inf('Ramman', 'Rammen').
inf('Shamash', 'Shamashes').
inf('Gilgamish', 'Gilgamishes').
inf('Hindu deity', 'Hindu deities').
inf('Ganesh', 'Ganeshes').
inf('Hanuman', 'Hanumen').
inf('Persian deity', 'Persian deities').
inf('Ahriman', 'Ahrimen').
inf('Chinese deity', 'Chinese deities').
inf('Japanese deity', 'Japanese deities').
inf('Hachiman', 'Hachimen').
inf('Ninigino-Mikoto', 'Ninigino-Mikotoes').
inf('goddess', 'goddesses').
inf('earth-goddess', 'earth-goddesses').
inf('earth goddess', 'earth goddesses').
inf('hypostasis', 'hypostases').
inf('hypostasis of Christ', 'hypostases of Christ').
inf('fairy', 'fairies').
inf('faery', 'faeries').
inf('elf', 'elves').
inf('pixy', 'pixies').
inf('dwarf', 'dwarves').
inf('sandman', 'sandmen').
inf('bad fairy', 'bad fairies').
inf('bogy', 'bogies').
inf('Prince of Darkness', 'Princes of Darkness').
inf('kelpy', 'kelpies').
inf('genie', 'genii').
inf('djinny', 'djinnies').
inf('tooth fairy', 'tooth fairies').
inf('Flying Dutchman', 'Flying Dutchmen').
inf('Greco-Roman deity', 'Greco-Roman deities').
inf('Graeco-Roman deity', 'Graeco-Roman deities').
inf('Echo', 'Echoes').
inf('Salmacis', 'Salmaces').
inf('Greek deity', 'Greek deities').
inf('Roman deity', 'Roman deities').
inf('Apollo', 'Apolloes').
inf('Phoebus Apollo', 'Phoebus Apolloes').
inf('Hero', 'Heroes').
inf('Nyx', 'Nyxes').
inf('Dido', 'Didoes').
inf('Nox', 'Noxes').
inf('Io', 'Ioes').
inf('Juno', 'Junoes').
inf('Clotho', 'Clothoes').
inf('Klotho', 'Klothoes').
inf('Lachesis', 'Lacheses').
inf('Erato', 'Eratoes').
inf('Nemesis', 'Nemeses').
inf('Pluto', 'Plutoes').
inf('Pythoness', 'Pythonesses').
inf('Titaness', 'Titanesses').
inf('Protector of Boundaries', 'Protectors of Boundaries').
inf('Leto', 'Letoes').
inf('Norse deity', 'Norse deities').
inf('Teutonic deity', 'Teutonic deities').
inf('Anglo-Saxon deity', 'Anglo-Saxon deities').
inf('Ajax', 'Ajaxes').
inf('Phrygian deity', 'Phrygian deities').
inf('Oedipus Rex', 'Oedipus Rexes').
inf('Helen of Troy', 'Helens of Troy').
inf('Iago', 'Iagoes').
inf('Mr. Moto', 'Mr. Motoes').
inf('Othello', 'Othelloes').
inf('Pangloss', 'Panglosses').
inf('Pluto', 'Plutoes').
inf('Tarzan of the Apes', 'Tarzans of the Apes').
inf('self', 'selves').
inf('anomaly', 'anomalies').
inf('unusual person', 'unusual people').
inf('benefactress', 'benefactresses').
inf('color-blind person', 'color-blind people').
inf('common man', 'common men').
inf('common person', 'common people').
inf('contadino', 'contadinoes').
inf('cosignatory', 'cosignatories').
inf('female person', 'female people').
inf('inexperienced person', 'inexperienced people').
inf('juvenile person', 'juvenile people').
inf('male person', 'male people').
inf('intermediary', 'intermediaries').
inf('mediatrix', 'mediatrices').
inf('nonreligious person', 'nonreligious people').
inf('match', 'matches').
inf('primitive person', 'primitive people').
inf('religious person', 'religious people').
inf('unfortunate person', 'unfortunate people').
inf('unwelcome person', 'unwelcome people').
inf('unpleasant person', 'unpleasant people').
inf('disagreeable person', 'disagreeable people').
inf('unskilled person', 'unskilled people').
inf('person of color', 'people of color').
inf('person of colour', 'people of colour').
inf('Black person', 'Black people').
inf('Negro', 'Negroes').
inf('Negress', 'Negresses').
inf('Black man', 'Black men').
inf('Black woman', 'Black women').
inf('soul brother', 'soul brethren').
inf('colored person', 'colored people').
inf('darky', 'darkies').
inf('mulatto', 'mulattoes').
inf('White person', 'White people').
inf('white man', 'white men').
inf('white woman', 'white women').
inf('white trash', 'white trashes').
inf('poor white trash', 'poor white trashes').
inf('honky', 'honkies').
inf('cooly', 'coolies').
inf('oriental person', 'oriental people').
inf('yellow man', 'yellow men').
inf('yellow woman', 'yellow women').
inf('Arapaho', 'Arapahoes').
inf('Blackfoot', 'Blackfeet').
inf('Caddo', 'Caddoes').
inf('Chimariko', 'Chimarikoes').
inf('Diegueno', 'Dieguenoes').
inf('Eyeish', 'Eyeishes').
inf('Fox', 'Foxes').
inf('Illinois', 'Illinois').
inf('Iroquois', 'Iroquois').
inf('Navaho', 'Navahoes').
inf('Navajo', 'Navajoes').
inf('Ofo', 'Ofoes').
inf('Oto', 'Otoes').
inf('Pamlico', 'Pamlicoes').
inf('Pomo', 'Pomoes').
inf('Pueblo', 'Puebloes').
inf('red man', 'red men').
inf('Salish', 'Salishes').
inf('Santee Sioux', 'Santee Siouxes').
inf('Eastern Sioux', 'Eastern Siouxes').
inf('Sahaptino', 'Sahaptinoes').
inf('Sioux', 'Siouxes').
inf('Teton Sioux', 'Teton Siouxes').
inf('Tutelo', 'Tuteloes').
inf('Winnebago', 'Winnebagoes').
inf('Yucateco', 'Yucatecoes').
inf('Assamese', 'Assamese').
inf('Kanarese', 'Kanarese').
inf('Canarese', 'Canarese').
inf('Malto', 'Maltoes').
inf('Abo', 'Aboes').
inf('Amish', 'Amishes').
inf('Jewess', 'Jewesses').
inf('sheeny', 'sheenies').
inf('Angolese', 'Angolese').
inf('Basotho', 'Basothoes').
inf('Herero', 'Hereroes').
inf('Sotho', 'Sothoes').
inf('Beninese', 'Beninese').
inf('Bhutanese', 'Bhutanese').
inf('Maraco', 'Maracoes').
inf('Burmese', 'Burmese').
inf('Chinese', 'Chinese').
inf('Chinaman', 'Chinamen').
inf('Congolese', 'Congolese').
inf('Czech', 'Czeches').
inf('Czech', 'Czeches').
inf('Dutch', 'Dutches').
inf('English person', 'English people').
inf('Englishman', 'Englishmen').
inf('Englishwoman', 'Englishwomen').
inf('pommy', 'pommies').
inf('Cornishman', 'Cornishmen').
inf('Cornishwoman', 'Cornishwomen').
inf('Cheremiss', 'Cheremisses').
inf('Ingerman', 'Ingermen').
inf('Frenchman', 'Frenchmen').
inf('Frenchwoman', 'Frenchwomen').
inf('French person', 'French people').
inf('Gabonese', 'Gabonese').
inf('Guyanese', 'Guyanese').
inf('Moro', 'Moroes').
inf('Dutchman', 'Dutchmen').
inf('Norman', 'Normen').
inf('Irish person', 'Irish people').
inf('Irishman', 'Irishmen').
inf('Irishwoman', 'Irishwomen').
inf('dago', 'dagoes').
inf('ginzo', 'ginzoes').
inf('Roman', 'Romen').
inf('Roman', 'Romen').
inf('Japanese', 'Japanese').
inf('Nipponese', 'Nipponese').
inf('Latino', 'Latinoes').
inf('Lebanese', 'Lebanese').
inf('Chicano', 'Chicanoes').
inf('taco', 'tacoes').
inf('Mexicano', 'Mexicanoes').
inf('Nepalese', 'Nepalese').
inf('Norseman', 'Norsemen').
inf('Filipino', 'Filipinoes').
inf('Portuguese', 'Portuguese').
inf('San Marinese', 'San Marinese').
inf('Northman', 'Northmen').
inf('Scotsman', 'Scotsmen').
inf('Scotchman', 'Scotchmen').
inf('Scotswoman', 'Scotswomen').
inf('Scotchwoman', 'Scotchwomen').
inf('Senegalese', 'Senegalese').
inf('Seychellois', 'Seychellois').
inf('Sinhalese', 'Sinhalese').
inf('Singhalese', 'Singhalese').
inf('Sudanese', 'Sudanese').
inf('British', 'Britishes').
inf('English', 'Englishes').
inf('Irish', 'Irishes').
inf('French', 'Frenches').
inf('Spanish', 'Spanishes').
inf('Swiss', 'Swisses').
inf('Taiwanese', 'Taiwanese').
inf('Siamese', 'Siamese').
inf('Togolese', 'Togolese').
inf('Ottoman', 'Ottomen').
inf('Chuvash', 'Chuvashes').
inf('Turkoman', 'Turkomen').
inf('Turcoman', 'Turcomen').
inf('Edo', 'Edoes').
inf('Igbo', 'Igboes').
inf('Alabaman', 'Alabamen').
inf('Oklahoman', 'Oklahomen').
inf('Vietnamese', 'Vietnamese').
inf('Annamese', 'Annamese').
inf('Welshman', 'Welshmen').
inf('Welsh', 'Welshes').
inf('Maltese', 'Maltese').
inf('German', 'Germen').
inf('East German', 'East Germen').
inf('Zairese', 'Zairese').
inf('Virgo', 'Virgoes').
inf('Fish', 'Fishes').
inf('abandoned person', 'abandoned people').
inf('abbess', 'abbesses').
inf('prioress', 'prioresses').
inf('able seaman', 'able seamen').
inf('able-bodied seaman', 'able-bodied seamen').
inf('schoolman', 'schoolmen').
inf('accessory', 'accessories').
inf('accessary', 'accessaries').
inf('accessory after the fact', 'accessories after the fact').
inf('accessory before the fact', 'accessories before the fact').
inf('accessory during the fact', 'accessories during the fact').
inf('customer\'s man', 'customer\'s men').
inf('virtuoso', 'virtuosoes').
inf('success', 'successes').
inf('actress', 'actresses').
inf('junky', 'junkies').
inf('adulteress', 'adulteresses').
inf('fornicatress', 'fornicatresses').
inf('hussy', 'hussies').
inf('loose woman', 'loose women').
inf('adventuress', 'adventuresses').
inf('adversary', 'adversaries').
inf('adverse witness', 'adverse witnesses').
inf('hostile witness', 'hostile witnesses').
inf('adman', 'admen').
inf('counselor-at-law', 'counselors-at-law').
inf('aficionado', 'aficionadoes').
inf('aficionado', 'aficionadoes').
inf('agent-in-place', 'agents-in-place').
inf('auxiliary', 'auxiliaries').
inf('aircraftsman', 'aircraftsmen').
inf('aircraftman', 'aircraftmen').
inf('aircrewman', 'aircrewmen').
inf('albino', 'albinoes').
inf('alky', 'alkies').
inf('lush', 'lushes').
inf('alderman', 'aldermen').
inf('aliterate person', 'aliterate people').
inf('ally', 'allies').
inf('alter ego', 'alter egoes').
inf('alto', 'altoes').
inf('virago', 'viragoes').
inf('ambassadress', 'ambassadresses').
inf('friend of the court', 'friends of the court').
inf('amigo', 'amigoes').
inf('ancestress', 'ancestresses').
inf('anchorman', 'anchormen').
inf('anchorperson', 'anchorpeople').
inf('antediluvian patriarch', 'antediluvian patriarches').
inf('antiquary', 'antiquaries').
inf('ape-man', 'ape-men').
inf('plaintiff in error', 'plaintiffs in error').
inf('supreme authority', 'supreme authorities').
inf('archduchess', 'archduchesses').
inf('bowman', 'bowmen').
inf('hierarch', 'hierarches').
inf('incendiary', 'incendiaries').
inf('artilleryman', 'artillerymen').
inf('creative person', 'creative people').
inf('ass', 'asses').
inf('bravo', 'bravoes').
inf('assemblyman', 'assemblymen').
inf('assemblywoman', 'assemblywomen').
inf('son of a bitch', 'sons of a bitch').
inf('spaceman', 'spacemen').
inf('auspex', 'auspexes').
inf('aunty', 'aunties').
inf('authoress', 'authoresses').
inf('authority', 'authorities').
inf('authority', 'authorities').
inf('airman', 'airmen').
inf('aviatrix', 'aviatrices').
inf('airwoman', 'airwomen').
inf('aviatress', 'aviatresses').
inf('baby', 'babies').
inf('baby', 'babies').
inf('baby', 'babies').
inf('baby', 'babies').
inf('unmarried man', 'unmarried men').
inf('bad person', 'bad people').
inf('baggageman', 'baggagemen').
inf('bag lady', 'bag ladies').
inf('bagman', 'bagmen').
inf('baldy', 'baldies').
inf('ballet mistress', 'ballet mistresses').
inf('banderillero', 'banderilleroes').
inf('novillero', 'novilleroes').
inf('torero', 'toreroes').
inf('bandsman', 'bandsmen').
inf('bar fly', 'bar flies').
inf('big businessman', 'big businessmen').
inf('barman', 'barmen').
inf('baseball coach', 'baseball coaches').
inf('basketball coach', 'basketball coaches').
inf('bass', 'basses').
inf('basso', 'bassoes').
inf('love child', 'love children').
inf('illegitimate child', 'illegitimate children').
inf('baroness', 'baronesses').
inf('batman', 'batmen').
inf('batsman', 'batsmen').
inf('batting coach', 'batting coaches').
inf('battle-ax', 'battle-axes').
inf('beadsman', 'beadsmen').
inf('bedesman', 'bedesmen').
inf('wolf', 'wolves').
inf('beggarman', 'beggarmen').
inf('beggarwoman', 'beggarwomen').
inf('bellman', 'bellmen').
inf('beneficiary', 'beneficiaries').
inf('bereaved person', 'bereaved people').
inf('best man', 'best men').
inf('big brother', 'big brethren').
inf('big fish', 'big fish').
inf('head honcho', 'head honchoes').
inf('bimbo', 'bimboes').
inf('bisexual person', 'bisexual people').
inf('chargeman', 'chargemen').
inf('blind person', 'blind people').
inf('blood brother', 'blood brethren').
inf('blue baby', 'blue babies').
inf('navy man', 'navy men').
inf('boatman', 'boatmen').
inf('waterman', 'watermen').
inf('bobby', 'bobbies').
inf('muscleman', 'musclemen').
inf('bolshy', 'bolshies').
inf('bondman', 'bondmen').
inf('bondsman', 'bondsmen').
inf('bondman', 'bondmen').
inf('bondsman', 'bondsmen').
inf('bondwoman', 'bondwomen').
inf('bondswoman', 'bondswomen').
inf('bondwoman', 'bondwomen').
inf('bondswoman', 'bondswomen').
inf('bondsman', 'bondsmen').
inf('bondswoman', 'bondswomen').
inf('border patrolman', 'border patrolmen').
inf('boss', 'bosses').
inf('bourgeois', 'bourgeois').
inf('sonny', 'sonnies').
inf('beau', 'beaux').
inf('young man', 'young men').
inf('bracero', 'braceroes').
inf('brahman', 'brahmen').
inf('brahman', 'brahmen').
inf('brakeman', 'brakemen').
inf('ledgeman', 'ledgemen').
inf('maid of honor', 'maids of honor').
inf('broth of a boy', 'broths of a boy').
inf('broth of a man', 'broths of a man').
inf('brother', 'brethren').
inf('blood brother', 'blood brethren').
inf('brother', 'brethren').
inf('brother', 'brethren').
inf('brother-in-law', 'brethren-in-law').
inf('buddy', 'buddies').
inf('brother', 'brethren').
inf('crony', 'cronies').
inf('bully', 'bullies').
inf('rowdy', 'rowdies').
inf('yobo', 'yoboes').
inf('yobbo', 'yobboes').
inf('bully', 'bullies').
inf('bunny', 'bunnies').
inf('burgess', 'burgesses').
inf('bushman', 'bushmen').
inf('Bushman', 'Bushmen').
inf('businessman', 'businessmen').
inf('man of affairs', 'men of affairs').
inf('businesswoman', 'businesswomen').
inf('businessperson', 'businesspeople').
inf('bourgeois', 'bourgeois').
inf('busman', 'busmen').
inf('busybody', 'busybodies').
inf('butch', 'butches').
inf('meatman', 'meatmen').
inf('pantryman', 'pantrymen').
inf('buttinsky', 'buttinskies').
inf('company', 'companies').
inf('cameraman', 'cameramen').
inf('canary', 'canaries').
inf('fresh fish', 'fresh fish').
inf('capo', 'capoes').
inf('sharpy', 'sharpies').
inf('career man', 'career men').
inf('castrato', 'castratoes').
inf('casualty', 'casualties').
inf('injured party', 'injured parties').
inf('casualty', 'casualties').
inf('catch', 'catches').
inf('match', 'matches').
inf('cattleman', 'cattlemen').
inf('cow man', 'cow men').
inf('beef man', 'beef men').
inf('cavalryman', 'cavalrymen').
inf('cavalryman', 'cavalrymen').
inf('caveman', 'cavemen').
inf('cave man', 'cave men').
inf('celebrity', 'celebrities').
inf('famous person', 'famous people').
inf('middle of the roader', 'middles of the roader').
inf('chairman of the board', 'chairmen of the board').
inf('hero', 'heroes').
inf('Chancellor of the Exchequer', 'Chancellors of the Exchequer').
inf('cuss', 'cusses').
inf('chapman', 'chapmen').
inf('character witness', 'character witnesses').
inf('charge of quarters', 'charges of quarters').
inf('charwoman', 'charwomen').
inf('cleaning woman', 'cleaning women').
inf('cleaning lady', 'cleaning ladies').
inf('woman', 'women').
inf('chatterbox', 'chatterboxes').
inf('jingo', 'jingoes').
inf('chief of staff', 'chiefs of staff').
inf('child', 'children').
inf('small fry', 'small fries').
inf('fry', 'fries').
inf('child', 'children').
inf('child', 'children').
inf('baby', 'babies').
inf('child', 'children').
inf('child prodigy', 'child prodigies').
inf('infant prodigy', 'infant prodigies').
inf('wonder child', 'wonder children').
inf('patsy', 'patsies').
inf('soft touch', 'soft touches').
inf('Father of the Church', 'Fathers of the Church').
inf('nobody', 'nobodies').
inf('nonentity', 'nonentities').
inf('city man', 'city men').
inf('clergyman', 'clergymen').
inf('man of the cloth', 'men of the cloth').
inf('churchman', 'churchmen').
inf('clumsy person', 'clumsy people').
inf('coach', 'coaches').
inf('coach', 'coaches').
inf('line coach', 'line coaches').
inf('pitching coach', 'pitching coaches').
inf('coachman', 'coachmen').
inf('coalman', 'coalmen').
inf('pitman', 'pitmen').
inf('coastguardsman', 'coastguardsmen').
inf('co-beneficiary', 'co-beneficiaries').
inf('cold fish', 'cold fish').
inf('college man', 'college men').
inf('coloratura soprano', 'coloratura sopranoes').
inf('comfort woman', 'comfort women').
inf('commander in chief', 'commanders in chief').
inf('generalissimo', 'generalissimoes').
inf('commando', 'commandoes').
inf('committeeman', 'committeemen').
inf('committeewoman', 'committeewomen').
inf('couch potato', 'couch potatoes').
inf('councilman', 'councilmen').
inf('councilwoman', 'councilwomen').
inf('company man', 'company men').
inf('Comptroller of the Currency', 'Comptrollers of the Currency').
inf('doxy', 'doxies').
inf('conductress', 'conductresses').
inf('henchman', 'henchmen').
inf('partner in crime', 'partners in crime').
inf('confidence man', 'confidence men').
inf('con man', 'con men').
inf('congressman', 'congressmen').
inf('congresswoman', 'congresswomen').
inf('middleman', 'middlemen').
inf('contemporary', 'contemporaries').
inf('contralto', 'contraltoes').
inf('Converso', 'Conversoes').
inf('cooky', 'cookies').
inf('minx', 'minges').
inf('newspaperman', 'newspapermen').
inf('newspaperwoman', 'newspaperwomen').
inf('pressman', 'pressmen').
inf('barrow-man', 'barrow-men').
inf('counterperson', 'counterpeople').
inf('counterwoman', 'counterwomen').
inf('counterman', 'countermen').
inf('counterrevolutionary', 'counterrevolutionaries').
inf('counterspy', 'counterspies').
inf('countess', 'countesses').
inf('countryman', 'countrymen').
inf('countrywoman', 'countrywomen').
inf('countryman', 'countrymen').
inf('countrywoman', 'countrywomen').
inf('coureur de bois', 'coureur de bois').
inf('cousin-german', 'cousin-germen').
inf('lovely', 'lovelies').
inf('cow', 'kine').
inf('cowman', 'cowmen').
inf('cattleman', 'cattlemen').
inf('vaquero', 'vaqueroes').
inf('cox', 'coxes').
inf('crabby person', 'crabby people').
inf('craftsman', 'craftsmen').
inf('journeyman', 'journeymen').
inf('craftsman', 'craftsmen').
inf('craftsman', 'craftsmen').
inf('crazy', 'crazies').
inf('loony', 'loonies').
inf('weirdo', 'weirdoes').
inf('weirdo', 'weirdoes').
inf('weirdy', 'weirdies').
inf('crewman', 'crewmen').
inf('crewman', 'crewmen').
inf('criollo', 'criolloes').
inf('balladeer', 'balladeer').
inf('crown princess', 'crown princesses').
inf('crown princess', 'crown princesses').
inf('bitch', 'bitches').
inf('curandero', 'curanderoes').
inf('minister of religion', 'ministers of religion').
inf('bionic man', 'bionic men').
inf('bionic woman', 'bionic women').
inf('daddy', 'daddies').
inf('dairyman', 'dairymen').
inf('dairyman', 'dairymen').
inf('dalesman', 'dalesmen').
inf('wench', 'wenches').
inf('lady', 'ladies').
inf('gentlewoman', 'gentlewomen').
inf('dandy', 'dandies').
inf('beau', 'beaux').
inf('deary', 'dearies').
inf('ducky', 'duckies').
inf('daughter-in-law', 'daughters-in-law').
inf('deaconess', 'deaconesses').
inf('dead person', 'dead people').
inf('deceased person', 'deceased people').
inf('deaf person', 'deaf people').
inf('deliveryman', 'deliverymen').
inf('superman', 'supermen').
inf('Ubermensch', 'Ubermensches').
inf('deputy', 'deputies').
inf('deputy', 'deputies').
inf('deputy', 'deputies').
inf('deputy', 'deputies').
inf('dervish', 'dervishes').
inf('deskman', 'deskmen').
inf('desperado', 'desperadoes').
inf('Director of Central Intelligence', 'Directors of Central Intelligence').
inf('dirty old man', 'dirty old men').
inf('displaced person', 'displaced people').
inf('stateless person', 'stateless people').
inf('frogman', 'frogmen').
inf('ex-wife', 'ex-wives').
inf('ex', 'exes').
inf('medico', 'medicoes').
inf('Doctor of the Church', 'Doctors of the Church').
inf('fox', 'foxes').
inf('dodo', 'dodoes').
inf('fogy', 'fogies').
inf('dog in the manger', 'dogs in the manger').
inf('dogsbody', 'dogsbodies').
inf('dominatrix', 'dominatrices').
inf('doorman', 'doormen').
inf('ostiary', 'ostiaries').
inf('ostiary', 'ostiaries').
inf('street person', 'street people').
inf('draftsman', 'draftsmen').
inf('draftsman', 'draftsmen').
inf('draughtsman', 'draughtsmen').
inf('draftsperson', 'draftspeople').
inf('dragoman', 'dragomen').
inf('needlewoman', 'needlewomen').
inf('seamstress', 'seamstresses').
inf('sempstress', 'sempstresses').
inf('navvy', 'navvies').
inf('junky', 'junkies').
inf('drunk-and-disorderly', 'drunk-and-disorderlies').
inf('rummy', 'rummies').
inf('wino', 'winoes').
inf('dry', 'dries').
inf('duchess', 'duchesses').
inf('dummy', 'dummies').
inf('booby', 'boobies').
inf('dummy', 'dummies').
inf('silent person', 'silent people').
inf('dumbass', 'dumbasses').
inf('dwarf', 'dwarves').
inf('sharpy', 'sharpies').
inf('eccentric person', 'eccentric people').
inf('editor in chief', 'editors in chief').
inf('elder statesman', 'elder statesmen').
inf('elder statesman', 'elder statesmen').
inf('lineman', 'linemen').
inf('linesman', 'linesmen').
inf('elevator man', 'elevator men').
inf('liftman', 'liftmen').
inf('embroideress', 'embroideresses').
inf('emissary', 'emissaries').
inf('emotional person', 'emotional people').
inf('empress', 'empresses').
inf('conjure man', 'conjure men').
inf('enchantress', 'enchantresses').
inf('witch', 'witches').
inf('enchantress', 'enchantresses').
inf('temptress', 'temptresses').
inf('enemy', 'enemies').
inf('foeman', 'foemen').
inf('end man', 'end men').
inf('end man', 'end men').
inf('corner man', 'corner men').
inf('enlisted man', 'enlisted men').
inf('enlisted person', 'enlisted people').
inf('enlisted woman', 'enlisted women').
inf('ENT man', 'ENT men').
inf('envoy extraordinary', 'envoy extraordinaries').
inf('minister plenipotentiary', 'minister plenipotentiaries').
inf('eparch', 'eparches').
inf('eparch', 'eparches').
inf('equerry', 'equerries').
inf('equerry', 'equerries').
inf('Eskimo', 'Eskimoes').
inf('ethnarch', 'ethnarches').
inf('eunuch', 'eunuches').
inf('everyman', 'everymen').
inf('exarch', 'exarches').
inf('exarch', 'exarches').
inf('exarch', 'exarches').
inf('executive secretary', 'executive secretaries').
inf('executrix', 'executrices').
inf('expert witness', 'expert witnesses').
inf('eyewitness', 'eyewitnesses').
inf('fairy', 'fairies').
inf('pansy', 'pansies').
inf('family man', 'family men').
inf('fancy man', 'fancy men').
inf('husbandman', 'husbandmen').
inf('father-in-law', 'fathers-in-law').
inf('fatso', 'fatsoes').
inf('fatty', 'fatties').
inf('fat person', 'fat people').
inf('roly-poly', 'roly-polies').
inf('female child', 'female children').
inf('swordsman', 'swordsmen').
inf('ferryman', 'ferrymen').
inf('groom-to-be', 'grooms-to-be').
inf('bride-to-be', 'brides-to-be').
inf('fiduciary', 'fiduciaries').
inf('fieldsman', 'fieldsmen').
inf('minister of finance', 'ministers of finance').
inf('moneyman', 'moneymen').
inf('fingerprint man', 'fingerprint men').
inf('snitch', 'snitches').
inf('canary', 'canaries').
inf('fireman', 'firemen').
inf('first baseman', 'first basemen').
inf('first lady', 'first ladies').
inf('first lady', 'first ladies').
inf('sergeant first class', 'sergeant first classes').
inf('fisherman', 'fishermen').
inf('fishwife', 'fishwives').
inf('flash in the pan', 'flashes in the pan').
inf('flatfoot', 'flatfeet').
inf('patrolman', 'patrolmen').
inf('foolish woman', 'foolish women').
inf('flunky', 'flunkies').
inf('yes-man', 'yes-men').
inf('fly-by-night', 'flies-by-night').
inf('enemy', 'enemies').
inf('foot', 'feet').
inf('football coach', 'football coaches').
inf('football hero', 'football heroes').
inf('footman', 'footmen').
inf('secretary of state', 'secretaries of state').
inf('boss', 'bosses').
inf('foreman', 'foremen').
inf('honcho', 'honchoes').
inf('boss', 'bosses').
inf('foreman', 'foremen').
inf('foreperson', 'forepeople').
inf('forewoman', 'forewomen').
inf('forewoman', 'forewomen').
inf('forelady', 'foreladies').
inf('foster-brother', 'foster-brethren').
inf('foster brother', 'foster brethren').
inf('foster-child', 'foster-children').
inf('foster child', 'foster children').
inf('foundress', 'foundresses').
inf('four-minute man', 'four-minute men').
inf('monstrosity', 'monstrosities').
inf('freedman', 'freedmen').
inf('freedwoman', 'freedwomen').
inf('self-employed person', 'self-employed people').
inf('freeman', 'freemen').
inf('freewoman', 'freewomen').
inf('freshman', 'freshmen').
inf('frontiersman', 'frontiersmen').
inf('backwoodsman', 'backwoodsmen').
inf('mountain man', 'mountain men').
inf('frontierswoman', 'frontierswomen').
inf('front man', 'front men').
inf('straw man', 'straw men').
inf('strawman', 'strawmen').
inf('fuddy-duddy', 'fuddy-duddies').
inf('fugitive from justice', 'fugitives from justice').
inf('fugleman', 'fuglemen').
inf('gagman', 'gagmen').
inf('gagman', 'gagmen').
inf('games-mistress', 'games-mistresses').
inf('garbage man', 'garbage men').
inf('garbageman', 'garbagemen').
inf('dustman', 'dustmen').
inf('nurseryman', 'nurserymen').
inf('gasman', 'gasmen').
inf('gaucho', 'gauchoes').
inf('gay man', 'gay men').
inf('Genoese', 'Genoese').
inf('gentleman', 'gentlemen').
inf('gentleman-at-arms', 'gentlemen-at-arms').
inf('gigolo', 'gigoloes').
inf('miss', 'misses').
inf('missy', 'missies').
inf('young lady', 'young ladies').
inf('young woman', 'young women').
inf('gitano', 'gitanoes').
inf('trencherman', 'trenchermen').
inf('G-man', 'G-men').
inf('government man', 'government men').
inf('godchild', 'godchildren').
inf('ball of fire', 'balls of fire').
inf('good-for-nothing', 'goods-for-nothing').
inf('good-for-naught', 'goods-for-naught').
inf('linksman', 'linksmen').
inf('golf pro', 'golf proes').
inf('good person', 'good people').
inf('goody-goody', 'goody-goodies').
inf('governess', 'governesses').
inf('grandchild', 'grandchildren').
inf('grand duchess', 'grand duchesses').
inf('granddaddy', 'granddaddies').
inf('granny', 'grannies').
inf('granny', 'grannies').
inf('divorced man', 'divorced men').
inf('great grandchild', 'great grandchildren').
inf('gringo', 'gringoes').
inf('groomsman', 'groomsmen').
inf('grouch', 'grouches').
inf('crosspatch', 'crosspatches').
inf('groundsman', 'groundsmen').
inf('surety', 'sureties').
inf('guardsman', 'guardsmen').
inf('guest of honor', 'guests of honor').
inf('gunman', 'gunmen').
inf('gun for hire', 'guns for hire').
inf('triggerman', 'triggermen').
inf('hit man', 'hit men').
inf('hitman', 'hitmen').
inf('torpedo', 'torpedoes').
inf('bozo', 'bozoes').
inf('witch', 'witches').
inf('bozo', 'bozoes').
inf('jackass', 'jackasses').
inf('goose', 'geese').
inf('zany', 'zanies').
inf('Haman', 'Hamen').
inf('handicapped person', 'handicapped people').
inf('handyman', 'handymen').
inf('jack of all trades', 'jacks of all trades').
inf('odd-job man', 'odd-job men').
inf('hangman', 'hangmen').
inf('hatchet man', 'hatchet men').
inf('hatchet man', 'hatchet men').
inf('iceman', 'icemen').
inf('head linesman', 'head linesmen').
inf('headman', 'headmen').
inf('headmistress', 'headmistresses').
inf('head of household', 'heads of household').
inf('head of state', 'heads of state').
inf('chief of state', 'chiefs of state').
inf('headsman', 'headsmen').
inf('headman', 'headmen').
inf('heavy', 'heavies').
inf('Heidelberg man', 'Heidelberg men').
inf('Homo heidelbergensis', 'Homo heidelbergenses').
inf('heir-at-law', 'heirs-at-law').
inf('heiress', 'heiresses').
inf('inheritress', 'inheritresses').
inf('inheritrix', 'inheritrices').
inf('helmsman', 'helmsmen').
inf('steersman', 'steersmen').
inf('hierarch', 'hierarches').
inf('herdsman', 'herdsmen').
inf('intersex', 'intersexes').
inf('epicene person', 'epicene people').
inf('solitary', 'solitaries').
inf('antihero', 'antiheroes').
inf('hero', 'heroes').
inf('heterosexual person', 'heterosexual people').
inf('straight person', 'straight people').
inf('Highness', 'Highnesses').
inf('highwayman', 'highwaymen').
inf('hillbilly', 'hillbillies').
inf('hippy', 'hippies').
inf('flower child', 'flower children').
inf('hired man', 'hired men').
inf('pensionary', 'pensionaries').
inf('hockey coach', 'hockey coaches').
inf('hodman', 'hodmen').
inf('holdup man', 'holdup men').
inf('stickup man', 'stickup men').
inf('homeless', 'homelesses').
inf('homeless person', 'homeless people').
inf('Secretary of State for the Home Department', 'Secretarys of State for the Home Department').
inf('homo', 'homoes').
inf('honest woman', 'honest women').
inf('guard of honor', 'guards of honor').
inf('horseman', 'horsemen').
inf('horseman', 'horsemen').
inf('horsewoman', 'horsewomen').
inf('plantsman', 'plantsmen').
inf('hostess', 'hostesses').
inf('hostess', 'hostesses').
inf('surety', 'sureties').
inf('hotelman', 'hotelmen').
inf('housewife', 'housewives').
inf('lady of the house', 'ladies of the house').
inf('woman of the house', 'women of the house').
inf('hunted person', 'hunted people').
inf('huntsman', 'huntsmen').
inf('huntress', 'huntresses').
inf('hubby', 'hubbies').
inf('married man', 'married men').
inf('ex', 'exes').
inf('phony', 'phonies').
inf('iceman', 'icemen').
inf('idolatress', 'idolatresses').
inf('uneducated person', 'uneducated people').
inf('illiterate person', 'illiterate people').
inf('important person', 'important people').
inf('influential person', 'influential people').
inf('pseudo', 'pseudoes').
inf('inamorato', 'inamoratoes').
inf('incompetent person', 'incompetent people').
inf('infantryman', 'infantrymen').
inf('thankless wretch', 'thankless wretches').
inf('ungrateful person', 'ungrateful people').
inf('learned person', 'learned people').
inf('relative-in-law', 'relatives-in-law').
inf('sleepless person', 'sleepless people').
inf('instructress', 'instructresses').
inf('insured person', 'insured people').
inf('middleman', 'middlemen').
inf('houseman', 'housemen').
inf('iron man', 'iron men').
inf('ironman', 'ironmen').
inf('hardwareman', 'hardwaremen').
inf('gypsy', 'gypsies').
inf('gipsy', 'gipsies').
inf('Jack of all trades', 'Jacks of all trades').
inf('janissary', 'janissaries').
inf('Javanese', 'Javanese').
inf('jazzman', 'jazzmen').
inf('jimdandy', 'jimdandies').
inf('middleman', 'middlemen').
inf('man in the street', 'men in the street').
inf('jinx', 'jinges').
inf('juryman', 'jurymen').
inf('jurywoman', 'jurywomen').
inf('justice of the peace', 'justices of the peace').
inf('justiciary', 'justiciaries').
inf('kiddy', 'kiddies').
inf('male monarch', 'male monarches').
inf('Rex', 'Rexes').
inf('King of England', 'Kings of England').
inf('King of Great Britain', 'Kings of Great Britain').
inf('King of France', 'Kings of France').
inf('King of the Germans', 'Kings of the Germans').
inf('Counsel to the Crown', 'Counsels to the Crown').
inf('kinsperson', 'kinspeople').
inf('family', 'families').
inf('kinsman', 'kinsmen').
inf('kinswoman', 'kinswomen').
inf('Klansman', 'Klansmen').
inf('bachelor-at-arms', 'bachelors-at-arms').
inf('knight of the square flag', 'knights of the square flag').
inf('Knight of the Round Table', 'Knights of the Round Table').
inf('odd fish', 'odd fish').
inf('kvetch', 'kvetches').
inf('labor coach', 'labor coaches').
inf('birthing coach', 'birthing coaches').
inf('flunky', 'flunkies').
inf('noblewoman', 'noblewomen').
inf('peeress', 'peeresses').
inf('lady', 'ladies').
inf('lady-in-waiting', 'ladies-in-waiting').
inf('landlady', 'landladies').
inf('landsman', 'landsmen').
inf('landsman', 'landsmen').
inf('landman', 'landmen').
inf('lapidary', 'lapidaries').
inf('lapidary', 'lapidaries').
inf('large person', 'large people').
inf('lass', 'lasses').
inf('latchkey child', 'latchkey children').
inf('Jehovah\'s Witness', 'Jehovah\'s Witnesses').
inf('lawman', 'lawmen').
inf('layman', 'laymen').
inf('layperson', 'laypeople').
inf('lay witness', 'lay witnesses').
inf('leading lady', 'leading ladies').
inf('leading man', 'leading men').
inf('leech', 'leeches').
inf('lefty', 'lefties').
inf('lefty', 'lefties').
inf('official emissary', 'official emissaries').
inf('legionary', 'legionaries').
inf('gay woman', 'gay women').
inf('letterman', 'lettermen').
inf('life', 'lives').
inf('lighterman', 'lightermen').
inf('bargeman', 'bargemen').
inf('light-of-love', 'lights-of-love').
inf('lineman', 'linemen').
inf('lineman', 'linemen').
inf('linesman', 'linesmen').
inf('linkman', 'linkmen').
inf('literate person', 'literate people').
inf('little brother', 'little brethren').
inf('liveryman', 'liverymen').
inf('lobsterman', 'lobstermen').
inf('lockman', 'lockmen').
inf('logomach', 'logomaches').
inf('lollipop lady', 'lollipop ladies').
inf('lollipop woman', 'lollipop women').
inf('lone wolf', 'lone wolves').
inf('longbowman', 'longbowmen').
inf('lookout man', 'lookout men').
inf('sentry', 'sentries').
inf('watch', 'watches').
inf('nobleman', 'noblemen').
inf('Lord of Misrule', 'Lords of Misrule').
inf('unsuccessful person', 'unsuccessful people').
inf('underboss', 'underbosses').
inf('Lot\'s wife', 'Lot\'s wives').
inf('lummox', 'lummoxes').
inf('lowerclassman', 'lowerclassmen').
inf('underclassman', 'underclassmen').
inf('low-birth-weight baby', 'low-birth-weight babies').
inf('lumberman', 'lumbermen').
inf('luminary', 'luminaries').
inf('notability', 'notabilities').
inf('madman', 'madmen').
inf('mommy', 'mommies').
inf('mammy', 'mammies').
inf('mummy', 'mummies').
inf('macho', 'machoes').
inf('madwoman', 'madwomen').
inf('maestro', 'maestroes').
inf('mafioso', 'mafiosoes').
inf('mafioso', 'mafiosoes').
inf('magnifico', 'magnificoes').
inf('mailman', 'mailmen').
inf('postman', 'postmen').
inf('major-domo', 'major-domoes').
inf('male child', 'male children').
inf('man-child', 'man-children').
inf('maltman', 'maltmen').
inf('mammy', 'mammies').
inf('man', 'men').
inf('man', 'men').
inf('man', 'men').
inf('man', 'men').
inf('man', 'men').
inf('manageress', 'manageresses').
inf('mandatary', 'mandataries').
inf('mandatory', 'mandatories').
inf('man-at-arms', 'men-at-arms').
inf('man of action', 'men of action').
inf('man of deeds', 'men of deeds').
inf('man of letters', 'men of letters').
inf('man of means', 'men of means').
inf('rich man', 'rich men').
inf('wealthy man', 'wealthy men').
inf('marchioness', 'marchionesses').
inf('marchioness', 'marchionesses').
inf('seaman', 'seamen').
inf('marksman', 'marksmen').
inf('marquess', 'marquesses').
inf('marquess', 'marquesses').
inf('Marrano', 'Marranoes').
inf('master-at-arms', 'masters-at-arms').
inf('master of ceremonies', 'masters of ceremonies').
inf('material witness', 'material witnesses').
inf('matriarch', 'matriarches').
inf('matriarch', 'matriarches').
inf('matron of honor', 'matrons of honor').
inf('mayoress', 'mayoresses').
inf('mayoress', 'mayoresses').
inf('queen of the May', 'queens of the May').
inf('meany', 'meanies').
inf('unkind person', 'unkind people').
inf('medical man', 'medical men').
inf('medico', 'medicoes').
inf('clansman', 'clansmen').
inf('clanswoman', 'clanswomen').
inf('mensch', 'mensches').
inf('mensh', 'menshes').
inf('wise man', 'wise men').
inf('mercenary', 'mercenaries').
inf('soldier of fortune', 'soldiers of fortune').
inf('mestizo', 'mestizoes').
inf('ladino', 'ladinoes').
inf('mezzo-soprano', 'mezzo-sopranoes').
inf('mezzo', 'mezzoes').
inf('middle-aged man', 'middle-aged men').
inf('midshipman', 'midshipmen').
inf('midwife', 'midwives').
inf('mikado', 'mikadoes').
inf('tenno', 'tennoes').
inf('Milanese', 'Milanese').
inf('military policeman', 'military policemen').
inf('militiaman', 'militiamen').
inf('milkman', 'milkmen').
inf('millionairess', 'millionairesses').
inf('Minuteman', 'Minutemen').
inf('miracle man', 'miracle men').
inf('ape-man', 'ape-men').
inf('missionary', 'missionaries').
inf('missionary', 'missionaries').
inf('missis', 'misses').
inf('mistress', 'mistresses').
inf('mistress', 'mistresses').
inf('kept woman', 'kept women').
inf('fancy woman', 'fancy women').
inf('hero', 'heroes').
inf('apotheosis', 'apotheoses').
inf('nonesuch', 'nonesuches').
inf('nonsuch', 'nonsuches').
inf('mooch', 'mooches').
inf('mortal enemy', 'mortal enemies').
inf('mother-in-law', 'mothers-in-law').
inf('motorcycle policeman', 'motorcycle policemen').
inf('motorman', 'motormen').
inf('mouse', 'mice').
inf('muffin man', 'muffin men').
inf('murderess', 'murderesses').
inf('muscleman', 'musclemen').
inf('deaf-and-dumb person', 'deaf-and-dumb people').
inf('namby-pamby', 'namby-pambies').
inf('nanny', 'nannies').
inf('nebbish', 'nebbishes').
inf('nebbech', 'nebbeches').
inf('negotiatress', 'negotiatresses').
inf('negotiatrix', 'negotiatrices').
inf('newborn baby', 'newborn babies').
inf('freshman', 'freshmen').
inf('next of kin', 'nexts of kin').
inf('night watchman', 'night watchmen').
inf('ninny', 'ninnies').
inf('nonperson', 'nonpeople').
inf('unperson', 'unpeople').
inf('notary', 'notaries').
inf('tyro', 'tyroes').
inf('tiro', 'tiroes').
inf('nude person', 'nude people').
inf('numen', 'numena').
inf('nurse-midwife', 'nurse-midwives').
inf('wacko', 'wackoes').
inf('whacko', 'whackoes').
inf('nympho', 'nymphoes').
inf('oarsman', 'oarsmen').
inf('oarswoman', 'oarswomen').
inf('functionary', 'functionaries').
inf('progeny', 'progenies').
inf('oilman', 'oilmen').
inf('oilman', 'oilmen').
inf('old man', 'old men').
inf('old lady', 'old ladies').
inf('old man', 'old men').
inf('old man', 'old men').
inf('old man', 'old men').
inf('old person', 'old people').
inf('old woman', 'old women').
inf('oligarch', 'oligarches').
inf('ombudsman', 'ombudsmen').
inf('one of the boys', 'ones of the boys').
inf('Orangeman', 'Orangemen').
inf('orderly', 'orderlies').
inf('orderly', 'orderlies').
inf('ordinary', 'ordinaries').
inf('ordinary', 'ordinaries').
inf('organization man', 'organization men').
inf('ostrich', 'ostriches').
inf('outdoorsman', 'outdoorsmen').
inf('outdoorswoman', 'outdoorswomen').
inf('pachuco', 'pachucoes').
inf('pain in the neck', 'pains in the neck').
inf('paparazzo', 'paparazzoes').
inf('Member of Parliament', 'Members of Parliament').
inf('party', 'parties').
inf('party boss', 'party bosses').
inf('political boss', 'political bosses').
inf('boss', 'bosses').
inf('party man', 'party men').
inf('passerby', 'passerbies').
inf('passer-by', 'passer-bies').
inf('patriarch', 'patriarches').
inf('patriarch', 'patriarches').
inf('patriarch', 'patriarches').
inf('patriarch', 'patriarches').
inf('patroness', 'patronesses').
inf('packman', 'packmen').
inf('pitchman', 'pitchmen').
inf('peer of the realm', 'peers of the realm').
inf('pensionary', 'pensionaries').
inf('false witness', 'false witnesses').
inf('personality', 'personalities').
inf('cuss', 'cusses').
inf('gadfly', 'gadflies').
inf('petit bourgeois', 'petit bourgeois').
inf('Pharaoh of Egypt', 'Pharaohs of Egypt').
inf('apothecary', 'apothecaries').
inf('lensman', 'lensmen').
inf('pickaninny', 'pickaninnies').
inf('piccaninny', 'piccaninnies').
inf('picaninny', 'picaninnies').
inf('Piltdown man', 'Piltdown men').
inf('Piltdown hoax', 'Piltdown hoaxes').
inf('fancy man', 'fancy men').
inf('pinko', 'pinkoes').
inf('small fry', 'small fries').
inf('pitchman', 'pitchmen').
inf('pituitary dwarf', 'pituitary dwarves').
inf('hypophysial dwarf', 'hypophysial dwarves').
inf('Levi-Lorrain dwarf', 'Levi-Lorrain dwarves').
inf('pivot man', 'pivot men').
inf('placeman', 'placemen').
inf('plainclothesman', 'plainclothesmen').
inf('plainsman', 'plainsmen').
inf('man-about-town', 'men-about-town').
inf('plenipotentiary', 'plenipotentiaries').
inf('stick-in-the-mud', 'sticks-in-the-mud').
inf('slowcoach', 'slowcoaches').
inf('plowman', 'plowmen').
inf('ploughman', 'ploughmen').
inf('poetess', 'poetesses').
inf('point man', 'point men').
inf('point man', 'point men').
inf('pointsman', 'pointsmen').
inf('point woman', 'point women').
inf('policeman', 'policemen').
inf('policewoman', 'policewomen').
inf('politico', 'politicoes').
inf('pontifex', 'pontifexes').
inf('wretch', 'wretches').
inf('poor person', 'poor people').
inf('Vicar of Christ', 'Vicars of Christ').
inf('Bishop of Rome', 'Bishops of Rome').
inf('posseman', 'possemen').
inf('poster child', 'poster children').
inf('proprietress', 'proprietresses').
inf('postmistress', 'postmistresses').
inf('potman', 'potmen').
inf('poultryman', 'poultrymen').
inf('Moloch', 'Moloches').
inf('human dynamo', 'human dynamoes').
inf('ball of fire', 'balls of fire').
inf('preacher man', 'preacher men').
inf('prebendary', 'prebendaries').
inf('premature baby', 'premature babies').
inf('preterm baby', 'preterm babies').
inf('President of the United States', 'Presidents of the United States').
inf('prexy', 'prexies').
inf('chairman', 'chairmen').
inf('chairwoman', 'chairwomen').
inf('chairperson', 'chairpeople').
inf('publicity man', 'publicity men').
inf('public relations man', 'public relations men').
inf('PR man', 'PR men').
inf('prevailing party', 'prevailing parties').
inf('quarry', 'quarries').
inf('priestess', 'priestesses').
inf('gravida I', 'gravida we').
inf('para I', 'para we').
inf('primordial dwarf', 'primordial dwarves').
inf('hypoplastic dwarf', 'hypoplastic dwarves').
inf('true dwarf', 'true dwarves').
inf('normal dwarf', 'normal dwarves').
inf('Prince of Wales', 'Princes of Wales').
inf('princess', 'princesses').
inf('pressman', 'pressmen').
inf('prisoner of war', 'prisoners of war').
inf('privateersman', 'privateersmen').
inf('procuress', 'procuresses').
inf('prodigy', 'prodigies').
inf('professional person', 'professional people').
inf('pro', 'proes').
inf('property man', 'property men').
inf('propman', 'propmen').
inf('prophetess', 'prophetesses').
inf('fancy woman', 'fancy women').
inf('sporting lady', 'sporting ladies').
inf('lady of pleasure', 'ladies of pleasure').
inf('woman of the street', 'women of the street').
inf('proxy', 'proxies').
inf('psychotic person', 'psychotic people').
inf('psycho', 'psychoes').
inf('public relations person', 'public relations people').
inf('puppy', 'puppies').
inf('pygmy', 'pygmies').
inf('pigmy', 'pigmies').
inf('pythoness', 'pythonesses').
inf('quarryman', 'quarrymen').
inf('Quebecois', 'Quebecois').
inf('female monarch', 'female monarches').
inf('Queen of England', 'Queens of England').
inf('quick study', 'quick studies').
inf('raftsman', 'raftsmen').
inf('raftman', 'raftmen').
inf('reactionary', 'reactionaries').
inf('rewrite man', 'rewrite men').
inf('fireman', 'firemen').
inf('remittance man', 'remittance men').
inf('Renaissance man', 'Renaissance men').
inf('Renaissance man', 'Renaissance men').
inf('repairman', 'repairmen').
inf('maintenance man', 'maintenance men').
inf('service man', 'service men').
inf('newsman', 'newsmen').
inf('newsperson', 'newspeople').
inf('newswoman', 'newswomen').
inf('repository', 'repositories').
inf('secretary', 'secretaries').
inf('director of research', 'directors of research').
inf('retired person', 'retired people').
inf('revolutionary', 'revolutionaries').
inf('Rhodesian man', 'Rhodesian men').
inf('Homo rhodesiensis', 'Homo rhodesienses').
inf('rich person', 'rich people').
inf('wealthy person', 'wealthy people').
inf('disreputable person', 'disreputable people').
inf('rifleman', 'riflemen').
inf('rifleman', 'riflemen').
inf('right-hand man', 'right-hand men').
inf('roadman', 'roadmen').
inf('cragsman', 'cragsmen').
inf('Emperor of Rome', 'Emperors of Rome').
inf('roomy', 'roomies').
inf('lowlife', 'lowlives').
inf('so-and-so', 'so-and-soes').
inf('roundsman', 'roundsmen').
inf('civil authority', 'civil authorities').
inf('sacred cow', 'sacred kine').
inf('cracksman', 'cracksmen').
inf('crewman', 'crewmen').
inf('holy man', 'holy men').
inf('holy person', 'holy people').
inf('saleswoman', 'saleswomen').
inf('saleslady', 'salesladies').
inf('salesman', 'salesmen').
inf('salesperson', 'salespeople').
inf('sandwichman', 'sandwichmen').
inf('Sassenach', 'Sassenaches').
inf('lech', 'leches').
inf('letch', 'letches').
inf('black sheep', 'black sheep').
inf('Scaramouch', 'Scaramouches').
inf('forgetful person', 'forgetful people').
inf('schmo', 'schmoes').
inf('shmo', 'shmoes').
inf('scholarly person', 'scholarly people').
inf('bookman', 'bookmen').
inf('schoolchild', 'schoolchildren').
inf('school-age child', 'school-age children').
inf('Schoolman', 'Schoolmen').
inf('medieval Schoolman', 'medieval Schoolmen').
inf('schoolmistress', 'schoolmistresses').
inf('mistress', 'mistresses').
inf('scratch', 'scratches').
inf('penman', 'penmen').
inf('sculptress', 'sculptresses').
inf('second baseman', 'second basemen').
inf('second-in-command', 'seconds-in-command').
inf('mediocrity', 'mediocrities').
inf('secretary', 'secretaries').
inf('secretary', 'secretaries').
inf('Secretary of Agriculture', 'Secretarys of Agriculture').
inf('Secretary of Commerce', 'Secretarys of Commerce').
inf('Secretary of Defense', 'Secretarys of Defense').
inf('Secretary of Education', 'Secretarys of Education').
inf('Secretary of Energy', 'Secretarys of Energy').
inf('Secretary of Health and Human Services', 'Secretarys of Health and Human Services').
inf('Secretary of Housing and Urban Development', 'Secretarys of Housing and Urban Development').
inf('Secretary of Labor', 'Secretarys of Labor').
inf('Secretary of State', 'Secretarys of State').
inf('Secretary of the Interior', 'Secretarys of the Interior').
inf('Secretary of the Treasury', 'Secretarys of the Treasury').
inf('Secretary of Transportation', 'Secretarys of Transportation').
inf('Secretary of Veterans Affairs', 'Secretarys of Veterans Affairs').
inf('sectary', 'sectaries').
inf('section man', 'section men').
inf('ladies\' man', 'ladies\' men').
inf('seductress', 'seductresses').
inf('seedsman', 'seedsmen').
inf('seedman', 'seedmen').
inf('selectman', 'selectmen').
inf('selectwoman', 'selectwomen').
inf('selfish person', 'selfish people').
inf('semipro', 'semiproes').
inf('sergeant at arms', 'sergeants at arms').
inf('serjeant-at-arms', 'serjeants-at-arms').
inf('serjeant-at-law', 'serjeants-at-law').
inf('sergeant-at-law', 'sergeants-at-law').
inf('serviceman', 'servicemen').
inf('military man', 'military men').
inf('man', 'men').
inf('Shah of Iran', 'Shahs of Iran').
inf('sheepman', 'sheepmen').
inf('sheepman', 'sheepmen').
inf('sheep', 'sheep').
inf('sheep', 'sheep').
inf('shepherdess', 'shepherdesses').
inf('shoofly', 'shooflies').
inf('tradesman', 'tradesmen').
inf('gunman', 'gunmen').
inf('showman', 'showmen').
inf('showman', 'showmen').
inf('shy person', 'shy people').
inf('sick person', 'sick people').
inf('diseased person', 'diseased people').
inf('sidesman', 'sidesmen').
inf('signalman', 'signalmen').
inf('signatory', 'signatories').
inf('silly', 'sillies').
inf('sis', 'ses').
inf('sissy', 'sissies').
inf('pansy', 'pansies').
inf('sister-in-law', 'sisters-in-law').
inf('skilled workman', 'skilled workmen').
inf('skivvy', 'skivvies').
inf('slovenly woman', 'slovenly women').
inf('sleeping beauty', 'sleeping beauties').
inf('slovenly person', 'slovenly people').
inf('slouch', 'slouches').
inf('small businessman', 'small businessmen').
inf('small-for-gestational-age infant', 'smalls-for-gestational-age infant').
inf('small person', 'small people').
inf('beauty', 'beauties').
inf('peach', 'peaches').
inf('dish', 'dishes').
inf('smoothy', 'smoothies').
inf('snake in the grass', 'snakes in the grass').
inf('social secretary', 'social secretaries').
inf('softy', 'softies').
inf('songstress', 'songstresses').
inf('son-in-law', 'sons-in-law').
inf('man of the world', 'men of the world').
inf('soprano', 'sopranoes').
inf('shaman', 'shamen').
inf('medicine man', 'medicine men').
inf('sorceress', 'sorceresses').
inf('soundman', 'soundmen').
inf('sourpuss', 'sourpusses').
inf('picklepuss', 'picklepusses').
inf('monarch', 'monarches').
inf('witness', 'witnesses').
inf('sphinx', 'sphinges').
inf('spokesman', 'spokesmen').
inf('spokesperson', 'spokespeople').
inf('spokeswoman', 'spokeswomen').
inf('sportsman', 'sportsmen').
inf('sportswoman', 'sportswomen').
inf('sporting man', 'sporting men').
inf('outdoor man', 'outdoor men').
inf('sporting man', 'sporting men').
inf('married person', 'married people').
inf('better half', 'better halves').
inf('spy', 'spies').
inf('spy', 'spies').
inf('squaw man', 'squaw men').
inf('stableman', 'stablemen').
inf('backup man', 'backup men').
inf('statesman', 'statesmen').
inf('stateswoman', 'stateswomen').
inf('actuary', 'actuaries').
inf('stay-at-home', 'stays-at-home').
inf('homebody', 'homebodies').
inf('steelman', 'steelmen').
inf('amanuensis', 'amanuenses').
inf('stepbrother', 'stepbrethren').
inf('half-brother', 'half-brethren').
inf('half brother', 'half brethren').
inf('stepchild', 'stepchildren').
inf('longshoreman', 'longshoremen').
inf('stewardess', 'stewardesses').
inf('air hostess', 'air hostesses').
inf('hostess', 'hostesses').
inf('stipendiary', 'stipendiaries').
inf('stockholder of record', 'stockholders of record').
inf('stockman', 'stockmen').
inf('unemotional person', 'unemotional people').
inf('fireman', 'firemen').
inf('straight man', 'straight men').
inf('straw boss', 'straw bosses').
inf('assistant foreman', 'assistant foremen').
inf('floozy', 'floozies').
inf('strongman', 'strongmen').
inf('strongman', 'strongmen').
inf('he-man', 'he-men').
inf('macho-man', 'macho-men').
inf('study', 'studies').
inf('stunt man', 'stunt men').
inf('stunt woman', 'stunt women').
inf('stupid person', 'stupid people').
inf('poor fish', 'poor fish').
inf('subsidiary', 'subsidiaries').
inf('sugar daddy', 'sugar daddies').
inf('supercargo', 'supercargoes').
inf('supergrass', 'supergrasses').
inf('grass', 'grasses').
inf('supernumerary', 'supernumeraries').
inf('supernumerary', 'supernumeraries').
inf('supremo', 'supremoes').
inf('swagman', 'swagmen').
inf('steady', 'steadies').
inf('pigman', 'pigmen').
inf('switchman', 'switchmen').
inf('toady', 'toadies').
inf('tallyman', 'tallymen').
inf('tallyman', 'tallymen').
inf('taoiseach', 'taoiseaches').
inf('taskmistress', 'taskmistresses').
inf('taxman', 'taxmen').
inf('exciseman', 'excisemen').
inf('collector of internal revenue', 'collectors of internal revenue').
inf('taximan', 'taximen').
inf('cabman', 'cabmen').
inf('cabby', 'cabbies').
inf('TV newsman', 'TV newsmen').
inf('earthman', 'earthmen').
inf('temporary', 'temporaries').
inf('tenderfoot', 'tenderfeet').
inf('tennis coach', 'tennis coaches').
inf('tennis pro', 'tennis proes').
inf('testatrix', 'testatrices').
inf('test-tube baby', 'test-tube babies').
inf('thin person', 'thin people').
inf('third baseman', 'third basemen').
inf('third party', 'third parties').
inf('thrush', 'thrushes').
inf('timberman', 'timbermen').
inf('Timorese', 'Timorese').
inf('T-man', 'T-men').
inf('symposiarch', 'symposiarches').
inf('toast mistress', 'toast mistresses').
inf('bambino', 'bambinoes').
inf('tollman', 'tollmen').
inf('plug-ugly', 'plug-uglies').
inf('tovarich', 'tovariches').
inf('tovarisch', 'tovarisches').
inf('tower of strength', 'towers of strength').
inf('pillar of strength', 'pillars of strength').
inf('townsman', 'townsmen').
inf('towny', 'townies').
inf('townsman', 'townsmen').
inf('trail boss', 'trail bosses').
inf('trainbandsman', 'trainbandsmen').
inf('trainman', 'trainmen').
inf('railroad man', 'railroad men').
inf('railwayman', 'railwaymen').
inf('railway man', 'railway men').
inf('traitress', 'traitresses').
inf('hobo', 'hoboes').
inf('traveling salesman', 'traveling salesmen').
inf('travelling salesman', 'travelling salesmen').
inf('roadman', 'roadmen').
inf('bagman', 'bagmen').
inf('tribesman', 'tribesmen').
inf('trophy wife', 'trophy wives').
inf('trusty', 'trusties').
inf('undersecretary', 'undersecretaries').
inf('understudy', 'understudies').
inf('standby', 'standbies').
inf('unemployed person', 'unemployed people').
inf('unmarried woman', 'unmarried women').
inf('unpleasant woman', 'unpleasant women').
inf('disagreeable woman', 'disagreeable women').
inf('usufructuary', 'usufructuaries').
inf('utility man', 'utility men').
inf('utility man', 'utility men').
inf('gentleman', 'gentlemen').
inf('gentleman\'s gentleman', 'gentleman\'s gentlemen').
inf('man', 'men').
inf('liegeman', 'liegemen').
inf('feudatory', 'feudatories').
inf('very important person', 'very important people').
inf('dignitary', 'dignitaries').
inf('vestryman', 'vestrymen').
inf('vestrywoman', 'vestrywomen').
inf('ex-serviceman', 'ex-servicemen').
inf('veterinary', 'veterinaries').
inf('vice chairman', 'vice chairmen').
inf('vigilance man', 'vigilance men').
inf('villainess', 'villainesses').
inf('virago', 'viragoes').
inf('virtuoso', 'virtuosoes').
inf('viscountess', 'viscountesses').
inf('viscountess', 'viscountesses').
inf('visionary', 'visionaries').
inf('visionary', 'visionaries').
inf('visiting fireman', 'visiting firemen').
inf('visually impaired person', 'visually impaired people').
inf('harpy', 'harpies').
inf('voluntary', 'voluntaries').
inf('voluptuary', 'voluptuaries').
inf('votary', 'votaries').
inf('votary', 'votaries').
inf('votary', 'votaries').
inf('street child', 'street children').
inf('waitress', 'waitresses').
inf('wally', 'wallies').
inf('bird of passage', 'birds of passage').
inf('war baby', 'war babies').
inf('wardress', 'wardresses').
inf('warehouseman', 'warehousemen').
inf('god of war', 'gods of war').
inf('washerman', 'washermen').
inf('laundryman', 'laundrymen').
inf('washwoman', 'washwomen').
inf('washerwoman', 'washerwomen').
inf('laundrywoman', 'laundrywomen').
inf('laundress', 'laundresses').
inf('watchman', 'watchmen').
inf('water witch', 'water witches').
inf('wuss', 'wusses').
inf('weatherman', 'weathermen').
inf('crybaby', 'crybabies').
inf('whirling dervish', 'whirling dervishes').
inf('witch', 'witches').
inf('widow woman', 'widow women').
inf('widowman', 'widowmen').
inf('wife', 'wives').
inf('married woman', 'married women').
inf('wild man', 'wild men').
inf('feral man', 'feral men').
inf('crybaby', 'crybabies').
inf('wingman', 'wingmen').
inf('wireman', 'wiremen').
inf('witness', 'witnesses').
inf('witness', 'witnesses').
inf('witness', 'witnesses').
inf('wolf', 'wolves').
inf('woman', 'women').
inf('woman', 'women').
inf('wonder woman', 'wonder women').
inf('woodsman', 'woodsmen').
inf('woodman', 'woodmen').
inf('woodsman', 'woodsmen').
inf('woodman', 'woodmen').
inf('workman', 'workmen').
inf('workingman', 'workingmen').
inf('working man', 'working men').
inf('working person', 'working people').
inf('louse', 'lice').
inf('worthy', 'worthies').
inf('matman', 'matmen').
inf('wretch', 'wretches').
inf('write-in candidate', 'writes-in candidate').
inf('yachtsman', 'yachtsmen').
inf('yachtswoman', 'yachtswomen').
inf('yardman', 'yardmen').
inf('yardman', 'yardmen').
inf('yeoman', 'yeomen').
inf('yeoman', 'yeomen').
inf('yeoman of the guard', 'yeomen of the guard').
inf('young man', 'young men').
inf('young person', 'young people').
inf('zany', 'zanies').
inf('Aalto', 'Aaltoes').
inf('Alvar Aalto', 'Alvar Aaltoes').
inf('Hugo Alvar Henrik Aalto', 'Hugo Alvar Henrik Aaltoes').
inf('Alexander I', 'Alexander we').
inf('Czar Alexander I', 'Czar Alexander we').
inf('Aleksandr Pavlovich', 'Aleksandr Pavloviches').
inf('Alonso', 'Alonsoes').
inf('Alicia Alonso', 'Alicia Alonsoes').
inf('Aristarchus of Samos', 'Aristarchuss of Samos').
inf('Jakob Hermandszoon', 'Jakob Hermandszoa').
inf('Satchmo', 'Satchmoes').
inf('Arnold of Brescia', 'Arnolds of Brescia').
inf('Artaxerxes I', 'Artaxerxes we').
inf('Asch', 'Asches').
inf('Sholem Asch', 'Sholem Asches').
inf('Shalom Asch', 'Shalom Asches').
inf('Sholom Asch', 'Sholom Asches').
inf('Scourge of God', 'Scourges of God').
inf('Scourge of the Gods', 'Scourges of the Gods').
inf('Auchincloss', 'Auchinclosses').
inf('Louis Auchincloss', 'Louis Auchinclosses').
inf('Louis Stanton Auchincloss', 'Louis Stanton Auchinclosses').
inf('Augustine of Hippo', 'Augustines of Hippo').
inf('Avogadro', 'Avogadroes').
inf('Amedeo Avogadro', 'Amedeo Avogadroes').
inf('Bach', 'Baches').
inf('Johann Sebastian Bach', 'Johann Sebastian Baches').
inf('Baruch', 'Baruches').
inf('Baruch', 'Baruches').
inf('Bernard Baruch', 'Bernard Baruches').
inf('Bernard Mannes Baruch', 'Bernard Mannes Baruches').
inf('Basil of Caesarea', 'Basils of Caesarea').
inf('Bellarmino', 'Bellarminoes').
inf('Bergman', 'Bergmen').
inf('Ingmar Bergman', 'Ingmar Bergmen').
inf('Bergman', 'Bergmen').
inf('Ingrid Bergman', 'Ingrid Bergmen').
inf('Bloch', 'Bloches').
inf('Ernest Bloch', 'Ernest Bloches').
inf('Apostle of Germany', 'Apostles of Germany').
inf('Duchess of Ferrara', 'Duchesses of Ferrara').
inf('Bosch', 'Bosches').
inf('Hieronymus Bosch', 'Hieronymus Bosches').
inf('Bowditch', 'Bowditches').
inf('Nathaniel Bowditch', 'Nathaniel Bowditches').
inf('Robert I', 'Robert we').
inf('Bruch', 'Bruches').
inf('Max Bruch', 'Max Bruches').
inf('Bruno', 'Brunoes').
inf('Giordano Bruno', 'Giordano Brunoes').
inf('Bruno', 'Brunoes').
inf('Saint Bruno', 'Saint Brunoes').
inf('St. Bruno', 'St. Brunoes').
inf('Bullfinch', 'Bullfinches').
inf('Charles Bullfinch', 'Charles Bullfinches').
inf('Burgess', 'Burgesses').
inf('Anthony Burgess', 'Anthony Burgesses').
inf('Bush', 'Bushes').
inf('George Bush', 'George Bushes').
inf('George H.W. Bush', 'George H.W. Bushes').
inf('George Herbert Walker Bush', 'George Herbert Walker Bushes').
inf('President Bush', 'President Bushes').
inf('Bush', 'Bushes').
inf('Vannevar Bush', 'Vannevar Bushes').
inf('Bush', 'Bushes').
inf('George Bush', 'George Bushes').
inf('George W. Bush', 'George W. Bushes').
inf('George Walker Bush', 'George Walker Bushes').
inf('President Bush', 'President Bushes').
inf('President George W. Bush', 'President George W. Bushes').
inf('Father of the Submarine', 'Fathers of the Submarine').
inf('Giovanni Cabato', 'Giovanni Cabatoes').
inf('Cagliostro', 'Cagliostroes').
inf('Count Alessandro di Cagliostro', 'Count Alessandro di Cagliostroes').
inf('Giuseppe Balsamo', 'Giuseppe Balsamoes').
inf('Guy of Burgundy', 'Guys of Burgundy').
inf('Calvino', 'Calvinoes').
inf('Italo Calvino', 'Italo Calvinoes').
inf('Caruso', 'Carusoes').
inf('Enrico Caruso', 'Enrico Carusoes').
inf('Cash', 'Cashes').
inf('Johnny Cash', 'Johnny Cashes').
inf('John Cash', 'John Cashes').
inf('Castro', 'Castroes').
inf('Fidel Castro', 'Fidel Castroes').
inf('Catherine I', 'Catherine we').
inf('Catherine of Aragon', 'Catherines of Aragon').
inf('Catherine de Medicis', 'Catherine de Medices').
inf('Cavendish', 'Cavendishes').
inf('Henry Cavendish', 'Henry Cavendishes').
inf('Chapman', 'Chapmen').
inf('John Chapman', 'John Chapmen').
inf('Charles I', 'Charles we').
inf('Charles I', 'Charles we').
inf('Charles I', 'Charles we').
inf('Chirico', 'Chiricoes').
inf('Giorgio de Chirico', 'Giorgio de Chiricoes').
inf('Duke of Marlborough', 'Dukes of Marlborough').
inf('Cicero', 'Ciceroes').
inf('Marcus Tullius Cicero', 'Marcus Tullius Ciceroes').
inf('Claudius I', 'Claudius we').
inf('Clemenceau', 'Clemenceaux').
inf('Georges Clemenceau', 'Georges Clemenceaux').
inf('Georges Eugene Benjamin Clemenceau', 'Georges Eugene Benjamin Clemenceaux').
inf('Guibert of Ravenna', 'Guiberts of Ravenna').
inf('Clovis I', 'Clovis we').
inf('Cocteau', 'Cocteaux').
inf('Jean Cocteau', 'Jean Cocteaux').
inf('Cristoforo Colombo', 'Cristoforo Colomboes').
inf('Constantine I', 'Constantine we').
inf('Cousteau', 'Cousteaux').
inf('Jacques Costeau', 'Jacques Costeaux').
inf('Jacques Yves Costeau', 'Jacques Yves Costeaux').
inf('Tashunca-Uitco', 'Tashunca-Uitcoes').
inf('Duke of Cumberland', 'Dukes of Cumberland').
inf('Curtiss', 'Curtisses').
inf('Glenn Curtiss', 'Glenn Curtisses').
inf('Glenn Hammond Curtiss', 'Glenn Hammond Curtisses').
inf('Darius I', 'Darius we').
inf('Father of Radio', 'Fathers of Radio').
inf('Delacroix', 'Delacroixes').
inf('Eugene Delacroix', 'Eugene Delacroixes').
inf('Ferdinand Victor Eugene Delacroix', 'Ferdinand Victor Eugene Delacroixes').
inf('Demetrius I', 'Demetrius we').
inf('De Niro', 'De Niroes').
inf('Robert De Niro', 'Robert De Niroes').
inf('Depardieu', 'Depardieu').
inf('Gerard Depardieu', 'Gerard Depardieu').
inf('Princess of Wales', 'Princesses of Wales').
inf('Duchesse de Valentinois', 'Duchesse de Valentinois').
inf('Dietrich', 'Dietriches').
inf('Marlene Dietrich', 'Marlene Dietriches').
inf('Maria Magdalene von Losch', 'Maria Magdalene von Losches').
inf('Dix', 'Dixes').
inf('Dorothea Dix', 'Dorothea Dixes').
inf('Dorothea Lynde Dix', 'Dorothea Lynde Dixes').
inf('Domingo', 'Domingoes').
inf('Placido Domingo', 'Placido Domingoes').
inf('Domingo de Guzman', 'Domingo de Guzmen').
inf('Domino', 'Dominoes').
inf('Fats Domino', 'Fats Dominoes').
inf('Antoine Domino', 'Antoine Dominoes').
inf('Donatello', 'Donatelloes').
inf('Douglass', 'Douglasses').
inf('Frederick Douglass', 'Frederick Douglasses').
inf('Draco', 'Dracoes').
inf('Du Bois', 'Du Bois').
inf('W. E. B. Du Bois', 'W. E. B. Du Bois').
inf('William Edward Burghardt Du Bois', 'William Edward Burghardt Du Bois').
inf('Eastman', 'Eastmen').
inf('George Eastman', 'George Eastmen').
inf('Edmund I', 'Edmund we').
inf('Edward I', 'Edward we').
inf('Duke of Windsor', 'Dukes of Windsor').
inf('Ehrlich', 'Ehrliches').
inf('Paul Ehrlich', 'Paul Ehrliches').
inf('Eijkman', 'Eijkmen').
inf('Christiaan Eijkman', 'Christiaan Eijkmen').
inf('Ekman', 'Ekmen').
inf('Vagn Walfrid Ekman', 'Vagn Walfrid Ekmen').
inf('Eleanor of Aquitaine', 'Eleanors of Aquitaine').
inf('El Greco', 'El Grecoes').
inf('Greco', 'Grecoes').
inf('Elizabeth I', 'Elizabeth we').
inf('Enesco', 'Enescoes').
inf('Georges Enesco', 'Georges Enescoes').
inf('Ethelred I', 'Ethelred we').
inf('Eusebius of Caesarea', 'Eusebiuss of Caesarea').
inf('Julius Ullman', 'Julius Ullmen').
inf('Farouk I', 'Farouk we').
inf('Faruk I', 'Faruk we').
inf('Ferdinand I', 'Ferdinand we').
inf('Ferdinand I', 'Ferdinand we').
inf('Ferdinand of Aragon', 'Ferdinands of Aragon').
inf('Feynman', 'Feynmen').
inf('Richard Feynman', 'Richard Feynmen').
inf('Richard Phillips Feynman', 'Richard Phillips Feynmen').
inf('Fox', 'Foxes').
inf('George Fox', 'George Foxes').
inf('Fox', 'Foxes').
inf('Charles James Fox', 'Charles James Foxes').
inf('Francis Joseph I', 'Francis Joseph we').
inf('Franz Josef I', 'Franz Josef we').
inf('Francis of Assisi', 'Frances of Assisi').
inf('Saint Francis', 'Saint Frances').
inf('St. Francis', 'St. Frances').
inf('Franco', 'Francoes').
inf('Francisco Franco', 'Francisco Francoes').
inf('El Caudillo', 'El Caudilloes').
inf('General Franco', 'General Francoes').
inf('Frederick I', 'Frederick we').
inf('Frederick I', 'Frederick we').
inf('Frederick William I', 'Frederick William we').
inf('French', 'Frenches').
inf('Daniel Chester French', 'Daniel Chester Frenches').
inf('Friedman', 'Friedmen').
inf('Milton Friedman', 'Milton Friedmen').
inf('Frisch', 'Frisches').
inf('Karl von Frisch', 'Karl von Frisches').
inf('Frisch', 'Frisches').
inf('Ragnar Frisch', 'Ragnar Frisches').
inf('Ragnar Anton Kittil Frisch', 'Ragnar Anton Kittil Frisches').
inf('Frisch', 'Frisches').
inf('Otto Frisch', 'Otto Frisches').
inf('Otto Robert Frisch', 'Otto Robert Frisches').
inf('Galois', 'Galois').
inf('Evariste Galois', 'Evariste Galois').
inf('Garbo', 'Garboes').
inf('Greta Garbo', 'Greta Garboes').
inf('Gauss', 'Gausses').
inf('Karl Gauss', 'Karl Gausses').
inf('Karl Friedrich Gauss', 'Karl Friedrich Gausses').
inf('Dr. Seuss', 'Dr. Seusses').
inf('Geoffrey of Monmouth', 'Geoffreys of Monmouth').
inf('George I', 'George we').
inf('Geronimo', 'Geronimoes').
inf('Gilgamesh', 'Gilgameshes').
inf('Gilman', 'Gilmen').
inf('Charlotte Anna Perkins Gilman', 'Charlotte Anna Perkins Gilmen').
inf('Dorothy Dix', 'Dorothy Dixes').
inf('Giotto', 'Giottoes').
inf('Giraudoux', 'Giraudouxes').
inf('Jean Giraudoux', 'Jean Giraudouxes').
inf('Hippolyte Jean Giraudoux', 'Hippolyte Jean Giraudouxes').
inf('Gish', 'Gishes').
inf('Lillian Gish', 'Lillian Gishes').
inf('Goldman', 'Goldmen').
inf('Emma Goldman', 'Emma Goldmen').
inf('Goodman', 'Goodmen').
inf('Benny Goodman', 'Benny Goodmen').
inf('Benjamin David Goodman', 'Benjamin David Goodmen').
inf('King of Swing', 'Kings of Swing').
inf('Grass', 'Grasses').
inf('Gunter Grass', 'Gunter Grasses').
inf('Gunter Wilhelm Grass', 'Gunter Wilhelm Grasses').
inf('Gregory I', 'Gregory we').
inf('Saint Gregory I', 'Saint Gregory we').
inf('St. Gregory I', 'St. Gregory we').
inf('Gregory of Nazianzen', 'Gregorys of Nazianzen').
inf('Gromyko', 'Gromykoes').
inf('Andrei Gromyko', 'Andrei Gromykoes').
inf('Andrei Andreyevich Gromyko', 'Andrei Andreyevich Gromykoes').
inf('Guinness', 'Guinnesses').
inf('Alec Guinness', 'Alec Guinnesses').
inf('Sir Alec Guinness', 'Sir Alec Guinnesses').
inf('Gustavus I', 'Gustavus we').
inf('Harold I', 'Harold we').
inf('King Harold I', 'King Harold we').
inf('Harold Harefoot', 'Harold Harefeet').
inf('Harefoot', 'Harefeet').
inf('Harriman', 'Harrimen').
inf('E. H. Harriman', 'E. H. Harrimen').
inf('Edward Henry Harriman', 'Edward Henry Harrimen').
inf('Harriman', 'Harrimen').
inf('Averell Harriman', 'Averell Harrimen').
inf('William Averell Harriman', 'William Averell Harrimen').
inf('Hellman', 'Hellmen').
inf('Lillian Hellman', 'Lillian Hellmen').
inf('Hendrix', 'Hendrixes').
inf('Jimi Hendrix', 'Jimi Hendrixes').
inf('James Marshall Hendrix', 'James Marshall Hendrixes').
inf('Henry I', 'Henry we').
inf('Henry of Navarre', 'Henrys of Navarre').
inf('Herman', 'Hermen').
inf('Woody Herman', 'Woody Hermen').
inf('Woodrow Charles Herman', 'Woodrow Charles Hermen').
inf('Hero', 'Heroes').
inf('Hero of Alexandria', 'Heroes of Alexandria').
inf('Hess', 'Hesses').
inf('Victor Hess', 'Victor Hesses').
inf('Victor Franz Hess', 'Victor Franz Hesses').
inf('Hess', 'Hesses').
inf('Rudolf Hess', 'Rudolf Hesses').
inf('Walther Richard Rudolf Hess', 'Walther Richard Rudolf Hesses').
inf('Hess', 'Hesses').
inf('Walter Hess', 'Walter Hesses').
inf('Walter Rudolf Hess', 'Walter Rudolf Hesses').
inf('Hess', 'Hesses').
inf('Dame Myra Hess', 'Dame Myra Hesses').
inf('Hirohito', 'Hirohitoes').
inf('Michinomiya Hirohito', 'Michinomiya Hirohitoes').
inf('Hoffman', 'Hoffmen').
inf('Dustin Hoffman', 'Dustin Hoffmen').
inf('Hoffman', 'Hoffmen').
inf('Malvina Hoffman', 'Malvina Hoffmen').
inf('Housman', 'Housmen').
inf('A. E. Housman', 'A. E. Housmen').
inf('Alfred Edward Housman', 'Alfred Edward Housmen').
inf('Hugo', 'Hugoes').
inf('Victor Hugo', 'Victor Hugoes').
inf('Victor-Marie Hugo', 'Victor-Marie Hugoes').
inf('Huss', 'Husses').
inf('John Huss', 'John Husses').
inf('Ignatius of Loyola', 'Ignatiuss of Loyola').
inf('Giovanni Battista Cibo', 'Giovanni Battista Ciboes').
inf('Ionesco', 'Ionescoes').
inf('Eugene Ionesco', 'Eugene Ionescoes').
inf('Isabella I', 'Isabella we').
inf('Ivan III Vasilievich', 'Ivan III Vasilieviches').
inf('Ivan Iv Vasilievich', 'Ivan Iv Vasilieviches').
inf('James I', 'James we').
inf('King James I', 'King James we').
inf('Joan of Arc', 'Joans of Arc').
inf('Jeroboam I', 'Jeroboam we').
inf('Jesus of Nazareth', 'Jesuss of Nazareth').
inf('El Nino', 'El Ninoes').
inf('John of Gaunt', 'Johns of Gaunt').
inf('Duke of Lancaster', 'Dukes of Lancaster').
inf('John Paul I', 'John Paul we').
inf('Albino Luciano', 'Albino Lucianoes').
inf('Jussieu', 'Jussieu').
inf('Antoine Laurent de Jussieu', 'Antoine Laurent de Jussieu').
inf('Justinian I', 'Justinian we').
inf('Kamehameha I', 'Kamehameha we').
inf('Kaufman', 'Kaufmen').
inf('George S. Kaufman', 'George S. Kaufmen').
inf('George Simon Kaufman', 'George Simon Kaufmen').
inf('Princess Grace of Monaco', 'Princess Grace of Monacoes').
inf('Knox', 'Knoxes').
inf('John Knox', 'John Knoxes').
inf('Koch', 'Koches').
inf('Robert Koch', 'Robert Koches').
inf('Kosciusko', 'Kosciuskoes').
inf('Thaddeus Kosciusko', 'Thaddeus Kosciuskoes').
inf('Kosciuszko', 'Kosciuszkoes').
inf('Tadeusz Andrzej Bonawentura Kosciuszko', 'Tadeusz Andrzej Bonawentura Kosciuszkoes').
inf('Kroto', 'Krotoes').
inf('Harold Kroto', 'Harold Krotoes').
inf('Harold W. Kroto', 'Harold W. Krotoes').
inf('Sir Harold Walter Kroto', 'Sir Harold Walter Krotoes').
inf('Prince of Smolensk', 'Princes of Smolensk').
inf('Lasso', 'Lassoes').
inf('Orlando di Lasso', 'Orlando di Lassoes').
inf('Lawrence of Arabia', 'Lawrences of Arabia').
inf('Le Duc Tho', 'Le Duc Thoes').
inf('Leo I', 'Leo we').
inf('St. Leo I', 'St. Leo we').
inf('Bruno', 'Brunoes').
inf('Bruno of Toul', 'Brunoes of Toul').
inf('Leonardo', 'Leonardoes').
inf('Levi-Strauss', 'Levi-Strausses').
inf('Claude Levi-Strauss', 'Claude Levi-Strausses').
inf('Li Po', 'Li Poes').
inf('Louis I', 'Louis we').
inf('Louis the German', 'Louis the Germen').
inf('Lubitsch', 'Lubitsches').
inf('Ernst Lubitsch', 'Ernst Lubitsches').
inf('Bela Ferenc Blasko', 'Bela Ferenc Blaskoes').
inf('Lysenko', 'Lysenkoes').
inf('Trofim Denisovich Lysenko', 'Trofim Denisovich Lysenkoes').
inf('Mach', 'Maches').
inf('Ernst Mach', 'Ernst Maches').
inf('MacLeish', 'MacLeishes').
inf('Archibald MacLeish', 'Archibald MacLeishes').
inf('Malevich', 'Maleviches').
inf('Kazimir Malevich', 'Kazimir Maleviches').
inf('Kazimir Severinovich Malevich', 'Kazimir Severinovich Maleviches').
inf('Malraux', 'Malrauxes').
inf('Andre Malraux', 'Andre Malrauxes').
inf('Marceau', 'Marceaux').
inf('Marcel Marceau', 'Marcel Marceaux').
inf('Marciano', 'Marcianoes').
inf('Rocco Marciano', 'Rocco Marcianoes').
inf('Rocky Marciano', 'Rocky Marcianoes').
inf('Marino', 'Marinoes').
inf('Giambattista Marino', 'Giambattista Marinoes').
inf('Marsh', 'Marshes').
inf('Ngaio Marsh', 'Ngaio Marshes').
inf('Marsh', 'Marshes').
inf('Reginald Marsh', 'Reginald Marshes').
inf('Marx', 'Marxes').
inf('Karl Marx', 'Karl Marxes').
inf('Marx', 'Marxes').
inf('Julius Marx', 'Julius Marxes').
inf('Groucho', 'Grouchoes').
inf('Marx', 'Marxes').
inf('Leonard Marx', 'Leonard Marxes').
inf('Chico', 'Chicoes').
inf('Marx', 'Marxes').
inf('Arthur Marx', 'Arthur Marxes').
inf('Harpo', 'Harpoes').
inf('Marx', 'Marxes').
inf('Herbert Marx', 'Herbert Marxes').
inf('Zeppo', 'Zeppoes').
inf('Mary I', 'Mary we').
inf('Maurois', 'Maurois').
inf('Andre Maurois', 'Andre Maurois').
inf('Meiji Tenno', 'Meiji Tennoes').
inf('Mutsuhito', 'Mutsuhitoes').
inf('Merckx', 'Merckxes').
inf('Eddy Merckx', 'Eddy Merckxes').
inf('Merman', 'Mermen').
inf('Ethel Merman', 'Ethel Mermen').
inf('Metternich', 'Metterniches').
inf('Klemens Metternich', 'Klemens Metterniches').
inf('Prince Klemens Wenzel Nepomuk Lothar von Metternich', 'Prince Klemens Wenzel Nepomuk Lothar von Metterniches').
inf('Michelangelo', 'Michelangeloes').
inf('Mirabeau', 'Mirabeaux').
inf('Comte de Mirabeau', 'Comte de Mirabeaux').
inf('Miro', 'Miroes').
inf('Joan Miro', 'Joan Miroes').
inf('Amedeo Modigliano', 'Amedeo Modiglianoes').
inf('Montesquieu', 'Montesquieu').
inf('Baron de la Brede et de Montesquieu', 'Baron de la Brede et de Montesquieu').
inf('Earl of Leicester', 'Earls of Leicester').
inf('Munch', 'Munches').
inf('Edvard Munch', 'Edvard Munches').
inf('Munro', 'Munroes').
inf('H. H. Munro', 'H. H. Munroes').
inf('Hector Hugh Munro', 'Hector Hugh Munroes').
inf('Murdoch', 'Murdoches').
inf('Iris Murdoch', 'Iris Murdoches').
inf('Dame Jean Iris Murdoch', 'Dame Jean Iris Murdoches').
inf('Murdoch', 'Murdoches').
inf('Rupert Murdoch', 'Rupert Murdoches').
inf('Keith Rupert Murdoch', 'Keith Rupert Murdoches').
inf('Murillo', 'Murilloes').
inf('Bartolome Esteban Murillo', 'Bartolome Esteban Murilloes').
inf('Napoleon I', 'Napoleon we').
inf('Nash', 'Nashes').
inf('Ogden Nash', 'Ogden Nashes').
inf('Nero', 'Neroes').
inf('Newman', 'Newmen').
inf('John Henry Newman', 'John Henry Newmen').
inf('Cardinal Newman', 'Cardinal Newmen').
inf('Newman', 'Newmen').
inf('Paul Newman', 'Paul Newmen').
inf('Paul Leonard Newman', 'Paul Leonard Newmen').
inf('Nicholas I', 'Nicholas we').
inf('Czar Nicholas I', 'Czar Nicholas we').
inf('Lady with the Lamp', 'Ladys with the Lamp').
inf('Norman', 'Normen').
inf('Greg Norman', 'Greg Normen').
inf('Gregory John Norman', 'Gregory John Normen').
inf('Norman', 'Normen').
inf('Jessye Norman', 'Jessye Normen').
inf('Norrish', 'Norrishes').
inf('Ronald George Wreyford Norrish', 'Ronald George Wreyford Norrishes').
inf('William of Occam', 'Williams of Occam').
inf('William of Ockham', 'Williams of Ockham').
inf('Offenbach', 'Offenbaches').
inf('Jacques Offenbach', 'Jacques Offenbaches').
inf('Okenfuss', 'Okenfusses').
inf('Lorenz Okenfuss', 'Lorenz Okenfusses').
inf('Ono', 'Onoes').
inf('Yoko Ono', 'Yoko Onoes').
inf('Orozco', 'Orozcoes').
inf('Jose Orozco', 'Jose Orozcoes').
inf('Jose Clemente Orozco', 'Jose Clemente Orozcoes').
inf('Osman I', 'Osman we').
inf('Othman I', 'Othman we').
inf('Otto I', 'Otto we').
inf('Otho I', 'Otho we').
inf('Publius Ovidius Naso', 'Publius Ovidius Nasoes').
inf('Pareto', 'Paretoes').
inf('Vilfredo Pareto', 'Vilfredo Paretoes').
inf('Parrish', 'Parrishes').
inf('Maxfield Parrish', 'Maxfield Parrishes').
inf('Maxfield Frederick Parrish', 'Maxfield Frederick Parrishes').
inf('Apostle of the Gentiles', 'Apostles of the Gentiles').
inf('Saul of Tarsus', 'Sauls of Tarsus').
inf('Alessandro Farnese', 'Alessandro Farnese').
inf('Peter I', 'Peter we').
inf('Czar Peter I', 'Czar Peter we').
inf('Petrarch', 'Petrarches').
inf('Duke of Edinburgh', 'Dukes of Edinburgh').
inf('Philip of Valois', 'Philips of Valois').
inf('Picasso', 'Picassoes').
inf('Pablo Picasso', 'Pablo Picassoes').
inf('Pirandello', 'Pirandelloes').
inf('Luigi Pirandello', 'Luigi Pirandelloes').
inf('Pitman', 'Pitmen').
inf('Sir Isaac Pitman', 'Sir Isaac Pitmen').
inf('Giuseppe Sarto', 'Giuseppe Sartoes').
inf('Giuseppe Melchiorre Sarto', 'Giuseppe Melchiorre Sartoes').
inf('Pizarro', 'Pizarroes').
inf('Francisco Pizarro', 'Francisco Pizarroes').
inf('Plato', 'Platoes').
inf('Plutarch', 'Plutarches').
inf('Polo', 'Poloes').
inf('Marco Polo', 'Marco Poloes').
inf('Ptolemy I', 'Ptolemy we').
inf('Rain-in-the-Face', 'Rains-in-the-Face').
inf('Rameau', 'Rameaux').
inf('Jean-Philippe Rameau', 'Jean-Philippe Rameaux').
inf('Reich', 'Reiches').
inf('Steve Reich', 'Steve Reiches').
inf('Stephen Michael Reich', 'Stephen Michael Reiches').
inf('Reich', 'Reiches').
inf('Wilhelm Reich', 'Wilhelm Reiches').
inf('Ricardo', 'Ricardoes').
inf('David Ricardo', 'David Ricardoes').
inf('Richard I', 'Richard we').
inf('Richelieu', 'Richelieu').
inf('Duc de Richelieu', 'Duc de Richelieu').
inf('Armand Jean du Plessis', 'Armand Jean du Plesses').
inf('Cardinal Richelieu', 'Cardinal Richelieu').
inf('Riesman', 'Riesmen').
inf('David Riesman', 'David Riesmen').
inf('Rochambeau', 'Rochambeaux').
inf('Comte de Rochambeau', 'Comte de Rochambeaux').
inf('Rollo', 'Rolloes').
inf('Rolf', 'Rolves').
inf('Hrolf', 'Hrolves').
inf('Desert Fox', 'Desert Foxes').
inf('Ross', 'Rosses').
inf('Betsy Ross', 'Betsy Rosses').
inf('Betsy Griscom Ross', 'Betsy Griscom Rosses').
inf('Ross', 'Rosses').
inf('Nellie Ross', 'Nellie Rosses').
inf('Nellie Tayloe Ross', 'Nellie Tayloe Rosses').
inf('Ross', 'Rosses').
inf('Sir Ronald Ross', 'Sir Ronald Rosses').
inf('Ross', 'Rosses').
inf('James Clark Ross', 'James Clark Rosses').
inf('Sir James Clark Ross', 'Sir James Clark Rosses').
inf('Ross', 'Rosses').
inf('John Ross', 'John Rosses').
inf('Sir John Ross', 'Sir John Rosses').
inf('Rothko', 'Rothkoes').
inf('Mark Rothko', 'Mark Rothkoes').
inf('Rothschild', 'Rothschildren').
inf('Rousseau', 'Rousseaux').
inf('Jean-Jacques Rousseau', 'Jean-Jacques Rousseaux').
inf('Rousseau', 'Rousseaux').
inf('Henri Rousseau', 'Henri Rousseaux').
inf('Le Douanier Rousseau', 'Le Douanier Rousseaux').
inf('Rush', 'Rushes').
inf('Benjamin Rush', 'Benjamin Rushes').
inf('Sultan of Swat', 'Sultans of Swat').
inf('Sacco', 'Saccoes').
inf('Nicola Sacco', 'Nicola Saccoes').
inf('Sappho', 'Sapphoes').
inf('Sax', 'Saxes').
inf('Adolphe Sax', 'Adolphe Saxes').
inf('Scorsese', 'Scorsese').
inf('Martin Scorsese', 'Martin Scorsese').
inf('Seaman', 'Seamen').
inf('Elizabeth Seaman', 'Elizabeth Seamen').
inf('Elizabeth Cochrane Seaman', 'Elizabeth Cochrane Seamen').
inf('Seleucus I', 'Seleucus we').
inf('George Guess', 'George Guesses').
inf('Bard of Avon', 'Bards of Avon').
inf('Sherman', 'Shermen').
inf('Roger Sherman', 'Roger Shermen').
inf('Sherman', 'Shermen').
inf('William Tecumseh Sherman', 'William Tecumseh Shermen').
inf('Shevchenko', 'Shevchenkoes').
inf('Taras Grigoryevich Shevchenko', 'Taras Grigoryevich Shevchenkoes').
inf('Shostakovich', 'Shostakoviches').
inf('Dmitri Shostakovich', 'Dmitri Shostakoviches').
inf('Dmitri Dmitrievich Shostakovich', 'Dmitri Dmitrievich Shostakoviches').
inf('Belle Miriam Silverman', 'Belle Miriam Silvermen').
inf('Duchess of Windsor', 'Duchesses of Windsor').
inf('Standish', 'Standishes').
inf('Miles Standish', 'Miles Standishes').
inf('Myles Standish', 'Myles Standishes').
inf('Steinman', 'Steinmen').
inf('David Barnard Steinman', 'David Barnard Steinmen').
inf('Strauss', 'Strausses').
inf('Johann Strauss', 'Johann Strausses').
inf('Strauss', 'Strausses').
inf('Johann Strauss', 'Johann Strausses').
inf('Strauss', 'Strausses').
inf('Richard Strauss', 'Richard Strausses').
inf('Suharto', 'Suhartoes').
inf('Sukarno', 'Sukarnoes').
inf('Achmad Sukarno', 'Achmad Sukarnoes').
inf('Lucius Cornelius Sulla Felix', 'Lucius Cornelius Sulla Felixes').
inf('Tarantino', 'Tarantinoes').
inf('Quentin Tarantino', 'Quentin Tarantinoes').
inf('Quentin Jerome Tarantino', 'Quentin Jerome Tarantinoes').
inf('Tasman', 'Tasmen').
inf('Abel Tasman', 'Abel Tasmen').
inf('Abel Janszoon Tasman', 'Abel Janszoon Tasmen').
inf('Tasso', 'Tassoes').
inf('Torquato Tasso', 'Torquato Tassoes').
inf('Teach', 'Teaches').
inf('Edward Teach', 'Edward Teaches').
inf('Thatch', 'Thatches').
inf('Edward Thatch', 'Edward Thatches').
inf('Teresa of Avila', 'Teresas of Avila').
inf('Thales of Miletus', 'Thaless of Miletus').
inf('Theodosius I', 'Theodosius we').
inf('Thoreau', 'Thoreaux').
inf('Henry David Thoreau', 'Henry David Thoreaux').
inf('Tiepolo', 'Tiepoloes').
inf('Giovanni Battista Tiepolo', 'Giovanni Battista Tiepoloes').
inf('Tillich', 'Tilliches').
inf('Paul Tillich', 'Paul Tilliches').
inf('Paul Johannes Tillich', 'Paul Johannes Tilliches').
inf('Tintoretto', 'Tintorettoes').
inf('Tito', 'Titoes').
inf('Marshal Tito', 'Marshal Titoes').
inf('Tojo', 'Tojoes').
inf('Trevino', 'Trevinoes').
inf('Lee Trevino', 'Lee Trevinoes').
inf('Lee Buck Trevino', 'Lee Buck Trevinoes').
inf('Supermex', 'Supermexes').
inf('Truman', 'Trumen').
inf('Harry Truman', 'Harry Trumen').
inf('Harry S Truman', 'Harry S Trumen').
inf('President Truman', 'President Trumen').
inf('Trumbo', 'Trumboes').
inf('Dalton Trumbo', 'Dalton Trumboes').
inf('Tubman', 'Tubmen').
inf('Harriet Tubman', 'Harriet Tubmen').
inf('Tuchman', 'Tuchmen').
inf('Barbara Tuchman', 'Barbara Tuchmen').
inf('Barbara Wertheim Tuchman', 'Barbara Wertheim Tuchmen').
inf('Odo', 'Odoes').
inf('Odo of Lagery', 'Odoes of Lagery').
inf('Otho', 'Othoes').
inf('Otho of Lagery', 'Othoes of Lagery').
inf('Bartolomeo Prignano', 'Bartolomeo Prignanoes').
inf('Utrillo', 'Utrilloes').
inf('Maurice Utrillo', 'Maurice Utrilloes').
inf('Varese', 'Varese').
inf('Edgar Varese', 'Edgar Varese').
inf('Varro', 'Varroes').
inf('Marcus Terentius Varro', 'Marcus Terentius Varroes').
inf('Vaux', 'Vauxes').
inf('Calvert Vaux', 'Calvert Vauxes').
inf('Veronese', 'Veronese').
inf('Paolo Veronese', 'Paolo Veronese').
inf('Verrazano', 'Verrazanoes').
inf('Giovanni da Verrazano', 'Giovanni da Verrazanoes').
inf('Verrazzano', 'Verrazzanoes').
inf('Giovanni da Verrazzano', 'Giovanni da Verrazzanoes').
inf('Doroteo Arango', 'Doroteo Arangoes').
inf('Publius Vergilius Maro', 'Publius Vergilius Maroes').
inf('Vizcaino', 'Vizcainoes').
inf('Sebastian Vizcaino', 'Sebastian Vizcainoes').
inf('Earl of Warwick', 'Earls of Warwick').
inf('Watteau', 'Watteaux').
inf('Jean Antoine Watteau', 'Jean Antoine Watteaux').
inf('Duke of Wellington', 'Dukes of Wellington').
inf('Whitman', 'Whitmen').
inf('Marcus Whitman', 'Marcus Whitmen').
inf('Whitman', 'Whitmen').
inf('Walt Whitman', 'Walt Whitmen').
inf('William I', 'William we').
inf('William of Orange', 'Williams of Orange').
inf('Wolf', 'Wolves').
inf('Friedrich August Wolf', 'Friedrich August Wolves').
inf('Wolf', 'Wolves').
inf('Hugo Wolf', 'Hugo Wolves').
inf('Woolf', 'Woolves').
inf('Virginia Woolf', 'Virginia Woolves').
inf('Adeline Virginia Stephen Woolf', 'Adeline Virginia Stephen Woolves').
inf('William of Wykeham', 'Williams of Wykeham').
inf('Xerxes I', 'Xerxes we').
inf('Yamamoto', 'Yamamotoes').
inf('Isoroku Yamamoto', 'Isoroku Yamamotoes').
inf('Yevtushenko', 'Yevtushenkoes').
inf('Yevgeni Yevtushenko', 'Yevgeni Yevtushenkoes').
inf('Yevgeni Aleksandrovich Yevtushenko', 'Yevgeni Aleksandrovich Yevtushenkoes').
inf('Zeeman', 'Zeemen').
inf('Pieter Zeeman', 'Pieter Zeemen').
inf('Zeno', 'Zenoes').
inf('Zeno of Citium', 'Zenoes of Citium').
inf('Zeno', 'Zenoes').
inf('Zeno of Elea', 'Zenoes of Elea').
inf('Zukerman', 'Zukermen').
inf('Pinchas Zukerman', 'Pinchas Zukermen').
inf('metempsychosis', 'metempsychoses').
inf('allotropy', 'allotropies').
inf('photoconductivity', 'photoconductivities').
inf('superconductivity', 'superconductivities').
inf('backwash', 'backwashes').
inf('knock-on effect', 'knocks-on effect').
inf('branch', 'branches').
inf('endogeny', 'endogenies').
inf('abiogenesis', 'abiogeneses').
inf('autogenesis', 'autogeneses').
inf('autogeny', 'autogenies').
inf('activation energy', 'activation energies').
inf('energy of activation', 'energies of activation').
inf('affinity', 'affinities').
inf('backwash', 'backwashes').
inf('wash', 'washes').
inf('alternation of generations', 'alternations of generations').
inf('heterogenesis', 'heterogeneses').
inf('xenogenesis', 'xenogeneses').
inf('alternative energy', 'alternative energies').
inf('metagenesis', 'metageneses').
inf('digenesis', 'digeneses').
inf('atomic energy', 'atomic energies').
inf('nuclear energy', 'nuclear energies').
inf('affinity', 'affinities').
inf('beam of light', 'beams of light').
inf('ray of light', 'rays of light').
inf('shaft of light', 'shafts of light').
inf('binding energy', 'binding energies').
inf('separation energy', 'separation energies').
inf('bioelectricity', 'bioelectricities').
inf('pedesis', 'pedeses').
inf('electrical capacity', 'electrical capacities').
inf('capacity', 'capacities').
inf('capillarity', 'capillarities').
inf('pyroelectricity', 'pyroelectricities').
inf('valency', 'valencies').
inf('chemical energy', 'chemical energies').
inf('cyclosis', 'cycloses').
inf('diapedesis', 'diapedeses').
inf('sirocco', 'siroccoes').
inf('easterly', 'easterlies').
inf('elastic energy', 'elastic energies').
inf('elastic potential energy', 'elastic potential energies').
inf('electricity', 'electricities').
inf('electricity', 'electricities').
inf('electrical energy', 'electrical energies').
inf('energy', 'energies').
inf('energy', 'energies').
inf('free energy', 'free energies').
inf('rest energy', 'rest energies').
inf('temperateness', 'temperatenesses').
inf('field of force', 'fields of force').
inf('line of force', 'lines of force').
inf('serendipity', 'serendipities').
inf('gravity', 'gravities').
inf('solar gravity', 'solar gravities').
inf('heat energy', 'heat energies').
inf('geothermal energy', 'geothermal energies').
inf('histocompatibility', 'histocompatibilities').
inf('sultriness', 'sultrinesses').
inf('hydroelectricity', 'hydroelectricities').
inf('hysteresis', 'hystereses').
inf('moment of inertia', 'moments of inertia').
inf('angular velocity', 'angular velocities').
inf('kinetic energy', 'kinetic energies').
inf('heat of dissociation', 'heats of dissociation').
inf('heat of formation', 'heats of formation').
inf('heat of solution', 'heats of solution').
inf('heat of transformation', 'heats of transformation').
inf('heat of condensation', 'heats of condensation').
inf('heat of fusion', 'heats of fusion').
inf('heat of solidification', 'heats of solidification').
inf('heat of sublimation', 'heats of sublimation').
inf('heat of vaporization', 'heats of vaporization').
inf('heat of vaporisation', 'heats of vaporisation').
inf('life', 'lives').
inf('biology', 'biologies').
inf('aerobiosis', 'aerobioses').
inf('atmospheric electricity', 'atmospheric electricities').
inf('luminous energy', 'luminous energies').
inf('magnetic flux', 'magnetic fluxes').
inf('flux', 'fluxes').
inf('trajectory', 'trajectories').
inf('ballistic trajectory', 'ballistic trajectories').
inf('mass deficiency', 'mass deficiencies').
inf('mechanical energy', 'mechanical energies').
inf('moment of a couple', 'moments of a couple').
inf('moment of a magnet', 'moments of a magnet').
inf('necrobiosis', 'necrobioses').
inf('apoptosis', 'apoptoses').
inf('necrosis', 'necroses').
inf('myonecrosis', 'myonecroses').
inf('halo', 'haloes').
inf('solar halo', 'solar haloes').
inf('northerly', 'northerlies').
inf('El Nino', 'El Ninoes').
inf('opacity', 'opacities').
inf('optical opacity', 'optical opacities').
inf('radiopacity', 'radiopacities').
inf('radio-opacity', 'radio-opacities').
inf('photoelectricity', 'photoelectricities').
inf('piezoelectricity', 'piezoelectricities').
inf('potential energy', 'potential energies').
inf('puff of air', 'puffs of air').
inf('push', 'pushes').
inf('radiant energy', 'radiant energies').
inf('resistivity', 'resistivities').
inf('flurry', 'flurries').
inf('snow flurry', 'snow flurries').
inf('solar energy', 'solar energies').
inf('southerly', 'southerlies').
inf('conductivity', 'conductivities').
inf('static electricity', 'static electricities').
inf('dynamic electricity', 'dynamic electricities').
inf('current electricity', 'current electricities').
inf('thermoelectricity', 'thermoelectricities').
inf('stress', 'stresses').
inf('syzygy', 'syzygies').
inf('bolt of lightning', 'bolts of lightning').
inf('tornado', 'tornadoes').
inf('transparency', 'transparencies').
inf('turbulency', 'turbulencies').
inf('vitality', 'vitalities').
inf('prevailing westerly', 'prevailing westerlies').
inf('westerly', 'westerlies').
inf('current of air', 'currents of air').
inf('parallax', 'parallaxes').
inf('heliocentric parallax', 'heliocentric parallaxes').
inf('annual parallax', 'annual parallaxes').
inf('stellar parallax', 'stellar parallaxes').
inf('geocentric parallax', 'geocentric parallaxes').
inf('diurnal parallax', 'diurnal parallaxes').
inf('horizontal parallax', 'horizontal parallaxes').
inf('solar parallax', 'solar parallaxes').
inf('moss', 'mosses').
inf('moss family', 'moss families').
inf('moss genus', 'moss genera').
inf('acrocarpous moss', 'acrocarpous mosses').
inf('pleurocarpous moss', 'pleurocarpous mosses').
inf('sphagnum moss', 'sphagnum mosses').
inf('peat moss', 'peat mosses').
inf('bog moss', 'bog mosses').
inf('scale moss', 'scale mosses').
inf('fern ally', 'fern allies').
inf('gymnosperm family', 'gymnosperm families').
inf('gymnosperm genus', 'gymnosperm genera').
inf('monocot family', 'monocot families').
inf('liliopsid family', 'liliopsid families').
inf('liliid monocot family', 'liliid monocot families').
inf('monocot genus', 'monocot genera').
inf('liliopsid genus', 'liliopsid genera').
inf('liliid monocot genus', 'liliid monocot genera').
inf('dicot family', 'dicot families').
inf('magnoliopsid family', 'magnoliopsid families').
inf('magnoliid dicot family', 'magnoliid dicot families').
inf('hamamelid dicot family', 'hamamelid dicot families').
inf('caryophylloid dicot family', 'caryophylloid dicot families').
inf('dilleniid dicot family', 'dilleniid dicot families').
inf('asterid dicot family', 'asterid dicot families').
inf('rosid dicot family', 'rosid dicot families').
inf('dicot genus', 'dicot genera').
inf('magnoliopsid genus', 'magnoliopsid genera').
inf('magnoliid dicot genus', 'magnoliid dicot genera').
inf('hamamelid dicot genus', 'hamamelid dicot genera').
inf('caryophylloid dicot genus', 'caryophylloid dicot genera').
inf('dilleniid dicot genus', 'dilleniid dicot genera').
inf('asterid dicot genus', 'asterid dicot genera').
inf('rosid dicot genus', 'rosid dicot genera').
inf('fungus family', 'fungus families').
inf('fungus genus', 'fungus genera').
inf('cycad family', 'cycad families').
inf('false sago', 'false sagoes').
inf('zamia family', 'zamia families').
inf('Bennettitis', 'Bennettitis').
inf('genus Bennettitis', 'genus Bennettitis').
inf('pine family', 'pine families').
inf('single-leaf', 'single-leaves').
inf('Pinus mugo', 'Pinus mugoes').
inf('Larix', 'Larixes').
inf('genus Larix', 'genus Larixes').
inf('larch', 'larches').
inf('larch', 'larches').
inf('American larch', 'American larches').
inf('black larch', 'black larches').
inf('western larch', 'western larches').
inf('Oregon larch', 'Oregon larches').
inf('subalpine larch', 'subalpine larches').
inf('European larch', 'European larches').
inf('Siberian larch', 'Siberian larches').
inf('Pseudolarix', 'Pseudolarixes').
inf('genus Pseudolarix', 'genus Pseudolarixes').
inf('golden larch', 'golden larches').
inf('balm of Gilead', 'balms of Gilead').
inf('cedar of Lebanon', 'cedars of Lebanon').
inf('Picea sitchensis', 'Picea sitchenses').
inf('Tsuga canadensis', 'Tsuga canadenses').
inf('cypress family', 'cypress families').
inf('cypress', 'cypresses').
inf('cypress', 'cypresses').
inf('gowen cypress', 'gowen cypresses').
inf('pygmy cypress', 'pygmy cypresses').
inf('Santa Cruz cypress', 'Santa Cruz cypresses').
inf('Arizona cypress', 'Arizona cypresses').
inf('Guadalupe cypress', 'Guadalupe cypresses').
inf('Cupressus guadalupensis', 'Cupressus guadalupenses').
inf('Monterey cypress', 'Monterey cypresses').
inf('Mexican cypress', 'Mexican cypresses').
inf('cedar of Goa', 'cedars of Goa').
inf('Portuguese cypress', 'Portuguese cypresses').
inf('Italian cypress', 'Italian cypresses').
inf('Mediterranean cypress', 'Mediterranean cypresses').
inf('Athrotaxis', 'Athrotaxes').
inf('genus Athrotaxis', 'genus Athrotaxes').
inf('Austrocedrus chilensis', 'Austrocedrus chilenses').
inf('white cypress', 'white cypresses').
inf('Lawson\'s cypress', 'Lawson\'s cypresses').
inf('yellow cypress', 'yellow cypresses').
inf('Nootka cypress', 'Nootka cypresses').
inf('Chamaecyparis nootkatensis', 'Chamaecyparis nootkatenses').
inf('juniper berry', 'juniper berries').
inf('redwood family', 'redwood families').
inf('bald cypress', 'bald cypresses').
inf('swamp cypress', 'swamp cypresses').
inf('pond bald cypress', 'pond bald cypresses').
inf('southern cypress', 'southern cypresses').
inf('pond cypress', 'pond cypresses').
inf('bald cypress', 'bald cypresses').
inf('Montezuma cypress', 'Montezuma cypresses').
inf('Mexican swamp cypress', 'Mexican swamp cypresses').
inf('sandarach', 'sandaraches').
inf('Thujopsis', 'Thujopses').
inf('genus Thujopsis', 'genus Thujopses').
inf('araucaria family', 'araucaria families').
inf('kaury', 'kauries').
inf('plum-yew family', 'plum-yew families').
inf('podocarpus family', 'podocarpus families').
inf('miro', 'miroes').
inf('yew family', 'yew families').
inf('ginkgo family', 'ginkgo families').
inf('genus Ginkgo', 'genus Ginkgoes').
inf('ginkgo', 'ginkgoes').
inf('gingko', 'gingkoes').
inf('ranalian complex', 'ranalian complexes').
inf('ovary', 'ovaries').
inf('nectary', 'nectaries').
inf('seed leaf', 'seed leaves').
inf('embryo', 'embryoes').
inf('floral leaf', 'floral leaves').
inf('calyx', 'calyxes').
inf('epicalyx', 'epicalyxes').
inf('false calyx', 'false calyxes').
inf('custard-apple family', 'custard-apple families').
inf('barberry family', 'barberry families').
inf('barberry', 'barberries').
inf('American barberry', 'American barberries').
inf('Berberis canadensis', 'Berberis canadenses').
inf('common barberry', 'common barberries').
inf('European barberry', 'European barberries').
inf('Japanese barberry', 'Japanese barberries').
inf('blue cohosh', 'blue cohoshes').
inf('holly-leaves barberry', 'holly-leaves barberries').
inf('calycanthus family', 'calycanthus families').
inf('strawberry-shrub family', 'strawberry-shrub families').
inf('strawberry bush', 'strawberry bushes').
inf('spicebush', 'spicebushes').
inf('Chimonanthus praecox', 'Chimonanthus praecoxes').
inf('lardizabala family', 'lardizabala families').
inf('laurel family', 'laurel families').
inf('spicebush', 'spicebushes').
inf('spice bush', 'spice bushes').
inf('American spicebush', 'American spicebushes').
inf('Benjamin bush', 'Benjamin bushes').
inf('avocado', 'avocadoes').
inf('magnolia family', 'magnolia families').
inf('moonseed family', 'moonseed families').
inf('nutmeg family', 'nutmeg families').
inf('water-lily family', 'water-lily families').
inf('water lily', 'water lilies').
inf('fragrant water lily', 'fragrant water lilies').
inf('pond lily', 'pond lilies').
inf('European white lily', 'European white lilies').
inf('Egyptian water lily', 'Egyptian water lilies').
inf('white lily', 'white lilies').
inf('cow lily', 'cow lilies').
inf('yellow pond lily', 'yellow pond lilies').
inf('yellow water lily', 'yellow water lilies').
inf('Nelumbo', 'Nelumboes').
inf('genus Nelumbo', 'genus Nelumboes').
inf('water-shield family', 'water-shield families').
inf('peony family', 'peony families').
inf('peony', 'peonies').
inf('paeony', 'paeonies').
inf('buttercup family', 'buttercup families').
inf('crowfoot family', 'crowfoot families').
inf('crowfoot', 'crowfeet').
inf('tall crowfoot', 'tall crowfeet').
inf('water crowfoot', 'water crowfeet').
inf('mountain lily', 'mountain lilies').
inf('Mount Cook lily', 'Mount Cook lilies').
inf('creeping crowfoot', 'creeping crowfeet').
inf('cursed crowfoot', 'cursed crowfeet').
inf('baneberry', 'baneberries').
inf('cohosh', 'cohoshes').
inf('baneberry', 'baneberries').
inf('red baneberry', 'red baneberries').
inf('redberry', 'redberries').
inf('red-berry', 'red-berries').
inf('snakeberry', 'snakeberries').
inf('white baneberry', 'white baneberries').
inf('white cohosh', 'white cohoshes').
inf('Anemone tetonensis', 'Anemone tetonenses').
inf('Anemone Canadensis', 'Anemone Canadenses').
inf('Aquilegia canadensis', 'Aquilegia canadenses').
inf('summer cohosh', 'summer cohoshes').
inf('black cohosh', 'black cohoshes').
inf('Clematis texensis', 'Clematis texenses').
inf('Delphinium ajacis', 'Delphinium ajaces').
inf('bear\'s foot', 'bear\'s feet').
inf('liverleaf', 'liverleaves').
inf('Hydrastis Canadensis', 'Hydrastis Canadenses').
inf('love-in-a-mist', 'love-in-a-mist').
inf('Trautvetteria carolinensis', 'Trautvetteria carolinenses').
inf('winter\'s bark family', 'winter\'s bark families').
inf('wax-myrtle family', 'wax-myrtle families').
inf('puckerbush', 'puckerbushes').
inf('bayberry', 'bayberries').
inf('candleberry', 'candleberries').
inf('swamp candleberry', 'swamp candleberries').
inf('waxberry', 'waxberries').
inf('bayberry wax', 'bayberry waxes').
inf('corkwood family', 'corkwood families').
inf('rush family', 'rush families').
inf('rush', 'rushes').
inf('bulrush', 'bulrushes').
inf('bullrush', 'bullrushes').
inf('common rush', 'common rushes').
inf('soft rush', 'soft rushes').
inf('jointed rush', 'jointed rushes').
inf('toad rush', 'toad rushes').
inf('hard rush', 'hard rushes').
inf('salt rush', 'salt rushes').
inf('slender rush', 'slender rushes').
inf('plant family', 'plant families').
inf('plant genus', 'plant genera').
inf('zebrawood family', 'zebrawood families').
inf('Connarus guianensis', 'Connarus guianenses').
inf('legume family', 'legume families').
inf('pea family', 'pea families').
inf('granadillo', 'granadilloes').
inf('Dipteryx', 'Dipteryxes').
inf('genus Dipteryx', 'genus Dipteryxes').
inf('poison bush', 'poison bushes').
inf('mimosa bush', 'mimosa bushes').
inf('saman', 'samen').
inf('zaman', 'zamen').
inf('Albizia saman', 'Albizia samen').
inf('Lysiloma bahamensis', 'Lysiloma bahamenses').
inf('tornillo', 'tornilloes').
inf('dogbane family', 'dogbane families').
inf('impala lily', 'impala lilies').
inf('kudu lily', 'kudu lilies').
inf('Mandevilla boliviensis', 'Mandevilla bolivienses').
inf('Dipladenia boliviensis', 'Dipladenia bolivienses').
inf('arum family', 'arum families').
inf('jack-in-the-pulpit', 'jacks-in-the-pulpit').
inf('Japanese leaf', 'Japanese leaves').
inf('giant taro', 'giant taroes').
inf('telingo potato', 'telingo potatoes').
inf('jack-in-the-pulpit', 'jacks-in-the-pulpit').
inf('taro', 'taroes').
inf('dalo', 'daloes').
inf('taro', 'taroes').
inf('eddo', 'eddoes').
inf('mother-in-law plant', 'mothers-in-law plant').
inf('mother-in-law\'s tongue', 'mothers-in-law\'s tongue').
inf('ceriman', 'cerimen').
inf('peace lily', 'peace lilies').
inf('calla lily', 'calla lilies').
inf('arum lily', 'arum lilies').
inf('duckweed family', 'duckweed families').
inf('ivy family', 'ivy families').
inf('life-of-man', 'lives-of-man').
inf('ivy', 'ivies').
inf('common ivy', 'common ivies').
inf('English ivy', 'English ivies').
inf('Hedera helix', 'Hedera helixes').
inf('Panax', 'Panaxes').
inf('genus Panax', 'genus Panaxes').
inf('birthwort family', 'birthwort families').
inf('Aristolochia clematitis', 'Aristolochia clematitis').
inf('Virginia serpentary', 'Virginia serpentaries').
inf('heartleaf', 'heartleaves').
inf('heart-leaf', 'heart-leaves').
inf('heartleaf', 'heartleaves').
inf('heart-leaf', 'heart-leaves').
inf('carnation family', 'carnation families').
inf('pink family', 'pink families').
inf('crown-of-the-field', 'crowns-of-the-field').
inf('Agrostemma githago', 'Agrostemma githagoes').
inf('mountain daisy', 'mountain daisies').
inf('snow-in-summer', 'snows-in-summer').
inf('love-in-a-mist', 'love-in-a-mist').
inf('Dianthus chinensis', 'Dianthus chinenses').
inf('catchfly', 'catchflies').
inf('maltese cross', 'maltese crosses').
inf('bouncing Bess', 'bouncing Besses').
inf('catchfly', 'catchflies').
inf('corn spurry', 'corn spurries').
inf('Spergula arvensis', 'Spergula arvenses').
inf('sand spurry', 'sand spurries').
inf('sea spurry', 'sea spurries').
inf('carpetweed family', 'carpetweed families').
inf('livingstone daisy', 'livingstone daisies').
inf('New Zealand spinach', 'New Zealand spinaches').
inf('amaranth family', 'amaranth families').
inf('alligator grass', 'alligator grasses').
inf('red fox', 'red foxes').
inf('bloodleaf', 'bloodleaves').
inf('saltwort family', 'saltwort families').
inf('goosefoot family', 'goosefoot families').
inf('goosefoot', 'goosefeet').
inf('wild spinach', 'wild spinaches').
inf('good-king-henry', 'good-king-henries').
inf('wild spinach', 'wild spinaches').
inf('oak-leaved goosefoot', 'oak-leaved goosefeet').
inf('oakleaf goosefoot', 'oakleaf goosefeet').
inf('red goosefoot', 'red goosefeet').
inf('nettle-leaved goosefoot', 'nettle-leaved goosefeet').
inf('nettleleaf goosefoot', 'nettleleaf goosefeet').
inf('red goosefoot', 'red goosefeet').
inf('French spinach', 'French spinaches').
inf('stinking goosefoot', 'stinking goosefeet').
inf('Atriplex', 'Atriplexes').
inf('genus Atriplex', 'genus Atriplexes').
inf('orach', 'oraches').
inf('saltbush', 'saltbushes').
inf('mountain spinach', 'mountain spinaches').
inf('Atriplex hortensis', 'Atriplex hortenses').
inf('desert holly', 'desert hollies').
inf('quail bush', 'quail bushes').
inf('quail brush', 'quail brushes').
inf('summer cypress', 'summer cypresses').
inf('burning bush', 'burning bushes').
inf('fire bush', 'fire bushes').
inf('fire-bush', 'fire-bushes').
inf('spinach', 'spinaches').
inf('prickly-seeded spinach', 'prickly-seeded spinaches').
inf('four-o\'clock family', 'four-o\'clock families').
inf('marvel-of-Peru', 'marvels-of-Peru').
inf('cactus family', 'cactus families').
inf('saguaro', 'saguaroes').
inf('sahuaro', 'sahuaroes').
inf('Barbados gooseberry', 'Barbados gooseberries').
inf('queen of the night', 'queens of the night').
inf('pokeweed family', 'pokeweed families').
inf('pigeon berry', 'pigeon berries').
inf('bloodberry', 'bloodberries').
inf('blood berry', 'blood berries').
inf('rougeberry', 'rougeberries').
inf('purslane family', 'purslane families').
inf('rose moss', 'rose mosses').
inf('pussly', 'pusslies').
inf('Carolina spring beauty', 'Carolina spring beauties').
inf('spring beauty', 'spring beauties').
inf('Virginia spring beauty', 'Virginia spring beauties').
inf('toad lily', 'toad lilies').
inf('Cuban spinach', 'Cuban spinaches').
inf('jewels-of-opar', 'jewelss-of-opar').
inf('caper family', 'caper families').
inf('mustard family', 'mustard families').
inf('cress', 'cresses').
inf('watercress', 'watercresses').
inf('stonecress', 'stonecresses').
inf('stone cress', 'stone cresses').
inf('jack-by-the-hedge', 'jacks-by-the-hedge').
inf('rose of Jericho', 'roses of Jericho').
inf('Arabidopsis', 'Arabidopses').
inf('genus Arabidopsis', 'genus Arabidopses').
inf('mouse-ear cress', 'mouse-ear cresses').
inf('rock cress', 'rock cresses').
inf('rockcress', 'rockcresses').
inf('Arabis Canadensis', 'Arabis Canadenses').
inf('tower cress', 'tower cresses').
inf('tower cress', 'tower cresses').
inf('horseradish', 'horseradishes').
inf('horse radish', 'horse radishes').
inf('horseradish', 'horseradishes').
inf('winter cress', 'winter cresses').
inf('scurvy grass', 'scurvy grasses').
inf('Belle Isle cress', 'Belle Isle cresses').
inf('early winter cress', 'early winter cresses').
inf('land cress', 'land cresses').
inf('American cress', 'American cresses').
inf('American watercress', 'American watercresses').
inf('Barbarea praecox', 'Barbarea praecoxes').
inf('rockcress', 'rockcresses').
inf('rocket cress', 'rocket cresses').
inf('Brassica rapa ruvo', 'Brassica rapa ruvoes').
inf('Brassica rapa pekinensis', 'Brassica rapa pekinenses').
inf('Brassica rapa chinensis', 'Brassica rapa chinenses').
inf('false flax', 'false flaxes').
inf('gold of pleasure', 'golds of pleasure').
inf('shepherd\'s pouch', 'shepherd\'s pouches').
inf('bittercress', 'bittercresses').
inf('bitter cress', 'bitter cresses').
inf('meadow cress', 'meadow cresses').
inf('Cardamine pratensis', 'Cardamine pratenses').
inf('coral-root bittercress', 'coral-root bittercresses').
inf('American watercress', 'American watercresses').
inf('mountain watercress', 'mountain watercresses').
inf('spring cress', 'spring cresses').
inf('purple cress', 'purple cresses').
inf('scurvy grass', 'scurvy grasses').
inf('common scurvy grass', 'common scurvy grasses').
inf('Diplotaxis', 'Diplotaxes').
inf('genus Diplotaxis', 'genus Diplotaxes').
inf('whitlow grass', 'whitlow grasses').
inf('common garden cress', 'common garden cresses').
inf('garden pepper cress', 'garden pepper cresses').
inf('pepper grass', 'pepper grasses').
inf('honesty', 'honesties').
inf('common watercress', 'common watercresses').
inf('Pritzelago', 'Pritzelagoes').
inf('genus Pritzelago', 'genus Pritzelagoes').
inf('chamois cress', 'chamois cresses').
inf('radish', 'radishes').
inf('wild radish', 'wild radishes').
inf('runch', 'runches').
inf('radish', 'radishes').
inf('radish', 'radishes').
inf('radish', 'radishes').
inf('Japanese radish', 'Japanese radishes').
inf('marsh cress', 'marsh cresses').
inf('yellow watercress', 'yellow watercresses').
inf('great yellowcress', 'great yellowcresses').
inf('Sinapis arvensis', 'Sinapis arvenses').
inf('Stephanomeria malheurensis', 'Stephanomeria malheurenses').
inf('pennycress', 'pennycresses').
inf('field pennycress', 'field pennycresses').
inf('penny grass', 'penny grasses').
inf('Turritis', 'Turritis').
inf('genus Turritis', 'genus Turritis').
inf('poppy family', 'poppy families').
inf('poppy', 'poppies').
inf('Iceland poppy', 'Iceland poppies').
inf('western poppy', 'western poppies').
inf('prickly poppy', 'prickly poppies').
inf('Iceland poppy', 'Iceland poppies').
inf('arctic poppy', 'arctic poppies').
inf('oriental poppy', 'oriental poppies').
inf('corn poppy', 'corn poppies').
inf('field poppy', 'field poppies').
inf('Flanders poppy', 'Flanders poppies').
inf('opium poppy', 'opium poppies').
inf('prickly poppy', 'prickly poppies').
inf('Mexican poppy', 'Mexican poppies').
inf('bush poppy', 'bush poppies').
inf('tree poppy', 'tree poppies').
inf('California poppy', 'California poppies').
inf('horn poppy', 'horn poppies').
inf('horned poppy', 'horned poppies').
inf('yellow horned poppy', 'yellow horned poppies').
inf('sea poppy', 'sea poppies').
inf('Mexican tulip poppy', 'Mexican tulip poppies').
inf('plume poppy', 'plume poppies').
inf('Meconopsis', 'Meconopses').
inf('genus Meconopsis', 'genus Meconopses').
inf('blue poppy', 'blue poppies').
inf('Welsh poppy', 'Welsh poppies').
inf('matilija poppy', 'matilija poppies').
inf('California tree poppy', 'California tree poppies').
inf('Sanguinaria canadensis', 'Sanguinaria canadenses').
inf('wind poppy', 'wind poppies').
inf('flaming poppy', 'flaming poppies').
inf('celandine poppy', 'celandine poppies').
inf('wood poppy', 'wood poppies').
inf('fumitory family', 'fumitory families').
inf('fumitory', 'fumitories').
inf('climbing fumitory', 'climbing fumitories').
inf('Dicentra canadensis', 'Dicentra canadenses').
inf('aster family', 'aster families').
inf('pink paper daisy', 'pink paper daisies').
inf('pellitory', 'pellitories').
inf('pellitory-of-Spain', 'pellitories-of-Spain').
inf('ladies\' tobacco', 'ladies\' tobaccoes').
inf('lady\'s tobacco', 'lady\'s tobaccoes').
inf('cat\'s foot', 'cat\'s feet').
inf('Anthemis arvensis', 'Anthemis arvenses').
inf('woolly daisy', 'woolly daisies').
inf('dwarf daisy', 'dwarf daisies').
inf('African daisy', 'African daisies').
inf('blue-eyed African daisy', 'blue-eyed African daisies').
inf('marguerite daisy', 'marguerite daisies').
inf('Paris daisy', 'Paris daisies').
inf('lamb succory', 'lamb succories').
inf('sagebrush', 'sagebrushes').
inf('sage brush', 'sage brushes').
inf('old man', 'old men').
inf('California sagebrush', 'California sagebrushes').
inf('bud brush', 'bud brushes').
inf('bud sagebrush', 'bud sagebrushes').
inf('old woman', 'old women').
inf('Michaelmas daisy', 'Michaelmas daisies').
inf('groundsel bush', 'groundsel bushes').
inf('coyote brush', 'coyote brushes').
inf('coyote bush', 'coyote bushes').
inf('daisy', 'daisies').
inf('common daisy', 'common daisies').
inf('English daisy', 'English daisies').
inf('Swan River daisy', 'Swan River daisies').
inf('Callistephus chinensis', 'Callistephus chinenses').
inf('blue succory', 'blue succories').
inf('centaury', 'centauries').
inf('crown daisy', 'crown daisies').
inf('tong ho', 'tong hoes').
inf('Chrysopsis', 'Chrysopses').
inf('genus Chrysopsis', 'genus Chrysopses').
inf('goldenbush', 'goldenbushes').
inf('rabbit brush', 'rabbit brushes').
inf('rabbit bush', 'rabbit bushes').
inf('chicory', 'chicories').
inf('succory', 'succories').
inf('chicory', 'chicories').
inf('Conyza canadensis', 'Conyza canadenses').
inf('Erigeron canadensis', 'Erigeron canadenses').
inf('genus Coreopsis', 'genus Coreopses').
inf('coreopsis', 'coreopses').
inf('subgenus Calliopsis', 'subgenus Calliopses').
inf('giant coreopsis', 'giant coreopses').
inf('calliopsis', 'calliopses').
inf('German ivy', 'German ivies').
inf('star of the veldt', 'stars of the veldt').
inf('Elephantopus', 'Elephantopera').
inf('genus Elephantopus', 'genus Elephantopera').
inf('elephant\'s-foot', 'elephant\'s-feet').
inf('brittlebush', 'brittlebushes').
inf('brittle bush', 'brittle bushes').
inf('incienso', 'inciensoes').
inf('Enceliopsis', 'Enceliopses').
inf('genus Enceliopsis', 'genus Enceliopses').
inf('orange daisy', 'orange daisies').
inf('seaside daisy', 'seaside daisies').
inf('showy daisy', 'showy daisies').
inf('hemp agrimony', 'hemp agrimonies').
inf('blue daisy', 'blue daisies').
inf('kingfisher daisy', 'kingfisher daisies').
inf('genus Filago', 'genus Filagoes').
inf('filago', 'filagoes').
inf('African daisy', 'African daisies').
inf('Barberton daisy', 'Barberton daisies').
inf('Transvaal daisy', 'Transvaal daisies').
inf('matchbush', 'matchbushes').
inf('vegetable sheep', 'vegetable sheep').
inf('goldenbush', 'goldenbushes').
inf('camphor daisy', 'camphor daisies').
inf('yellow spiny daisy', 'yellow spiny daisies').
inf('hoary golden bush', 'hoary golden bushes').
inf('Indian potato', 'Indian potatoes').
inf('yellow paper daisy', 'yellow paper daisies').
inf('genus Heliopsis', 'genus Heliopses').
inf('heliopsis', 'heliopses').
inf('alpine coltsfoot', 'alpine coltsfeet').
inf('edelweiss', 'edelweisses').
inf('oxeye daisy', 'oxeye daisies').
inf('ox-eyed daisy', 'ox-eyed daisies').
inf('moon daisy', 'moon daisies').
inf('white daisy', 'white daisies').
inf('oxeye daisy', 'oxeye daisies').
inf('shasta daisy', 'shasta daisies').
inf('Pyrenees daisy', 'Pyrenees daisies').
inf('north island edelweiss', 'north island edelweisses').
inf('African daisy', 'African daisies').
inf('tahoka daisy', 'tahoka daisies').
inf('blackfoot daisy', 'blackfoot daisies').
inf('lion\'s foot', 'lion\'s feet').
inf('gall of the earth', 'galls of the earth').
inf('daisybush', 'daisybushes').
inf('daisy-bush', 'daisy-bushes').
inf('daisy bush', 'daisy bushes').
inf('New Zealand daisybush', 'New Zealand daisybushes').
inf('sweet coltsfoot', 'sweet coltsfeet').
inf('sweet coltsfoot', 'sweet coltsfeet').
inf('bugloss', 'buglosses').
inf('vegetable sheep', 'vegetable sheep').
inf('black salsify', 'black salsifies').
inf('viper\'s grass', 'viper\'s grasses').
inf('silver sagebrush', 'silver sagebrushes').
inf('big sagebrush', 'big sagebrushes').
inf('Solidago', 'Solidagoes').
inf('genus Solidago', 'genus Solidagoes').
inf('Solidago canadensis', 'Solidago canadenses').
inf('Solidago missouriensis', 'Solidago missourienses').
inf('costmary', 'costmaries').
inf('bible leaf', 'bible leaves').
inf('camphor dune tansy', 'camphor dune tansies').
inf('painted daisy', 'painted daisies').
inf('northern dune tansy', 'northern dune tansies').
inf('tansy', 'tansies').
inf('Easter daisy', 'Easter daisies').
inf('stemless daisy', 'stemless daisies').
inf('yellow salsify', 'yellow salsifies').
inf('salsify', 'salsifies').
inf('salsify', 'salsifies').
inf('meadow salsify', 'meadow salsifies').
inf('Tragopogon pratensis', 'Tragopogon pratenses').
inf('turfing daisy', 'turfing daisies').
inf('turfing daisy', 'turfing daisies').
inf('Tussilago', 'Tussilagoes').
inf('genus Tussilago', 'genus Tussilagoes').
inf('coltsfoot', 'coltsfeet').
inf('cowpen daisy', 'cowpen daisies').
inf('butter daisy', 'butter daisies').
inf('loasa family', 'loasa families').
inf('bellflower family', 'bellflower families').
inf('orchid family', 'orchid families').
inf('genus Calypso', 'genus Calypsoes').
inf('calypso', 'calypsoes').
inf('Encyclia tampensis', 'Encyclia tampenses').
inf('chatterbox', 'chatterboxes').
inf('Gymnadeniopsis', 'Gymnadeniopses').
inf('genus Gymnadeniopsis', 'genus Gymnadeniopses').
inf('Habenaria unalascensis', 'Habenaria unalascenses').
inf('Malaxis', 'Malaxes').
inf('genus Malaxis', 'genus Malaxes').
inf('Phalaenopsis', 'Phalaenopses').
inf('genus Phalaenopsis', 'genus Phalaenopses').
inf('Psychopsis', 'Psychopses').
inf('genus Psychopsis', 'genus Psychopses').
inf('yam family', 'yam families').
inf('air potato', 'air potatoes').
inf('elephant\'s-foot', 'elephant\'s-feet').
inf('cush-cush', 'cush-cushes').
inf('black bryony', 'black bryonies').
inf('primrose family', 'primrose families').
inf('Primula sinensis', 'Primula sinenses').
inf('poor man\'s weatherglass', 'poor man\'s weatherglasses').
inf('Anagallis arvensis', 'Anagallis arvenses').
inf('Glaux', 'Glauxes').
inf('genus Glaux', 'genus Glauxes').
inf('sea trifoly', 'sea trifolies').
inf('myrsine family', 'myrsine families').
inf('coralberry', 'coralberries').
inf('spiceberry', 'spiceberries').
inf('marlberry', 'marlberries').
inf('leadwort family', 'leadwort families').
inf('sea-lavender family', 'sea-lavender families').
inf('genus Plumbago', 'genus Plumbagoes').
inf('plumbago', 'plumbagoes').
inf('marsh rosemary', 'marsh rosemaries').
inf('barbasco', 'barbascoes').
inf('Jacquinia keyensis', 'Jacquinia keyenses').
inf('grass family', 'grass families').
inf('grass', 'grasses').
inf('beach grass', 'beach grasses').
inf('bunchgrass', 'bunchgrasses').
inf('bunch grass', 'bunch grasses').
inf('midgrass', 'midgrasses').
inf('shortgrass', 'shortgrasses').
inf('short-grass', 'short-grasses').
inf('sword grass', 'sword grasses').
inf('tallgrass', 'tallgrasses').
inf('tall-grass', 'tall-grasses').
inf('lemongrass', 'lemongrasses').
inf('lemon grass', 'lemon grasses').
inf('goat grass', 'goat grasses').
inf('wheatgrass', 'wheatgrasses').
inf('wheat-grass', 'wheat-grasses').
inf('crested wheatgrass', 'crested wheatgrasses').
inf('crested wheat grass', 'crested wheat grasses').
inf('fairway crested wheat grass', 'fairway crested wheat grasses').
inf('dog grass', 'dog grasses').
inf('couch grass', 'couch grasses').
inf('quackgrass', 'quackgrasses').
inf('quack grass', 'quack grasses').
inf('quick grass', 'quick grasses').
inf('witch grass', 'witch grasses').
inf('witchgrass', 'witchgrasses').
inf('bearded wheatgrass', 'bearded wheatgrasses').
inf('western wheatgrass', 'western wheatgrasses').
inf('bluestem wheatgrass', 'bluestem wheatgrasses').
inf('intermediate wheatgrass', 'intermediate wheatgrasses').
inf('slender wheatgrass', 'slender wheatgrasses').
inf('bent grass', 'bent grasses').
inf('bent-grass', 'bent-grasses').
inf('velvet bent grass', 'velvet bent grasses').
inf('cloud grass', 'cloud grasses').
inf('creeping bentgrass', 'creeping bentgrasses').
inf('Alopecurus pratensis', 'Alopecurus pratenses').
inf('foxtail grass', 'foxtail grasses').
inf('broom grass', 'broom grasses').
inf('tall oat grass', 'tall oat grasses').
inf('tall meadow grass', 'tall meadow grasses').
inf('evergreen grass', 'evergreen grasses').
inf('Arundo', 'Arundoes').
inf('genus Arundo', 'genus Arundoes').
inf('Arundo donax', 'Arundo donaxes').
inf('wild oat grass', 'wild oat grasses').
inf('bromegrass', 'bromegrasses').
inf('awnless bromegrass', 'awnless bromegrasses').
inf('chess', 'chesses').
inf('downy bromegrass', 'downy bromegrasses').
inf('downy chess', 'downy chesses').
inf('cheatgrass', 'cheatgrasses').
inf('Bromus arvensis', 'Bromus arvenses').
inf('Japanese chess', 'Japanese chesses').
inf('grama grass', 'grama grasses').
inf('gramma grass', 'gramma grasses').
inf('buffalo grass', 'buffalo grasses').
inf('reed grass', 'reed grasses').
inf('feather reed grass', 'feather reed grasses').
inf('Australian reed grass', 'Australian reed grasses').
inf('burgrass', 'burgrasses').
inf('bur grass', 'bur grasses').
inf('buffel grass', 'buffel grasses').
inf('finger grass', 'finger grasses').
inf('Rhodes grass', 'Rhodes grasses').
inf('windmill grass', 'windmill grasses').
inf('creeping windmill grass', 'creeping windmill grasses').
inf('star grass', 'star grasses').
inf('pampas grass', 'pampas grasses').
inf('Bermuda grass', 'Bermuda grasses').
inf('devil grass', 'devil grasses').
inf('Bahama grass', 'Bahama grasses').
inf('scutch grass', 'scutch grasses').
inf('star grass', 'star grasses').
inf('giant star grass', 'giant star grasses').
inf('orchard grass', 'orchard grasses').
inf('cocksfoot', 'cocksfeet').
inf('Egyptian grass', 'Egyptian grasses').
inf('crowfoot grass', 'crowfoot grasses').
inf('crabgrass', 'crabgrasses').
inf('crab grass', 'crab grasses').
inf('finger grass', 'finger grasses').
inf('smooth crabgrass', 'smooth crabgrasses').
inf('large crabgrass', 'large crabgrasses').
inf('hairy finger grass', 'hairy finger grasses').
inf('barnyard grass', 'barnyard grasses').
inf('barn grass', 'barn grasses').
inf('billion-dollar grass', 'billion-dollar grasses').
inf('yardgrass', 'yardgrasses').
inf('yard grass', 'yard grasses').
inf('wire grass', 'wire grasses').
inf('goose grass', 'goose grasses').
inf('lyme grass', 'lyme grasses').
inf('giant ryegrass', 'giant ryegrasses').
inf('sea lyme grass', 'sea lyme grasses').
inf('European dune grass', 'European dune grasses').
inf('Elymus canadensis', 'Elymus canadenses').
inf('love grass', 'love grasses').
inf('bay grass', 'bay grasses').
inf('teff grass', 'teff grasses').
inf('weeping love grass', 'weeping love grasses').
inf('African love grass', 'African love grasses').
inf('plume grass', 'plume grasses').
inf('Ravenna grass', 'Ravenna grasses').
inf('wool grass', 'wool grasses').
inf('fescue grass', 'fescue grasses').
inf('silver grass', 'silver grasses').
inf('manna grass', 'manna grasses').
inf('sweet grass', 'sweet grasses').
inf('reed meadow grass', 'reed meadow grasses').
inf('velvet grass', 'velvet grasses').
inf('creeping soft grass', 'creeping soft grasses').
inf('barley grass', 'barley grasses').
inf('squirreltail grass', 'squirreltail grasses').
inf('rye grass', 'rye grasses').
inf('ryegrass', 'ryegrasses').
inf('perennial ryegrass', 'perennial ryegrasses').
inf('English ryegrass', 'English ryegrasses').
inf('Italian ryegrass', 'Italian ryegrasses').
inf('Oryzopsis', 'Oryzopses').
inf('genus Oryzopsis', 'genus Oryzopses').
inf('ricegrass', 'ricegrasses').
inf('rice grass', 'rice grasses').
inf('silkgrass', 'silkgrasses').
inf('silk grass', 'silk grasses').
inf('smilo', 'smiloes').
inf('smilo grass', 'smilo grasses').
inf('panic grass', 'panic grasses').
inf('witchgrass', 'witchgrasses').
inf('witch grass', 'witch grasses').
inf('old witchgrass', 'old witchgrasses').
inf('old witch grass', 'old witch grasses').
inf('tumble grass', 'tumble grasses').
inf('switch grass', 'switch grasses').
inf('goose grass', 'goose grasses').
inf('dallisgrass', 'dallisgrasses').
inf('dallis grass', 'dallis grasses').
inf('Bahia grass', 'Bahia grasses').
inf('knotgrass', 'knotgrasses').
inf('fountain grass', 'fountain grasses').
inf('feathertop grass', 'feathertop grasses').
inf('reed canary grass', 'reed canary grasses').
inf('ribbon grass', 'ribbon grasses').
inf('canary grass', 'canary grasses').
inf('birdseed grass', 'birdseed grasses').
inf('Phalaris canariensis', 'Phalaris canarienses').
inf('hardinggrass', 'hardinggrasses').
inf('Harding grass', 'Harding grasses').
inf('toowomba canary grass', 'toowomba canary grasses').
inf('timothy', 'timothies').
inf('herd\'s grass', 'herd\'s grasses').
inf('carrizo', 'carrizoes').
inf('bluegrass', 'bluegrasses').
inf('blue grass', 'blue grasses').
inf('meadowgrass', 'meadowgrasses').
inf('meadow grass', 'meadow grasses').
inf('Kentucky bluegrass', 'Kentucky bluegrasses').
inf('Kentucy blue grass', 'Kentucy blue grasses').
inf('June grass', 'June grasses').
inf('Poa pratensis', 'Poa pratenses').
inf('wood meadowgrass', 'wood meadowgrasses').
inf('broom beard grass', 'broom beard grasses').
inf('prairie grass', 'prairie grasses').
inf('wire grass', 'wire grasses').
inf('bristlegrass', 'bristlegrasses').
inf('bristle grass', 'bristle grasses').
inf('yellow bristlegrass', 'yellow bristlegrasses').
inf('yellow bristle grass', 'yellow bristle grasses').
inf('glaucous bristlegrass', 'glaucous bristlegrasses').
inf('green bristlegrass', 'green bristlegrasses').
inf('rough bristlegrass', 'rough bristlegrasses').
inf('bottle-grass', 'bottle-grasses').
inf('bottle grass', 'bottle grasses').
inf('Hungarian grass', 'Hungarian grasses').
inf('milo', 'miloes').
inf('sorgo', 'sorgoes').
inf('sorgho', 'sorghoes').
inf('Johnson grass', 'Johnson grasses').
inf('Aleppo grass', 'Aleppo grasses').
inf('means grass', 'means grasses').
inf('cordgrass', 'cordgrasses').
inf('cord grass', 'cord grasses').
inf('salt reed grass', 'salt reed grasses').
inf('prairie cordgrass', 'prairie cordgrasses').
inf('freshwater cordgrass', 'freshwater cordgrasses').
inf('slough grass', 'slough grasses').
inf('smut grass', 'smut grasses').
inf('carpet grass', 'carpet grasses').
inf('rush grass', 'rush grasses').
inf('rush-grass', 'rush-grasses').
inf('St. Augustine grass', 'St. Augustine grasses').
inf('buffalo grass', 'buffalo grasses').
inf('cereal grass', 'cereal grasses').
inf('wheat berry', 'wheat berries').
inf('Manila grass', 'Manila grasses').
inf('Japanese carpet grass', 'Japanese carpet grasses').
inf('Korean lawn grass', 'Korean lawn grasses').
inf('Japanese lawn grass', 'Japanese lawn grasses').
inf('mascarene grass', 'mascarene grasses').
inf('Korean velvet grass', 'Korean velvet grasses').
inf('sedge family', 'sedge families').
inf('yellow nutgrass', 'yellow nutgrasses').
inf('Egyptian paper rush', 'Egyptian paper rushes').
inf('paper rush', 'paper rushes').
inf('nutgrass', 'nutgrasses').
inf('nut grass', 'nut grasses').
inf('Carex', 'Carexes').
inf('genus Carex', 'genus Carexes').
inf('cotton grass', 'cotton grasses').
inf('cotton rush', 'cotton rushes').
inf('common cotton grass', 'common cotton grasses').
inf('hardstem bulrush', 'hardstem bulrushes').
inf('hardstemmed bulrush', 'hardstemmed bulrushes').
inf('wool grass', 'wool grasses').
inf('spike rush', 'spike rushes').
inf('Eleocharis dulcis', 'Eleocharis dulces').
inf('needle spike rush', 'needle spike rushes').
inf('needle rush', 'needle rushes').
inf('slender spike rush', 'slender spike rushes').
inf('hair grass', 'hair grasses').
inf('creeping spike rush', 'creeping spike rushes').
inf('screw-pine family', 'screw-pine families').
inf('cattail family', 'cattail families').
inf('bullrush', 'bullrushes').
inf('bulrush', 'bulrushes').
inf('lesser bullrush', 'lesser bullrushes').
inf('bur-reed family', 'bur-reed families').
inf('caryopsis', 'caryopses').
inf('gourd family', 'gourd families').
inf('Cucurbita pepo', 'Cucurbita pepoes').
inf('squash', 'squashes').
inf('summer squash', 'summer squashes').
inf('Cucurbita pepo melopepo', 'Cucurbita pepo melopepoes').
inf('yellow squash', 'yellow squashes').
inf('marrow squash', 'marrow squashes').
inf('pattypan squash', 'pattypan squashes').
inf('spaghetti squash', 'spaghetti squashes').
inf('winter squash', 'winter squashes').
inf('acorn squash', 'acorn squashes').
inf('hubbard squash', 'hubbard squashes').
inf('turban squash', 'turban squashes').
inf('buttercup squash', 'buttercup squashes').
inf('butternut squash', 'butternut squashes').
inf('winter crookneck squash', 'winter crookneck squashes').
inf('bryony', 'bryonies').
inf('briony', 'brionies').
inf('white bryony', 'white bryonies').
inf('red bryony', 'red bryonies').
inf('Cucumis melo', 'Cucumis meloes').
inf('Cucumis melo cantalupensis', 'Cucumis melo cantalupenses').
inf('calabash', 'calabashes').
inf('Goodenia family', 'Goodenia families').
inf('lobelia family', 'lobelia families').
inf('Indian tobacco', 'Indian tobaccoes').
inf('mallow family', 'mallow families').
inf('gumbo', 'gumboes').
inf('velvetleaf', 'velvetleaves').
inf('velvet-leaf', 'velvet-leaves').
inf('Rose of China', 'Roses of China').
inf('Hibiscus rosa-sinensis', 'Hibiscus rosa-sinenses').
inf('rose of Sharon', 'roses of Sharon').
inf('balibago', 'balibagoes').
inf('flower-of-an-hour', 'flowers-of-an-hour').
inf('flowers-of-an-hour', 'flowerss-of-an-hour').
inf('velvetleaf', 'velvetleaves').
inf('velvet-leaf', 'velvet-leaves').
inf('jellyleaf', 'jellyleaves').
inf('Bombax', 'Bombaxes').
inf('genus Bombax', 'genus Bombaxes').
inf('cream-of-tartar tree', 'creams-of-tartar tree').
inf('Ochroma lagopus', 'Ochroma lagopera').
inf('Pseudobombax', 'Pseudobombaxes').
inf('genus Pseudobombax', 'genus Pseudobombaxes').
inf('elaeocarpus family', 'elaeocarpus families').
inf('makomako', 'makomakoes').
inf('New Zealand wine berry', 'New Zealand wine berries').
inf('wineberry', 'wineberries').
inf('Jamaican cherry', 'Jamaican cherries').
inf('breakax', 'breakaxes').
inf('Sloanea jamaicensis', 'Sloanea jamaicenses').
inf('sterculia family', 'sterculia families').
inf('Firmiana simplex', 'Firmiana simplexes').
inf('flannelbush', 'flannelbushes').
inf('flannel bush', 'flannel bushes').
inf('California beauty', 'California beauties').
inf('red beech', 'red beeches').
inf('crow\'s foot', 'crow\'s feet').
inf('linden family', 'linden families').
inf('protea family', 'protea families').
inf('smoke bush', 'smoke bushes').
inf('Chilean firebush', 'Chilean firebushes').
inf('needlebush', 'needlebushes').
inf('needle-bush', 'needle-bushes').
inf('needle bush', 'needle bushes').
inf('prickly ash', 'prickly ashes').
inf('heath family', 'heath families').
inf('Erica tetralix', 'Erica tetralixes').
inf('Prince-of-Wales\'-heath', 'Princes-of-Wales\'-heath').
inf('Prince of Wales heath', 'Princes of Wales heath').
inf('bog rosemary', 'bog rosemaries').
inf('common bog rosemary', 'common bog rosemaries').
inf('madrono', 'madronoes').
inf('Irish strawberry', 'Irish strawberries').
inf('Arbutus unedo', 'Arbutus unedoes').
inf('bearberry', 'bearberries').
inf('common bearberry', 'common bearberries').
inf('red bearberry', 'red bearberries').
inf('wild cranberry', 'wild cranberries').
inf('mealberry', 'mealberries').
inf('hog cranberry', 'hog cranberries').
inf('sand berry', 'sand berries').
inf('sandberry', 'sandberries').
inf('mountain box', 'mountain boxes').
inf('alpine bearberry', 'alpine bearberries').
inf('black bearberry', 'black bearberries').
inf('leatherleaf', 'leatherleaves').
inf('creeping snowberry', 'creeping snowberries').
inf('maidenhair berry', 'maidenhair berries').
inf('teaberry', 'teaberries').
inf('checkerberry', 'checkerberries').
inf('groundberry', 'groundberries').
inf('ground-berry', 'ground-berries').
inf('huckleberry', 'huckleberries').
inf('black huckleberry', 'black huckleberries').
inf('dangleberry', 'dangleberries').
inf('dangle-berry', 'dangle-berries').
inf('box huckleberry', 'box huckleberries').
inf('calico bush', 'calico bushes').
inf('wild rosemary', 'wild rosemaries').
inf('switch-ivy', 'switch-ivies').
inf('staggerbush', 'staggerbushes').
inf('stagger bush', 'stagger bushes').
inf('maleberry', 'maleberries').
inf('male berry', 'male berries').
inf('he-huckleberry', 'he-huckleberries').
inf('fetterbush', 'fetterbushes').
inf('fetter bush', 'fetter bushes').
inf('fool\'s huckleberry', 'fool\'s huckleberries').
inf('minniebush', 'minniebushes').
inf('minnie bush', 'minnie bushes').
inf('lily-of-the-valley tree', 'lilies-of-the-valley tree').
inf('fetterbush', 'fetterbushes').
inf('mountain fetterbush', 'mountain fetterbushes').
inf('cranberry', 'cranberries').
inf('American cranberry', 'American cranberries').
inf('large cranberry', 'large cranberries').
inf('European cranberry', 'European cranberries').
inf('small cranberry', 'small cranberries').
inf('blueberry', 'blueberries').
inf('blueberry bush', 'blueberry bushes').
inf('huckleberry', 'huckleberries').
inf('farkleberry', 'farkleberries').
inf('sparkleberry', 'sparkleberries').
inf('low-bush blueberry', 'low-bush blueberries').
inf('low blueberry', 'low blueberries').
inf('rabbiteye blueberry', 'rabbiteye blueberries').
inf('rabbit-eye blueberry', 'rabbit-eye blueberries').
inf('dwarf bilberry', 'dwarf bilberries').
inf('dwarf blueberry', 'dwarf blueberries').
inf('high-bush blueberry', 'high-bush blueberries').
inf('tall bilberry', 'tall bilberries').
inf('swamp blueberry', 'swamp blueberries').
inf('evergreen blueberry', 'evergreen blueberries').
inf('evergreen huckleberry', 'evergreen huckleberries').
inf('bilberry', 'bilberries').
inf('thin-leaved bilberry', 'thin-leaved bilberries').
inf('mountain blue berry', 'mountain blue berries').
inf('bilberry', 'bilberries').
inf('whortleberry', 'whortleberries').
inf('whinberry', 'whinberries').
inf('blaeberry', 'blaeberries').
inf('bog bilberry', 'bog bilberries').
inf('bog whortleberry', 'bog whortleberries').
inf('moor berry', 'moor berries').
inf('dryland blueberry', 'dryland blueberries').
inf('dryland berry', 'dryland berries').
inf('grouseberry', 'grouseberries').
inf('grouse-berry', 'grouse-berries').
inf('grouse whortleberry', 'grouse whortleberries').
inf('deerberry', 'deerberries').
inf('squaw huckleberry', 'squaw huckleberries').
inf('cowberry', 'cowberries').
inf('mountain cranberry', 'mountain cranberries').
inf('lingonberry', 'lingonberries').
inf('lingenberry', 'lingenberries').
inf('lingberry', 'lingberries').
inf('foxberry', 'foxberries').
inf('white-alder family', 'white-alder families').
inf('sweet pepperbush', 'sweet pepperbushes').
inf('pepper bush', 'pepper bushes').
inf('diapensia family', 'diapensia families').
inf('genus Galax', 'genus Galaxes').
inf('galax', 'galaxes').
inf('galaxy', 'galaxies').
inf('coltsfoot', 'coltsfeet').
inf('pixy', 'pixies').
inf('epacris family', 'epacris families').
inf('native cranberry', 'native cranberries').
inf('groundberry', 'groundberries').
inf('ground-berry', 'ground-berries').
inf('wintergreen family', 'wintergreen families').
inf('shinleaf', 'shinleaves').
inf('love-in-winter', 'love-in-winter').
inf('beech family', 'beech families').
inf('beech', 'beeches').
inf('beech', 'beeches').
inf('common beech', 'common beeches').
inf('European beech', 'European beeches').
inf('copper beech', 'copper beeches').
inf('purple beech', 'purple beeches').
inf('American beech', 'American beeches').
inf('white beech', 'white beeches').
inf('red beech', 'red beeches').
inf('weeping beech', 'weeping beeches').
inf('Japanese beech', 'Japanese beeches').
inf('Castanea ozarkensis', 'Castanea ozarkenses').
inf('Castanopsis', 'Castanopses').
inf('genus Castanopsis', 'genus Castanopses').
inf('southern beech', 'southern beeches').
inf('evergreen beech', 'evergreen beeches').
inf('myrtle beech', 'myrtle beeches').
inf('New Zealand beech', 'New Zealand beeches').
inf('silver beech', 'silver beeches').
inf('roble beech', 'roble beeches').
inf('rauli beech', 'rauli beeches').
inf('black beech', 'black beeches').
inf('hard beech', 'hard beeches').
inf('Quercus ilex', 'Quercus ilexes').
inf('birch family', 'birch families').
inf('birch', 'birches').
inf('birch', 'birches').
inf('yellow birch', 'yellow birches').
inf('Betula alleghaniensis', 'Betula alleghanienses').
inf('American white birch', 'American white birches').
inf('paper birch', 'paper birches').
inf('paperbark birch', 'paperbark birches').
inf('canoe birch', 'canoe birches').
inf('grey birch', 'grey birches').
inf('gray birch', 'gray birches').
inf('American grey birch', 'American grey birches').
inf('American gray birch', 'American gray birches').
inf('silver birch', 'silver birches').
inf('common birch', 'common birches').
inf('European white birch', 'European white birches').
inf('downy birch', 'downy birches').
inf('white birch', 'white birches').
inf('black birch', 'black birches').
inf('river birch', 'river birches').
inf('red birch', 'red birches').
inf('sweet birch', 'sweet birches').
inf('cherry birch', 'cherry birches').
inf('black birch', 'black birches').
inf('Yukon white birch', 'Yukon white birches').
inf('swamp birch', 'swamp birches').
inf('water birch', 'water birches').
inf('mountain birch', 'mountain birches').
inf('Western paper birch', 'Western paper birches').
inf('Western birch', 'Western birches').
inf('Newfoundland dwarf birch', 'Newfoundland dwarf birches').
inf('American dwarf birch', 'American dwarf birches').
inf('Ostryopsis', 'Ostryopses').
inf('genus Ostryopsis', 'genus Ostryopses').
inf('gentian family', 'gentian families').
inf('centaury', 'centauries').
inf('lesser centaury', 'lesser centauries').
inf('tufted centaury', 'tufted centauries').
inf('seaside centaury', 'seaside centauries').
inf('broad leaved centaury', 'broad leaved centauries').
inf('slender centaury', 'slender centauries').
inf('columbo', 'columboes').
inf('American columbo', 'American columboes').
inf('Gentianopsis', 'Gentianopses').
inf('genus Gentianopsis', 'genus Gentianopses').
inf('American centaury', 'American centauries').
inf('Salvadora family', 'Salvadora families').
inf('olive family', 'olive families').
inf('fringe bush', 'fringe bushes').
inf('tanglebush', 'tanglebushes').
inf('ash', 'ashes').
inf('ash', 'ashes').
inf('white ash', 'white ashes').
inf('swamp ash', 'swamp ashes').
inf('flowering ash', 'flowering ashes').
inf('flowering ash', 'flowering ashes').
inf('European ash', 'European ashes').
inf('common European ash', 'common European ashes').
inf('Oregon ash', 'Oregon ashes').
inf('black ash', 'black ashes').
inf('basket ash', 'basket ashes').
inf('brown ash', 'brown ashes').
inf('hoop ash', 'hoop ashes').
inf('manna ash', 'manna ashes').
inf('flowering ash', 'flowering ashes').
inf('red ash', 'red ashes').
inf('downy ash', 'downy ashes').
inf('green ash', 'green ashes').
inf('blue ash', 'blue ashes').
inf('mountain ash', 'mountain ashes').
inf('Fraxinus texensis', 'Fraxinus texenses').
inf('pumpkin ash', 'pumpkin ashes').
inf('Arizona ash', 'Arizona ashes').
inf('bloodwort family', 'bloodwort families').
inf('kangaroo\'s-foot', 'kangaroo\'s-feet').
inf('Australian sword lily', 'Australian sword lilies').
inf('witch-hazel family', 'witch-hazel families').
inf('Corylopsis', 'Corylopses').
inf('genus Corylopsis', 'genus Corylopses').
inf('Parrotiopsis', 'Parrotiopses').
inf('genus Parrotiopsis', 'genus Parrotiopses').
inf('walnut family', 'walnut families').
inf('black hickory', 'black hickories').
inf('hickory', 'hickories').
inf('hickory', 'hickories').
inf('water hickory', 'water hickories').
inf('pignut hickory', 'pignut hickories').
inf('brown hickory', 'brown hickories').
inf('black hickory', 'black hickories').
inf('bitternut hickory', 'bitternut hickories').
inf('bitter hickory', 'bitter hickories').
inf('swamp hickory', 'swamp hickories').
inf('Carya illinoensis', 'Carya illinoenses').
inf('Carya illinoinsis', 'Carya illinoinses').
inf('big shellbark hickory', 'big shellbark hickories').
inf('king nut hickory', 'king nut hickories').
inf('nutmeg hickory', 'nutmeg hickories').
inf('shagbark hickory', 'shagbark hickories').
inf('shellbark hickory', 'shellbark hickories').
inf('mockernut hickory', 'mockernut hickories').
inf('black hickory', 'black hickories').
inf('white-heart hickory', 'white-heart hickories').
inf('big-bud hickory', 'big-bud hickories').
inf('combretum family', 'combretum families').
inf('oleaster family', 'oleaster families').
inf('silverberry', 'silverberries').
inf('silver berry', 'silver berries').
inf('silverbush', 'silverbushes').
inf('silver-bush', 'silver-bushes').
inf('silver berry', 'silver berries').
inf('water-milfoil family', 'water-milfoil families').
inf('loosestrife family', 'loosestrife families').
inf('grass poly', 'grass polies').
inf('pride-of-India', 'prides-of-India').
inf('myrtle family', 'myrtle families').
inf('bayberry', 'bayberries').
inf('Jamaica bayberry', 'Jamaica bayberries').
inf('sour cherry', 'sour cherries').
inf('Surinam cherry', 'Surinam cherries').
inf('feijoa bush', 'feijoa bushes').
inf('guava bush', 'guava bushes').
inf('Eucalyptus camaldulensis', 'Eucalyptus camaldulenses').
inf('white ash', 'white ashes').
inf('alpine ash', 'alpine ashes').
inf('Eucalyptus delegatensis', 'Eucalyptus delegatenses').
inf('white mountain ash', 'white mountain ashes').
inf('black sally', 'black sallies').
inf('mountain ash', 'mountain ashes').
inf('eucalyptus kino', 'eucalyptus kinoes').
inf('sour-gum family', 'sour-gum families').
inf('tupelo family', 'tupelo families').
inf('tupelo', 'tupeloes').
inf('tupelo', 'tupeloes').
inf('evening-primrose family', 'evening-primrose families').
inf('mangrove family', 'mangrove families').
inf('daphne family', 'daphne families').
inf('wicopy', 'wicopies').
inf('ling ko', 'ling koes').
inf('meadow-beauty family', 'meadow-beauty families').
inf('deer grass', 'deer grasses').
inf('meadow beauty', 'meadow beauties').
inf('canna lily', 'canna lilies').
inf('arrowroot family', 'arrowroot families').
inf('banana family', 'banana families').
inf('strelitzia family', 'strelitzia families').
inf('bird of paradise', 'birds of paradise').
inf('Ravenala madagascariensis', 'Ravenala madagascarienses').
inf('ginger family', 'ginger families').
inf('grains of paradise', 'grainss of paradise').
inf('begonia family', 'begonia families').
inf('Begonia rex', 'Begonia rexes').
inf('St John\'s wort family', 'St John\'s wort families').
inf('orange grass', 'orange grasses').
inf('St Andrews\'s cross', 'St Andrews\'s crosses').
inf('low St Andrew\'s cross', 'low St Andrew\'s crosses').
inf('Chinese gooseberry', 'Chinese gooseberries').
inf('Actinidia chinensis', 'Actinidia chinenses').
inf('canella family', 'canella families').
inf('papaya family', 'papaya families').
inf('rockrose family', 'rockrose families').
inf('poverty grass', 'poverty grasses').
inf('flacourtia family', 'flacourtia families').
inf('kei apple bush', 'kei apple bushes').
inf('Ceylon gooseberry', 'Ceylon gooseberries').
inf('wild peach', 'wild peaches').
inf('ocotillo', 'ocotilloes').
inf('ochna family', 'ochna families').
inf('bird\'s-eye bush', 'bird\'s-eye bushes').
inf('passionflower family', 'passionflower families').
inf('purple granadillo', 'purple granadilloes').
inf('sweet calabash', 'sweet calabashes').
inf('love-in-a-mist', 'love-in-a-mist').
inf('mignonette family', 'mignonette families').
inf('tamarisk family', 'tamarisk families').
inf('Tamarix', 'Tamarixes').
inf('genus Tamarix', 'genus Tamarixes').
inf('violet family', 'violet families').
inf('field pansy', 'field pansies').
inf('Viola arvensis', 'Viola arvenses').
inf('Viola canadensis', 'Viola canadenses').
inf('tufted pansy', 'tufted pansies').
inf('pansy', 'pansies').
inf('Viola tricolor hortensis', 'Viola tricolor hortenses').
inf('wild pansy', 'wild pansies').
inf('love-in-idleness', 'love-in-idleness').
inf('pink of my John', 'pinks of my John').
inf('nettle family', 'nettle families').
inf('China grass', 'China grasses').
inf('Laportea canadensis', 'Laportea canadenses').
inf('pellitory-of-the-wall', 'pellitories-of-the-wall').
inf('wall pellitory', 'wall pellitories').
inf('pellitory', 'pellitories').
inf('hemp family', 'hemp families').
inf('mulberry family', 'mulberry families').
inf('mulberry', 'mulberries').
inf('white mulberry', 'white mulberries').
inf('black mulberry', 'black mulberries').
inf('red mulberry', 'red mulberries').
inf('Ficus bengalensis', 'Ficus bengalenses').
inf('paper mulberry', 'paper mulberries').
inf('elm family', 'elm families').
inf('Ulmus sarniensis', 'Ulmus sarnienses').
inf('Ulmus campestris sarniensis', 'Ulmus campestris sarnienses').
inf('hackberry', 'hackberries').
inf('European hackberry', 'European hackberries').
inf('Mediterranean hackberry', 'Mediterranean hackberries').
inf('American hackberry', 'American hackberries').
inf('sugarberry', 'sugarberries').
inf('iris family', 'iris families').
inf('sword lily', 'sword lilies').
inf('blackberry-lily', 'blackberry-lilies').
inf('leopard lily', 'leopard lilies').
inf('Belamcanda chinensis', 'Belamcanda chinenses').
inf('sword lily', 'sword lilies').
inf('corn lily', 'corn lilies').
inf('blue-eyed grass', 'blue-eyed grasses').
inf('Sparaxis', 'Sparaxes').
inf('genus Sparaxis', 'genus Sparaxes').
inf('amaryllis family', 'amaryllis families').
inf('belladonna lily', 'belladonna lilies').
inf('naked lady', 'naked ladies').
inf('blood lily', 'blood lilies').
inf('Jacobean lily', 'Jacobean lilies').
inf('Aztec lily', 'Aztec lilies').
inf('Hypoxis', 'Hypoxes').
inf('genus Hypoxis', 'genus Hypoxes').
inf('star grass', 'star grasses').
inf('American star grass', 'American star grasses').
inf('lily family', 'lily families').
inf('lily', 'lilies').
inf('mountain lily', 'mountain lilies').
inf('Canada lily', 'Canada lilies').
inf('wild yellow lily', 'wild yellow lilies').
inf('meadow lily', 'meadow lilies').
inf('wild meadow lily', 'wild meadow lilies').
inf('Madonna lily', 'Madonna lilies').
inf('white lily', 'white lilies').
inf('Annunciation lily', 'Annunciation lilies').
inf('Lent lily', 'Lent lilies').
inf('tiger lily', 'tiger lilies').
inf('leopard lily', 'leopard lilies').
inf('pine lily', 'pine lilies').
inf('Columbia tiger lily', 'Columbia tiger lilies').
inf('Oregon lily', 'Oregon lilies').
inf('tiger lily', 'tiger lilies').
inf('devil lily', 'devil lilies').
inf('Easter lily', 'Easter lilies').
inf('Bermuda lily', 'Bermuda lilies').
inf('white trumpet lily', 'white trumpet lilies').
inf('coast lily', 'coast lilies').
inf('Michigan lily', 'Michigan lilies').
inf('leopard lily', 'leopard lilies').
inf('panther lily', 'panther lilies').
inf('wood lily', 'wood lilies').
inf('Turk\'s cap-lily', 'Turk\'s cap-lilies').
inf('lily of the Nile', 'lilies of the Nile').
inf('African lily', 'African lilies').
inf('blue African lily', 'blue African lilies').
inf('star grass', 'star grasses').
inf('ague grass', 'ague grasses').
inf('aloe family', 'aloe families').
inf('Aloe ferox', 'Aloe feroxes').
inf('Kniphofia praecox', 'Kniphofia praecoxes').
inf('Peruvian lily', 'Peruvian lilies').
inf('lily of the Incas', 'lilies of the Incas').
inf('Saint-Bernard\'s-lily', 'Saint-Bernard\'s-lilies').
inf('Anthericum liliago', 'Anthericum liliagoes').
inf('amber lily', 'amber lilies').
inf('smilax', 'smilaxes').
inf('mariposa lily', 'mariposa lilies').
inf('globe lily', 'globe lilies').
inf('white globe lily', 'white globe lilies').
inf('yellow globe lily', 'yellow globe lilies').
inf('rose globe lily', 'rose globe lilies').
inf('sego lily', 'sego lilies').
inf('camass', 'camasses').
inf('quamash', 'quamashes').
inf('camosh', 'camoshes').
inf('camash', 'camashes').
inf('Camassia quamash', 'Camassia quamashes').
inf('dogtooth', 'dogteeth').
inf('trout lily', 'trout lilies').
inf('European dogtooth', 'European dogteeth').
inf('fawn lily', 'fawn lilies').
inf('glacier lily', 'glacier lilies').
inf('snow lily', 'snow lilies').
inf('avalanche lily', 'avalanche lilies').
inf('fritillary', 'fritillaries').
inf('checkered lily', 'checkered lilies').
inf('rice-grain fritillary', 'rice-grain fritillaries').
inf('black fritillary', 'black fritillaries').
inf('white fritillary', 'white fritillaries').
inf('snake\'s head fritillary', 'snake\'s head fritillaries').
inf('leper lily', 'leper lilies').
inf('adobe lily', 'adobe lilies').
inf('pink fritillary', 'pink fritillaries').
inf('scarlet fritillary', 'scarlet fritillaries').
inf('naked lady', 'naked ladies').
inf('glory lily', 'glory lilies').
inf('climbing lily', 'climbing lilies').
inf('creeping lily', 'creeping lilies').
inf('day lily', 'day lilies').
inf('daylily', 'daylilies').
inf('lemon lily', 'lemon lilies').
inf('plantain lily', 'plantain lilies').
inf('day lily', 'day lilies').
inf('star-of-Bethlehem', 'stars-of-Bethlehem').
inf('squaw grass', 'squaw grasses').
inf('bear grass', 'bear grasses').
inf('Xerophyllum tenax', 'Xerophyllum tenaxes').
inf('grass tree family', 'grass tree families').
inf('alkali grass', 'alkali grasses').
inf('trillium family', 'trillium families').
inf('wood lily', 'wood lilies').
inf('Smilax', 'Smilaxes').
inf('genus Smilax', 'genus Smilaxes').
inf('lily of the valley', 'lilies of the valley').
inf('May lily', 'May lilies').
inf('Clinton\'s lily', 'Clinton\'s lilies').
inf('lilyturf', 'lilyturves').
inf('lily turf', 'lily turves').
inf('agave family', 'agave families').
inf('sisal family', 'sisal families').
inf('Dracaena draco', 'Dracaena dracoes').
inf('bear grass', 'bear grasses').
inf('Sansevieria guineensis', 'Sansevieria guineenses').
inf('mother-in-law\'s tongue', 'mothers-in-law\'s tongue').
inf('bear grass', 'bear grasses').
inf('bear grass', 'bear grasses').
inf('buckbean family', 'buckbean families').
inf('butterfly bush', 'butterfly bushes').
inf('flax family', 'flax families').
inf('flax', 'flaxes').
inf('bird of paradise', 'birds of paradise').
inf('pride of barbados', 'prides of barbados').
inf('mountain ebony', 'mountain ebonies').
inf('Delonix', 'Delonixes').
inf('genus Delonix', 'genus Delonixes').
inf('campeachy', 'campeachies').
inf('ringworm bush', 'ringworm bushes').
inf('tamarindo', 'tamarindoes').
inf('false indigo', 'false indigoes').
inf('bastard indigo', 'bastard indigoes').
inf('false indigo', 'false indigoes').
inf('bastard indigo', 'bastard indigoes').
inf('silverbush', 'silverbushes').
inf('silver-bush', 'silver-bushes').
inf('kidney vetch', 'kidney vetches').
inf('Indian potato', 'Indian potatoes').
inf('Aspalathus cedcarbergensis', 'Aspalathus cedcarbergenses').
inf('milk vetch', 'milk vetches').
inf('milk-vetch', 'milk-vetches').
inf('alpine milk vetch', 'alpine milk vetches').
inf('purple milk vetch', 'purple milk vetches').
inf('wild indigo', 'wild indigoes').
inf('false indigo', 'false indigoes').
inf('blue false indigo', 'blue false indigoes').
inf('white false indigo', 'white false indigoes').
inf('Cercis', 'Cerces').
inf('genus Cercis', 'genus Cerces').
inf('Cercis canadensis', 'Cercis canadenses').
inf('Chamaecytisus palmensis', 'Chamaecytisus palmenses').
inf('garbanzo', 'garbanzoes').
inf('Codariocalyx', 'Codariocalyxes').
inf('genus Codariocalyx', 'genus Codariocalyxes').
inf('crown vetch', 'crown vetches').
inf('rattlebox', 'rattleboxes').
inf('American rattlebox', 'American rattleboxes').
inf('Indian rattlebox', 'Indian rattleboxes').
inf('Cyamopsis', 'Cyamopses').
inf('genus Cyamopsis', 'genus Cyamopses').
inf('Dalbergia cearensis', 'Dalbergia cearenses').
inf('cocobolo', 'cocoboloes').
inf('Desmanthus ilinoensis', 'Desmanthus ilinoenses').
inf('ceibo', 'ceiboes').
inf('poison bush', 'poison bushes').
inf('Glycine max', 'Glycine maxes').
inf('sweet vetch', 'sweet vetches').
inf('horseshoe vetch', 'horseshoe vetches').
inf('indigo', 'indigoes').
inf('running postman', 'running postmen').
inf('grass vetch', 'grass vetches').
inf('Lathyrus pratensis', 'Lathyrus pratenses').
inf('pride of California', 'prides of California').
inf('tuberous vetch', 'tuberous vetches').
inf('spring vetch', 'spring vetches').
inf('Lupinus texensis', 'Lupinus texenses').
inf('Medicago', 'Medicagoes').
inf('genus Medicago', 'genus Medicagoes').
inf('balsam of tolu', 'balsams of tolu').
inf('balsam of Peru', 'balsams of Peru').
inf('purple loco', 'purple locoes').
inf('Phaseolus limensis', 'Phaseolus limenses').
inf('stingaree-bush', 'stingaree-bushes').
inf('native holly', 'native hollies').
inf('Indian beech', 'Indian beeches').
inf('Pterocarpus angolensis', 'Pterocarpus angolenses').
inf('kino', 'kinoes').
inf('East India kino', 'East India kinoes').
inf('Malabar kino', 'Malabar kinoes').
inf('juniper bush', 'juniper bushes').
inf('Sophora sinensis', 'Sophora sinenses').
inf('frijolito', 'frijolitoes').
inf('frijolillo', 'frijolilloes').
inf('coral bush', 'coral bushes').
inf('flame bush', 'flame bushes').
inf('bastard indigo', 'bastard indigoes').
inf('Thermopsis', 'Thermopses').
inf('genus Thermopsis', 'genus Thermopses').
inf('pride of Bolivia', 'prides of Bolivia').
inf('Ulex', 'Ulexes').
inf('genus Ulex', 'genus Ulexes').
inf('vetch', 'vetches').
inf('tufted vetch', 'tufted vetches').
inf('bird vetch', 'bird vetches').
inf('bitter betch', 'bitter betches').
inf('spring vetch', 'spring vetches').
inf('bush vetch', 'bush vetches').
inf('hairy vetch', 'hairy vetches').
inf('Vigna sinensis', 'Vigna sinenses').
inf('Virgilia capensis', 'Virgilia capenses').
inf('Wisteria chinensis', 'Wisteria chinenses').
inf('palm family', 'palm families').
inf('palmetto', 'palmettoes').
inf('coco', 'cocoes').
inf('carnauba wax', 'carnauba waxes').
inf('genus Corozo', 'genus Corozoes').
inf('corozo', 'corozoes').
inf('Elaeis guineensis', 'Elaeis guineenses').
inf('phoenix', 'phoenixes').
inf('genus Phoenix', 'genus Phoenixes').
inf('vegetable ivory', 'vegetable ivories').
inf('jupaty', 'jupaties').
inf('cabbage palmetto', 'cabbage palmettoes').
inf('Sabal palmetto', 'Sabal palmettoes').
inf('saw palmetto', 'saw palmettoes').
inf('scrub palmetto', 'scrub palmettoes').
inf('Thrinax', 'Thrinaxes').
inf('genus Thrinax', 'genus Thrinaxes').
inf('silver thatch', 'silver thatches').
inf('silvertop palmetto', 'silvertop palmettoes').
inf('silver thatch', 'silver thatches').
inf('Thrinax keyensis', 'Thrinax keyenses').
inf('plantain family', 'plantain families').
inf('Plantago', 'Plantagoes').
inf('genus Plantago', 'genus Plantagoes').
inf('ribgrass', 'ribgrasses').
inf('ripple-grass', 'ripple-grasses').
inf('white-man\'s foot', 'white-man\'s feet').
inf('whiteman\'s foot', 'whiteman\'s feet').
inf('buckwheat family', 'buckwheat families').
inf('Rumex', 'Rumexes').
inf('genus Rumex', 'genus Rumexes').
inf('sour grass', 'sour grasses').
inf('yellow-eyed grass family', 'yellow-eyed grass families').
inf('yellow-eyed grass', 'yellow-eyed grasses').
inf('spiderwort family', 'spiderwort families').
inf('St.-Bruno\'s-lily', 'St.-Bruno\'s-lilies').
inf('pineapple family', 'pineapple families').
inf('Spanish moss', 'Spanish mosses').
inf('black moss', 'black mosses').
inf('long moss', 'long mosses').
inf('pipewort family', 'pipewort families').
inf('pickerelweed family', 'pickerelweed families').
inf('water star grass', 'water star grasses').
inf('naiad family', 'naiad families').
inf('water-plantain family', 'water-plantain families').
inf('frogbit family', 'frogbit families').
inf('frog\'s-bit family', 'frog\'s-bit families').
inf('ditchmoss', 'ditchmosses').
inf('Elodea canadensis', 'Elodea canadenses').
inf('tape grass', 'tape grasses').
inf('eelgrass', 'eelgrasses').
inf('wild celery', 'wild celeries').
inf('pondweed family', 'pondweed families').
inf('arrow-grass family', 'arrow-grass families').
inf('arrow grass', 'arrow grasses').
inf('eelgrass family', 'eelgrass families').
inf('eelgrass', 'eelgrasses').
inf('rose family', 'rose families').
inf('rosebush', 'rosebushes').
inf('Rosa chinensis', 'Rosa chinenses').
inf('brierpatch', 'brierpatches').
inf('brier patch', 'brier patches').
inf('agrimony', 'agrimonies').
inf('fragrant agrimony', 'fragrant agrimonies').
inf('serviceberry', 'serviceberries').
inf('shadbush', 'shadbushes').
inf('alder-leaved serviceberry', 'alder-leaved serviceberries').
inf('icaco', 'icacoes').
inf('Chrysobalanus icaco', 'Chrysobalanus icacoes').
inf('quince bush', 'quince bushes').
inf('strawberry', 'strawberries').
inf('garden strawberry', 'garden strawberries').
inf('cultivated strawberry', 'cultivated strawberries').
inf('wild strawberry', 'wild strawberries').
inf('wood strawberry', 'wood strawberries').
inf('beach strawberry', 'beach strawberries').
inf('Chilean strawberry', 'Chilean strawberries').
inf('Fragaria chiloensis', 'Fragaria chiloenses').
inf('Virginia strawberry', 'Virginia strawberries').
inf('scarlet strawberry', 'scarlet strawberries').
inf('Christmas berry', 'Christmas berries').
inf('Malus ioensis', 'Malus ioenses').
inf('goose-tansy', 'goose-tansies').
inf('goose grass', 'goose grasses').
inf('Prunus alleghaniensis', 'Prunus alleghanienses').
inf('hog plum bush', 'hog plum bushes').
inf('beach plum bush', 'beach plum bushes').
inf('cherry', 'cherries').
inf('cherry', 'cherries').
inf('wild cherry', 'wild cherries').
inf('wild cherry', 'wild cherries').
inf('sweet cherry', 'sweet cherries').
inf('heart cherry', 'heart cherries').
inf('oxheart cherry', 'oxheart cherries').
inf('mazzard cherry', 'mazzard cherries').
inf('Western sand cherry', 'Western sand cherries').
inf('Rocky Mountains cherry', 'Rocky Mountains cherries').
inf('laurel cherry', 'laurel cherries').
inf('sour cherry', 'sour cherries').
inf('morello', 'morelloes').
inf('marasca cherry', 'marasca cherries').
inf('maraschino cherry', 'maraschino cherries').
inf('Prunus dulcis', 'Prunus dulces').
inf('holly-leaved cherry', 'holly-leaved cherries').
inf('holly-leaf cherry', 'holly-leaf cherries').
inf('evergreen cherry', 'evergreen cherries').
inf('fuji cherry', 'fuji cherries').
inf('oriental bush cherry', 'oriental bush cherries').
inf('laurel cherry', 'laurel cherries').
inf('Catalina cherry', 'Catalina cherries').
inf('bird cherry', 'bird cherries').
inf('European bird cherry', 'European bird cherries').
inf('common bird cherry', 'common bird cherries').
inf('hagberry', 'hagberries').
inf('pin cherry', 'pin cherries').
inf('peach', 'peaches').
inf('sand cherry', 'sand cherries').
inf('black cherry', 'black cherries').
inf('rum cherry', 'rum cherries').
inf('flowering cherry', 'flowering cherries').
inf('oriental cherry', 'oriental cherries').
inf('Japanese cherry', 'Japanese cherries').
inf('Japanese flowering cherry', 'Japanese flowering cherries').
inf('Japanese flowering cherry', 'Japanese flowering cherries').
inf('rosebud cherry', 'rosebud cherries').
inf('winter flowering cherry', 'winter flowering cherries').
inf('chokecherry', 'chokecherries').
inf('chokecherry', 'chokecherries').
inf('western chokecherry', 'western chokecherries').
inf('bramble bush', 'bramble bushes').
inf('lawyerbush', 'lawyerbushes').
inf('lawyer bush', 'lawyer bushes').
inf('blackberry', 'blackberries').
inf('blackberry bush', 'blackberry bushes').
inf('true blackberry', 'true blackberries').
inf('sand blackberry', 'sand blackberries').
inf('dewberry', 'dewberries').
inf('dewberry bush', 'dewberry bushes').
inf('running blackberry', 'running blackberries').
inf('western blackberry', 'western blackberries').
inf('western dewberry', 'western dewberries').
inf('boysenberry', 'boysenberries').
inf('boysenberry bush', 'boysenberry bushes').
inf('loganberry', 'loganberries').
inf('American dewberry', 'American dewberries').
inf('Rubus canadensis', 'Rubus canadenses').
inf('Northern dewberry', 'Northern dewberries').
inf('American dewberry', 'American dewberries').
inf('Southern dewberry', 'Southern dewberries').
inf('swamp dewberry', 'swamp dewberries').
inf('swamp blackberry', 'swamp blackberries').
inf('European dewberry', 'European dewberries').
inf('raspberry', 'raspberries').
inf('raspberry bush', 'raspberry bushes').
inf('red raspberry', 'red raspberries').
inf('wild raspberry', 'wild raspberries').
inf('European raspberry', 'European raspberries').
inf('American raspberry', 'American raspberries').
inf('black raspberry', 'black raspberries').
inf('blackcap raspberry', 'blackcap raspberries').
inf('thimbleberry', 'thimbleberries').
inf('salmonberry', 'salmonberries').
inf('salmonberry', 'salmonberries').
inf('salmon berry', 'salmon berries').
inf('thimbleberry', 'thimbleberries').
inf('cloudberry', 'cloudberries').
inf('dwarf mulberry', 'dwarf mulberries').
inf('baked-apple berry', 'baked-apple berries').
inf('salmonberry', 'salmonberries').
inf('flowering raspberry', 'flowering raspberries').
inf('purple-flowering raspberry', 'purple-flowering raspberries').
inf('thimbleberry', 'thimbleberries').
inf('wineberry', 'wineberries').
inf('mountain ash', 'mountain ashes').
inf('European mountain ash', 'European mountain ashes').
inf('rowanberry', 'rowanberries').
inf('American mountain ash', 'American mountain ashes').
inf('Western mountain ash', 'Western mountain ashes').
inf('Sorbus sitchensis', 'Sorbus sitchenses').
inf('madder family', 'madder families').
inf('blolly', 'blollies').
inf('West Indian snowberry', 'West Indian snowberries').
inf('goose grass', 'goose grasses').
inf('Galium mollugo', 'Galium mollugoes').
inf('marmalade box', 'marmalade boxes').
inf('scarlet bush', 'scarlet bushes').
inf('coloradillo', 'coloradilloes').
inf('partridgeberry', 'partridgeberries').
inf('boxberry', 'boxberries').
inf('twinberry', 'twinberries').
inf('Psychotria capensis', 'Psychotria capenses').
inf('negro peach', 'negro peaches').
inf('Vangueria madagascariensis', 'Vangueria madagascarienses').
inf('honeysuckle family', 'honeysuckle families').
inf('beauty bush', 'beauty bushes').
inf('Lonicera canadensis', 'Lonicera canadenses').
inf('twinberry', 'twinberries').
inf('snowberry', 'snowberries').
inf('common snowberry', 'common snowberries').
inf('waxberry', 'waxberries').
inf('coralberry', 'coralberries').
inf('elderberry bush', 'elderberry bushes').
inf('black elderberry', 'black elderberries').
inf('Sambucus canadensis', 'Sambucus canadenses').
inf('blue elderberry', 'blue elderberries').
inf('elderberry', 'elderberries').
inf('cranberry bush', 'cranberry bushes').
inf('American cranberry bush', 'American cranberry bushes').
inf('highbush cranberry', 'highbush cranberries').
inf('European cranberrybush', 'European cranberrybushes').
inf('European cranberry bush', 'European cranberry bushes').
inf('Scabiosa arvensis', 'Scabiosa arvenses').
inf('balsam family', 'balsam families').
inf('Impatiens capensis', 'Impatiens capenses').
inf('geranium family', 'geranium families').
inf('pin grass', 'pin grasses').
inf('muskus grass', 'muskus grasses').
inf('torchwood family', 'torchwood families').
inf('gumbo-limbo', 'gumbo-limboes').
inf('balm of gilead', 'balms of gilead').
inf('Commiphora meccanensis', 'Commiphora meccanenses').
inf('sweet cicely', 'sweet cicelies').
inf('barbados cherry', 'barbados cherries').
inf('Surinam cherry', 'Surinam cherries').
inf('West Indian cherry', 'West Indian cherries').
inf('mahogany family', 'mahogany families').
inf('mahogany', 'mahoganies').
inf('mahogany', 'mahoganies').
inf('chinaberry', 'chinaberries').
inf('pride-of-India', 'prides-of-India').
inf('azederach', 'azederaches').
inf('azedarach', 'azedaraches').
inf('Melia azederach', 'Melia azederaches').
inf('Melia azedarach', 'Melia azedaraches').
inf('African scented mahogany', 'African scented mahoganies').
inf('cedar mahogany', 'cedar mahoganies').
inf('sapele mahogany', 'sapele mahoganies').
inf('silver ash', 'silver ashes').
inf('native beech', 'native beeches').
inf('flindosy', 'flindosies').
inf('African mahogany', 'African mahoganies').
inf('true mahogany', 'true mahoganies').
inf('Cuban mahogany', 'Cuban mahoganies').
inf('Dominican mahogany', 'Dominican mahoganies').
inf('Honduras mahogany', 'Honduras mahoganies').
inf('Philippine mahogany', 'Philippine mahoganies').
inf('Philippine mahogany', 'Philippine mahoganies').
inf('caracolito', 'caracolitoes').
inf('Ruptiliocarpon caracolito', 'Ruptiliocarpon caracolitoes').
inf('wood-sorrel family', 'wood-sorrel families').
inf('goatsfoot', 'goatsfeet').
inf('goat\'s foot', 'goat\'s feet').
inf('milkwort family', 'milkwort families').
inf('bird-on-the-wing', 'birds-on-the-wing').
inf('rue family', 'rue families').
inf('herb of grace', 'herbs of grace').
inf('pomelo', 'pomeloes').
inf('pummelo', 'pummeloes').
inf('Citrus sinensis', 'Citrus sinenses').
inf('tangelo', 'tangeloes').
inf('Citrus tangelo', 'Citrus tangeloes').
inf('dittany', 'dittanies').
inf('burning bush', 'burning bushes').
inf('prickly ash', 'prickly ashes').
inf('sea ash', 'sea ashes').
inf('quassia family', 'quassia families').
inf('tree of heaven', 'trees of heaven').
inf('tree of the gods', 'trees of the gods').
inf('wild mango', 'wild mangoes').
inf('Irvingia gabonensis', 'Irvingia gabonenses').
inf('nasturtium family', 'nasturtium families').
inf('Indian cress', 'Indian cresses').
inf('bean-caper family', 'bean-caper families').
inf('Zygophyllum fabago', 'Zygophyllum fabagoes').
inf('palo santo', 'palo santoes').
inf('creosote bush', 'creosote bushes').
inf('willow family', 'willow families').
inf('Salix', 'Salixes').
inf('genus Salix', 'genus Salixes').
inf('Salix sitchensis', 'Salix sitchenses').
inf('sandalwood family', 'sandalwood families').
inf('bastard toadflax', 'bastard toadflaxes').
inf('mistletoe family', 'mistletoe families').
inf('mistletoe family', 'mistletoe families').
inf('soapberry family', 'soapberry families').
inf('soapberry', 'soapberries').
inf('jaboncillo', 'jaboncilloes').
inf('chinaberry', 'chinaberries').
inf('longanberry', 'longanberries').
inf('Litchi chinensis', 'Litchi chinenses').
inf('honey berry', 'honey berries').
inf('mamoncillo', 'mamoncilloes').
inf('box family', 'box families').
inf('box', 'boxes').
inf('common box', 'common boxes').
inf('European box', 'European boxes').
inf('spindle-tree family', 'spindle-tree families').
inf('staff-tree family', 'staff-tree families').
inf('spindleberry', 'spindleberries').
inf('burning bush', 'burning bushes').
inf('strawberry bush', 'strawberry bushes').
inf('cyrilla family', 'cyrilla families').
inf('titi family', 'titi families').
inf('crowberry family', 'crowberry families').
inf('crowberry', 'crowberries').
inf('maple family', 'maple families').
inf('Acer negundo', 'Acer negundoes').
inf('holly family', 'holly families').
inf('holly', 'hollies').
inf('Ilex', 'Ilexes').
inf('genus Ilex', 'genus Ilexes').
inf('Chinese holly', 'Chinese hollies').
inf('bearberry', 'bearberries').
inf('winterberry', 'winterberries').
inf('inkberry', 'inkberries').
inf('gallberry', 'gallberries').
inf('gall-berry', 'gall-berries').
inf('evergreen winterberry', 'evergreen winterberries').
inf('Ilex paraguariensis', 'Ilex paraguarienses').
inf('American holly', 'American hollies').
inf('Christmas holly', 'Christmas hollies').
inf('low gallberry holly', 'low gallberry hollies').
inf('tall gallberry holly', 'tall gallberry hollies').
inf('yaupon holly', 'yaupon hollies').
inf('deciduous holly', 'deciduous hollies').
inf('juneberry holly', 'juneberry hollies').
inf('largeleaf holly', 'largeleaf hollies').
inf('Geogia holly', 'Geogia hollies').
inf('common winterberry holly', 'common winterberry hollies').
inf('smooth winterberry holly', 'smooth winterberry hollies').
inf('sumac family', 'sumac families').
inf('smoke bush', 'smoke bushes').
inf('mango', 'mangoes').
inf('sumach', 'sumaches').
inf('sugar-bush', 'sugar-bushes').
inf('squawbush', 'squawbushes').
inf('squaw-bush', 'squaw-bushes').
inf('skunkbush', 'skunkbushes').
inf('poison ash', 'poison ashes').
inf('Toxicodendron vernix', 'Toxicodendron vernixes').
inf('Rhus vernix', 'Rhus vernixes').
inf('poison ivy', 'poison ivies').
inf('poison mercury', 'poison mercuries').
inf('horse-chestnut family', 'horse-chestnut families').
inf('bladdernut family', 'bladdernut families').
inf('ebony family', 'ebony families').
inf('ebony', 'ebonies').
inf('ebony', 'ebonies').
inf('sapodilla family', 'sapodilla families').
inf('caimito', 'caimitoes').
inf('Chrysophyllum cainito', 'Chrysophyllum cainitoes').
inf('satinleaf', 'satinleaves').
inf('satin leaf', 'satin leaves').
inf('caimitillo', 'caimitilloes').
inf('sweetleaf family', 'sweetleaf families').
inf('sweetleaf', 'sweetleaves').
inf('Asiatic sweetleaf', 'Asiatic sweetleaves').
inf('sapphire berry', 'sapphire berries').
inf('storax family', 'storax families').
inf('styrax family', 'styrax families').
inf('storax', 'storaxes').
inf('genus Styrax', 'genus Styraxes').
inf('styrax', 'styraxes').
inf('pitcher-plant family', 'pitcher-plant families').
inf('sundew family', 'sundew families').
inf('stonecrop family', 'stonecrop families').
inf('cunonia family', 'cunonia families').
inf('Christmas bush', 'Christmas bushes').
inf('hydrangea family', 'hydrangea families').
inf('Hydrangea macrophylla hortensis', 'Hydrangea macrophylla hortenses').
inf('decumary', 'decumaries').
inf('saxifrage family', 'saxifrage families').
inf('mother-of-thousands', 'mothers-of-thousands').
inf('grass-of-Parnassus', 'grasses-of-Parnassus').
inf('youth-on-age', 'youths-on-age').
inf('gooseberry family', 'gooseberry families').
inf('currant bush', 'currant bushes').
inf('gooseberry', 'gooseberries').
inf('gooseberry bush', 'gooseberry bushes').
inf('plane-tree family', 'plane-tree families').
inf('phlox family', 'phlox families').
inf('charity', 'charities').
inf('genus Phlox', 'genus Phloxes').
inf('phlox', 'phloxes').
inf('chickweed phlox', 'chickweed phloxes').
inf('sand phlox', 'sand phloxes').
inf('mountain phlox', 'mountain phloxes').
inf('moss phlox', 'moss phloxes').
inf('dwarf phlox', 'dwarf phloxes').
inf('acanthus family', 'acanthus families').
inf('bear\'s breech', 'bear\'s breeches').
inf('sea holly', 'sea hollies').
inf('Chilopsis', 'Chilopses').
inf('genus Chilopsis', 'genus Chilopses').
inf('calabash', 'calabashes').
inf('calabash', 'calabashes').
inf('borage family', 'borage families').
inf('Borago', 'Boragoes').
inf('genus Borago', 'genus Boragoes').
inf('bugloss', 'buglosses').
inf('Anchusa capensis', 'Anchusa capenses').
inf('viper\'s bugloss', 'viper\'s buglosses').
inf('morning-glory family', 'morning-glory families').
inf('wild morning-glory', 'wild morning-glories').
inf('Convolvulus arvensis', 'Convolvulus arvenses').
inf('scammony', 'scammonies').
inf('scammony', 'scammonies').
inf('wild morning-glory', 'wild morning-glories').
inf('morning glory', 'morning glories').
inf('common morning glory', 'common morning glories').
inf('common morning glory', 'common morning glories').
inf('star-glory', 'star-glories').
inf('sweet potato', 'sweet potatoes').
inf('man-of-the-earth', 'men-of-the-earth').
inf('red morning-glory', 'red morning-glories').
inf('man-of-the-earth', 'men-of-the-earth').
inf('scammony', 'scammonies').
inf('Ipomoea orizabensis', 'Ipomoea orizabenses').
inf('beach morning glory', 'beach morning glories').
inf('Japanese morning glory', 'Japanese morning glories').
inf('imperial Japanese morning glory', 'imperial Japanese morning glories').
inf('gesneria family', 'gesneria families').
inf('waterleaf family', 'waterleaf families').
inf('waterleaf', 'waterleaves').
inf('Virginia waterleaf', 'Virginia waterleaves').
inf('shawny', 'shawnies').
inf('mint family', 'mint families').
inf('mother of thyme', 'mothers of thyme').
inf('Acinos arvensis', 'Acinos arvenses').
inf('Ajuga genevensis', 'Ajuga genevenses').
inf('Collinsonia canadensis', 'Collinsonia canadenses').
inf('Apalachicola rosemary', 'Apalachicola rosemaries').
inf('Galeopsis', 'Galeopses').
inf('genus Galeopsis', 'genus Galeopses').
inf('ground ivy', 'ground ivies').
inf('gill-over-the-ground', 'gills-over-the-ground').
inf('Hyssopus', 'Hyssopera').
inf('genus Hyssopus', 'genus Hyssopera').
inf('Lycopus', 'Lycopera').
inf('genus Lycopus', 'genus Lycopera').
inf('oregano', 'oreganoes').
inf('Majorana hortensis', 'Majorana hortenses').
inf('dittany of crete', 'dittanies of crete').
inf('cretan dittany', 'cretan dittanies').
inf('crete dittany', 'crete dittanies').
inf('Mentha arvensis', 'Mentha arvenses').
inf('savory', 'savories').
inf('bells of Ireland', 'bellss of Ireland').
inf('patchouly', 'patchoulies').
inf('rosemary', 'rosemaries').
inf('meadow clary', 'meadow claries').
inf('Salvia pratensis', 'Salvia pratenses').
inf('clary', 'claries').
inf('wild clary', 'wild claries').
inf('savory', 'savories').
inf('summer savory', 'summer savories').
inf('Satureja hortensis', 'Satureja hortenses').
inf('Satureia hortensis', 'Satureia hortenses').
inf('winter savory', 'winter savories').
inf('Sideritis', 'Sideritis').
inf('genus Sideritis', 'genus Sideritis').
inf('California romero', 'California romeroes').
inf('bladderwort family', 'bladderwort families').
inf('broomrape family', 'broomrape families').
inf('sesame family', 'sesame families').
inf('benny', 'bennies').
inf('figwort family', 'figwort families').
inf('foxglove family', 'foxglove families').
inf('Indian paintbrush', 'Indian paintbrushes').
inf('desert paintbrush', 'desert paintbrushes').
inf('giant red paintbrush', 'giant red paintbrushes').
inf('great plains paintbrush', 'great plains paintbrushes').
inf('sulfur paintbrush', 'sulfur paintbrushes').
inf('blue toadflax', 'blue toadflaxes').
inf('old-field toadflax', 'old-field toadflaxes').
inf('Linaria canadensis', 'Linaria canadenses').
inf('toadflax', 'toadflaxes').
inf('devil\'s flax', 'devil\'s flaxes').
inf('flannel leaf', 'flannel leaves').
inf('Verbascum lychnitis', 'Verbascum lychnitis').
inf('torch', 'torches').
inf('Veronica arvensis', 'Veronica arvenses').
inf('potato family', 'potato families').
inf('poroporo', 'poroporoes').
inf('Uruguay potato', 'Uruguay potatoes').
inf('trompillo', 'trompilloes').
inf('prairie berry', 'prairie berries').
inf('African holly', 'African hollies').
inf('wild potato', 'wild potatoes').
inf('eggplant bush', 'eggplant bushes').
inf('poisonberry', 'poisonberries').
inf('poison-berry', 'poison-berries').
inf('garden huckleberry', 'garden huckleberries').
inf('wonderberry', 'wonderberries').
inf('sunberry', 'sunberries').
inf('Jerusalem cherry', 'Jerusalem cherries').
inf('winter cherry', 'winter cherries').
inf('Madeira winter cherry', 'Madeira winter cherries').
inf('potato', 'potatoes').
inf('white potato', 'white potatoes').
inf('lady-of-the-night', 'ladies-of-the-night').
inf('jalapeno', 'jalapenoes').
inf('pimento', 'pimentoes').
inf('pimiento', 'pimientoes').
inf('tree tomato', 'tree tomatoes').
inf('tamarillo', 'tamarilloes').
inf('apple of Peru', 'apples of Peru').
inf('Duke of Argyll\'s tea tree', 'Dukes of Argyll\'s tea tree').
inf('Christmas berry', 'Christmas berries').
inf('tomato', 'tomatoes').
inf('cherry tomato', 'cherry tomatoes').
inf('plum tomato', 'plum tomatoes').
inf('apple of Peru', 'apples of Peru').
inf('shoo fly', 'shoo flies').
inf('tobacco', 'tobaccoes').
inf('flowering tobacco', 'flowering tobaccoes').
inf('Jasmine tobacco', 'Jasmine tobaccoes').
inf('common tobacco', 'common tobaccoes').
inf('wild tobacco', 'wild tobaccoes').
inf('Indian tobacco', 'Indian tobaccoes').
inf('tree tobacco', 'tree tobaccoes').
inf('ground cherry', 'ground cherries').
inf('husk tomato', 'husk tomatoes').
inf('downy ground cherry', 'downy ground cherries').
inf('strawberry tomato', 'strawberry tomatoes').
inf('winter cherry', 'winter cherries').
inf('bladder cherry', 'bladder cherries').
inf('cape gooseberry', 'cape gooseberries').
inf('purple ground cherry', 'purple ground cherries').
inf('strawberry tomato', 'strawberry tomatoes').
inf('dwarf cape gooseberry', 'dwarf cape gooseberries').
inf('tomatillo', 'tomatilloes').
inf('jamberry', 'jamberries').
inf('Mexican husk tomato', 'Mexican husk tomatoes').
inf('tomatillo', 'tomatilloes').
inf('purple ground cherry', 'purple ground cherries').
inf('jamberry', 'jamberries').
inf('genus Salpiglossis', 'genus Salpiglosses').
inf('salpiglossis', 'salpiglosses').
inf('marmalade bush', 'marmalade bushes').
inf('fire bush', 'fire bushes').
inf('fire-bush', 'fire-bushes').
inf('verbena family', 'verbena families').
inf('vervain family', 'vervain families').
inf('spurge family', 'spurge families').
inf('snow-on-the-mountain', 'snows-on-the-mountain').
inf('snow-in-summer', 'snows-in-summer').
inf('Mexican flameleaf', 'Mexican flameleaves').
inf('painted leaf', 'painted leaves').
inf('paint leaf', 'paint leaves').
inf('fire-on-the-mountain', 'fires-on-the-mountain').
inf('painted leaf', 'painted leaves').
inf('crown of thorns', 'crowns of thorns').
inf('three-seeded mercury', 'three-seeded mercuries').
inf('herb mercury', 'herb mercuries').
inf('herbs mercury', 'herbs mercuries').
inf('dog\'s mercury', 'dog\'s mercuries').
inf('dog mercury', 'dog mercuries').
inf('tread-softly', 'tread-softlies').
inf('Hevea brasiliensis', 'Hevea brasilienses').
inf('Manihot dulcis', 'Manihot dulces').
inf('Jewbush', 'Jewbushes').
inf('Jew-bush', 'Jew-bushes').
inf('Jew bush', 'Jew bushes').
inf('tea family', 'tea families').
inf('Camellia sinensis', 'Camellia sinenses').
inf('carrot family', 'carrot families').
inf('wild celery', 'wild celeries').
inf('celery', 'celeries').
inf('cultivated celery', 'cultivated celeries').
inf('knob celery', 'knob celeries').
inf('root celery', 'root celeries').
inf('turnip-rooted celery', 'turnip-rooted celeries').
inf('cilantro', 'cilantroes').
inf('eryngo', 'eryngoes').
inf('eringo', 'eringoes').
inf('sea holly', 'sea hollies').
inf('sweet cicely', 'sweet cicelies').
inf('footsteps-of-spring', 'footstepss-of-spring').
inf('dogwood family', 'dogwood families').
inf('redbrush', 'redbrushes').
inf('bunchberry', 'bunchberries').
inf('crackerberry', 'crackerberries').
inf('pudding berry', 'pudding berries').
inf('Cornus canadensis', 'Cornus canadenses').
inf('cornelian cherry', 'cornelian cherries').
inf('valerian family', 'valerian families').
inf('cutch', 'cutches').
inf('kutch', 'kutches').
inf('Prince-of-Wales fern', 'Princes-of-Wales fern').
inf('Prince-of-Wales feather', 'Princes-of-Wales feather').
inf('Prince-of-Wales plume', 'Princes-of-Wales plume').
inf('curly grass', 'curly grasses').
inf('nardo', 'nardoes').
inf('floating-moss', 'floating-mosses').
inf('Rhizopogon idahoensis', 'Rhizopogon idahoenses').
inf('genus Rhizopus', 'genus Rhizopera').
inf('rhizopus', 'rhizopera').
inf('Saprolegnia ferax', 'Saprolegnia feraxes').
inf('Albugo', 'Albugoes').
inf('genus Albugo', 'genus Albugoes').
inf('damping off fungus', 'dampings off fungus').
inf('beard moss', 'beard mosses').
inf('reindeer moss', 'reindeer mosses').
inf('arctic moss', 'arctic mosses').
inf('Iceland moss', 'Iceland mosses').
inf('Agaricus arvensis', 'Agaricus arvenses').
inf('Lactarius delicioso', 'Lactarius deliciosoes').
inf('nameko', 'namekoes').
inf('Pholiota nameko', 'Pholiota namekoes').
inf('man-on-a-horse', 'men-on-a-horse').
inf('hen-of-the-woods', 'hens-of-the-woods').
inf('hen of the woods', 'hens of the woods').
inf('Strobilomyces floccopus', 'Strobilomyces floccopera').
inf('Ustilago', 'Ustilagoes').
inf('genus Ustilago', 'genus Ustilagoes').
inf('Hygrophorus tennesseensis', 'Hygrophorus tennesseenses').
inf('plant process', 'plant processes').
inf('apophysis', 'apophyses').
inf('fruiting body', 'fruiting bodies').
inf('paraphysis', 'paraphyses').
inf('flesh', 'fleshes').
inf('bush', 'bushes').
inf('burning bush', 'burning bushes').
inf('suffrutex', 'suffrutexes').
inf('axis', 'axes').
inf('caudex', 'caudexes').
inf('caudex', 'caudexes').
inf('spadix', 'spadixes').
inf('berry', 'berries').
inf('pyxis', 'pyxes').
inf('buckthorn family', 'buckthorn families').
inf('buckthorn berry', 'buckthorn berries').
inf('yellow berry', 'yellow berries').
inf('bearberry', 'bearberries').
inf('indian cherry', 'indian cherries').
inf('coffeeberry', 'coffeeberries').
inf('redberry', 'redberries').
inf('red-berry', 'red-berries').
inf('jujube bush', 'jujube bushes').
inf('grapevine family', 'grapevine families').
inf('Vitis', 'Vitis').
inf('genus Vitis', 'genus Vitis').
inf('Boston ivy', 'Boston ivies').
inf('Japanese ivy', 'Japanese ivies').
inf('American ivy', 'American ivies').
inf('pepper family', 'pepper families').
inf('lizard\'s-tail family', 'lizard\'s-tail families').
inf('swamp lily', 'swamp lilies').
inf('Anemopsis', 'Anemopses').
inf('genus Anemopsis', 'genus Anemopses').
inf('leaf', 'leaves').
inf('amplexicaul leaf', 'amplexicaul leaves').
inf('greenery', 'greeneries').
inf('scale leaf', 'scale leaves').
inf('fig leaf', 'fig leaves').
inf('simple leaf', 'simple leaves').
inf('compound leaf', 'compound leaves').
inf('trifoliolate leaf', 'trifoliolate leaves').
inf('quinquefoliate leaf', 'quinquefoliate leaves').
inf('palmate leaf', 'palmate leaves').
inf('pinnate leaf', 'pinnate leaves').
inf('bijugate leaf', 'bijugate leaves').
inf('bijugous leaf', 'bijugous leaves').
inf('decompound leaf', 'decompound leaves').
inf('acerate leaf', 'acerate leaves').
inf('acuminate leaf', 'acuminate leaves').
inf('cordate leaf', 'cordate leaves').
inf('cuneate leaf', 'cuneate leaves').
inf('deltoid leaf', 'deltoid leaves').
inf('elliptic leaf', 'elliptic leaves').
inf('ensiform leaf', 'ensiform leaves').
inf('hastate leaf', 'hastate leaves').
inf('lanceolate leaf', 'lanceolate leaves').
inf('linear leaf', 'linear leaves').
inf('elongate leaf', 'elongate leaves').
inf('lyrate leaf', 'lyrate leaves').
inf('obtuse leaf', 'obtuse leaves').
inf('oblanceolate leaf', 'oblanceolate leaves').
inf('oblong leaf', 'oblong leaves').
inf('obovate leaf', 'obovate leaves').
inf('ovate leaf', 'ovate leaves').
inf('orbiculate leaf', 'orbiculate leaves').
inf('pandurate leaf', 'pandurate leaves').
inf('panduriform leaf', 'panduriform leaves').
inf('peltate leaf', 'peltate leaves').
inf('perfoliate leaf', 'perfoliate leaves').
inf('reniform leaf', 'reniform leaves').
inf('sagittate-leaf', 'sagittate-leaves').
inf('sagittiform leaf', 'sagittiform leaves').
inf('spatulate leaf', 'spatulate leaves').
inf('bipinnate leaf', 'bipinnate leaves').
inf('even-pinnate leaf', 'even-pinnate leaves').
inf('abruptly-pinnate leaf', 'abruptly-pinnate leaves').
inf('odd-pinnate leaf', 'odd-pinnate leaves').
inf('pedate leaf', 'pedate leaves').
inf('entire leaf', 'entire leaves').
inf('crenate leaf', 'crenate leaves').
inf('serrate leaf', 'serrate leaves').
inf('dentate leaf', 'dentate leaves').
inf('denticulate leaf', 'denticulate leaves').
inf('emarginate leaf', 'emarginate leaves').
inf('erose leaf', 'erose leaves').
inf('runcinate leaf', 'runcinate leaves').
inf('lobed leaf', 'lobed leaves').
inf('parallel-veined leaf', 'parallel-veined leaves').
inf('parted leaf', 'parted leaves').
inf('prickly-edged leaf', 'prickly-edged leaves').
inf('branch', 'branches').
inf('tree branch', 'tree branches').
inf('withy', 'withies').
inf('fern family', 'fern families').
inf('fern genus', 'fern genera').
inf('polypody', 'polypodies').
inf('grey polypody', 'grey polypodies').
inf('gray polypody', 'gray polypodies').
inf('leatherleaf', 'leatherleaves').
inf('leathery polypody', 'leathery polypodies').
inf('coast polypody', 'coast polypodies').
inf('rock polypody', 'rock polypodies').
inf('common polypody', 'common polypodies').
inf('golden polypody', 'golden polypodies').
inf('snake polypody', 'snake polypodies').
inf('golden polypody', 'golden polypodies').
inf('walking leaf', 'walking leaves').
inf('Ceterach', 'Ceteraches').
inf('genus Ceterach', 'genus Ceteraches').
inf('Asplenium ceterach', 'Asplenium ceteraches').
inf('Phyllitis', 'Phyllitis').
inf('genus Phyllitis', 'genus Phyllitis').
inf('Davallia canariensis', 'Davallia canarienses').
inf('Australian hare\'s foot', 'Australian hare\'s feet').
inf('Lastreopsis', 'Lastreopses').
inf('genus Lastreopsis', 'genus Lastreopses').
inf('Polystichum lonchitis', 'Polystichum lonchitis').
inf('Woodsia ilvensis', 'Woodsia ilvenses').
inf('Bolbitis', 'Bolbitis').
inf('genus Bolbitis', 'genus Bolbitis').
inf('Nephrolepis exaltata bostoniensis', 'Nephrolepis exaltata bostonienses').
inf('Cheilanthes alabamensis', 'Cheilanthes alabamenses').
inf('Pellaea ornithopus', 'Pellaea ornithopera').
inf('horsetail family', 'horsetail families').
inf('scouring rush', 'scouring rushes').
inf('variegated scouring rush', 'variegated scouring rushes').
inf('club moss', 'club mosses').
inf('club-moss', 'club-mosses').
inf('clubmoss family', 'clubmoss families').
inf('shining clubmoss', 'shining clubmosses').
inf('alpine clubmoss', 'alpine clubmosses').
inf('fir clubmoss', 'fir clubmosses').
inf('mountain clubmoss', 'mountain clubmosses').
inf('little clubmoss', 'little clubmosses').
inf('Lycopodium selago', 'Lycopodium selagoes').
inf('staghorn moss', 'staghorn mosses').
inf('tree clubmoss', 'tree clubmosses').
inf('foxtail grass', 'foxtail grasses').
inf('spikemoss', 'spikemosses').
inf('spike moss', 'spike mosses').
inf('little club moss', 'little club mosses').
inf('meadow spikemoss', 'meadow spikemosses').
inf('basket spikemoss', 'basket spikemosses').
inf('rock spikemoss', 'rock spikemosses').
inf('rose of Jericho', 'roses of Jericho').
inf('quillwort family', 'quillwort families').
inf('Parathelypteris novae-boracensis', 'Parathelypteris novae-boracenses').
inf('Dryopteris noveboracensis', 'Dryopteris noveboracenses').
inf('milkweed family', 'milkweed families').
inf('Indian paintbrush', 'Indian paintbrushes').
inf('tree of knowledge', 'trees of knowledge').
inf('community', 'communities').
inf('severalty', 'severalties').
inf('proprietary', 'proprietaries').
inf('property', 'properties').
inf('intellectual property', 'intellectual properties').
inf('community property', 'community properties').
inf('personal property', 'personal properties').
inf('personalty', 'personalties').
inf('private property', 'private properties').
inf('real property', 'real properties').
inf('realty', 'realties').
inf('spiritualty', 'spiritualties').
inf('spirituality', 'spiritualities').
inf('church property', 'church properties').
inf('temporalty', 'temporalties').
inf('temporality', 'temporalities').
inf('public property', 'public properties').
inf('money', 'monies').
inf('big money', 'big monies').
inf('estate for life', 'estates for life').
inf('barony', 'baronies').
inf('seigneury', 'seigneuries').
inf('seigniory', 'seigniories').
inf('signory', 'signories').
inf('orangery', 'orangeries').
inf('transferred property', 'transferred properties').
inf('easy money', 'easy monies').
inf('easy money', 'easy monies').
inf('tight money', 'tight monies').
inf('gross', 'grosses').
inf('royalty', 'royalties').
inf('paper loss', 'paper losses').
inf('margin of profit', 'margins of profit').
inf('stolen property', 'stolen properties').
inf('booty', 'booties').
inf('dirty money', 'dirty monies').
inf('borough English', 'borough Englishes').
inf('legacy', 'legacies').
inf('patrimony', 'patrimonies').
inf('free lunch', 'free lunches').
inf('dowry', 'dowries').
inf('dowery', 'doweries').
inf('largess', 'largesses').
inf('subsidy', 'subsidies').
inf('grant-in-aid', 'grants-in-aid').
inf('gratuity', 'gratuities').
inf('prize money', 'prize monies').
inf('offertory', 'offertories').
inf('hearth money', 'hearth monies').
inf('soft money', 'soft monies').
inf('patrimony', 'patrimonies').
inf('chantry', 'chantries').
inf('bounty', 'bounties').
inf('cost-of-living allowance', 'costs-of-living allowance').
inf('outgo', 'outgoes').
inf('salary', 'salaries').
inf('rate of pay', 'rates of pay').
inf('time and a half', 'time and a halves').
inf('rate of payment', 'rates of payment').
inf('blood money', 'blood monies').
inf('conscience money', 'conscience monies').
inf('palimony', 'palimonies').
inf('alimony', 'alimonies').
inf('ransom money', 'ransom monies').
inf('blood money', 'blood monies').
inf('hush money', 'hush monies').
inf('vigorish', 'vigorishes').
inf('blood money', 'blood monies').
inf('indemnity', 'indemnities').
inf('redress', 'redresses').
inf('smart money', 'smart monies').
inf('annuity', 'annuities').
inf('annuity in advance', 'annuities in advance').
inf('ordinary annuity', 'ordinary annuities').
inf('reversionary annuity', 'reversionary annuities').
inf('survivorship annuity', 'survivorship annuities').
inf('cost-of-living benefit', 'costs-of-living benefit').
inf('gratuity', 'gratuities').
inf('baksheesh', 'baksheeshes').
inf('bakshish', 'bakshishes').
inf('backsheesh', 'backsheeshes').
inf('Christmas box', 'Christmas boxes').
inf('delinquency', 'delinquencies').
inf('penalty', 'penalties').
inf('cost of living', 'costs of living').
inf('highway robbery', 'highway robberies').
inf('cost of capital', 'costs of capital').
inf('levy', 'levies').
inf('tax', 'taxes').
inf('tax liability', 'tax liabilities').
inf('single tax', 'single taxes').
inf('income tax', 'income taxes').
inf('estimated tax', 'estimated taxes').
inf('withholding tax', 'withholding taxes').
inf('capital loss', 'capital losses').
inf('capital gains tax', 'capital gains taxes').
inf('capital levy', 'capital levies').
inf('departure tax', 'departure taxes').
inf('property tax', 'property taxes').
inf('land tax', 'land taxes').
inf('council tax', 'council taxes').
inf('franchise tax', 'franchise taxes').
inf('gift tax', 'gift taxes').
inf('inheritance tax', 'inheritance taxes').
inf('estate tax', 'estate taxes').
inf('death tax', 'death taxes').
inf('death duty', 'death duties').
inf('direct tax', 'direct taxes').
inf('indirect tax', 'indirect taxes').
inf('hidden tax', 'hidden taxes').
inf('poll tax', 'poll taxes').
inf('progressive tax', 'progressive taxes').
inf('graduated tax', 'graduated taxes').
inf('proportional tax', 'proportional taxes').
inf('degressive tax', 'degressive taxes').
inf('stamp tax', 'stamp taxes').
inf('stamp duty', 'stamp duties').
inf('surtax', 'surtaxes').
inf('supertax', 'supertaxes').
inf('transfer tax', 'transfer taxes').
inf('duty', 'duties').
inf('excise tax', 'excise taxes').
inf('sales tax', 'sales taxes').
inf('nuisance tax', 'nuisance taxes').
inf('value-added tax', 'value-added taxes').
inf('ad valorem tax', 'ad valorem taxes').
inf('gasoline tax', 'gasoline taxes').
inf('customs duty', 'customs duties').
inf('ship money', 'ship monies').
inf('tonnage duty', 'tonnage duties').
inf('anti-dumping duty', 'anti-dumping duties').
inf('import duty', 'import duties').
inf('export duty', 'export duties').
inf('countervailing duty', 'countervailing duties').
inf('rate of interest', 'rates of interest').
inf('usury', 'usuries').
inf('vigorish', 'vigorishes').
inf('drop-off charge', 'drops-off charge').
inf('price of admission', 'prices of admission').
inf('entrance money', 'entrance monies').
inf('license tax', 'license taxes').
inf('rate of depreciation', 'rates of depreciation').
inf('rate of exchange', 'rates of exchange').
inf('loss', 'losses').
inf('loss', 'losses').
inf('financial loss', 'financial losses').
inf('sum of money', 'sums of money').
inf('amount of money', 'amounts of money').
inf('equity', 'equities').
inf('sweat equity', 'sweat equities').
inf('equity', 'equities').
inf('stock of record', 'stocks of record').
inf('government security', 'government securities').
inf('agency security', 'agency securities').
inf('mortgage-backed security', 'mortgage-backed securities').
inf('registered security', 'registered securities').
inf('smart money', 'smart monies').
inf('kitty', 'kitties').
inf('security', 'securities').
inf('security', 'securities').
inf('surety', 'sureties').
inf('earnest money', 'earnest monies').
inf('guaranty', 'guaranties').
inf('corpus', 'corpora').
inf('seed money', 'seed monies').
inf('cash in hand', 'cashes in hand').
inf('treasury', 'treasuries').
inf('money supply', 'money supplies').
inf('public treasury', 'public treasuries').
inf('bursary', 'bursaries').
inf('subtreasury', 'subtreasuries').
inf('certificate of deposit', 'certificates of deposit').
inf('stash', 'stashes').
inf('kitty', 'kitties').
inf('ruby', 'rubies').
inf('medium of exchange', 'media of exchange').
inf('bill of exchange', 'bills of exchange').
inf('order of payment', 'orders of payment').
inf('cheap money', 'cheap monies').
inf('line of credit', 'lines of credit').
inf('letter of credit', 'letters of credit').
inf('giro', 'giroes').
inf('money', 'monies').
inf('money', 'monies').
inf('dinero', 'dineroes').
inf('lolly', 'lollies').
inf('pelf', 'pelves').
inf('scratch', 'scratches').
inf('token money', 'token monies').
inf('currency', 'currencies').
inf('fractional currency', 'fractional currencies').
inf('cash', 'cashes').
inf('cash', 'cashes').
inf('hard cash', 'hard cashes').
inf('hard currency', 'hard currencies').
inf('hard currency', 'hard currencies').
inf('paper money', 'paper monies').
inf('folding money', 'folding monies').
inf('paper currency', 'paper currencies').
inf('metal money', 'metal monies').
inf('piece of eight', 'pieces of eight').
inf('halfpenny', 'halfpennies').
inf('ha\'penny', 'ha\'pennies').
inf('penny', 'pennies').
inf('new penny', 'new pennies').
inf('Maundy money', 'Maundy monies').
inf('fiat money', 'fiat monies').
inf('fifty', 'fifties').
inf('twenty', 'twenties').
inf('limited liability', 'limited liabilities').
inf('certificate of indebtedness', 'certificates of indebtedness').
inf('note of hand', 'notes of hand').
inf('book of account', 'books of account').
inf('entry', 'entries').
inf('accounting entry', 'accounting entries').
inf('ledger entry', 'ledger entries').
inf('adjusting entry', 'adjusting entries').
inf('credit entry', 'credit entries').
inf('debit entry', 'debit entries').
inf('method of accounting', 'methods of accounting').
inf('accrual basis', 'accrual bases').
inf('cash basis', 'cash bases').
inf('pooling of interest', 'poolings of interest').
inf('profit and loss', 'profit and losses').
inf('divvy', 'divvies').
inf('balance of trade', 'balances of trade').
inf('balance of payments', 'balances of payments').
inf('balance of international payments', 'balances of international payments').
inf('inventory', 'inventories').
inf('mess of pottage', 'messes of pottage').
inf('security', 'securities').
inf('tax-exempt security', 'tax-exempt securities').
inf('zero-coupon security', 'zero-coupon securities').
inf('zero coupon security', 'zero coupon securities').
inf('convertible security', 'convertible securities').
inf('letter security', 'letter securities').
inf('listed security', 'listed securities').
inf('unlisted security', 'unlisted securities').
inf('over the counter security', 'over the counter securities').
inf('OTC security', 'OTC securities').
inf('petty cash', 'petty cashes').
inf('pocket money', 'pocket monies').
inf('pin money', 'pin monies').
inf('spending money', 'spending monies').
inf('ready cash', 'ready cashes').
inf('cold cash', 'cold cashes').
inf('ready money', 'ready monies').
inf('Acheson process', 'Acheson processes').
inf('adiabatic process', 'adiabatic processes').
inf('agenesis', 'ageneses').
inf('agglutinating activity', 'agglutinating activities').
inf('amelogenesis', 'amelogeneses').
inf('amitosis', 'amitoses').
inf('amylolysis', 'amylolyses').
inf('anaglyphy', 'anaglyphies').
inf('anamorphosis', 'anamorphoses').
inf('anastalsis', 'anastalses').
inf('androgenesis', 'androgeneses').
inf('androgeny', 'androgenies').
inf('angiogenesis', 'angiogeneses').
inf('anisogamy', 'anisogamies').
inf('anthropogenesis', 'anthropogeneses').
inf('anthropogeny', 'anthropogenies').
inf('antisepsis', 'antisepses').
inf('asepsis', 'asepses').
inf('aphaeresis', 'aphaereses').
inf('apheresis', 'aphereses').
inf('aphesis', 'apheses').
inf('apogamy', 'apogamies').
inf('apomixis', 'apomixes').
inf('agamogenesis', 'agamogeneses').
inf('autocatalysis', 'autocatalyses').
inf('autolysis', 'autolyses').
inf('autoradiography', 'autoradiographies').
inf('autotypy', 'autotypies').
inf('auxesis', 'auxeses').
inf('bacteriolysis', 'bacteriolyses').
inf('bacteriostasis', 'bacteriostases').
inf('Bessemer process', 'Bessemer processes').
inf('biosynthesis', 'biosyntheses').
inf('biogenesis', 'biogeneses').
inf('blastogenesis', 'blastogeneses').
inf('anthesis', 'antheses').
inf('bodily process', 'bodily processes').
inf('body process', 'body processes').
inf('activity', 'activities').
inf('catabiosis', 'catabioses').
inf('catalysis', 'catalyses').
inf('cavity', 'cavities').
inf('cenogenesis', 'cenogeneses').
inf('kenogenesis', 'kenogeneses').
inf('caenogenesis', 'caenogeneses').
inf('cainogenesis', 'cainogeneses').
inf('kainogenesis', 'kainogeneses').
inf('chemical process', 'chemical processes').
inf('chemosynthesis', 'chemosyntheses').
inf('chromatography', 'chromatographies').
inf('column chromatography', 'column chromatographies').
inf('use of goods and services', 'uses of goods and services').
inf('recovery', 'recoveries').
inf('cyanide process', 'cyanide processes').
inf('cytogenesis', 'cytogeneses').
inf('cytogeny', 'cytogenies').
inf('cytolysis', 'cytolyses').
inf('decoction process', 'decoction processes').
inf('eponymy', 'eponymies').
inf('decline in quality', 'declines in quality').
inf('diakinesis', 'diakineses').
inf('digital photography', 'digital photographies').
inf('metathesis', 'metatheses').
inf('dry plate process', 'dry plate processes').
inf('ecchymosis', 'ecchymoses').
inf('economic process', 'economic processes').
inf('electrolysis', 'electrolyses').
inf('electrophoresis', 'electrophoreses').
inf('cataphoresis', 'cataphoreses').
inf('dielectrolysis', 'dielectrolyses').
inf('ionophoresis', 'ionophoreses').
inf('ellipsis', 'ellipses').
inf('eclipsis', 'eclipses').
inf('enuresis', 'enureses').
inf('epigenesis', 'epigeneses').
inf('epitaxy', 'epitaxies').
inf('erythropoiesis', 'erythropoieses').
inf('ecesis', 'eceses').
inf('phylogeny', 'phylogenies').
inf('phylogenesis', 'phylogeneses').
inf('fibrinolysis', 'fibrinolyses').
inf('fissiparity', 'fissiparities').
inf('fractional process', 'fractional processes').
inf('galactosis', 'galactoses').
inf('gametogenesis', 'gametogeneses').
inf('geological process', 'geological processes').
inf('geologic process', 'geologic processes').
inf('glycogenesis', 'glycogeneses').
inf('glycolysis', 'glycolyses').
inf('ontogeny', 'ontogenies').
inf('ontogenesis', 'ontogeneses').
inf('gynogenesis', 'gynogeneses').
inf('Haber process', 'Haber processes').
inf('Haber-Bosch process', 'Haber-Bosch processes').
inf('hatch', 'hatches').
inf('hematopoiesis', 'hematopoieses').
inf('haematopoiesis', 'haematopoieses').
inf('hemopoiesis', 'hemopoieses').
inf('haemopoiesis', 'haemopoieses').
inf('hemogenesis', 'hemogeneses').
inf('haemogenesis', 'haemogeneses').
inf('hematogenesis', 'hematogeneses').
inf('haematogenesis', 'haematogeneses').
inf('hemimetamorphosis', 'hemimetamorphoses').
inf('hemimetaboly', 'hemimetabolies').
inf('heterometaboly', 'heterometabolies').
inf('hemolysis', 'hemolyses').
inf('haemolysis', 'haemolyses').
inf('hematolysis', 'hematolyses').
inf('haematolysis', 'haematolyses').
inf('heredity', 'heredities').
inf('heterospory', 'heterospories').
inf('holometaboly', 'holometabolies').
inf('homospory', 'homospories').
inf('human process', 'human processes').
inf('hydrolysis', 'hydrolyses').
inf('hyperhidrosis', 'hyperhidroses').
inf('hyperidrosis', 'hyperidroses').
inf('polyhidrosis', 'polyhidroses').
inf('hypostasis', 'hypostases').
inf('hypostasis', 'hypostases').
inf('epistasis', 'epistases').
inf('immunoelectrophoresis', 'immunoelectrophoreses').
inf('incontinency', 'incontinencies').
inf('industrial process', 'industrial processes').
inf('influx', 'influxes').
inf('inrush', 'inrushes').
inf('intumescency', 'intumescencies').
inf('irreversible process', 'irreversible processes').
inf('isogamy', 'isogamies').
inf('cytokinesis', 'cytokineses').
inf('karyokinesis', 'karyokineses').
inf('karyolysis', 'karyolyses').
inf('leach', 'leaches').
inf('linguistic process', 'linguistic processes').
inf('lithuresis', 'lithureses').
inf('loss', 'losses').
inf('lymphopoiesis', 'lymphopoieses').
inf('lysis', 'lyses').
inf('lysis', 'lyses').
inf('Markov process', 'Markov processes').
inf('Markoff process', 'Markoff processes').
inf('meiosis', 'meioses').
inf('miosis', 'mioses').
inf('metabolic process', 'metabolic processes').
inf('metamorphosis', 'metamorphoses').
inf('metastasis', 'metastases').
inf('metathesis', 'metatheses').
inf('mitosis', 'mitoses').
inf('ecdysis', 'ecdyses').
inf('monogenesis', 'monogeneses').
inf('morphallaxis', 'morphallaxes').
inf('morphogenesis', 'morphogeneses').
inf('natural process', 'natural processes').
inf('activity', 'activities').
inf('necrolysis', 'necrolyses').
inf('neurogenesis', 'neurogeneses').
inf('nucleosynthesis', 'nucleosyntheses').
inf('oogenesis', 'oogeneses').
inf('open-hearth process', 'open-hearth processes').
inf('organic process', 'organic processes').
inf('biological process', 'biological processes').
inf('orogeny', 'orogenies').
inf('osmosis', 'osmoses').
inf('reverse osmosis', 'reverse osmoses').
inf('osteolysis', 'osteolyses').
inf('efflux', 'effluxes').
inf('overactivity', 'overactivities').
inf('redox', 'redoxes').
inf('palingenesis', 'palingeneses').
inf('paper chromatography', 'paper chromatographies').
inf('paper electrophoresis', 'paper electrophoreses').
inf('carrier electrophoresis', 'carrier electrophoreses').
inf('parthenocarpy', 'parthenocarpies').
inf('parthenogenesis', 'parthenogeneses').
inf('parthenogeny', 'parthenogenies').
inf('parthenogenesis', 'parthenogeneses').
inf('parthenogeny', 'parthenogenies').
inf('pathogenesis', 'pathogeneses').
inf('pathologic process', 'pathologic processes').
inf('pathological process', 'pathological processes').
inf('peace process', 'peace processes').
inf('piss', 'pisses').
inf('peristalsis', 'peristalses').
inf('diaphoresis', 'diaphoreses').
inf('hidrosis', 'hidroses').
inf('phagocytosis', 'phagocytoses').
inf('phase of cell division', 'phases of cell division').
inf('photography', 'photographies').
inf('photosynthesis', 'photosyntheses').
inf('pinocytosis', 'pinocytoses').
inf('roughness', 'roughnesses').
inf('powder photography', 'powder photographies').
inf('precession of the equinoxes', 'precessions of the equinoxes').
inf('proteolysis', 'proteolyses').
inf('psilosis', 'psiloses').
inf('psychoanalytic process', 'psychoanalytic processes').
inf('psychogenesis', 'psychogeneses').
inf('psychogenesis', 'psychogeneses').
inf('pyrochemical process', 'pyrochemical processes').
inf('pyrochemistry', 'pyrochemistries').
inf('radioactivity', 'radioactivities').
inf('radiography', 'radiographies').
inf('skiagraphy', 'skiagraphies').
inf('radiolysis', 'radiolyses').
inf('rain-wash', 'rain-washes').
inf('rally', 'rallies').
inf('reversible process', 'reversible processes').
inf('schizogony', 'schizogonies').
inf('search', 'searches').
inf('amphimixis', 'amphimixes').
inf('social process', 'social processes').
inf('Solvay process', 'Solvay processes').
inf('spermatogenesis', 'spermatogeneses').
inf('stationary stochastic process', 'stationary stochastic processes').
inf('stochastic process', 'stochastic processes').
inf('supply', 'supplies').
inf('survival of the fittest', 'survivals of the fittest').
inf('symphysis', 'symphyses').
inf('synapsis', 'synapses').
inf('syneresis', 'synereses').
inf('synaeresis', 'synaereses').
inf('syneresis', 'synereses').
inf('synaeresis', 'synaereses').
inf('synergy', 'synergies').
inf('synizesis', 'synizeses').
inf('synezesis', 'synezeses').
inf('synthesis', 'syntheses').
inf('odontiasis', 'odontiases').
inf('teratogenesis', 'teratogeneses').
inf('thrombolysis', 'thrombolyses').
inf('wash', 'washes').
inf('zymosis', 'zymoses').
inf('zymosis', 'zymoses').
inf('zymolysis', 'zymolyses').
inf('fundamental quantity', 'fundamental quantities').
inf('definite quantity', 'definite quantities').
inf('indefinite quantity', 'indefinite quantities').
inf('relative quantity', 'relative quantities').
inf('system of measurement', 'systems of measurement').
inf('system of weights and measures', 'systems of weights and measures').
inf('utility', 'utilities').
inf('marginal utility', 'marginal utilities').
inf('sufficiency', 'sufficiencies').
inf('normality', 'normalities').
inf('majority', 'majorities').
inf('absolute majority', 'absolute majorities').
inf('plurality', 'pluralities').
inf('relative majority', 'relative majorities').
inf('chlorinity', 'chlorinities').
inf('solubility', 'solubilities').
inf('toxicity', 'toxicities').
inf('cytotoxicity', 'cytotoxicities').
inf('unit of measurement', 'units of measurement').
inf('coefficient of absorption', 'coefficients of absorption').
inf('coefficient of drag', 'coefficients of drag').
inf('coefficient of friction', 'coefficients of friction').
inf('coefficient of mutual induction', 'coefficients of mutual induction').
inf('coefficient of self induction', 'coefficients of self induction').
inf('coefficient of elasticity', 'coefficients of elasticity').
inf('modulus of elasticity', 'moduluss of elasticity').
inf('modulus of rigidity', 'moduluss of rigidity').
inf('coefficient of expansion', 'coefficients of expansion').
inf('expansivity', 'expansivities').
inf('coefficient of reflection', 'coefficients of reflection').
inf('reflectivity', 'reflectivities').
inf('coefficient of viscosity', 'coefficients of viscosity').
inf('absolute viscosity', 'absolute viscosities').
inf('dynamic viscosity', 'dynamic viscosities').
inf('constant of gravitation', 'constants of gravitation').
inf('cardinality', 'cardinalities').
inf('factor of proportionality', 'factors of proportionality').
inf('constant of proportionality', 'constants of proportionality').
inf('prime quantity', 'prime quantities').
inf('match', 'matches').
inf('radix', 'radixes').
inf('frequency', 'frequencies').
inf('absolute frequency', 'absolute frequencies').
inf('googolplex', 'googolplexes').
inf('magnetic flux', 'magnetic fluxes').
inf('cubic inch', 'cubic inches').
inf('cubic foot', 'cubic feet').
inf('unit of viscosity', 'units of viscosity').
inf('minute of arc', 'minutes of arc').
inf('square inch', 'square inches').
inf('square foot', 'square feet').
inf('perch', 'perches').
inf('field capacity', 'field capacities').
inf('acre-foot', 'acre-feet').
inf('acre inch', 'acre inches').
inf('board foot', 'board feet').
inf('parity', 'parities').
inf('mho', 'mhoes').
inf('Mx', 'Mxes').
inf('microgauss', 'microgausses').
inf('gauss', 'gausses').
inf('abhenry', 'abhenries').
inf('millihenry', 'millihenries').
inf('henry', 'henries').
inf('lux', 'luxes').
inf('lx', 'lxes').
inf('inch', 'inches').
inf('foot', 'feet').
inf('perch', 'perches').
inf('coss', 'cosses').
inf('mesh', 'meshes').
inf('euro', 'euroes').
inf('centesimo', 'centesimoes').
inf('centimo', 'centimoes').
inf('centavo', 'centavoes').
inf('Cape Verde escudo', 'Cape Verde escudoes').
inf('escudo', 'escudoes').
inf('Portuguese escudo', 'Portuguese escudoes').
inf('escudo', 'escudoes').
inf('conto', 'contoes').
inf('pengo', 'pengoes').
inf('kobo', 'koboes').
inf('avo', 'avoes').
inf('boliviano', 'bolivianoes').
inf('Chilean peso', 'Chilean pesoes').
inf('peso', 'pesoes').
inf('Colombian peso', 'Colombian pesoes').
inf('peso', 'pesoes').
inf('Cuban peso', 'Cuban pesoes').
inf('peso', 'pesoes').
inf('Dominican peso', 'Dominican pesoes').
inf('peso', 'pesoes').
inf('Guinea-Bissau peso', 'Guinea-Bissau pesoes').
inf('peso', 'pesoes').
inf('Mexican peso', 'Mexican pesoes').
inf('peso', 'pesoes').
inf('Philippine peso', 'Philippine pesoes').
inf('peso', 'pesoes').
inf('Uruguayan peso', 'Uruguayan pesoes').
inf('peso', 'pesoes').
inf('penny', 'pennies').
inf('qurush', 'qurushes').
inf('mongo', 'mongoes').
inf('zloty', 'zloties').
inf('millimeter of mercury', 'millimeters of mercury').
inf('pounds per square inch', 'pounds per square inches').
inf('cicero', 'ciceroes').
inf('column inch', 'column inches').
inf('inch', 'inches').
inf('system of weights', 'systems of weights').
inf('avoirdupois', 'avoirdupois').
inf('catty', 'catties').
inf('kilo', 'kiloes').
inf('Board of Trade unit', 'Boards of Trade unit').
inf('complex quantity', 'complex quantities').
inf('imaginary', 'imaginaries').
inf('one-half', 'one-halves').
inf('half', 'halves').
inf('moiety', 'moieties').
inf('mediety', 'medieties').
inf('nix', 'nixes').
inf('zero', 'zeroes').
inf('zilch', 'zilches').
inf('zippo', 'zippoes').
inf('zero', 'zeroes').
inf('I', 'we').
inf('unity', 'unities').
inf('two', 'twoes').
inf('distich', 'distiches').
inf('leash', 'leashes').
inf('trinity', 'trinities').
inf('ternary', 'ternaries').
inf('terzetto', 'terzettoes').
inf('quaternary', 'quaternaries').
inf('quaternity', 'quaternities').
inf('six', 'sixes').
inf('septenary', 'septenaries').
inf('eighter from Decatur', 'eighters from Decatur').
inf('octonary', 'octonaries').
inf('Nina from Carolina', 'Ninas from Carolina').
inf('twenty', 'twenties').
inf('twenty-two', 'twenty-twoes').
inf('twenty-six', 'twenty-sixes').
inf('thirty', 'thirties').
inf('forty', 'forties').
inf('fifty', 'fifties').
inf('sixty', 'sixties').
inf('seventy', 'seventies').
inf('eighty', 'eighties').
inf('ninety', 'nineties').
inf('century', 'centuries').
inf('gross', 'grosses').
inf('thou', 'ye').
inf('millenary', 'millenaries').
inf('great gross', 'great grosses').
inf('aleph-zero', 'aleph-zeroes').
inf('capacity', 'capacities').
inf('formatted capacity', 'formatted capacities').
inf('unformatted capacity', 'unformatted capacities').
inf('catch', 'catches').
inf('large indefinite quantity', 'large indefinite quantities').
inf('point of accumulation', 'points of accumulation').
inf('small indefinite quantity', 'small indefinite quantities').
inf('splash', 'splashes').
inf('box', 'boxes').
inf('dish', 'dishes').
inf('glass', 'glasses').
inf('diddly', 'diddlies').
inf('touch', 'touches').
inf('pinch', 'pinches').
inf('batch', 'batches').
inf('mass', 'masses').
inf('mess', 'messes').
inf('plenty', 'plenties').
inf('plurality', 'pluralities').
inf('billyo', 'billyoes').
inf('billy-ho', 'billy-hoes').
inf('much', 'muches').
inf('supply', 'supplies').
inf('capacity', 'capacities').
inf('vital capacity', 'vital capacities').
inf('causality', 'causalities').
inf('indebtedness', 'indebtednesses').
inf('contradictory', 'contradictories').
inf('contrary', 'contraries').
inf('isometry', 'isometries').
inf('identity', 'identities').
inf('parity', 'parities').
inf('evenness', 'evennesses').
inf('oddness', 'oddnesses').
inf('basis', 'bases').
inf('connectedness', 'connectednesses').
inf('unconnectedness', 'unconnectednesses').
inf('relevancy', 'relevancies').
inf('materiality', 'materialities').
inf('cogency', 'cogencies').
inf('germaneness', 'germanenesses').
inf('applicability', 'applicabilities').
inf('pertinency', 'pertinencies').
inf('relatedness', 'relatednesses').
inf('irrelevancy', 'irrelevancies').
inf('inapplicability', 'inapplicabilities').
inf('immateriality', 'immaterialities').
inf('unrelatedness', 'unrelatednesses').
inf('extraneousness', 'extraneousnesses').
inf('transitivity', 'transitivities').
inf('transitiveness', 'transitivenesses').
inf('intransitivity', 'intransitivities').
inf('intransitiveness', 'intransitivenesses').
inf('transitivity', 'transitivities').
inf('reflexivity', 'reflexivities').
inf('reflexiveness', 'reflexivenesses').
inf('reflexivity', 'reflexivities').
inf('reflexiveness', 'reflexivenesses').
inf('restrictiveness', 'restrictivenesses').
inf('modality', 'modalities').
inf('modality', 'modalities').
inf('hyponymy', 'hyponymies').
inf('hypernymy', 'hypernymies').
inf('synonymy', 'synonymies').
inf('synonymity', 'synonymities').
inf('synonymousness', 'synonymousnesses').
inf('antonymy', 'antonymies').
inf('holonymy', 'holonymies').
inf('whole to part relation', 'wholes to part relation').
inf('meronymy', 'meronymies').
inf('part to whole relation', 'parts to whole relation').
inf('troponymy', 'troponymies').
inf('homonymy', 'homonymies').
inf('basis', 'bases').
inf('affinity', 'affinities').
inf('sympathy', 'sympathies').
inf('affinity', 'affinities').
inf('line of descent', 'lines of descent').
inf('affinity', 'affinities').
inf('consanguinity', 'consanguinities').
inf('paternity', 'paternities').
inf('maternity', 'maternities').
inf('case-to-infection proportion', 'cases-to-infection proportion').
inf('case-to-infection ratio', 'cases-to-infection ratio').
inf('commensurateness', 'commensuratenesses').
inf('proportionateness', 'proportionatenesses').
inf('hospital occupancy', 'hospital occupancies').
inf('hotel occupancy', 'hotel occupancies').
inf('albedo', 'albedoes').
inf('cephalic index', 'cephalic indexes').
inf('breadth index', 'breadth indexes').
inf('cranial index', 'cranial indexes').
inf('efficiency', 'efficiencies').
inf('figure of merit', 'figures of merit').
inf('facial index', 'facial indexes').
inf('frequency', 'frequencies').
inf('relative frequency', 'relative frequencies').
inf('order of magnitude', 'orders of magnitude').
inf('output-to-input ratio', 'outputs-to-input ratio').
inf('price-to-earnings ratio', 'prices-to-earnings ratio').
inf('productivity', 'productivities').
inf('proportionality', 'proportionalities').
inf('refractive index', 'refractive indexes').
inf('index of refraction', 'indices of refraction').
inf('relative humidity', 'relative humidities').
inf('factor of safety', 'factors of safety').
inf('signal-to-noise ratio', 'signals-to-noise ratio').
inf('signal-to-noise', 'signals-to-noise').
inf('stoichiometry', 'stoichiometries').
inf('morbidity', 'morbidities').
inf('orthogonality', 'orthogonalities').
inf('perpendicularity', 'perpendicularities').
inf('north by east', 'norths by east').
inf('northeast by north', 'northeasts by north').
inf('northeast by east', 'northeasts by east').
inf('east by north', 'easts by north').
inf('east by south', 'easts by south').
inf('southeast by east', 'southeasts by east').
inf('southeast by south', 'southeasts by south').
inf('south by east', 'souths by east').
inf('south by west', 'souths by west').
inf('southwest by south', 'southwests by south').
inf('southwest by west', 'southwests by west').
inf('west by south', 'wests by south').
inf('west by north', 'wests by north').
inf('northwest by west', 'northwests by west').
inf('northwest by north', 'northwests by north').
inf('north by west', 'norths by west').
inf('chemistry', 'chemistries').
inf('interpersonal chemistry', 'interpersonal chemistries').
inf('alchemy', 'alchemies').
inf('reciprocality', 'reciprocalities').
inf('reciprocity', 'reciprocities').
inf('complementarity', 'complementarities').
inf('correlativity', 'correlativities').
inf('mutuality', 'mutualities').
inf('interdependency', 'interdependencies').
inf('symbiosis', 'symbioses').
inf('trophobiosis', 'trophobioses').
inf('mutuality', 'mutualities').
inf('mutualness', 'mutualnesses').
inf('interrelatedness', 'interrelatednesses').
inf('chronology', 'chronologies').
inf('synchrony', 'synchronies').
inf('synchronicity', 'synchronicities').
inf('synchroneity', 'synchroneities').
inf('asynchrony', 'asynchronies').
inf('scale of measurement', 'scales of measurement').
inf('index', 'indexes').
inf('standard of measurement', 'standards of measurement').
inf('oppositeness', 'oppositenesses').
inf('antithesis', 'antitheses').
inf('polarity', 'polarities').
inf('polarity', 'polarities').
inf('positivity', 'positivities').
inf('positiveness', 'positivenesses').
inf('negativity', 'negativities').
inf('negativeness', 'negativenesses').
inf('complementarity', 'complementarities').
inf('contradictoriness', 'contradictorinesses').
inf('incompatibility', 'incompatibilities').
inf('mutual exclusiveness', 'mutual exclusivenesses').
inf('inconsistency', 'inconsistencies').
inf('contrary', 'contraries').
inf('contrariety', 'contrarieties').
inf('contrary', 'contraries').
inf('convexity', 'convexities').
inf('entasis', 'entases').
inf('angularity', 'angularities').
inf('concavity', 'concavities').
inf('catenary', 'catenaries').
inf('recess', 'recesses').
inf('halo', 'haloes').
inf('helix', 'helixes').
inf('double helix', 'double helixes').
inf('eccentricity', 'eccentricities').
inf('element of a cone', 'elements of a cone').
inf('element of a cylinder', 'elements of a cylinder').
inf('vortex', 'vortexes').
inf('box', 'boxes').
inf('sinuosity', 'sinuosities').
inf('sinuousness', 'sinuousnesses').
inf('tortuosity', 'tortuosities').
inf('tortuousness', 'tortuousnesses').
inf('crookedness', 'crookednesses').
inf('arch', 'arches').
inf('true anomaly', 'true anomalies').
inf('angle of inclination', 'angles of inclination').
inf('inclination of an orbit', 'inclinations of an orbit').
inf('angle of incidence', 'angles of incidence').
inf('angle of attack', 'angles of attack').
inf('angle of reflection', 'angles of reflection').
inf('angle of refraction', 'angles of refraction').
inf('angle of extinction', 'angles of extinction').
inf('angle of dip', 'angles of dip').
inf('gibbosity', 'gibbosities').
inf('gibbousness', 'gibbousnesses').
inf('belly', 'bellies').
inf('symmetry', 'symmetries').
inf('ellipsoid of revolution', 'ellipsoids of revolution').
inf('boundary', 'boundaries').
inf('periphery', 'peripheries').
inf('outer boundary', 'outer boundaries').
inf('scratch', 'scratches').
inf('notch', 'notches').
inf('scotch', 'scotches').
inf('crow\'s foot', 'crow\'s feet').
inf('line of life', 'lines of life').
inf('line of heart', 'lines of heart').
inf('line of fate', 'lines of fate').
inf('line of destiny', 'lines of destiny').
inf('line of Saturn', 'lines of Saturn').
inf('cranny', 'crannies').
inf('shapelessness', 'shapelessnesses').
inf('vacancy', 'vacancies').
inf('emptiness', 'emptinesses').
inf('cavity', 'cavities').
inf('branch', 'branches').
inf('crotch', 'crotches').
inf('pouch', 'pouches').
inf('Platonic body', 'Platonic bodies').
inf('knife', 'knives').
inf('angle of view', 'angles of view').
inf('conditionality', 'conditionalities').
inf('stymy', 'stymies').
inf('state of affairs', 'states of affairs').
inf('company', 'companies').
inf('society', 'societies').
inf('freemasonry', 'freemasonries').
inf('anaclisis', 'anaclises').
inf('intimacy', 'intimacies').
inf('childlessness', 'childlessnesses').
inf('conflict of interest', 'conflicts of interest').
inf('crisis', 'crises').
inf('crunch', 'crunches').
inf('hot potato', 'hot potatoes').
inf('how-do-you-do', 'how-do-you-does').
inf('how-d\'ye-do', 'how-d\'ye-does').
inf('intestacy', 'intestacies').
inf('pass', 'passes').
inf('purgatory', 'purgatories').
inf('size of it', 'sizes of it').
inf('state of nature', 'states of nature').
inf('standard of living', 'standards of living').
inf('standard of life', 'standards of life').
inf('state of the art', 'states of the art').
inf('ultimacy', 'ultimacies').
inf('ultimateness', 'ultimatenesses').
inf('extremity', 'extremities').
inf('profoundness', 'profoundnesses').
inf('ordinary', 'ordinaries').
inf('homelessness', 'homelessnesses').
inf('vagrancy', 'vagrancies').
inf('tinderbox', 'tinderboxes').
inf('urgency', 'urgencies').
inf('executive clemency', 'executive clemencies').
inf('equality', 'equalities').
inf('egality', 'egalities').
inf('quality', 'qualities').
inf('seniority', 'seniorities').
inf('precedency', 'precedencies').
inf('priority', 'priorities').
inf('transcendency', 'transcendencies').
inf('superiority', 'superiorities').
inf('lowness', 'lownesses').
inf('lowliness', 'lowlinesses').
inf('inferiority', 'inferiorities').
inf('subordinateness', 'subordinatenesses').
inf('subsidiarity', 'subsidiarities').
inf('subservientness', 'subservientnesses').
inf('bastardy', 'bastardies').
inf('illegitimacy', 'illegitimacies').
inf('left-handedness', 'left-handednesses').
inf('nationality', 'nationalities').
inf('beingness', 'beingnesses').
inf('actuality', 'actualities').
inf('entelechy', 'entelechies').
inf('genuineness', 'genuinenesses').
inf('reality', 'realities').
inf('realness', 'realnesses').
inf('reality', 'realities').
inf('historicalness', 'historicalnesses').
inf('verity', 'verities').
inf('trueness', 'truenesses').
inf('eternity', 'eternities').
inf('timelessness', 'timelessnesses').
inf('eternal life', 'eternal lives').
inf('immanency', 'immanencies').
inf('inherency', 'inherencies').
inf('ubiety', 'ubieties').
inf('ubiquity', 'ubiquities').
inf('ubiquitousness', 'ubiquitousnesses').
inf('hereness', 'herenesses').
inf('thereness', 'therenesses').
inf('thereness', 'therenesses').
inf('allopatry', 'allopatries').
inf('sympatry', 'sympatries').
inf('nonentity', 'nonentities').
inf('unreality', 'unrealities').
inf('irreality', 'irrealities').
inf('falsity', 'falsities').
inf('falseness', 'falsenesses').
inf('spuriousness', 'spuriousnesses').
inf('awayness', 'awaynesses').
inf('life', 'lives').
inf('life', 'lives').
inf('aliveness', 'alivenesses').
inf('defunctness', 'defunctnesses').
inf('life', 'lives').
inf('ghetto', 'ghettoes').
inf('transcendency', 'transcendencies').
inf('matrimony', 'matrimonies').
inf('bigamy', 'bigamies').
inf('endogamy', 'endogamies').
inf('exogamy', 'exogamies').
inf('marriage of convenience', 'marriages of convenience').
inf('monandry', 'monandries').
inf('monogamy', 'monogamies').
inf('monogamousness', 'monogamousnesses').
inf('monogyny', 'monogynies').
inf('serial monogamy', 'serial monogamies').
inf('polyandry', 'polyandries').
inf('polygamy', 'polygamies').
inf('polygyny', 'polygynies').
inf('celibacy', 'celibacies').
inf('virginity', 'virginities').
inf('polity', 'polities').
inf('rule of law', 'rules of law').
inf('tranquillity', 'tranquillities').
inf('tranquility', 'tranquilities').
inf('harmony', 'harmonies').
inf('comity', 'comities').
inf('comity of nations', 'comities of nations').
inf('stability', 'stabilities').
inf('amity', 'amities').
inf('peaceableness', 'peaceablenesses').
inf('peacefulness', 'peacefulnesses').
inf('community', 'communities').
inf('community of interests', 'communities of interests').
inf('meeting of minds', 'meetings of minds').
inf('sense of the meeting', 'senses of the meeting').
inf('unanimity', 'unanimities').
inf('anarchy', 'anarchies').
inf('lawlessness', 'lawlessnesses').
inf('immunodeficiency', 'immunodeficiencies').
inf('bloodiness', 'bloodinesses').
inf('incompatibility', 'incompatibilities').
inf('histoincompatibility', 'histoincompatibilities').
inf('Rh incompatibility', 'Rh incompatibilities').
inf('instability', 'instabilities').
inf('topsy-turvyness', 'topsy-turvynesses').
inf('rowdiness', 'rowdinesses').
inf('roughness', 'roughnesses').
inf('disorderliness', 'disorderlinesses').
inf('hurly burly', 'hurly burlies').
inf('to-do', 'to-does').
inf('splash', 'splashes').
inf('tumultuousness', 'tumultuousnesses').
inf('tempestuousness', 'tempestuousnesses').
inf('hostility', 'hostilities').
inf('enmity', 'enmities').
inf('latent hostility', 'latent hostilities').
inf('clash', 'clashes').
inf('clash', 'clashes').
inf('state of war', 'states of war').
inf('disunity', 'disunities').
inf('darkness', 'darknesses').
inf('total darkness', 'total darknesses').
inf('lightlessness', 'lightlessnesses').
inf('blackness', 'blacknesses').
inf('pitch blackness', 'pitch blacknesses').
inf('semidarkness', 'semidarknesses').
inf('cloudiness', 'cloudinesses').
inf('shadiness', 'shadinesses').
inf('shadowiness', 'shadowinesses').
inf('dimness', 'dimnesses').
inf('duskiness', 'duskinesses').
inf('somberness', 'sombernesses').
inf('sombreness', 'sombrenesses').
inf('obscurity', 'obscurities').
inf('obscureness', 'obscurenesses').
inf('ecstasy', 'ecstasies').
inf('quality of life', 'qualities of life').
inf('happiness', 'happinesses').
inf('felicity', 'felicities').
inf('blessedness', 'blessednesses').
inf('bliss', 'blisses').
inf('blissfulness', 'blissfulnesses').
inf('walking on air', 'walkings on air').
inf('ecstasy', 'ecstasies').
inf('unhappiness', 'unhappinesses').
inf('sadness', 'sadnesses').
inf('sorrowfulness', 'sorrowfulnesses').
inf('poignancy', 'poignancies').
inf('blamelessness', 'blamelessnesses').
inf('inculpability', 'inculpabilities').
inf('inculpableness', 'inculpablenesses').
inf('guiltlessness', 'guiltlessnesses').
inf('purity', 'purities').
inf('pureness', 'purenesses').
inf('sinlessness', 'sinlessnesses').
inf('whiteness', 'whitenesses').
inf('cleanness', 'cleannesses').
inf('guiltiness', 'guiltinesses').
inf('blameworthiness', 'blameworthinesses').
inf('culpability', 'culpabilities').
inf('culpableness', 'culpablenesses').
inf('complicity', 'complicities').
inf('criminality', 'criminalities').
inf('criminalness', 'criminalnesses').
inf('guilt by association', 'guilts by association').
inf('impeachability', 'impeachabilities').
inf('indictability', 'indictabilities').
inf('autonomy', 'autonomies').
inf('liberty', 'liberties').
inf('sovereignty', 'sovereignties').
inf('autarky', 'autarkies').
inf('autarchy', 'autarchies').
inf('freedom of the seas', 'freedoms of the seas').
inf('independency', 'independencies').
inf('liberty', 'liberties').
inf('civil liberty', 'civil liberties').
inf('political liberty', 'political liberties').
inf('liberty', 'liberties').
inf('captivity', 'captivities').
inf('slavery', 'slaveries').
inf('captivity', 'captivities').
inf('custody', 'custodies').
inf('solitary', 'solitaries').
inf('delegacy', 'delegacies').
inf('agency', 'agencies').
inf('free agency', 'free agencies').
inf('autonomy', 'autonomies').
inf('self-sufficiency', 'self-sufficiencies').
inf('separateness', 'separatenesses').
inf('dependency', 'dependencies').
inf('helplessness', 'helplessnesses').
inf('contingency', 'contingencies').
inf('balance of power', 'balances of power').
inf('homeostasis', 'homeostases').
inf('isostasy', 'isostasies').
inf('instability', 'instabilities').
inf('shakiness', 'shakinesses').
inf('tremolo', 'tremoloes').
inf('motionlessness', 'motionlessnesses').
inf('stillness', 'stillnesses').
inf('lifelessness', 'lifelessnesses').
inf('stationariness', 'stationarinesses').
inf('immobility', 'immobilities').
inf('fixedness', 'fixednesses').
inf('activity', 'activities').
inf('activeness', 'activenesses').
inf('agency', 'agencies').
inf('busyness', 'busynesses').
inf('inactivity', 'inactivities').
inf('inactiveness', 'inactivenesses').
inf('anergy', 'anergies').
inf('hitch', 'hitches').
inf('dormancy', 'dormancies').
inf('quiescency', 'quiescencies').
inf('stagnancy', 'stagnancies').
inf('stagnancy', 'stagnancies').
inf('stasis', 'stases').
inf('recess', 'recesses').
inf('torpidity', 'torpidities').
inf('lethargy', 'lethargies').
inf('sluggishness', 'sluggishnesses').
inf('state of mind', 'states of mind').
inf('frame of mind', 'frames of mind').
inf('weariness', 'wearinesses').
inf('tiredness', 'tirednesses').
inf('grogginess', 'grogginesses').
inf('loginess', 'loginesses').
inf('logginess', 'logginesses').
inf('drunkenness', 'drunkennesses').
inf('inebriety', 'inebrieties').
inf('tipsiness', 'tipsinesses').
inf('insobriety', 'insobrieties').
inf('grogginess', 'grogginesses').
inf('sottishness', 'sottishnesses').
inf('soberness', 'sobernesses').
inf('sobriety', 'sobrieties').
inf('acephaly', 'acephalies').
inf('acidosis', 'acidoses').
inf('ketoacidosis', 'ketoacidoses').
inf('diabetic acidosis', 'diabetic acidoses').
inf('metabolic acidosis', 'metabolic acidoses').
inf('respiratory acidosis', 'respiratory acidoses').
inf('carbon dioxide acidosis', 'carbon dioxide acidoses').
inf('starvation acidosis', 'starvation acidoses').
inf('alkalosis', 'alkaloses').
inf('metabolic alkalosis', 'metabolic alkaloses').
inf('respiratory alkalosis', 'respiratory alkaloses').
inf('agalactosis', 'agalactoses').
inf('ankylosis', 'ankyloses').
inf('anchylosis', 'anchyloses').
inf('aneuploidy', 'aneuploidies').
inf('wakefulness', 'wakefulnesses').
inf('sleeplessness', 'sleeplessnesses').
inf('anhidrosis', 'anhidroses').
inf('anhydrosis', 'anhydroses').
inf('arteriectasis', 'arteriectases').
inf('arthropathy', 'arthropathies').
inf('asynergy', 'asynergies').
inf('hypnosis', 'hypnoses').
inf('self-hypnosis', 'self-hypnoses').
inf('sleepiness', 'sleepinesses').
inf('drowsiness', 'drowsinesses').
inf('oscitancy', 'oscitancies').
inf('imminency', 'imminencies').
inf('imminentness', 'imminentnesses').
inf('impendency', 'impendencies').
inf('forthcomingness', 'forthcomingnesses').
inf('readiness', 'readinesses').
inf('preparedness', 'preparednesses').
inf('ready', 'readies').
inf('diverticulosis', 'diverticuloses').
inf('emergency', 'emergencies').
inf('clutch', 'clutches').
inf('exigency', 'exigencies').
inf('criticality', 'criticalities').
inf('flux', 'fluxes').
inf('state of flux', 'states of flux').
inf('elastosis', 'elastoses').
inf('flatulency', 'flatulencies').
inf('alertness', 'alertnesses').
inf('angriness', 'angrinesses').
inf('fever pitch', 'fever pitches').
inf('cybersex', 'cybersexes').
inf('horniness', 'horninesses').
inf('hotness', 'hotnesses').
inf('hungriness', 'hungrinesses').
inf('emptiness', 'emptinesses').
inf('edacity', 'edacities').
inf('ravenousness', 'ravenousnesses').
inf('voracity', 'voracities').
inf('voraciousness', 'voraciousnesses').
inf('thirstiness', 'thirstinesses').
inf('altitude sickness', 'altitude sicknesses').
inf('mountain sickness', 'mountain sicknesses').
inf('hyperthermy', 'hyperthermies').
inf('flux', 'fluxes').
inf('muscularity', 'muscularities').
inf('impotency', 'impotencies').
inf('barrenness', 'barrennesses').
inf('sterility', 'sterilities').
inf('infertility', 'infertilities').
inf('cacogenesis', 'cacogeneses').
inf('dysgenesis', 'dysgeneses').
inf('false pregnancy', 'false pregnancies').
inf('pseudocyesis', 'pseudocyeses').
inf('pregnancy', 'pregnancies').
inf('maternity', 'maternities').
inf('gravidity', 'gravidities').
inf('gravidness', 'gravidnesses').
inf('parity', 'parities').
inf('abdominal pregnancy', 'abdominal pregnancies').
inf('ovarian pregnancy', 'ovarian pregnancies').
inf('tubal pregnancy', 'tubal pregnancies').
inf('ectopic pregnancy', 'ectopic pregnancies').
inf('extrauterine pregnancy', 'extrauterine pregnancies').
inf('eccyesis', 'eccyeses').
inf('metacyesis', 'metacyeses').
inf('entopic pregnancy', 'entopic pregnancies').
inf('parturiency', 'parturiencies').
inf('obliquity', 'obliquities').
inf('healthiness', 'healthinesses').
inf('wholeness', 'wholenesses').
inf('haleness', 'halenesses').
inf('energy', 'energies').
inf('vitality', 'vitalities').
inf('blush', 'blushes').
inf('flush', 'flushes').
inf('rosiness', 'rosinesses').
inf('freshness', 'freshnesses').
inf('sturdiness', 'sturdinesses').
inf('fertility', 'fertilities').
inf('fecundity', 'fecundities').
inf('potency', 'potencies').
inf('unhealthiness', 'unhealthinesses').
inf('cholestasis', 'cholestases').
inf('astereognosis', 'astereognoses').
inf('cheilosis', 'cheiloses').
inf('colpoxerosis', 'colpoxeroses').
inf('dystrophy', 'dystrophies').
inf('osteodystrophy', 'osteodystrophies').
inf('adenosis', 'adenoses').
inf('hyperactivity', 'hyperactivities').
inf('impacted tooth', 'impacted teeth').
inf('learning disability', 'learning disabilities').
inf('cellularity', 'cellularities').
inf('hypercellularity', 'hypercellularities').
inf('hypocellularity', 'hypocellularities').
inf('illness', 'illnesses').
inf('unwellness', 'unwellnesses').
inf('malady', 'maladies').
inf('sickness', 'sicknesses').
inf('biliousness', 'biliousnesses').
inf('dependency', 'dependencies').
inf('anabiosis', 'anabioses').
inf('cryptobiosis', 'cryptobioses').
inf('ectasis', 'ectases').
inf('lymphangiectasis', 'lymphangiectases').
inf('drunkenness', 'drunkennesses').
inf('amyloidosis', 'amyloidoses').
inf('anuresis', 'anureses').
inf('catastrophic illness', 'catastrophic illnesses').
inf('decompression sickness', 'decompression sicknesses').
inf('fluorosis', 'fluoroses').
inf('gammopathy', 'gammopathies').
inf('salmonellosis', 'salmonelloses').
inf('catalepsy', 'catalepsies').
inf('disease of the neuromuscular junction', 'diseases of the neuromuscular junction').
inf('angiopathy', 'angiopathies').
inf('aspergillosis', 'aspergilloses').
inf('acanthocytosis', 'acanthocytoses').
inf('agranulocytosis', 'agranulocytoses').
inf('agranulosis', 'agranuloses').
inf('anthrax', 'anthraxes').
inf('cutaneous anthrax', 'cutaneous anthraxes').
inf('pulmonary anthrax', 'pulmonary anthraxes').
inf('inhalation anthrax', 'inhalation anthraxes').
inf('enteropathy', 'enteropathies').
inf('idiopathy', 'idiopathies').
inf('pycnosis', 'pycnoses').
inf('pyknosis', 'pyknoses').
inf('milk sickness', 'milk sicknesses').
inf('mimesis', 'mimeses').
inf('carcinomatous myopathy', 'carcinomatous myopathies').
inf('onycholysis', 'onycholyses').
inf('onychosis', 'onychoses').
inf('periodontitis', 'periodontitis').
inf('gingivitis', 'gingivitis').
inf('touch', 'touches').
inf('apoplexy', 'apoplexies').
inf('akinesis', 'akineses').
inf('encephalopathy', 'encephalopathies').
inf('cystoparalysis', 'cystoparalyses').
inf('epilepsy', 'epilepsies').
inf('akinetic epilepsy', 'akinetic epilepsies').
inf('cortical epilepsy', 'cortical epilepsies').
inf('focal epilepsy', 'focal epilepsies').
inf('grand mal epilepsy', 'grand mal epilepsies').
inf('generalized epilepsy', 'generalized epilepsies').
inf('Jacksonian epilepsy', 'Jacksonian epilepsies').
inf('myoclonus epilepsy', 'myoclonus epilepsies').
inf('petit mal epilepsy', 'petit mal epilepsies').
inf('musicogenic epilepsy', 'musicogenic epilepsies').
inf('photogenic epilepsy', 'photogenic epilepsies').
inf('posttraumatic epilepsy', 'posttraumatic epilepsies').
inf('traumatic epilepsy', 'traumatic epilepsies').
inf('procursive epilepsy', 'procursive epilepsies').
inf('psychomotor epilepsy', 'psychomotor epilepsies').
inf('temporal lobe epilepsy', 'temporal lobe epilepsies').
inf('reflex epilepsy', 'reflex epilepsies').
inf('sensory epilepsy', 'sensory epilepsies').
inf('tonic epilepsy', 'tonic epilepsies').
inf('Erb\'s palsy', 'Erb\'s palsies').
inf('Erb-Duchenne paralysis', 'Erb-Duchenne paralyses').
inf('nympholepsy', 'nympholepsies').
inf('ataxy', 'ataxies').
inf('atopognosis', 'atopognoses').
inf('brachydactyly', 'brachydactylies').
inf('cryptorchidy', 'cryptorchidies').
inf('ectrodactyly', 'ectrodactylies').
inf('enteroptosis', 'enteroptoses').
inf('fetal distress', 'fetal distresses').
inf('foetal distress', 'foetal distresses').
inf('multiple sclerosis', 'multiple scleroses').
inf('disseminated sclerosis', 'disseminated scleroses').
inf('disseminated multiple sclerosis', 'disseminated multiple scleroses').
inf('shaking palsy', 'shaking palsies').
inf('cerebral palsy', 'cerebral palsies').
inf('spastic paralysis', 'spastic paralyses').
inf('choriomeningitis', 'choriomeningitis').
inf('flaccid paralysis', 'flaccid paralyses').
inf('anorthography', 'anorthographies').
inf('amaurosis', 'amauroses').
inf('word deafness', 'word deafnesses').
inf('word blindness', 'word blindnesses').
inf('thrombosis', 'thromboses').
inf('cerebral thrombosis', 'cerebral thromboses').
inf('coronary thrombosis', 'coronary thromboses').
inf('coronary', 'coronaries').
inf('hepatomegaly', 'hepatomegalies').
inf('cardiopathy', 'cardiopathies').
inf('heterotaxy', 'heterotaxies').
inf('hyperpiesis', 'hyperpieses').
inf('amyotrophy', 'amyotrophies').
inf('amyotrophic lateral sclerosis', 'amyotrophic lateral scleroses').
inf('aortic stenosis', 'aortic stenoses').
inf('enterostenosis', 'enterostenoses').
inf('laryngostenosis', 'laryngostenoses').
inf('pulmonary stenosis', 'pulmonary stenoses').
inf('pyloric stenosis', 'pyloric stenoses').
inf('rhinostenosis', 'rhinostenoses').
inf('stenosis', 'stenoses').
inf('arteriolosclerosis', 'arterioloscleroses').
inf('arteriosclerosis', 'arterioscleroses').
inf('arterial sclerosis', 'arterial scleroses').
inf('hardening of the arteries', 'hardenings of the arteries').
inf('induration of the arteries', 'indurations of the arteries').
inf('atherogenesis', 'atherogeneses').
inf('atherosclerosis', 'atheroscleroses').
inf('athetosis', 'athetoses').
inf('sclerosis', 'scleroses').
inf('cardiomyopathy', 'cardiomyopathies').
inf('myocardiopathy', 'myocardiopathies').
inf('hypertrophic cardiomyopathy', 'hypertrophic cardiomyopathies').
inf('mitral stenosis', 'mitral stenoses').
inf('mitral valve stenosis', 'mitral valve stenoses').
inf('nephropathy', 'nephropathies').
inf('nephrosis', 'nephroses').
inf('insufficiency', 'insufficiencies').
inf('coronary insufficiency', 'coronary insufficiencies').
inf('cardiac insufficiency', 'cardiac insufficiencies').
inf('nephritis', 'nephritis').
inf('nephrosclerosis', 'nephroscleroses').
inf('nephroangiosclerosis', 'nephroangioscleroses').
inf('renal insufficiency', 'renal insufficiencies').
inf('cholelithiasis', 'cholelithiases').
inf('enterolithiasis', 'enterolithiases').
inf('nephrocalcinosis', 'nephrocalcinoses').
inf('nephrolithiasis', 'nephrolithiases').
inf('renal lithiasis', 'renal lithiases').
inf('lipomatosis', 'lipomatoses').
inf('lithiasis', 'lithiases').
inf('glomerulonephritis', 'glomerulonephritis').
inf('cirrhosis', 'cirrhoses').
inf('cirrhosis of the liver', 'cirrhoses of the liver').
inf('adenopathy', 'adenopathies').
inf('thyrotoxicosis', 'thyrotoxicoses').
inf('achondroplasty', 'achondroplasties').
inf('chondrodystrophy', 'chondrodystrophies').
inf('pox', 'pox').
inf('smallpox', 'smallpox').
inf('pseudosmallpox', 'pseudosmallpox').
inf('milk pox', 'milk pox').
inf('white pox', 'white pox').
inf('West Indian smallpox', 'West Indian smallpox').
inf('Cuban itch', 'Cuban itches').
inf('Kaffir pox', 'Kaffir pox').
inf('blastomycosis', 'blastomycoses').
inf('chromoblastomycosis', 'chromoblastomycoses').
inf('dhobi itch', 'dhobi itches').
inf('athlete\'s foot', 'athlete\'s feet').
inf('barber\'s itch', 'barber\'s itches').
inf('tinea capitis', 'tinea capitis').
inf('jock itch', 'jock itches').
inf('blindness', 'blindnesses').
inf('sightlessness', 'sightlessnesses').
inf('cecity', 'cecities').
inf('legal blindness', 'legal blindnesses').
inf('brucellosis', 'brucelloses').
inf('anergy', 'anergies').
inf('severe combined immunodeficiency', 'severe combined immunodeficiencies').
inf('dysentery', 'dysenteries').
inf('hepatitis', 'hepatitis').
inf('viral hepatitis', 'viral hepatitis').
inf('infectious hepatitis', 'infectious hepatitis').
inf('serum hepatitis', 'serum hepatitis').
inf('delta hepatitis', 'delta hepatitis').
inf('cancer of the liver', 'cancers of the liver').
inf('herpes simplex', 'herpes simplexes').
inf('chickenpox', 'chickenpox').
inf('Cupid\'s itch', 'Cupid\'s itches').
inf('pox', 'pox').
inf('infectious mononucleosis', 'infectious mononucleoses').
inf('mononucleosis', 'mononucleoses').
inf('mono', 'monoes').
inf('leprosy', 'leprosies').
inf('tuberculoid leprosy', 'tuberculoid leprosies').
inf('lepromatous leprosy', 'lepromatous leprosies').
inf('necrotizing enterocolitis', 'necrotizing enterocolitis').
inf('listeriosis', 'listerioses').
inf('listeria meningitis', 'listeria meningitis').
inf('lymphocytic choriomeningitis', 'lymphocytic choriomeningitis').
inf('meningitis', 'meningitis').
inf('epidemic parotitis', 'epidemic parotitis').
inf('cerebrospinal meningitis', 'cerebrospinal meningitis').
inf('epidemic meningitis', 'epidemic meningitis').
inf('poliomyelitis', 'poliomyelitis').
inf('infantile paralysis', 'infantile paralyses').
inf('acute anterior poliomyelitis', 'acute anterior poliomyelitis').
inf('rickettsiosis', 'rickettsioses').
inf('rickettsialpox', 'rickettsialpox').
inf('sweating sickness', 'sweating sicknesses').
inf('tuberculosis', 'tuberculoses').
inf('miliary tuberculosis', 'miliary tuberculoses').
inf('pulmonary tuberculosis', 'pulmonary tuberculoses').
inf('phthisis', 'phthises').
inf('pertussis', 'pertusses').
inf('respiratory illness', 'respiratory illnesses').
inf('bronchitis', 'bronchitis').
inf('bronchiolitis', 'bronchiolitis').
inf('chronic bronchitis', 'chronic bronchitis').
inf('coccidioidomycosis', 'coccidioidomycoses').
inf('coccidiomycosis', 'coccidiomycoses').
inf('cryptococcosis', 'cryptococcoses').
inf('pneumocytosis', 'pneumocytoses').
inf('pneumothorax', 'pneumothoraxes').
inf('psittacosis', 'psittacoses').
inf('ornithosis', 'ornithoses').
inf('pneumoconiosis', 'pneumoconioses').
inf('pneumonoconiosis', 'pneumonoconioses').
inf('anthracosis', 'anthracoses').
inf('asbestosis', 'asbestoses').
inf('siderosis', 'sideroses').
inf('silicosis', 'silicoses').
inf('genetic abnormality', 'genetic abnormalities').
inf('macrencephaly', 'macrencephalies').
inf('anencephaly', 'anencephalies').
inf('adactyly', 'adactylies').
inf('color blindness', 'color blindnesses').
inf('colour blindness', 'colour blindnesses').
inf('color vision deficiency', 'color vision deficiencies').
inf('colour vision deficiency', 'colour vision deficiencies').
inf('dichromacy', 'dichromacies').
inf('dichromasy', 'dichromasies').
inf('red-green dichromacy', 'red-green dichromacies').
inf('red-green color blindness', 'red-green color blindnesses').
inf('red-green colour blindness', 'red-green colour blindnesses').
inf('green-blindness', 'green-blindnesses').
inf('red-blindness', 'red-blindnesses').
inf('yellow-blue dichromacy', 'yellow-blue dichromacies').
inf('yellow-blue color blindness', 'yellow-blue color blindnesses').
inf('yellow-blindness', 'yellow-blindnesses').
inf('blue-blindness', 'blue-blindnesses').
inf('monochromacy', 'monochromacies').
inf('monochromasy', 'monochromasies').
inf('cystic fibrosis', 'cystic fibroses').
inf('pancreatic fibrosis', 'pancreatic fibroses').
inf('mucoviscidosis', 'mucoviscidoses').
inf('dysostosis multiplex', 'dysostosis multiplexes').
inf('lipochondrodystrophy', 'lipochondrodystrophies').
inf('mucopolysaccharidosis', 'mucopolysaccharidoses').
inf('neurofibromatosis', 'neurofibromatoses').
inf('ichthyosis', 'ichthyoses').
inf('clinocephaly', 'clinocephalies').
inf('clinodactyly', 'clinodactylies').
inf('muscular dystrophy', 'muscular dystrophies').
inf('dystrophy', 'dystrophies').
inf('oligodactyly', 'oligodactylies').
inf('otosclerosis', 'otoscleroses').
inf('Becker muscular dystrophy', 'Becker muscular dystrophies').
inf('distal muscular dystrophy', 'distal muscular dystrophies').
inf('Duchenne\'s muscular dystrophy', 'Duchenne\'s muscular dystrophies').
inf('pseudohypertrophic dystrophy', 'pseudohypertrophic dystrophies').
inf('limb-girdle muscular dystrophy', 'limb-girdle muscular dystrophies').
inf('myotonic muscular dystrophy', 'myotonic muscular dystrophies').
inf('myotonic dystrophy', 'myotonic dystrophies').
inf('oculopharyngeal muscular dystrophy', 'oculopharyngeal muscular dystrophies').
inf('oxycephaly', 'oxycephalies').
inf('acrocephaly', 'acrocephalies').
inf('chlorosis', 'chloroses').
inf('greensickness', 'greensicknesses').
inf('juvenile amaurotic idiocy', 'juvenile amaurotic idiocies').
inf('infantile amaurotic idiocy', 'infantile amaurotic idiocies').
inf('gastroenteritis', 'gastroenteritis').
inf('pleurisy', 'pleurisies').
inf('purulent pleurisy', 'purulent pleurisies').
inf('pyelitis', 'pyelitis').
inf('pharyngitis', 'pharyngitis').
inf('quinsy', 'quinsies').
inf('peritonsillar abscess', 'peritonsillar abscesses').
inf('spasmodic laryngitis', 'spasmodic laryngitis').
inf('glossoptosis', 'glossoptoses').
inf('hypermotility', 'hypermotilities').
inf('amebiasis', 'amebiases').
inf('amoebiasis', 'amoebiases').
inf('amebiosis', 'amebioses').
inf('amoebiosis', 'amoebioses').
inf('amebic dysentery', 'amebic dysenteries').
inf('amoebic dysentery', 'amoebic dysenteries').
inf('fascioliasis', 'fascioliases').
inf('fasciolosis', 'fascioloses').
inf('fasciolopsiasis', 'fasciolopsiases').
inf('dracunculiasis', 'dracunculiases').
inf('enterobiasis', 'enterobiases').
inf('mycosis', 'mycoses').
inf('giardiasis', 'giardiases').
inf('leishmaniasis', 'leishmaniases').
inf('leishmaniosis', 'leishmanioses').
inf('itch', 'itches').
inf('schistosomiasis', 'schistosomiases').
inf('bilharziasis', 'bilharziases').
inf('sepsis', 'sepses').
inf('visceral leishmaniasis', 'visceral leishmaniases').
inf('cutaneous leishmaniasis', 'cutaneous leishmaniases').
inf('Old World leishmaniasis', 'Old World leishmaniases').
inf('mucocutaneous leishmaniasis', 'mucocutaneous leishmaniases').
inf('New World leishmaniasis', 'New World leishmaniases').
inf('American leishmaniasis', 'American leishmaniases').
inf('nasopharyngeal leishmaniasis', 'nasopharyngeal leishmaniases').
inf('candidiasis', 'candidiases').
inf('moniliasis', 'moniliases').
inf('dermatomycosis', 'dermatomycoses').
inf('dermatophytosis', 'dermatophytoses').
inf('keratomycosis', 'keratomycoses').
inf('phycomycosis', 'phycomycoses').
inf('sporotrichosis', 'sporotrichoses').
inf('thrush', 'thrushes').
inf('aspergillosis', 'aspergilloses').
inf('shigellosis', 'shigelloses').
inf('bacillary dysentery', 'bacillary dysenteries').
inf('streptococcus tonsilitis', 'streptococcus tonsilitis').
inf('sty', 'sties').
inf('toxoplasmosis', 'toxoplasmoses').
inf('trichomoniasis', 'trichomoniases').
inf('arthritis', 'arthritis').
inf('rheumatoid arthritis', 'rheumatoid arthritis').
inf('atrophic arthritis', 'atrophic arthritis').
inf('psoriatic arthritis', 'psoriatic arthritis').
inf('juvenile rheumatoid arthritis', 'juvenile rheumatoid arthritis').
inf('osteoarthritis', 'osteoarthritis').
inf('degenerative arthritis', 'degenerative arthritis').
inf('osteosclerosis', 'osteoscleroses').
inf('cystitis', 'cystitis').
inf('gouty arthritis', 'gouty arthritis').
inf('urarthritis', 'urarthritis').
inf('spondylarthritis', 'spondylarthritis').
inf('ozone sickness', 'ozone sicknesses').
inf('toxemia of pregnancy', 'toxemias of pregnancy').
inf('toxaemia of pregnancy', 'toxaemias of pregnancy').
inf('erythroblastosis', 'erythroblastoses').
inf('hemoglobinopathy', 'hemoglobinopathies').
inf('haemoglobinopathy', 'haemoglobinopathies').
inf('hemoptysis', 'hemoptyses').
inf('haemoptysis', 'haemoptyses').
inf('histiocytosis', 'histiocytoses').
inf('molar pregnancy', 'molar pregnancies').
inf('water on the knee', 'water on the knee').
inf('lipidosis', 'lipidoses').
inf('lysogeny', 'lysogenies').
inf('lysogenicity', 'lysogenicities').
inf('leukocytosis', 'leukocytoses').
inf('leucocytosis', 'leucocytoses').
inf('lymphocytosis', 'lymphocytoses').
inf('microcytosis', 'microcytoses').
inf('peliosis', 'pelioses').
inf('cystic mastitis', 'cystic mastitis').
inf('avitaminosis', 'avitaminoses').
inf('hypovitaminosis', 'hypovitaminoses').
inf('hypervitaminosis', 'hypervitaminoses').
inf('thyromegaly', 'thyromegalies').
inf('mental abnormality', 'mental abnormalities').
inf('zinc deficiency', 'zinc deficiencies').
inf('Alpine scurvy', 'Alpine scurvies').
inf('mal rosso', 'mal rossoes').
inf('Saint Ignatius\' itch', 'Saint Ignatius\' itches').
inf('rachitis', 'rachitis').
inf('scurvy', 'scurvies').
inf('motion sickness', 'motion sicknesses').
inf('kinetosis', 'kinetoses').
inf('airsickness', 'airsicknesses').
inf('air sickness', 'air sicknesses').
inf('car sickness', 'car sicknesses').
inf('seasickness', 'seasicknesses').
inf('algidity', 'algidities').
inf('siriasis', 'siriases').
inf('endometriosis', 'endometrioses').
inf('adenomyosis', 'adenomyoses').
inf('pathology', 'pathologies').
inf('symphysis', 'symphyses').
inf('hemochromatosis', 'hemochromatoses').
inf('classic hemochromatosis', 'classic hemochromatoses').
inf('idiopathic hemochromatosis', 'idiopathic hemochromatoses').
inf('acquired hemochromatosis', 'acquired hemochromatoses').
inf('macrocytosis', 'macrocytoses').
inf('fibrosis', 'fibroses').
inf('myelofibrosis', 'myelofibroses').
inf('mastopathy', 'mastopathies').
inf('mazopathy', 'mazopathies').
inf('neuropathy', 'neuropathies').
inf('hereditary motor and sensory neuropathy', 'hereditary motor and sensory neuropathies').
inf('mononeuropathy', 'mononeuropathies').
inf('multiple mononeuropathy', 'multiple mononeuropathies').
inf('myopathy', 'myopathies').
inf('dermatomyositis', 'dermatomyositis').
inf('polymyositis', 'polymyositis').
inf('inclusion body myositis', 'inclusion body myositis').
inf('osteopetrosis', 'osteopetroses').
inf('osteoporosis', 'osteoporoses').
inf('deformity', 'deformities').
inf('misshapenness', 'misshapennesses').
inf('Arnold-Chiari deformity', 'Arnold-Chiari deformities').
inf('clawfoot', 'clawfeet').
inf('cleft foot', 'cleft feet').
inf('cheiloschisis', 'cheiloschises').
inf('clubfoot', 'clubfeet').
inf('disease of the skin', 'diseases of the skin').
inf('ankylosing spondylitis', 'ankylosing spondylitis').
inf('rheumatoid spondylitis', 'rheumatoid spondylitis').
inf('acantholysis', 'acantholyses').
inf('acanthosis', 'acanthoses').
inf('actinic dermatitis', 'actinic dermatitis').
inf('atopic dermatitis', 'atopic dermatitis').
inf('bubble gum dermatitis', 'bubble gum dermatitis').
inf('contact dermatitis', 'contact dermatitis').
inf('Rhus dermatitis', 'Rhus dermatitis').
inf('poison ivy', 'poison ivies').
inf('diaper rash', 'diaper rashes').
inf('diaper dermatitis', 'diaper dermatitis').
inf('neurodermatitis', 'neurodermatitis').
inf('schistosome dermatitis', 'schistosome dermatitis').
inf('swimmer\'s itch', 'swimmer\'s itches').
inf('dermatitis', 'dermatitis').
inf('dermatosis', 'dermatoses').
inf('furunculosis', 'furunculoses').
inf('impetigo', 'impetigoes').
inf('keratonosis', 'keratonoses').
inf('keratosis', 'keratoses').
inf('actinic keratosis', 'actinic keratoses').
inf('seborrheic keratosis', 'seborrheic keratoses').
inf('livedo', 'livedoes').
inf('melanosis', 'melanoses').
inf('pityriasis', 'pityriases').
inf('prurigo', 'prurigoes').
inf('psoriasis', 'psoriases').
inf('seborrheic dermatitis', 'seborrheic dermatitis').
inf('vitiligo', 'vitiligoes').
inf('xanthomatosis', 'xanthomatoses').
inf('xanthoma multiplex', 'xanthoma multiplexes').
inf('lipid granulomatosis', 'lipid granulomatoses').
inf('lipoid granulomatosis', 'lipoid granulomatoses').
inf('xanthosis', 'xanthoses').
inf('exostosis', 'exostoses').
inf('malignancy', 'malignancies').
inf('lymphadenopathy', 'lymphadenopathies').
inf('cancer of the blood', 'cancers of the blood').
inf('embryoma of the kidney', 'embryomas of the kidney').
inf('carcinoma in situ', 'carcinomata in situ').
inf('actinomycosis', 'actinomycoses').
inf('cervicofacial actinomycosis', 'cervicofacial actinomycoses').
inf('retinopathy', 'retinopathies').
inf('diabetic retinopathy', 'diabetic retinopathies').
inf('adenitis', 'adenitis').
inf('alveolitis', 'alveolitis').
inf('alveolitis', 'alveolitis').
inf('angiitis', 'angiitis').
inf('aortitis', 'aortitis').
inf('rheumatic aortitis', 'rheumatic aortitis').
inf('appendicitis', 'appendicitis').
inf('arteritis', 'arteritis').
inf('periarteritis', 'periarteritis').
inf('polyarteritis', 'polyarteritis').
inf('Takayasu\'s arteritis', 'Takayasu\'s arteritis').
inf('temporal arteritis', 'temporal arteritis').
inf('ophthalmitis', 'ophthalmitis').
inf('thoracic actinomycosis', 'thoracic actinomycoses').
inf('abdominal actinomycosis', 'abdominal actinomycoses').
inf('anaplasmosis', 'anaplasmoses').
inf('anthrax', 'anthraxes').
inf('aspergillosis', 'aspergilloses').
inf('bagassosis', 'bagassoses').
inf('bagascosis', 'bagascoses').
inf('balanitis', 'balanitis').
inf('balanoposthitis', 'balanoposthitis').
inf('blepharitis', 'blepharitis').
inf('bursitis', 'bursitis').
inf('brucellosis', 'brucelloses').
inf('bovine spongiform encephalitis', 'bovine spongiform encephalitis').
inf('camelpox', 'camelpox').
inf('costiasis', 'costiases').
inf('cowpox', 'cowpox').
inf('pasteurellosis', 'pasteurelloses').
inf('distomatosis', 'distomatoses').
inf('Lyme arthritis', 'Lyme arthritis').
inf('ketosis', 'ketoses').
inf('monocytosis', 'monocytoses').
inf('thrombocytosis', 'thrombocytoses').
inf('ochronosis', 'ochronoses').
inf('kaliuresis', 'kaliureses').
inf('kaluresis', 'kalureses').
inf('natriuresis', 'natriureses').
inf('madness', 'madnesses').
inf('rhinotracheitis', 'rhinotracheitis').
inf('leptospirosis', 'leptospiroses').
inf('moon blindness', 'moon blindnesses').
inf('myxomatosis', 'myxomatoses').
inf('psittacosis', 'psittacoses').
inf('sweating sickness', 'sweating sicknesses').
inf('milk sickness', 'milk sicknesses').
inf('yatobyo', 'yatobyoes').
inf('zoonosis', 'zoonoses').
inf('brown rot gummosis', 'brown rot gummoses').
inf('gummosis', 'gummoses').
inf('gummosis', 'gummoses').
inf('little potato', 'little potatoes').
inf('scorch', 'scorches').
inf('leaf scorch', 'leaf scorches').
inf('verticilliosis', 'verticillioses').
inf('yellow dwarf', 'yellow dwarves').
inf('yellow dwarf of potato', 'yellow dwarf of potatoes').
inf('potato yellow dwarf', 'potato yellow dwarves').
inf('onion yellow dwarf', 'onion yellow dwarves').
inf('injury', 'injuries').
inf('scratch', 'scratches').
inf('gash', 'gashes').
inf('slash', 'slashes').
inf('ecchymosis', 'ecchymoses').
inf('mouse', 'mice').
inf('scorch', 'scorches').
inf('diastasis', 'diastases').
inf('spondylolisthesis', 'spondylolistheses').
inf('cryopathy', 'cryopathies').
inf('penetrating injury', 'penetrating injuries').
inf('pinch', 'pinches').
inf('whiplash', 'whiplashes').
inf('whiplash injury', 'whiplash injuries').
inf('wrench', 'wrenches').
inf('trench foot', 'trench feet').
inf('immersion foot', 'immersion feet').
inf('mask of pregnancy', 'masks of pregnancy').
inf('cyanosis', 'cyanoses').
inf('diuresis', 'diureses').
inf('acrocyanosis', 'acrocyanoses').
inf('Persian Gulf illness', 'Persian Gulf illnesses').
inf('regional enteritis', 'regional enteritis').
inf('regional ileitis', 'regional ileitis').
inf('mucous colitis', 'mucous colitis').
inf('ulcerative colitis', 'ulcerative colitis').
inf('narcolepsy', 'narcolepsies').
inf('nephrosis', 'nephroses').
inf('radiation sickness', 'radiation sicknesses').
inf('tetany', 'tetanies').
inf('abscess', 'abscesses').
inf('abscessed tooth', 'abscessed teeth').
inf('purulency', 'purulencies').
inf('mumification necrosis', 'mumification necroses').
inf('clostridial myonecrosis', 'clostridial myonecroses').
inf('progressive emphysematous necrosis', 'progressive emphysematous necroses').
inf('hemosiderosis', 'hemosideroses').
inf('haemosiderosis', 'haemosideroses').
inf('puffiness', 'puffinesses').
inf('bubo', 'buboes').
inf('chemosis', 'chemoses').
inf('palsy', 'palsies').
inf('dropsy', 'dropsies').
inf('intumescency', 'intumescencies').
inf('tumidity', 'tumidities').
inf('tumidness', 'tumidnesses').
inf('proud flesh', 'proud fleshes').
inf('hyperbilirubinemia of the newborn', 'hyperbilirubinemias of the newborn').
inf('jaundice of the newborn', 'jaundices of the newborn').
inf('hydrothorax', 'hydrothoraxes').
inf('hemothorax', 'hemothoraxes').
inf('haemothorax', 'haemothoraxes').
inf('stuffiness', 'stuffinesses').
inf('rash', 'rashes').
inf('skin rash', 'skin rashes').
inf('heat rash', 'heat rashes').
inf('nettle rash', 'nettle rashes').
inf('numbness', 'numbnesses').
inf('agony', 'agonies').
inf('Passion of Christ', 'Passions of Christ').
inf('distress', 'distresses').
inf('lumbago', 'lumbagoes').
inf('neuralgy', 'neuralgies').
inf('myosis', 'myoses').
inf('diaphragmatic pleurisy', 'diaphragmatic pleurisies').
inf('tic douloureux', 'tic douloureuxes').
inf('smartness', 'smartnesses').
inf('stitch', 'stitches').
inf('rebound tenderness', 'rebound tendernesses').
inf('tenderness', 'tendernesses').
inf('soreness', 'sorenesses').
inf('rawness', 'rawnesses').
inf('cardiomegaly', 'cardiomegalies').
inf('pyrosis', 'pyroses').
inf('gastroesophageal reflux', 'gastroesophageal refluxes').
inf('esophageal reflux', 'esophageal refluxes').
inf('oesophageal reflux', 'oesophageal refluxes').
inf('hepatojugular reflux', 'hepatojugular refluxes').
inf('ureterorenal reflux', 'ureterorenal refluxes').
inf('vesicoureteral reflux', 'vesicoureteral refluxes').
inf('reflux', 'refluxes').
inf('hot flash', 'hot flashes').
inf('flush', 'flushes').
inf('upset stomach', 'upset stomaches').
inf('redness', 'rednesses').
inf('carditis', 'carditis').
inf('endocarditis', 'endocarditis').
inf('subacute bacterial endocarditis', 'subacute bacterial endocarditis').
inf('myocarditis', 'myocarditis').
inf('pancarditis', 'pancarditis').
inf('pericarditis', 'pericarditis').
inf('cellulitis', 'cellulitis').
inf('cervicitis', 'cervicitis').
inf('cheilitis', 'cheilitis').
inf('cholangitis', 'cholangitis').
inf('cholecystitis', 'cholecystitis').
inf('chorditis', 'chorditis').
inf('chorditis', 'chorditis').
inf('colitis', 'colitis').
inf('colpitis', 'colpitis').
inf('colpocystitis', 'colpocystitis').
inf('conjunctivitis', 'conjunctivitis').
inf('corditis', 'corditis').
inf('costochondritis', 'costochondritis').
inf('dacryocystitis', 'dacryocystitis').
inf('diverticulitis', 'diverticulitis').
inf('encephalitis', 'encephalitis').
inf('cephalitis', 'cephalitis').
inf('phrenitis', 'phrenitis').
inf('encephalomyelitis', 'encephalomyelitis').
inf('endarteritis', 'endarteritis').
inf('acute hemorrhagic encephalitis', 'acute hemorrhagic encephalitis').
inf('equine encephalitis', 'equine encephalitis').
inf('equine encephalomyelitis', 'equine encephalomyelitis').
inf('herpes simplex encephalitis', 'herpes simplex encephalitis').
inf('herpes encephalitis', 'herpes encephalitis').
inf('acute inclusion body encephalitis', 'acute inclusion body encephalitis').
inf('leukoencephalitis', 'leukoencephalitis').
inf('meningoencephalitis', 'meningoencephalitis').
inf('cerebromeningitis', 'cerebromeningitis').
inf('encephalomeningitis', 'encephalomeningitis').
inf('panencephalitis', 'panencephalitis').
inf('sleeping sickness', 'sleeping sicknesses').
inf('sleepy sickness', 'sleepy sicknesses').
inf('epidemic encephalitis', 'epidemic encephalitis').
inf('lethargic encephalitis', 'lethargic encephalitis').
inf('West Nile encephalitis', 'West Nile encephalitis').
inf('subacute sclerosing panencephalitis', 'subacute sclerosing panencephalitis').
inf('inclusion body encephalitis', 'inclusion body encephalitis').
inf('subacute inclusion body encephalitis', 'subacute inclusion body encephalitis').
inf('sclerosing leukoencephalitis', 'sclerosing leukoencephalitis').
inf('subacute sclerosing leukoencephalitis', 'subacute sclerosing leukoencephalitis').
inf('Dawson\'s encephalitis', 'Dawson\'s encephalitis').
inf('Van Bogaert encephalitis', 'Van Bogaert encephalitis').
inf('rubella panencephalitis', 'rubella panencephalitis').
inf('endocervicitis', 'endocervicitis').
inf('enteritis', 'enteritis').
inf('necrotizing enteritis', 'necrotizing enteritis').
inf('epicondylitis', 'epicondylitis').
inf('epididymitis', 'epididymitis').
inf('epiglottitis', 'epiglottitis').
inf('episcleritis', 'episcleritis').
inf('esophagitis', 'esophagitis').
inf('oesophagitis', 'oesophagitis').
inf('fibrositis', 'fibrositis').
inf('fibromyositis', 'fibromyositis').
inf('folliculitis', 'folliculitis').
inf('funiculitis', 'funiculitis').
inf('gastritis', 'gastritis').
inf('acute gastritis', 'acute gastritis').
inf('chronic gastritis', 'chronic gastritis').
inf('glossitis', 'glossitis').
inf('acute glossitis', 'acute glossitis').
inf('chronic glossitis', 'chronic glossitis').
inf('Moeller\'s glossitis', 'Moeller\'s glossitis').
inf('hydrarthrosis', 'hydrarthroses').
inf('ileitis', 'ileitis').
inf('iridocyclitis', 'iridocyclitis').
inf('iridokeratitis', 'iridokeratitis').
inf('iritis', 'iritis').
inf('jejunitis', 'jejunitis').
inf('jejunoileitis', 'jejunoileitis').
inf('keratitis', 'keratitis').
inf('keratoconjunctivitis', 'keratoconjunctivitis').
inf('keratoiritis', 'keratoiritis').
inf('keratoscleritis', 'keratoscleritis').
inf('labyrinthitis', 'labyrinthitis').
inf('laminitis', 'laminitis').
inf('laryngitis', 'laryngitis').
inf('laryngopharyngitis', 'laryngopharyngitis').
inf('laryngotracheobronchitis', 'laryngotracheobronchitis').
inf('leptomeningitis', 'leptomeningitis').
inf('lymphadenitis', 'lymphadenitis').
inf('lymphangitis', 'lymphangitis').
inf('mastitis', 'mastitis').
inf('mastoiditis', 'mastoiditis').
inf('metritis', 'metritis').
inf('endometritis', 'endometritis').
inf('myelitis', 'myelitis').
inf('myositis', 'myositis').
inf('myometritis', 'myometritis').
inf('trichinosis', 'trichinoses').
inf('trichiniasis', 'trichiniases').
inf('neuritis', 'neuritis').
inf('oophoritis', 'oophoritis').
inf('orchitis', 'orchitis').
inf('osteitis', 'osteitis').
inf('osteomyelitis', 'osteomyelitis').
inf('otitis', 'otitis').
inf('ovaritis', 'ovaritis').
inf('pancreatitis', 'pancreatitis').
inf('parametritis', 'parametritis').
inf('parotitis', 'parotitis').
inf('peritonitis', 'peritonitis').
inf('phalangitis', 'phalangitis').
inf('phlebitis', 'phlebitis').
inf('phlebothrombosis', 'phlebothromboses').
inf('venous thrombosis', 'venous thromboses').
inf('polyneuritis', 'polyneuritis').
inf('multiple neuritis', 'multiple neuritis').
inf('retrobulbar neuritis', 'retrobulbar neuritis').
inf('infectious polyneuritis', 'infectious polyneuritis').
inf('Landry\'s paralysis', 'Landry\'s paralyses').
inf('thrombophlebitis', 'thrombophlebitis').
inf('pneumonitis', 'pneumonitis').
inf('posthitis', 'posthitis').
inf('proctitis', 'proctitis').
inf('prostatitis', 'prostatitis').
inf('rachitis', 'rachitis').
inf('radiculitis', 'radiculitis').
inf('chorioretinitis', 'chorioretinitis').
inf('retinitis', 'retinitis').
inf('rhinitis', 'rhinitis').
inf('sinusitis', 'sinusitis').
inf('pansinusitis', 'pansinusitis').
inf('salpingitis', 'salpingitis').
inf('scleritis', 'scleritis').
inf('sialadenitis', 'sialadenitis').
inf('splenitis', 'splenitis').
inf('spondylitis', 'spondylitis').
inf('stomatitis', 'stomatitis').
inf('vesicular stomatitis', 'vesicular stomatitis').
inf('synovitis', 'synovitis').
inf('tarsitis', 'tarsitis').
inf('tendinitis', 'tendinitis').
inf('tendonitis', 'tendonitis').
inf('tenonitis', 'tenonitis').
inf('lateral epicondylitis', 'lateral epicondylitis').
inf('lateral humeral epicondylitis', 'lateral humeral epicondylitis').
inf('tenosynovitis', 'tenosynovitis').
inf('tendosynovitis', 'tendosynovitis').
inf('tendonous synovitis', 'tendonous synovitis').
inf('thyroiditis', 'thyroiditis').
inf('tonsillitis', 'tonsillitis').
inf('tracheitis', 'tracheitis').
inf('tracheobronchitis', 'tracheobronchitis').
inf('tympanitis', 'tympanitis').
inf('ulitis', 'ulitis').
inf('ureteritis', 'ureteritis').
inf('uveitis', 'uveitis').
inf('uvulitis', 'uvulitis').
inf('vaginitis', 'vaginitis').
inf('valvulitis', 'valvulitis').
inf('vasculitis', 'vasculitis').
inf('vasovesiculitis', 'vasovesiculitis').
inf('vesiculitis', 'vesiculitis').
inf('vulvitis', 'vulvitis').
inf('vulvovaginitis', 'vulvovaginitis').
inf('sickness', 'sicknesses').
inf('morning sickness', 'morning sicknesses').
inf('queasiness', 'queasinesses').
inf('squeamishness', 'squeamishnesses').
inf('twitch', 'twitches').
inf('cicatrix', 'cicatrices').
inf('callosity', 'callosities').
inf('febrility', 'febrilities').
inf('febricity', 'febricities').
inf('feverishness', 'feverishnesses').
inf('atrophy', 'atrophies').
inf('hypertrophy', 'hypertrophies').
inf('adenomegaly', 'adenomegalies').
inf('dactylomegaly', 'dactylomegalies').
inf('elephantiasis', 'elephantiases').
inf('nevoid elephantiasis', 'nevoid elephantiases').
inf('filariasis', 'filariases').
inf('splenomegaly', 'splenomegalies').
inf('acromegaly', 'acromegalies').
inf('shortness of breath', 'shortnesses of breath').
inf('breathlessness', 'breathlessnesses').
inf('epistaxis', 'epistaxes').
inf('irregularity', 'irregularities').
inf('looseness of the bowels', 'loosenesses of the bowels').
inf('looseness', 'loosenesses').
inf('dizziness', 'dizzinesses').
inf('giddiness', 'giddinesses').
inf('lightheadedness', 'lightheadednesses').
inf('vertigo', 'vertigoes').
inf('wheeziness', 'wheezinesses').
inf('anxiety', 'anxieties').
inf('anxiousness', 'anxiousnesses').
inf('castration anxiety', 'castration anxieties').
inf('hypochondriasis', 'hypochondriases').
inf('overanxiety', 'overanxieties').
inf('hallucinosis', 'hallucinoses').
inf('identity crisis', 'identity crises').
inf('nervousness', 'nervousnesses').
inf('tenseness', 'tensenesses').
inf('stress', 'stresses').
inf('delusions of grandeur', 'delusionss of grandeur').
inf('delusions of persecution', 'delusionss of persecution').
inf('zoanthropy', 'zoanthropies').
inf('mental soundness', 'mental soundnesses').
inf('sanity', 'sanities').
inf('saneness', 'sanenesses').
inf('lucidity', 'lucidities').
inf('rationality', 'rationalities').
inf('reasonableness', 'reasonablenesses').
inf('mental illness', 'mental illnesses').
inf('psychopathy', 'psychopathies').
inf('phobic neurosis', 'phobic neuroses').
inf('encopresis', 'encopreses').
inf('folie a deux', 'folie a deuxes').
inf('sociopathic personality', 'sociopathic personalities').
inf('psychopathic personality', 'psychopathic personalities').
inf('combat neurosis', 'combat neuroses').
inf('schizotypal personality', 'schizotypal personalities').
inf('frenzy', 'frenzies').
inf('fury', 'furies').
inf('manic depressive illness', 'manic depressive illnesses').
inf('manic-depressive psychosis', 'manic-depressive psychoses').
inf('neurosis', 'neuroses').
inf('psychoneurosis', 'psychoneuroses').
inf('hysterical neurosis', 'hysterical neuroses').
inf('hysterocatalepsy', 'hysterocatalepsies').
inf('anxiety neurosis', 'anxiety neuroses').
inf('depersonalization neurosis', 'depersonalization neuroses').
inf('depersonalisation neurosis', 'depersonalisation neuroses').
inf('split personality', 'split personalities').
inf('multiple personality', 'multiple personalities').
inf('insanity', 'insanities').
inf('lunacy', 'lunacies').
inf('madness', 'madnesses').
inf('insaneness', 'insanenesses').
inf('dementedness', 'dementednesses').
inf('Korsakoff\'s psychosis', 'Korsakoff\'s psychoses').
inf('Korsakov\'s psychosis', 'Korsakov\'s psychoses').
inf('polyneuritic psychosis', 'polyneuritic psychoses').
inf('senile psychosis', 'senile psychoses').
inf('rhinopathy', 'rhinopathies').
inf('Wernicke\'s encephalopathy', 'Wernicke\'s encephalopathies').
inf('irrationality', 'irrationalities').
inf('mental unsoundness', 'mental unsoundnesses').
inf('craziness', 'crazinesses').
inf('daftness', 'daftnesses').
inf('flakiness', 'flakinesses').
inf('psychosis', 'psychoses').
inf('schizophrenic psychosis', 'schizophrenic psychoses').
inf('dementia praecox', 'dementia praecoxes').
inf('voicelessness', 'voicelessnesses').
inf('defect of speech', 'defects of speech').
inf('psilosis', 'psiloses').
inf('fuss', 'fusses').
inf('tizzy', 'tizzies').
inf('melancholy', 'melancholies').
inf('slough of despond', 'sloughs of despond').
inf('restlessness', 'restlessnesses').
inf('difficulty', 'difficulties').
inf('bitch', 'bitches').
inf('quandary', 'quandaries').
inf('box', 'boxes').
inf('pinch', 'pinches').
inf('fix', 'fixes').
inf('mess', 'messes').
inf('kettle of fish', 'kettles of fish').
inf('stress', 'stresses').
inf('balance-of-payments problem', 'balances-of-payments problem').
inf('appro', 'approes').
inf('contentedness', 'contentednesses').
inf('apostasy', 'apostasies').
inf('wilderness', 'wildernesses').
inf('discreteness', 'discretenesses').
inf('distinctness', 'distinctnesses').
inf('separateness', 'separatenesses').
inf('severalty', 'severalties').
inf('loneliness', 'lonelinesses').
inf('solitariness', 'solitarinesses').
inf('insularity', 'insularities').
inf('anomy', 'anomies').
inf('privacy', 'privacies').
inf('privateness', 'privatenesses').
inf('secrecy', 'secrecies').
inf('hiddenness', 'hiddennesses').
inf('covertness', 'covertnesses').
inf('confidentiality', 'confidentialities').
inf('discontinuity', 'discontinuities').
inf('disconnectedness', 'disconnectednesses').
inf('separability', 'separabilities').
inf('incoherency', 'incoherencies').
inf('disjointedness', 'disjointednesses').
inf('connectedness', 'connectednesses').
inf('tangency', 'tangencies').
inf('interconnectedness', 'interconnectednesses').
inf('coherency', 'coherencies').
inf('cohesiveness', 'cohesivenesses').
inf('consistency', 'consistencies').
inf('continuity', 'continuities').
inf('maturity', 'maturities').
inf('matureness', 'maturenesses').
inf('ripeness', 'ripenesses').
inf('muliebrity', 'muliebrities').
inf('immaturity', 'immaturities').
inf('immatureness', 'immaturenesses').
inf('post-maturity', 'post-maturities').
inf('greenness', 'greennesses').
inf('callowness', 'callownesses').
inf('jejuneness', 'jejunenesses').
inf('juvenility', 'juvenilities').
inf('prematureness', 'prematurenesses').
inf('prematurity', 'prematurities').
inf('puerility', 'puerilities').
inf('infancy', 'infancies').
inf('second class', 'second classes').
inf('dignity', 'dignities').
inf('nobility', 'nobilities').
inf('baronetcy', 'baronetcies').
inf('barony', 'baronies').
inf('viscountcy', 'viscountcies').
inf('viscounty', 'viscounties').
inf('saliency', 'saliencies').
inf('strikingness', 'strikingnesses').
inf('conspicuousness', 'conspicuousnesses').
inf('visibility', 'visibilities').
inf('grandness', 'grandnesses').
inf('emphasis', 'emphases').
inf('stress', 'stresses').
inf('primacy', 'primacies').
inf('prestigiousness', 'prestigiousnesses').
inf('obscurity', 'obscurities').
inf('anonymity', 'anonymities').
inf('namelessness', 'namelessnesses').
inf('humbleness', 'humblenesses').
inf('obscureness', 'obscurenesses').
inf('lowliness', 'lowlinesses').
inf('limbo', 'limboes').
inf('glory', 'glories').
inf('celebrity', 'celebrities').
inf('infamy', 'infamies').
inf('notoriety', 'notorieties').
inf('corruptness', 'corruptnesses').
inf('ignominy', 'ignominies').
inf('degeneracy', 'degeneracies').
inf('decadency', 'decadencies').
inf('infamy', 'infamies').
inf('reproach', 'reproaches').
inf('ascendancy', 'ascendancies').
inf('ascendency', 'ascendencies').
inf('mastery', 'masteries').
inf('supremacy', 'supremacies').
inf('prepotency', 'prepotencies').
inf('paramountcy', 'paramountcies').
inf('sovereignty', 'sovereignties').
inf('suzerainty', 'suzerainties').
inf('tyranny', 'tyrannies').
inf('monopoly', 'monopolies').
inf('monopoly', 'monopolies').
inf('monopsony', 'monopsonies').
inf('oligopoly', 'oligopolies').
inf('nemesis', 'nemeses').
inf('comfortableness', 'comfortablenesses').
inf('coziness', 'cozinesses').
inf('cosiness', 'cosinesses').
inf('snugness', 'snugnesses').
inf('uncomfortableness', 'uncomfortablenesses').
inf('incommodiousness', 'incommodiousnesses').
inf('uneasiness', 'uneasinesses').
inf('wretchedness', 'wretchednesses').
inf('wellness', 'wellnesses').
inf('misery', 'miseries').
inf('wretchedness', 'wretchednesses').
inf('miserableness', 'miserablenesses').
inf('anguish', 'anguishes').
inf('deficiency', 'deficiencies').
inf('mineral deficiency', 'mineral deficiencies').
inf('shortness', 'shortnesses').
inf('stringency', 'stringencies').
inf('tightness', 'tightnesses').
inf('necessity', 'necessities').
inf('requisiteness', 'requisitenesses').
inf('urgency', 'urgencies').
inf('hurry', 'hurries').
inf('imperativeness', 'imperativenesses').
inf('insistency', 'insistencies').
inf('press', 'presses').
inf('criticality', 'criticalities').
inf('criticalness', 'criticalnesses').
inf('cruciality', 'crucialities').
inf('fullness', 'fullnesses').
inf('satiety', 'satieties').
inf('excess', 'excesses').
inf('solidity', 'solidities').
inf('acariasis', 'acariases').
inf('acariosis', 'acarioses').
inf('acaridiasis', 'acaridiases').
inf('ascariasis', 'ascariases').
inf('coccidiosis', 'coccidioses').
inf('echinococcosis', 'echinococcoses').
inf('hydatidosis', 'hydatidoses').
inf('helminthiasis', 'helminthiases').
inf('myiasis', 'myiases').
inf('onchocerciasis', 'onchocerciases').
inf('river blindness', 'river blindnesses').
inf('opisthorchiasis', 'opisthorchiases').
inf('pediculosis', 'pediculoses').
inf('lousiness', 'lousinesses').
inf('pediculosis capitis', 'pediculosis capitis').
inf('trombiculiasis', 'trombiculiases').
inf('trichuriasis', 'trichuriases').
inf('emptiness', 'emptinesses').
inf('blankness', 'blanknesses').
inf('hollowness', 'hollownesses').
inf('nothingness', 'nothingnesses').
inf('nullity', 'nullities').
inf('nihility', 'nihilities').
inf('vacancy', 'vacancies').
inf('vacuity', 'vacuities').
inf('nakedness', 'nakednesses').
inf('nudity', 'nudities').
inf('nudeness', 'nudenesses').
inf('undress', 'undresses').
inf('bareness', 'barenesses').
inf('baldness', 'baldnesses').
inf('phalacrosis', 'phalacroses').
inf('hairlessness', 'hairlessnesses').
inf('male-patterned baldness', 'male-patterned baldnesses').
inf('male pattern baldness', 'male pattern baldnesses').
inf('state of grace', 'states of grace').
inf('flawlessness', 'flawlessnesses').
inf('polish', 'polishes').
inf('finish', 'finishes').
inf('intactness', 'intactnesses').
inf('integrity', 'integrities').
inf('unity', 'unities').
inf('wholeness', 'wholenesses').
inf('completeness', 'completenesses').
inf('entirety', 'entireties').
inf('entireness', 'entirenesses').
inf('integrality', 'integralities').
inf('totality', 'totalities').
inf('comprehensiveness', 'comprehensivenesses').
inf('fullness', 'fullnesses').
inf('partialness', 'partialnesses').
inf('incompleteness', 'incompletenesses').
inf('rawness', 'rawnesses').
inf('sketchiness', 'sketchinesses').
inf('imperfectness', 'imperfectnesses').
inf('weakness', 'weaknesses').
inf('insufficiency', 'insufficiencies').
inf('inadequacy', 'inadequacies').
inf('glitch', 'glitches').
inf('congenital anomaly', 'congenital anomalies').
inf('congenital abnormality', 'congenital abnormalities').
inf('hydrocephaly', 'hydrocephalies').
inf('hydronephrosis', 'hydronephroses').
inf('plagiocephaly', 'plagiocephalies').
inf('polysomy', 'polysomies').
inf('scaphocephaly', 'scaphocephalies').
inf('tetralogy of Fallot', 'tetralogies of Fallot').
inf('Fallot\'s tetralogy', 'Fallot\'s tetralogies').
inf('rachischisis', 'rachischises').
inf('polydactyly', 'polydactylies').
inf('hyperdactyly', 'hyperdactylies').
inf('syndactyly', 'syndactylies').
inf('defectiveness', 'defectivenesses').
inf('faultiness', 'faultinesses').
inf('bugginess', 'bugginesses').
inf('crudeness', 'crudenesses').
inf('crudity', 'crudities').
inf('primitiveness', 'primitivenesses').
inf('rudeness', 'rudenesses').
inf('lameness', 'lamenesses').
inf('sickness', 'sicknesses').
inf('destiny', 'destinies').
inf('luckiness', 'luckinesses').
inf('prosperity', 'prosperities').
inf('successfulness', 'successfulnesses').
inf('mercy', 'mercies').
inf('weakness', 'weaknesses').
inf('success', 'successes').
inf('adversity', 'adversities').
inf('extremity', 'extremities').
inf('distress', 'distresses').
inf('cross', 'crosses').
inf('crown of thorns', 'crowns of thorns').
inf('solvency', 'solvencies').
inf('insolvency', 'insolvencies').
inf('bankruptcy', 'bankruptcies').
inf('bankruptcy', 'bankruptcies').
inf('state of matter', 'states of matter').
inf('liquidness', 'liquidnesses').
inf('liquidity', 'liquidities').
inf('solidness', 'solidnesses').
inf('possibility', 'possibilities').
inf('possibleness', 'possiblenesses').
inf('conceivableness', 'conceivablenesses').
inf('conceivability', 'conceivabilities').
inf('achievability', 'achievabilities').
inf('attainability', 'attainabilities').
inf('attainableness', 'attainablenesses').
inf('potentiality', 'potentialities').
inf('potency', 'potencies').
inf('latency', 'latencies').
inf('impossibility', 'impossibilities').
inf('impossibleness', 'impossiblenesses').
inf('inconceivability', 'inconceivabilities').
inf('inconceivableness', 'inconceivablenesses').
inf('unattainableness', 'unattainablenesses').
inf('hopefulness', 'hopefulnesses').
inf('opportunity', 'opportunities').
inf('purity', 'purities').
inf('pureness', 'purenesses').
inf('plainness', 'plainnesses').
inf('impurity', 'impurities').
inf('impureness', 'impurenesses').
inf('dirtiness', 'dirtinesses').
inf('putridity', 'putridities').
inf('credit crunch', 'credit crunches').
inf('liquidity crisis', 'liquidity crises').
inf('economic crisis', 'economic crises').
inf('prosperity', 'prosperities').
inf('softness', 'softnesses').
inf('indebtedness', 'indebtednesses').
inf('liability', 'liabilities').
inf('wealthiness', 'wealthinesses').
inf('richness', 'richnesses').
inf('lap of luxury', 'laps of luxury').
inf('luxury', 'luxuries').
inf('luxuriousness', 'luxuriousnesses').
inf('sumptuousness', 'sumptuousnesses').
inf('old money', 'old monies').
inf('sufficiency', 'sufficiencies').
inf('poverty', 'poverties').
inf('poorness', 'poornesses').
inf('neediness', 'needinesses').
inf('penury', 'penuries').
inf('beggary', 'beggaries').
inf('mendicancy', 'mendicancies').
inf('mendicity', 'mendicities').
inf('impecuniousness', 'impecuniousnesses').
inf('pennilessness', 'pennilessnesses').
inf('penuriousness', 'penuriousnesses').
inf('sanitariness', 'sanitarinesses').
inf('asepsis', 'asepses').
inf('antisepsis', 'antisepses').
inf('sterility', 'sterilities').
inf('sterileness', 'sterilenesses').
inf('unsanitariness', 'unsanitarinesses').
inf('filthiness', 'filthinesses').
inf('foulness', 'foulnesses').
inf('nastiness', 'nastinesses').
inf('cleanness', 'cleannesses').
inf('cleanliness', 'cleanlinesses').
inf('spotlessness', 'spotlessnesses').
inf('immaculateness', 'immaculatenesses').
inf('orderliness', 'orderlinesses').
inf('spit and polish', 'spit and polishes').
inf('tidiness', 'tidinesses').
inf('neatness', 'neatnesses').
inf('spruceness', 'sprucenesses').
inf('trimness', 'trimnesses').
inf('dirtiness', 'dirtinesses').
inf('uncleanness', 'uncleannesses').
inf('dinginess', 'dinginesses').
inf('dustiness', 'dustinesses').
inf('griminess', 'griminesses').
inf('grubbiness', 'grubbinesses').
inf('smuttiness', 'smuttinesses').
inf('sootiness', 'sootinesses').
inf('sordidness', 'sordidnesses').
inf('squalidness', 'squalidnesses').
inf('disorderliness', 'disorderlinesses').
inf('untidiness', 'untidinesses').
inf('sloppiness', 'sloppinesses').
inf('slovenliness', 'slovenlinesses').
inf('unkemptness', 'unkemptnesses').
inf('shagginess', 'shagginesses').
inf('mess', 'messes').
inf('messiness', 'messinesses').
inf('muss', 'musses').
inf('mussiness', 'mussinesses').
inf('normality', 'normalities').
inf('normalcy', 'normalcies').
inf('averageness', 'averagenesses').
inf('commonness', 'commonnesses').
inf('expectedness', 'expectednesses').
inf('typicality', 'typicalities').
inf('abnormality', 'abnormalities').
inf('abnormalcy', 'abnormalcies').
inf('atelectasis', 'atelectases').
inf('atypicality', 'atypicalities').
inf('untypicality', 'untypicalities').
inf('aberrancy', 'aberrancies').
inf('chromosomal anomaly', 'chromosomal anomalies').
inf('chrosomal abnormality', 'chrosomal abnormalities').
inf('monosomy', 'monosomies').
inf('trisomy', 'trisomies').
inf('kyphosis', 'kyphoses').
inf('lordosis', 'lordoses').
inf('scoliosis', 'scolioses').
inf('subnormality', 'subnormalities').
inf('anomaly', 'anomalies').
inf('anomalousness', 'anomalousnesses').
inf('pycnodysostosis', 'pycnodysostoses').
inf('lactase deficiency', 'lactase deficiencies').
inf('ateleiosis', 'ateleioses').
inf('ateliosis', 'atelioses').
inf('macrocephaly', 'macrocephalies').
inf('megacephaly', 'megacephalies').
inf('megalocephaly', 'megalocephalies').
inf('microcephaly', 'microcephalies').
inf('nanocephaly', 'nanocephalies').
inf('phimosis', 'phimoses').
inf('ecology', 'ecologies').
inf('canvass', 'canvasses').
inf('milieu', 'milieu').
inf('lap of the gods', 'laps of the gods').
inf('responsibility', 'responsibilities').
inf('smogginess', 'smogginesses').
inf('inhospitableness', 'inhospitablenesses').
inf('air mass', 'air masses').
inf('fogginess', 'fogginesses').
inf('murkiness', 'murkinesses').
inf('calmness', 'calmnesses').
inf('mildness', 'mildnesses').
inf('clemency', 'clemencies').
inf('balminess', 'balminesses').
inf('softness', 'softnesses').
inf('stillness', 'stillnesses').
inf('windlessness', 'windlessnesses').
inf('inclemency', 'inclemencies').
inf('inclementness', 'inclementnesses').
inf('storminess', 'storminesses').
inf('boisterousness', 'boisterousnesses').
inf('breeziness', 'breezinesses').
inf('windiness', 'windinesses').
inf('tempestuousness', 'tempestuousnesses').
inf('choppiness', 'choppinesses').
inf('roughness', 'roughnesses').
inf('cloudiness', 'cloudinesses').
inf('gloominess', 'gloominesses').
inf('glumness', 'glumnesses').
inf('bleakness', 'bleaknesses').
inf('bareness', 'barenesses').
inf('nakedness', 'nakednesses').
inf('unsusceptibility', 'unsusceptibilities').
inf('immunity', 'immunities').
inf('immunity', 'immunities').
inf('immunogenicity', 'immunogenicities').
inf('active immunity', 'active immunities').
inf('passive immunity', 'passive immunities').
inf('autoimmunity', 'autoimmunities').
inf('acquired immunity', 'acquired immunities').
inf('natural immunity', 'natural immunities').
inf('innate immunity', 'innate immunities').
inf('racial immunity', 'racial immunities').
inf('amnesty', 'amnesties').
inf('diplomatic immunity', 'diplomatic immunities').
inf('indemnity', 'indemnities').
inf('impunity', 'impunities').
inf('susceptibility', 'susceptibilities').
inf('susceptibleness', 'susceptiblenesses').
inf('liability', 'liabilities').
inf('taxability', 'taxabilities').
inf('ratability', 'ratabilities').
inf('rateability', 'rateabilities').
inf('capability', 'capabilities').
inf('capacity', 'capacities').
inf('activity', 'activities').
inf('sensitivity', 'sensitivities').
inf('food allergy', 'food allergies').
inf('immediate allergy', 'immediate allergies').
inf('atopy', 'atopies').
inf('atopic allergy', 'atopic allergies').
inf('serum sickness', 'serum sicknesses').
inf('delayed allergy', 'delayed allergies').
inf('allergy', 'allergies').
inf('anaphylaxis', 'anaphylaxes').
inf('hypersensitivity', 'hypersensitivities').
inf('allergic rhinitis', 'allergic rhinitis').
inf('pollinosis', 'pollinoses').
inf('diathesis', 'diatheses').
inf('reactivity', 'reactivities').
inf('suggestibility', 'suggestibilities').
inf('wetness', 'wetnesses').
inf('wateriness', 'waterinesses').
inf('muddiness', 'muddinesses').
inf('sloppiness', 'sloppinesses').
inf('humidity', 'humidities').
inf('humidness', 'humidnesses').
inf('mugginess', 'mugginesses').
inf('dampness', 'dampnesses').
inf('moistness', 'moistnesses').
inf('dankness', 'danknesses').
inf('clamminess', 'clamminesses').
inf('rawness', 'rawnesses').
inf('sogginess', 'sogginesses').
inf('dryness', 'drynesses').
inf('waterlessness', 'waterlessnesses').
inf('aridity', 'aridities').
inf('aridness', 'aridnesses').
inf('thirstiness', 'thirstinesses').
inf('sereness', 'serenesses').
inf('safety', 'safeties').
inf('biosafety', 'biosafeties').
inf('risklessness', 'risklessnesses').
inf('invulnerability', 'invulnerabilities').
inf('impregnability', 'impregnabilities').
inf('security', 'securities').
inf('public security', 'public securities').
inf('secureness', 'securenesses').
inf('indemnity', 'indemnities').
inf('collective security', 'collective securities').
inf('cause of death', 'causes of death').
inf('hazardousness', 'hazardousnesses').
inf('perilousness', 'perilousnesses').
inf('insecurity', 'insecurities').
inf('jeopardy', 'jeopardies').
inf('sword of Damocles', 'swords of Damocles').
inf('riskiness', 'riskinesses').
inf('speculativeness', 'speculativenesses').
inf('vulnerability', 'vulnerabilities').
inf('insecureness', 'insecurenesses').
inf('tensity', 'tensities').
inf('tenseness', 'tensenesses').
inf('tautness', 'tautnesses').
inf('tonicity', 'tonicities').
inf('atonicity', 'atonicities').
inf('atony', 'atonies').
inf('laxness', 'laxnesses').
inf('laxity', 'laxities').
inf('fitness', 'fitnesses').
inf('physical fitness', 'physical fitnesses').
inf('soundness', 'soundnesses').
inf('seaworthiness', 'seaworthinesses').
inf('fitness', 'fitnesses').
inf('airworthiness', 'airworthinesses').
inf('unfitness', 'unfitnesses').
inf('softness', 'softnesses').
inf('infirmity', 'infirmities').
inf('frailty', 'frailties').
inf('debility', 'debilities').
inf('feebleness', 'feeblenesses').
inf('frailness', 'frailnesses').
inf('astheny', 'asthenies').
inf('cachexy', 'cachexies').
inf('disability', 'disabilities').
inf('disability of walking', 'disabilities of walking').
inf('lameness', 'lamenesses').
inf('gimpiness', 'gimpinesses').
inf('gameness', 'gamenesses').
inf('deafness', 'deafnesses').
inf('hearing loss', 'hearing losses').
inf('conductive hearing loss', 'conductive hearing losses').
inf('conduction deafness', 'conduction deafnesses').
inf('middle-ear deafness', 'middle-ear deafnesses').
inf('hyperacusis', 'hyperacuses').
inf('sensorineural hearing loss', 'sensorineural hearing losses').
inf('nerve deafness', 'nerve deafnesses').
inf('tone deafness', 'tone deafnesses').
inf('deaf-muteness', 'deaf-mutenesses').
inf('muteness', 'mutenesses').
inf('nearsightedness', 'nearsightednesses').
inf('shortsightedness', 'shortsightednesses').
inf('hypermetropy', 'hypermetropies').
inf('farsightedness', 'farsightednesses').
inf('longsightedness', 'longsightednesses').
inf('day blindness', 'day blindnesses').
inf('night blindness', 'night blindnesses').
inf('moon blindness', 'moon blindnesses').
inf('photoretinitis', 'photoretinitis').
inf('farsightedness', 'farsightednesses').
inf('snowblindness', 'snowblindnesses').
inf('snow-blindness', 'snow-blindnesses').
inf('detachment of the retina', 'detachments of the retina').
inf('eyelessness', 'eyelessnesses').
inf('figural blindness', 'figural blindnesses').
inf('paralysis', 'paralyses').
inf('palsy', 'palsies').
inf('paresis', 'pareses').
inf('paraparesis', 'parapareses').
inf('metroptosis', 'metroptoses').
inf('nephroptosis', 'nephroptoses').
inf('ptosis', 'ptoses').
inf('brow ptosis', 'brow ptoses').
inf('unilateral paralysis', 'unilateral paralyses').
inf('unsoundness', 'unsoundnesses').
inf('putridness', 'putridnesses').
inf('rottenness', 'rottennesses').
inf('rancidity', 'rancidities').
inf('foulness', 'foulnesses').
inf('impropriety', 'improprieties').
inf('iniquity', 'iniquities').
inf('wickedness', 'wickednesses').
inf('darkness', 'darknesses').
inf('malady', 'maladies').
inf('merchantability', 'merchantabilities').
inf('urinary hesitancy', 'urinary hesitancies').
inf('sarcoidosis', 'sarcoidoses').
inf('dermatosclerosis', 'dermatoscleroses').
inf('pyelonephritis', 'pyelonephritis').
inf('acute pyelonephritis', 'acute pyelonephritis').
inf('chronic pyelonephritis', 'chronic pyelonephritis').
inf('nongonococcal urethritis', 'nongonococcal urethritis').
inf('rhinosporidiosis', 'rhinosporidioses').
inf('urethritis', 'urethritis').
inf('nonspecific urethritis', 'nonspecific urethritis').
inf('stasis', 'stases').
inf('climax', 'climaxes').
inf('homozygosity', 'homozygosities').
inf('heterozygosity', 'heterozygosities').
inf('neotony', 'neotonies').
inf('plurality', 'pluralities').
inf('polyvalency', 'polyvalencies').
inf('polyvalency', 'polyvalencies').
inf('multivalency', 'multivalencies').
inf('amphidiploidy', 'amphidiploidies').
inf('diploidy', 'diploidies').
inf('haploidy', 'haploidies').
inf('heteroploidy', 'heteroploidies').
inf('polyploidy', 'polyploidies').
inf('kraurosis', 'krauroses').
inf('ureterostenosis', 'ureterostenoses').
inf('uropathy', 'uropathies').
inf('varicosis', 'varicoses').
inf('varicosity', 'varicosities').
inf('varix', 'varixes').
inf('hypertonicity', 'hypertonicities').
inf('hypotonicity', 'hypotonicities').
inf('hypertonicity', 'hypertonicities').
inf('hypotonicity', 'hypotonicities').
inf('leakiness', 'leakinesses').
inf('paternity', 'paternities').
inf('rustiness', 'rustinesses').
inf('chemistry', 'chemistries').
inf('impurity', 'impurities').
inf('dross', 'drosses').
inf('Botox', 'Botoxes').
inf('slurry', 'slurries').
inf('Perspex', 'Perspexes').
inf('plexiglass', 'plexiglasses').
inf('amino', 'aminoes').
inf('antimony', 'antimonies').
inf('Co', 'Coes').
inf('Ho', 'Hoes').
inf('I', 'we').
inf('mercury', 'mercuries').
inf('Mo', 'Moes').
inf('No', 'Noes').
inf('Po', 'Poes').
inf('ader wax', 'ader waxes').
inf('earth wax', 'earth waxes').
inf('mineral wax', 'mineral waxes').
inf('Mexican onyx', 'Mexican onyxes').
inf('mineral pitch', 'mineral pitches').
inf('borax', 'boraxes').
inf('emery', 'emeries').
inf('gesso', 'gessoes').
inf('isinglass', 'isinglasses').
inf('biomass', 'biomasses').
inf('ribose', 'ribose').
inf('putty', 'putties').
inf('iron putty', 'iron putties').
inf('red-lead putty', 'red-lead putties').
inf('spirits of wine', 'spiritss of wine').
inf('Turkish tobacco', 'Turkish tobaccoes').
inf('Alnico', 'Alnicoes').
inf('brass', 'brasses').
inf('alpha-beta brass', 'alpha-beta brasses').
inf('alpha brass', 'alpha brasses').
inf('Cox', 'Coxes').
inf('indigo', 'indigoes').
inf('ivory', 'ivories').
inf('mother-of-pearl', 'mothers-of-pearl').
inf('calf', 'calves').
inf('cassava starch', 'cassava starches').
inf('chamois', 'chamois').
inf('chammy', 'chammies').
inf('shammy', 'shammies').
inf('crush', 'crushes').
inf('morocco', 'moroccoes').
inf('Levant morocco', 'Levant moroccoes').
inf('fox', 'foxes').
inf('ash', 'ashes').
inf('fly ash', 'fly ashes').
inf('bearing brass', 'bearing brasses').
inf('royal jelly', 'royal jellies').
inf('benzoate of soda', 'benzoates of soda').
inf('bicarbonate of soda', 'bicarbonates of soda').
inf('Maalox', 'Maaloxes').
inf('bleach', 'bleaches').
inf('chloride of lime', 'chlorides of lime').
inf('bone ash', 'bone ashes').
inf('bouncing putty', 'bouncing putties').
inf('box calf', 'box calves').
inf('calx', 'calxes').
inf('candelilla wax', 'candelilla waxes').
inf('carbonado', 'carbonadoes').
inf('cellulose', 'cellulose').
inf('carboxymethyl cellulose', 'carboxymethyl cellulose').
inf('diethylaminoethyl cellulose', 'diethylaminoethyl cellulose').
inf('DEAE cellulose', 'DEAE cellulose').
inf('cartridge brass', 'cartridge brasses').
inf('nitrocellulose', 'nitrocellulose').
inf('pyrocellulose', 'pyrocellulose').
inf('animal starch', 'animal starches').
inf('plumbago', 'plumbagoes').
inf('fulminate of mercury', 'fulminates of mercury').
inf('fulminating mercury', 'fulminating mercuries').
inf('stainless', 'stainlesses').
inf('chalcedony', 'chalcedonies').
inf('calcedony', 'calcedonies').
inf('clunch', 'clunches').
inf('Clorox', 'Cloroxes').
inf('complex', 'complexes').
inf('deoxyribose', 'deoxyribose').
inf('depilatory', 'depilatories').
inf('halitosis', 'halitoses').
inf('mouthwash', 'mouthwashes').
inf('piss', 'pisses').
inf('barf', 'barves').
inf('rubbish', 'rubbishes').
inf('trash', 'trashes').
inf('flux', 'fluxes').
inf('soldering flux', 'soldering fluxes').
inf('pearl ash', 'pearl ashes').
inf('Klorvess', 'Klorvesses').
inf('fiberglass', 'fiberglasses').
inf('fibreglass', 'fibreglasses').
inf('flax', 'flaxes').
inf('floor wax', 'floor waxes').
inf('fructose', 'fructose').
inf('galactose', 'galactose').
inf('natural glass', 'natural glasses').
inf('quartz glass', 'quartz glasses').
inf('opal glass', 'opal glasses').
inf('milk glass', 'milk glasses').
inf('optical glass', 'optical glasses').
inf('crown glass', 'crown glasses').
inf('optical crown glass', 'optical crown glasses').
inf('flint glass', 'flint glasses').
inf('crown glass', 'crown glasses').
inf('volcanic glass', 'volcanic glasses').
inf('glass', 'glasses').
inf('soft glass', 'soft glasses').
inf('ground glass', 'ground glasses').
inf('ground glass', 'ground glasses').
inf('lead glass', 'lead glasses').
inf('safety glass', 'safety glasses').
inf('laminated glass', 'laminated glasses').
inf('shatterproof glass', 'shatterproof glasses').
inf('soluble glass', 'soluble glasses').
inf('water glass', 'water glasses').
inf('stained glass', 'stained glasses').
inf('Tiffany glass', 'Tiffany glasses').
inf('wire glass', 'wire glasses').
inf('glucose', 'glucose').
inf('dextroglucose', 'dextroglucose').
inf('blood glucose', 'blood glucose').
inf('glycerin jelly', 'glycerin jellies').
inf('cluster of differentiation 4', 'clusters of differentiation 4').
inf('cluster of differentiation 8', 'clusters of differentiation 8').
inf('gneiss', 'gneisses').
inf('gondang wax', 'gondang waxes').
inf('fig wax', 'fig waxes').
inf('groundmass', 'groundmasses').
inf('guano', 'guanoes').
inf('attar of roses', 'attars of roses').
inf('oil of cloves', 'oils of cloves').
inf('oil of turpentine', 'oils of turpentine').
inf('spirit of turpentine', 'spirits of turpentine').
inf('colophony', 'colophonies').
inf('balm of Gilead', 'balms of Gilead').
inf('asa dulcis', 'asa dulces').
inf('butea kino', 'butea kinoes').
inf('Bengal kino', 'Bengal kinoes').
inf('kino', 'kinoes').
inf('gum kino', 'gum kinoes').
inf('epoxy', 'epoxies').
inf('gumbo', 'gumboes').
inf('high brass', 'high brasses').
inf('pitch', 'pitches').
inf('hypo', 'hypoes').
inf('Japan wax', 'Japan waxes').
inf('jelly', 'jellies').
inf('ketone body', 'ketone bodies').
inf('acetone body', 'acetone bodies').
inf('ketose', 'ketose').
inf('Kleenex', 'Kleenexes').
inf('lactose', 'lactose').
inf('gabbro', 'gabbroes').
inf('lino', 'linoes').
inf('liquid bleach', 'liquid bleaches').
inf('loess', 'loesses').
inf('low brass', 'low brasses').
inf('maltose', 'maltose').
inf('high-strength brass', 'high-strength brasses').
inf('mash', 'mashes').
inf('sour mash', 'sour mashes').
inf('matrix', 'matrices').
inf('bichloride of mercury', 'bichlorides of mercury').
inf('monosaccharose', 'monosaccharose').
inf('montan wax', 'montan waxes').
inf('naval brass', 'naval brasses').
inf('Admiralty brass', 'Admiralty brasses').
inf('flimsy', 'flimsies').
inf('onyx', 'onyxes').
inf('opopanax', 'opopanaxes').
inf('pad of paper', 'pads of paper').
inf('petroleum jelly', 'petroleum jellies').
inf('mineral jelly', 'mineral jellies').
inf('sugar of lead', 'sugars of lead').
inf('pisang wax', 'pisang waxes').
inf('plaster of Paris', 'plasters of Paris').
inf('yellow prussiate of potash', 'yellow prussiate of potashes').
inf('permanganate of potash', 'permanganates of potash').
inf('polish', 'polishes').
inf('porphyry', 'porphyries').
inf('potash', 'potashes').
inf('caustic potash', 'caustic potashes').
inf('Pyrex', 'Pyrexes').
inf('red brass', 'red brasses').
inf('latex', 'latexes').
inf('ruby', 'rubies').
inf('cream of tartar', 'creams of tartar').
inf('starch', 'starches').
inf('sardonyx', 'sardonyxes').
inf('scale wax', 'scale waxes').
inf('factor I', 'factor we').
inf('antibody', 'antibodies').
inf('autoantibody', 'autoantibodies').
inf('Rh antibody', 'Rh antibodies').
inf('heterophil antibody', 'heterophil antibodies').
inf('heterophile antibody', 'heterophile antibodies').
inf('Forssman antibody', 'Forssman antibodies').
inf('isoantibody', 'isoantibodies').
inf('alloantibody', 'alloantibodies').
inf('monoclonal antibody', 'monoclonal antibodies').
inf('mephitis', 'mephitis').
inf('shoe polish', 'shoe polishes').
inf('silex', 'silexes').
inf('silvex', 'silvexes').
inf('ski wax', 'ski waxes').
inf('dross', 'drosses').
inf('slush', 'slushes').
inf('soda ash', 'soda ashes').
inf('sodium carboxymethyl cellulose', 'sodium carboxymethyl cellulose').
inf('spinel ruby', 'spinel rubies').
inf('balas ruby', 'balas rubies').
inf('spirits of ammonia', 'spiritss of ammonia').
inf('starch', 'starches').
inf('cornstarch', 'cornstarches').
inf('sago', 'sagoes').
inf('pearl sago', 'pearl sagoes').
inf('Otaheite arrowroot starch', 'Otaheite arrowroot starches').
inf('soman', 'somen').
inf('sternutatory', 'sternutatories').
inf('stucco', 'stuccoes').
inf('saccharose', 'saccharose').
inf('jaggery', 'jaggeries').
inf('jagghery', 'jaggheries').
inf('jaggary', 'jaggaries').
inf('thatch', 'thatches').
inf('B complex', 'B complexes').
inf('vitamin B complex', 'vitamin B complexes').
inf('oil of vitriol', 'oils of vitriol').
inf('water of crystallization', 'water of crystallization').
inf('water of crystallisation', 'water of crystallisation').
inf('water of hydration', 'water of hydration').
inf('wax', 'waxes').
inf('beeswax', 'beeswaxes').
inf('Ghedda wax', 'Ghedda waxes').
inf('earwax', 'earwaxes').
inf('paraffin wax', 'paraffin waxes').
inf('vegetable wax', 'vegetable waxes').
inf('shellac wax', 'shellac waxes').
inf('lac wax', 'lac waxes').
inf('lemongrass', 'lemongrasses').
inf('lemon grass', 'lemon grasses').
inf('shoddy', 'shoddies').
inf('flowers of zinc', 'flowerss of zinc').
inf('vesicatory', 'vesicatories').
inf('vernix', 'vernixes').
inf('period of time', 'periods of time').
inf('water under the bridge', 'water under the bridge').
inf('history', 'histories').
inf('futurity', 'futurities').
inf('time to come', 'times to come').
inf('history', 'histories').
inf('Age of Mammals', 'Ages of Mammals').
inf('Age of Man', 'Ages of Man').
inf('Holocene epoch', 'Holocene epoches').
inf('Recent epoch', 'Recent epoches').
inf('Pleistocene epoch', 'Pleistocene epoches').
inf('Glacial epoch', 'Glacial epoches').
inf('Pliocene epoch', 'Pliocene epoches').
inf('Miocene epoch', 'Miocene epoches').
inf('Oligocene epoch', 'Oligocene epoches').
inf('Eocene epoch', 'Eocene epoches').
inf('Paleocene epoch', 'Paleocene epoches').
inf('Age of Reptiles', 'Ages of Reptiles').
inf('Age of Fishes', 'Ages of Fishes').
inf('stretch', 'stretches').
inf('day of rest', 'days of rest').
inf('leave of absence', 'leaves of absence').
inf('pass', 'passes').
inf('liberty', 'liberties').
inf('life', 'lives').
inf('life', 'lives').
inf('life', 'lives').
inf('millenary', 'millenaries').
inf('bimillenary', 'bimillenaries').
inf('shelf life', 'shelf lives').
inf('life expectancy', 'life expectancies').
inf('incipiency', 'incipiencies').
inf('afterlife', 'afterlives').
inf('immortality', 'immortalities').
inf('time of life', 'times of life').
inf('infancy', 'infancies').
inf('prepuberty', 'prepuberties').
inf('puberty', 'puberties').
inf('bloom of youth', 'blooms of youth').
inf('age of consent', 'ages of consent').
inf('majority', 'majorities').
inf('minority', 'minorities').
inf('prime of life', 'primes of life').
inf('maturity', 'maturities').
inf('maturity', 'maturities').
inf('senility', 'senilities').
inf('change of life', 'changes of life').
inf('unit of time', 'units of time').
inf('Cinco de Mayo', 'Cinco de Mayoes').
inf('day of the month', 'days of the month').
inf('sell-by date', 'sells-by date').
inf('Feast of Booths', 'Feasts of Booths').
inf('Feast of Tabernacles', 'Feasts of Tabernacles').
inf('day of the week', 'days of the week').
inf('break of day', 'breaks of day').
inf('break of the day', 'breaks of the day').
inf('week from Monday', 'weeks from Monday').
inf('Day of Judgment', 'Days of Judgment').
inf('Day of Judgement', 'Days of Judgement').
inf('day of reckoning', 'days of reckoning').
inf('crack of doom', 'cracks of doom').
inf('end of the world', 'ends of the world').
inf('point in time', 'points in time').
inf('time of arrival', 'times of arrival').
inf('time of departure', 'times of departure').
inf('Rosh Hodesh', 'Rosh Hodeshes').
inf('Rosh Chodesh', 'Rosh Chodeshes').
inf('Day of Atonement', 'Days of Atonement').
inf('First of May', 'Firsts of May').
inf('Fourth of July', 'Fourths of July').
inf('Pasch', 'Pasches').
inf('Pasch', 'Pasches').
inf('Solemnity of Mary', 'Solemnitys of Mary').
inf('Ascension of the Lord', 'Ascensions of the Lord').
inf('Feast of the Circumcision', 'Feasts of the Circumcision').
inf('Assumption of Mary', 'Assumptions of Mary').
inf('Feast of Dormition', 'Feasts of Dormition').
inf('Epiphany of Our Lord', 'Epiphanys of Our Lord').
inf('Hallowmass', 'Hallowmasses').
inf('Pesach', 'Pesaches').
inf('Feast of the Unleavened Bread', 'Feasts of the Unleavened Bread').
inf('Feast of Weeks', 'Feasts of Weeks').
inf('Rejoicing over the Law', 'Rejoicings over the Law').
inf('Rejoicing of the Law', 'Rejoicings of the Law').
inf('Rejoicing in the Law', 'Rejoicings in the Law').
inf('Ninth of Av', 'Ninths of Av').
inf('Ninth of Ab', 'Ninths of Ab').
inf('Fast of Av', 'Fasts of Av').
inf('Fast of Ab', 'Fasts of Ab').
inf('Fast of Gedaliah', 'Fasts of Gedaliah').
inf('Fast of Tevet', 'Fasts of Tevet').
inf('Fast of Esther', 'Fasts of Esther').
inf('Fast of the Firstborn', 'Fasts of the Firstborn').
inf('Fast of Tammuz', 'Fasts of Tammuz').
inf('Festival of Lights', 'Festivals of Lights').
inf('Feast of Lights', 'Feasts of Lights').
inf('Feast of Dedication', 'Feasts of Dedication').
inf('Feast of the Dedication', 'Feasts of the Dedication').
inf('decennary', 'decennaries').
inf('century', 'centuries').
inf('quattrocento', 'quattrocentoes').
inf('twentieth century', 'twentieth centuries').
inf('half-century', 'half-centuries').
inf('quarter-century', 'quarter-centuries').
inf('phase of the moon', 'phases of the moon').
inf('full-of-the-moon', 'fulls-of-the-moon').
inf('March', 'Marches').
inf('mid-March', 'mid-Marches').
inf('Rabi I', 'Rabi we').
inf('Jumada I', 'Jumada we').
inf('Jomada I', 'Jomada we').
inf('Feast of Sacrifice', 'Feasts of Sacrifice').
inf('equinox', 'equinoxes').
inf('vernal equinox', 'vernal equinoxes').
inf('March equinox', 'March equinoxes').
inf('spring equinox', 'spring equinoxes').
inf('autumnal equinox', 'autumnal equinoxes').
inf('September equinox', 'September equinoxes').
inf('fall equinox', 'fall equinoxes').
inf('life', 'lives').
inf('time of day', 'times of day').
inf('time of year', 'times of year').
inf('month of Sundays', 'months of Sundays').
inf('eternity', 'eternities').
inf('infinity', 'infinities').
inf('moment of truth', 'moments of truth').
inf('moment of truth', 'moments of truth').
inf('patch', 'patches').
inf('mo', 'moes').
inf('blink of an eye', 'blinks of an eye').
inf('flash', 'flashes').
inf('jiffy', 'jiffies').
inf('epoch', 'epoches').
inf('epoch', 'epoches').
inf('year of grace', 'years of grace').
inf('anniversary', 'anniversaries').
inf('day of remembrance', 'days of remembrance').
inf('wedding anniversary', 'wedding anniversaries').
inf('silver wedding anniversary', 'silver wedding anniversaries').
inf('golden wedding anniversary', 'golden wedding anniversaries').
inf('diamond wedding anniversary', 'diamond wedding anniversaries').
inf('semicentenary', 'semicentenaries').
inf('centenary', 'centenaries').
inf('bicentenary', 'bicentenaries').
inf('tercentenary', 'tercentenaries').
inf('quatercentenary', 'quatercentenaries').
inf('quincentenary', 'quincentenaries').
inf('millenary', 'millenaries').
inf('bimillenary', 'bimillenaries').
inf('time out of mind', 'times out of mind').
inf('by-and-by', 'by-and-bies').
inf('antiquity', 'antiquities').
inf('prehistory', 'prehistories').
inf('glacial epoch', 'glacial epoches').
inf('top of the inning', 'tops of the inning').
inf('bottom of the inning', 'bottoms of the inning').
inf('period of play', 'periods of play').
inf('half', 'halves').
inf('first half', 'first halves').
inf('second half', 'second halves').
inf('last half', 'last halves').
inf('Reign of Terror', 'Reigns of Terror').
inf('reign of terror', 'reigns of terror').
inf('turn of the century', 'turns of the century').
inf('tempo', 'tempoes').
inf('accelerando', 'accelerandoes').
inf('allegretto', 'allegrettoes').
inf('allegro', 'allegroes').
inf('allegro con spirito', 'allegro con spiritoes').
inf('meno mosso', 'meno mossoes').
inf('rubato', 'rubatoes').
inf('get-go', 'get-goes').
inf('presidency', 'presidencies').
inf('vice-presidency', 'vice-presidencies').
inf('finish', 'finishes').
inf('expiry', 'expiries').
inf('track-to-track seek time', 'tracks-to-tracks seek time').
inf('latency', 'latencies').
inf('eternity', 'eternities').
inf('term of a contract', 'terms of a contract').
inf('half life', 'half lives').
inf('half-life', 'half-lives').
inf('rate of attrition', 'rates of attrition').
inf('fertility', 'fertilities').
inf('natality', 'natalities').
inf('mortality', 'mortalities').
inf('rate of flow', 'rates of flow').
inf('flux', 'fluxes').
inf('frequency', 'frequencies').
inf('oftenness', 'oftennesses').
inf('rate of growth', 'rates of growth').
inf('isometry', 'isometries').
inf('rate of inflation', 'rates of inflation').
inf('rate of return', 'rates of return').
inf('return on invested capital', 'returns on invested capital').
inf('return on investment', 'returns on investment').
inf('rate of respiration', 'rates of respiration').
inf('velocity', 'velocities').
inf('tempo', 'tempoes').
inf('escape velocity', 'escape velocities').
inf('hypervelocity', 'hypervelocities').
inf('muzzle velocity', 'muzzle velocities').
inf('peculiar velocity', 'peculiar velocities').
inf('radial velocity', 'radial velocities').
inf('speed of light', 'speeds of light').
inf('terminal velocity', 'terminal velocities').
inf('sampling frequency', 'sampling frequencies').
inf('Nyquist frequency', 'Nyquist frequencies').
inf('infant mortality', 'infant mortalities').
inf('neonatal mortality', 'neonatal mortalities').
inf('channel capacity', 'channel capacities').
inf('neutron flux', 'neutron fluxes').
inf('radiant flux', 'radiant fluxes').
inf('luminous flux', 'luminous fluxes').
inf('term of office', 'terms of office').
inf('incumbency', 'incumbencies').
inf('go', 'goes').
inf('watch', 'watches').
inf('watch', 'watches').
inf('dogwatch', 'dogwatches').
inf('graveyard watch', 'graveyard watches').
inf('middle watch', 'middle watches').
inf('midwatch', 'midwatches').
inf('night watch', 'night watches').
inf('hitch', 'hitches').
inf('term of enlistment', 'terms of enlistment').
inf('tour of duty', 'tours of duty').
inf('flush', 'flushes').
inf('epoch', 'epoches').
inf('date of reference', 'dates of reference').
inf('rotational latency', 'rotational latencies').
inf('latency', 'latencies').
inf('regency', 'regencies').
