# WordNet3.1

Backing this up as there's no guarantee WordNet will remain available.

Also added an additional couple of files:

- noun_inflection_module.pl
- noun_inflection.pl
- noun_inflection_strings.pl

The noun_inflection module reduces the number of data items
by implementing the "add s onto the end rule" and only storing
nouns for which this is not the case. It should be easier to maintain
fewer exceptions. With a bit more cleverness it should also be possible
to add a rule for "ends with es".

Where `_strings` denotes that file's data is in strings.

There are generated via piping with:

```
swipl -f wn_s.pl -g "findall(W, s(_, _, W, n, _, _), Words), maplist(writeln, Words)." -t "halt." | python plurals.py > noun_inflection_strings.pl
```

The order of `"` and `'` in the python program needs adjusting depending
on whether strings or atoms are desired. The swipl command prints all
nouns, these are read by the python program which uses [textblob](https://textblob.readthedocs.io/en/dev/quickstart.html#words-inflection-and-lemmatization) to pluralize the word, along with a modicum of cleverness to avoid the "dogss" glitch. The Python program prints out `inflection/2` predicates, that are then piped into a file with a Prolog file extension.

```
%! inflection(Singluar, Plural).
```
